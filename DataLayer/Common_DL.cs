﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
namespace DataLayer
{
   public class Common_DL
    {

       public List<ELCNoFileNo> GetActiveLC()
       {

           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var v = from p in obj.LC_Active_List()
                   orderby p.LCNo
                   select p;

           ELCNoFileNo objc;
           List<ELCNoFileNo> liob = new List<ELCNoFileNo>();

           foreach (var ob in v)
           {
               objc = new ELCNoFileNo();

               objc.LCNo = ob.LCNo;
               objc.FileNo = ob.FileNo;
               objc.LCID = ob.ID;
               objc.TotalLC = Convert.ToDecimal(ob.LCValueUDS);
               objc.TotalCI = Convert.ToDecimal(ob.TotalCI);
               liob.Add(objc);
           }
           return liob;
       }
        public ELCNoFileNo GetbyLC(string lcno)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.LC_Active_List()
                    where p.LCNo == lcno 
                    orderby p.LCNo
                    select p;

            ELCNoFileNo objc;
            objc = new ELCNoFileNo();

            foreach (var ob in v)
            {

                objc.NoOfDays = ob.NoOfDaysLC ?? 0;
                objc.LCNo = ob.LCNo;
                objc.FileNo = ob.FileNo;
                objc.LCID = ob.ID;
                
            }
            return objc;
        }
        public ELCNoFileNo Getbyfileno(string fileno)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.LC_Active_List()
                    where p.FileNo == fileno
                    orderby p.LCNo
                    select p;

            ELCNoFileNo objc;

            objc = new ELCNoFileNo();
            foreach (var ob in v)
            {
                objc = new ELCNoFileNo();
                objc.NoOfDays = ob.NoOfDaysLC ?? 0;
                objc.LCNo = ob.LCNo;
                objc.FileNo = ob.FileNo;
                objc.LCID = ob.ID;
                objc.Customer = ob.CName;
                objc.Notes = ob.ClosingNotes;
                objc.LcDate = ob.LCDate.Date.ToString("dd-MMM-yyyy");
                objc.TotalLC =Convert.ToDecimal( ob.LCValueUDS);
                objc.TotalCI = Convert.ToDecimal(ob.TotalCI);
               
            }
            return objc;
        }
        public void SetupCompany(CompanyInfo i,byte[] b)
        {

            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.Setup_Company_Info_INS(i.CompanyName,i.OfficeAdd,i.FactoryAdd,i.officeContact,i.FactoryContact,i.LienBank,i.COM_INVOICE_START,b);//(objDes.ID, objDes.DesignationTitle, objDes.DesignationShort, Convert.ToDecimal(objDes.StartingBasic), Convert.ToDecimal(objDes.Increment), Convert.ToDecimal(objDes.MaxBasic), objDes.Rank, ref res);

               


            }
            catch
            {
                
            }
        }


        public CompanyInfo GetCompanyInfo()
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            return ( from p in obj.CompanyInfos
                   
                    select p).ToList()[0];
        }
        public List<EColor> GetAllColor()
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_COLOR()
                    orderby p.ColorName
                    select p;

            EColor objc;
            List<EColor> liob = new List<EColor>();
            objc = new EColor();
            objc.ID = 0;
            objc.ColorName = "";
            liob.Add(objc);
            foreach (var ob in v)
            {
                objc = new EColor();
                objc.ID = Convert.ToInt32(ob.SL);
                objc.ColorName = ob.ColorName;
                objc.ColorCode = ob.ColorCode;

                liob.Add(objc);
            }
            return liob;
        }
        public List<EClasue> GetAllClasue()
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_CLAUSE()
                    orderby p.ClauseName
                    select p;

            EClasue objc;
            List<EClasue> liob = new List<EClasue>();
           
            foreach (var ob in v)
            {
                objc = new EClasue();
                objc.ID = Convert.ToInt32(ob.ID);
                objc.ClauseName = ob.ClauseName;
              

                liob.Add(objc);
            }
            return liob;
        }
        public bool InsertUpdateColor(EColor objDes)
        {
           
            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.Insert_Color_SP(objDes.ID, objDes.ColorName, objDes.ColorCode);//(objDes.ID, objDes.DesignationTitle, objDes.DesignationShort, Convert.ToDecimal(objDes.StartingBasic), Convert.ToDecimal(objDes.Increment), Convert.ToDecimal(objDes.MaxBasic), objDes.Rank, ref res);
               
                    return true;
                

            }
            catch
            {
                return false;
            }


        }
        public bool InsertUpdateClasue(EClasue objDes)
        {

            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.Insert_Clauses_sp(objDes.ID, objDes.ClauseName);//(objDes.ID, objDes.DesignationTitle, objDes.DesignationShort, Convert.ToDecimal(objDes.StartingBasic), Convert.ToDecimal(objDes.Increment), Convert.ToDecimal(objDes.MaxBasic), objDes.Rank, ref res);

                return true;


            }
            catch
            {
                return false;
            }


        }
        public List<EDescription> GetAllDescription()
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_DESCRIPTION()
                    orderby p.Descriptions
                    select p;

            EDescription objc;
            List<EDescription> liob = new List<EDescription>();
            objc = new EDescription();
            objc.ID = 0;
            objc.DescriptionName = "--Select Description--";
            liob.Add(objc);
            foreach (var ob in v)
            {
                objc = new EDescription();
                objc.ID = Convert.ToInt32(ob.SL);
                objc.DescriptionName = ob.Descriptions;
                objc.TypeID = ob.Others;

                liob.Add(objc);
            }
            return liob;
        }
        public List<EDescriptionType> GetAllDescriptionType()
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_DESCRIPTION_TYPE()
                    orderby p.DesType
                    select p;

            EDescriptionType objc;
            List<EDescriptionType> liob = new List<EDescriptionType>();
            objc = new EDescriptionType();
            objc.ID = 0;
            objc.TypeName = "--Select Type--";
            liob.Add(objc);
            foreach (var ob in v)
            {
                objc = new EDescriptionType();
                objc.ID = Convert.ToInt32(ob.DID);
                objc.TypeName = ob.DesType;
               

                liob.Add(objc);
            }
            return liob;
        }

        public List<EDescriptionNotes> GetAllDescriptionNotes()
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_DESCRIPTION_Notes()
                    orderby p.NotesName
                    select p;

            EDescriptionNotes objc;
            List<EDescriptionNotes> liob = new List<EDescriptionNotes>();
            objc = new EDescriptionNotes();
            objc.ID = 0;
            objc.DescriptionName = "NONE";
            liob.Add(objc);
            foreach (var ob in v)
            {
                objc = new EDescriptionNotes();
                objc.ID = Convert.ToInt32(ob.ID);
                objc.DescriptionName = ob.NotesName;


                liob.Add(objc);
            }
            return liob;
        }
        public bool InsertUpdateDescription(EDescription objDes)
        {
            

            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.Insert_Description_sp(objDes.ID, objDes.DescriptionName, objDes.TypeID);
                
                    return true;
               

            }
            catch
            {
                return false;
            }


        }
        public bool InsertUpdateDescriptionNotes(EDescriptionNotes objDes)
        {


            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.Insert_DescriptionNotes_sp(objDes.ID, objDes.DescriptionName);

                return true;


            }
            catch
            {
                return false;
            }


        }
        public bool ChangePassword(string UserID,string Password)
        {


            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.ChangePassword(UserID,Password);

                return true;


            }
            catch
            {
                return false;
            }


        }
        public bool IsAuthenticated(string User, string password, ref string userName)
        {
            DBProcessDataContext ob = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            ob.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            try
            {
                var data = from v in ob.NSEC_USERs
                           where v.UserCode==User
                           select v;
                string pasword = "";
                foreach (var v in data)
                {
                    pasword = v.Password.Trim();
                    userName = v.EmployeeName;
                }
                if (pasword == password)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
        }
        public List<NSEC_USER> GetAllUser()
        {
            DBProcessDataContext ob = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            ob.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            try
            {
                var data = from v in ob.NSEC_USERs
                           
                           select v;
                List<NSEC_USER> li = new List<NSEC_USER>();
                NSEC_USER obj;
                foreach (var v in data)
                {
                    obj = new NSEC_USER();
                    obj.UserCode = v.UserCode;
                    obj.EmployeeName = v.EmployeeName;
                    li.Add(obj);
                }
                obj = new NSEC_USER();
                obj.UserCode = "--Select User--";
               
                li.Add(obj);
                return li;
               

            }
            catch
            {
                return null;
            }
        }
        public List<NSEC_PERMISSION> GetAllUserPermission(string UserID)
        {
            DBProcessDataContext ob = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            ob.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            try
            {
                var data = from v in ob.NSEC_PERMISSIONs
                           where v.UserID==UserID
                           select v;
                List<NSEC_PERMISSION> li = new List<NSEC_PERMISSION>();
                NSEC_PERMISSION obj;
                foreach (var v in data)
                {
                    obj = new NSEC_PERMISSION();
                    
                    obj.ModuleID = v.ModuleID;
                    li.Add(obj);
                }

                return li;


            }
            catch
            {
                return null;
            }
        }
        public bool SaveUserPermission(List<NSEC_PERMISSION> li,string UserID)
        {


            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                foreach (var v in li)
                {
                    obj.Save_USER_PERMISSION(UserID, v.ModuleID);
                }
               

                return true;


            }
            catch
            {
                return false;
            }


        }
        public bool DeleteUserPermission(string UserID)
        {


            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                
                    obj.DELETE_USER_PERMISSION(UserID);
              


                return true;


            }
            catch
            {
                return false;
            }


        }
        //public List<ESize> GetAllSize()
        //{

        //    DBProcessDataContext obj = new DBProcessDataContext();
        //    DBConnect objdbc = new DBConnect();
        //    obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
        //    var v = from p in obj.INIT_Sizes_GET_SP()
        //            orderby p.SizeCode
        //            select p;

        //    ESize objc;
        //    List<ESize> liob = new List<ESize>();
        //    objc = new ESize();
        //    objc.ID = 0;
        //    objc.SizeCode = "--Select Size--";
        //    liob.Add(objc);
        //    foreach (var ob in v)
        //    {
        //        objc = new ESize();
        //        objc.ID = ob.ID;
        //        objc.SizeCode = ob.SizeCode;
        //        objc.SizeName = ob.SizeDesc;

        //        liob.Add(objc);
        //    }
        //    return liob;
        //}
        //public bool InsertUpdateSize(ESize objDes)
        //{
        //    string res = "";

        //    try
        //    {
        //        DBProcessDataContext obj = new DBProcessDataContext();
        //        DBConnect objdbc = new DBConnect();
        //        obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

        //        obj.INIT_SIZE_INS_UPD_SP(objDes.ID, objDes.SizeCode, objDes.SizeName, objDes.IsActive, objDes.UserID, ref res);//(objDes.ID, objDes.DesignationTitle, objDes.DesignationShort, Convert.ToDecimal(objDes.StartingBasic), Convert.ToDecimal(objDes.Increment), Convert.ToDecimal(objDes.MaxBasic), objDes.Rank, ref res);
        //        if (res == "1")
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }

        //    }
        //    catch
        //    {
        //        return false;
        //    }


        //}
        //public List<EStyle> GetAllStyle()
        //{

        //    DBProcessDataContext obj = new DBProcessDataContext();
        //    DBConnect objdbc = new DBConnect();
        //    obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
        //    var v = from p in obj.INIT_Style_GET_SP()
        //            orderby p.StyleCode
        //            select p;

        //    EStyle objc;
        //    List<EStyle> liob = new List<EStyle>();
        //    objc = new EStyle();
        //    objc.ID = 0;
        //    objc.StyleCode = "--Select Style--";
        //    liob.Add(objc);
        //    foreach (var ob in v)
        //    {
        //        objc = new EStyle();
        //        objc.ID = ob.ID;
        //        objc.StyleCode = ob.StyleCode;
        //        objc.StyleName = ob.StyleDesc;

        //        liob.Add(objc);
        //    }
        //    return liob;
        //}
        //public bool InsertUpdateStyle(EStyle objDes)
        //{
        //    string res = "";

        //    try
        //    {
        //        DBProcessDataContext obj = new DBProcessDataContext();
        //        DBConnect objdbc = new DBConnect();
        //        obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

        //        obj.INIT_Style_INS_UPD_SP(objDes.ID, objDes.StyleCode, objDes.StyleName, objDes.IsActive, objDes.UserID, ref res);//(objDes.ID, objDes.DesignationTitle, objDes.DesignationShort, Convert.ToDecimal(objDes.StartingBasic), Convert.ToDecimal(objDes.Increment), Convert.ToDecimal(objDes.MaxBasic), objDes.Rank, ref res);
        //        if (res == "1")
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }

        //    }
        //    catch
        //    {
        //        return false;
        //    }


        //}

        #region Customer
        public List<ECustomer> GetAllCustomer()
        {
            List<ECustomer> liob = new List<ECustomer>();
            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
                var v = from p in obj.GET_ALL_CUSTOMER()
                        where p.CName.Trim().Length>3
                        orderby p.CName
                        select p;

                ECustomer objc;
               
                objc = new ECustomer();
                objc.ID = 0;
                objc.CustomerName = "--Select Customer--";
                liob.Add(objc);
                foreach (var ob in v)
                {
                    objc = new ECustomer();
                    objc.ID = ob.CustID;
                    objc.CustomerCode = ob.CustShort;
                    objc.CustomerName = ob.CName;
                    objc.CustomerAddress = ob.Address;

                    objc.CustomerAdd = ob.CName + " / " + ob.Address;
                    
                    objc.Telephone = ob.Telephone;
                    objc.Email = ob.Email;
                    objc.FAX = ob.Fax;
                    objc.ContactPerson = ob.Contact;
                    objc.BuyingHouse = ob.BuyingHouse;
                    objc.MainBuyer = ob.MainBuyer;

                    objc.VAT = ob.VATRegNo;
                    objc.TIN = ob.TINNo;
                    objc.BOND = ob.BondLicenseNo;
                    objc.ERC = ob.ERCNo;
                    objc.IRC = ob.IRCNo;
                    objc.MainBuyer = ob.MainBuyer;


                    liob.Add(objc);
                }
            }

            catch
            {

            }
            return liob;
        }

       
        public ECustomer GetACustomer(Int32 CID)
        {
            ECustomer objc;
            objc = new ECustomer();
            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
                var v = from p in obj.LC_Informations
                        from q in obj.Customers
                        where p.ID==CID && p.CustomerID==q.CustID
                        orderby q.CName
                        select q;

              

                foreach (var ob in v)
                {
                   
                    objc.ID = ob.CustID;
                    objc.CustomerCode = ob.CustShort;
                    objc.CustomerName = ob.CName;
                    objc.CustomerAddress = ob.Address;
                    objc.Telephone = ob.Telephone;
                    objc.Email = ob.Email;
                    objc.FAX = ob.Fax;
                    objc.ContactPerson = ob.Contact;
                    objc.BuyingHouse = ob.BuyingHouse;
                    objc.MainBuyer = ob.MainBuyer;
                    objc.BIN = ob.BIN;
                    objc.VAT = ob.VATRegNo;
                    objc.TIN = ob.TINNo;
                    objc.BOND = ob.BondLicenseNo;
                    objc.ERC = ob.ERCNo;
                    objc.IRC = ob.IRCNo;
                    objc.MainBuyer = ob.MainBuyer;


                   
                }

                return objc;
            }

            catch
            {
                return null;
            }
           
        }
        public bool InsertUpdateCustomer(ECustomer objDes,ref string sid )
        {
           

            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
              //  sid = "0";
                obj.INSERT_CUSTOMER_SP(Convert.ToInt32(objDes.ID), objDes.CustomerCode, objDes.CustomerName,objDes.CustomerAddress,objDes.Telephone,objDes.Email,objDes.FAX,objDes.ContactPerson,objDes.MainBuyer,objDes.BuyingHouse, objDes.UserID,ref sid);//(objDes.ID, objDes.DesignationTitle, objDes.DesignationShort, Convert.ToDecimal(objDes.StartingBasic), Convert.ToDecimal(objDes.Increment), Convert.ToDecimal(objDes.MaxBasic), objDes.Rank, ref res);
               
                    return true;
               

            }
            catch
            {
                return false;
            }


        }
        #endregion Customer

        #region CustomerBank

        public bool InsertUpdateCustomerBank(CustomerBank oc)
        {


            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
                //  sid = "0";
                obj.CustomerBank_INS_UPD (oc.ID,oc.CustomerID,oc.BankName,oc.BranchName,oc.BranchAddress,oc.TINNO,oc.RegNo,oc.BankBIN);

                return true;


            }
            catch
            {
                return false;
            }


        }

        public List<CustomerBank> GetCustomerBank(Int32 CusID)
        {
           
            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
                return ( from p in obj.CustomerBanks
                         where p.CustomerID==@CusID
                      
                        orderby p.BankName
                        select p).ToList();

            }

            catch
            {
                return null;
            }
            
        }
        public List<CustomerBank> GetCustomerBankShort(Int32 CusID)
        {

            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                var k = from p in obj.CustomerBanks
                        where p.CustomerID == @CusID
                        select p;

                List<CustomerBank> li = new List<CustomerBank>();
                CustomerBank ob;
                foreach (var am in k)
                {
                    ob = new CustomerBank();
                    ob.BankName = am.BankName + " - " + am.BranchName;
                    ob.ID = am.ID;
                    li.Add(ob);
                }
                return li;

            }

            catch
            {
                return null;
            }

        }
        #endregion
        public bool InsertUpdateEmployee(NSEC_USER objDes)
        {
            string res = "";

            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.INSERT_Employee_SP(Convert.ToInt32(objDes.ID), objDes.EmployeeName,objDes.Designation,objDes.Address,objDes.Phone,objDes.Email,objDes.UserCode,objDes.Password,objDes.UserCode);

                return true;


            }
            catch
            {
                return false;
            }


        }

        public List<NSEC_USER> GetAllEmployee()
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ALL_Employee()
                    orderby p.EmployeeName
                    select p;

            NSEC_USER objc;
            List<NSEC_USER> liob = new List<NSEC_USER>();
            
            foreach (var ob in v)
            {
                objc = new NSEC_USER();
                objc.ID = ob.ID;
                objc.EmployeeName = ob.EmployeeName;
                objc.Designation = ob.Designation;
                objc.Address = ob.Address;
                objc.Phone = ob.Phone;
                objc.Email = ob.Email;
                objc.UserCode = ob.UserCode;
                objc.Password = ob.Password;



                liob.Add(objc);
            }
            return liob;
        }
        #region Measurement
        public List<EUnit> GetAllUnit()
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_UNIT()
                    orderby p.UNIT
                    select p;

            EUnit objc;
            List<EUnit> liob = new List<EUnit>();
           
            foreach (var ob in v)
            {
                objc = new EUnit();
               
                objc.UnitName = ob.UNIT;
               
                liob.Add(objc);
            }
            return liob;
        }
       
        #endregion Measurement

        public bool DeleteObject(EObject objDes)
        {

            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.Update_Table_Mode(objDes.ID,objDes.Mode,objDes.UserID,objDes.FormID);

                return true;


            }
            catch
            {
                return false;
            }


        }
    }
}
