﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.IO;

using System.Data;
using Entities;

namespace DataLayer
{
   public class DBConnect
    {
     
       public DBConnect()
       {
         
       }
       public Dictionary<string, string> ReadConnectionStringParts()
       {
           Dictionary<string, string> dbParts = new Dictionary<string, string>();
           string _strConn;

           try
           {


               dbParts.Add("Server", EGlobal.DBServerName);
               dbParts.Add("Database", EGlobal.DBDatabase);
               dbParts.Add("UserId", EGlobal.DBUserID);
               dbParts.Add("Password", EGlobal.DBPassword);

               //_strConn = "Data Source=" + IMSEncrypt.DecryptText(_server) + ";Initial Catalog=" + IMSEncrypt.DecryptText(_db) + ";Persist Security Info=True;User ID=" + IMSEncrypt.DecryptText(_uid) + ";Password=" + IMSEncrypt.DecryptText(_password);
           }
           catch (Exception ex)
           {

               // IMSLog.Debug("DBConfiguration : " + "ReadConnectionString() :: " + ex.Message);
               _strConn = string.Empty;
           }
           return dbParts;
       }

       private void READ_DB_PATH()
       {
           //Application.StartupPath 
           if (EGlobal.DBServerName == null || EGlobal.DBServerName.Length < 5)
           {
               string DBPath = EGlobal.ApplicationPath + "\\RESOURCE_INFORMATION.txt";
               StreamReader STRREDDB = new StreamReader(DBPath);
              
               EGlobal.DBServerName = STRREDDB.ReadLine();
               
               EGlobal.DBDatabase = STRREDDB.ReadLine();
          
               EGlobal.DBUserID = STRREDDB.ReadLine();
               
               EGlobal.DBPassword = STRREDDB.ReadLine();

               EGlobal.FactoryDBServerName = STRREDDB.ReadLine();

               EGlobal.FactoryDBDatabase = STRREDDB.ReadLine();

               EGlobal.FactoryDBUserID = STRREDDB.ReadLine();

               EGlobal.FactoryDBPassword = STRREDDB.ReadLine();

               //EGlobal.TPDBServerName = STRREDDB.ReadLine();
               //EGlobal.TPDBDatabase = STRREDDB.ReadLine();
             
               //EGlobal.TPDBUserID = STRREDDB.ReadLine();
              
               //EGlobal.TPDBPassword = STRREDDB.ReadLine();


               STRREDDB.Close();
               
           }
           
          
       }
       
       public string GetConnectionForLINQ()
       {
           READ_DB_PATH();

           
           return "Data Source=" + EGlobal.DBServerName + ";Initial Catalog=" + EGlobal.DBDatabase + ";User ID=" + EGlobal.DBUserID + ";Password=" + EGlobal.DBPassword + "";// providerName=System.Data.SqlClient";
          
       }
       public string GetConnectionOfFactoryForLINQ()
       {
           READ_DB_PATH();


           return "Data Source=" + EGlobal.FactoryDBServerName + ";Initial Catalog=" + EGlobal.FactoryDBDatabase + ";User ID=" + EGlobal.FactoryDBUserID + ";Password=" + EGlobal.FactoryDBPassword + "";// providerName=System.Data.SqlClient";

       }

       public string GetConnection()
       {
           READ_DB_PATH();
          // return "Provider=SQLOLEDB.1;" + "Server=" + Global._DBSERNAME + ";DataBase=" + Global._DBNAME + ";UID=" + Global._DBUID + ";Password=" + Global._DBPWD + ";";


           return "Data Source=" + EGlobal.DBServerName + ";Initial Catalog=" + EGlobal.DBDatabase + ";User ID=" + EGlobal.DBUserID + ";Password=" + EGlobal.DBPassword+ ""; 
           
           
       }





       ////public void OpenConn()
       ////{
       ////    try
       ////    {
       ////        if (_OleDbConnection.State == ConnectionState.Closed)
       ////        {
       ////            _OleDbConnection.Open();
       ////        }

       ////    }
       ////    catch
       ////    {

       ////    }
       ////}
       ////public void CloseConn()
       ////{
       ////    try
       ////    {
       ////        if (_OleDbConnection.State == ConnectionState.Open)
       ////        {
       ////            _OleDbConnection.Close();
       ////        }

       ////    }
       ////    catch
       ////    {

       ////    }
       ////}

   
    
    }
}
