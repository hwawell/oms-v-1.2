﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Services;
namespace DataLayer
{
   public class FactoryOperation_DL
    {
       public bool SaveApprovedOrder(EOrder ob, List<EOrderProduct> liop, List<EOrderProductDetails> liopd)
       {
           try
           {
              
               bool Res = InsertApprovedOrder(ob);
               if (Res == true)
               {

               }
               return true;


           }
           catch
           {
               return false;
           }
       }
       public bool InsertApprovedOrder(EOrder ob)
       {

           try
           {
               string id = "-1";
               DBFactoryDataContext obj = new DBFactoryDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionOfFactoryForLINQ();

              
               //obj.NEW_INSERT_ORDER_SP(ob.PINo,Convert.ToInt64(ob.CustID),Convert.ToDateTime( ob.PDate),Convert.ToDateTime( ob.RcvDate),Convert.ToDateTime( ob.ShpDate), ob.LCNo,ob.FRNO, ob.MainBuyer, ob.BuyerRef, ob.OrderTypeName, ob.ProductionType, ob.Notes,"E" ,ob.UserID);
               obj.NEW_INSERT_ORDER_Q_SP(ob.PINo, Convert.ToInt64(ob.CustID),ob.MEmail,ob.CustomerName ,Convert.ToDateTime(ob.PDate), Convert.ToDateTime(ob.RcvDate), Convert.ToDateTime(ob.ShpDate), ob.LCNo, ob.MainBuyer, ob.BuyerRef, ob.OrderTypeName, ob.Notes,ob.RevisedNotes, ob.UserID);

              
               return true;


           }
           catch
           {
               return false;
           }


       }
       public bool SaveApprovedOrder(EOrderApproval ob)
       {

           try
           {
               string id = "-1";
               DBFactoryDataContext obj = new DBFactoryDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionOfFactoryForLINQ();

               //obj.NEW_INSERT_ORDER_SP(ob.PINo, ob.CustID,,,,, ob.PDate, ob.RcvDate, ob.ShpDate, ob.LCNo, ob.MainBuyer, ob.BuyerRef, ob.FRNO, ob.OrderTypeID, ob.ProductionType, ob.Notes, ob.UserID, ref id);
               obj.NEW_ORDER_APPROVAL(ob.PINO,ob.ApproveFor,ob.ApprovalDate,ob.IsApproved,ob.UserID);


               return true;


           }
           catch
           {
               return false;
           }


       }
       public bool OrderProductDetailsList(List<EOrderProduct> lipi, List<EOrderProductDetails> liopd)
       {
           try
           {
               DBFactoryDataContext obj = new DBFactoryDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionOfFactoryForLINQ();
              
               foreach (EOrderProduct objOp in lipi)
               {
                   OrderProductDetails(objOp, liopd.FindAll(o => o.OPID == objOp.OPID));
                  
                       
                   
               }
               return true;

           }
           catch
           {
               return false;
           }
       }
       public bool OrderProductDetails(EOrderProduct objOp, List<EOrderProductDetails> liopd)
       {
           try
           {
               DBFactoryDataContext obj = new DBFactoryDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionOfFactoryForLINQ();

               
                   string res = "";
                   string strXML = DataSerialization.SerializeObject<List<EOrderProductDetails>>(liopd);
                   // strXML = strXML.Substring(39, (strXML.Length - 39));

                  // obj.NEW_INSERT_OrderProduct_SP(objOp.OPID, objOp.PINO, objOp.Description, objOp.Notes, objOp.Weight, objOp.Width, objOp.Unit, Convert.ToDecimal(objOp.PerUnitKG), Convert.ToDecimal(objOp.UPDown), objOp.ISExtraQty, objOp.UserID, strXML, ref res);
                  // obj.NEW_INSERT_OrderProduct_Q_SP(objOp.OPID, objOp.PINO, objOp.Description, objOp.Notes, objOp.Weight, objOp.Width,objOp.WidthOption ,objOp.Unit, Convert.ToDecimal(objOp.PerUnitKG), Convert.ToDecimal(objOp.UPDown), objOp.ISExtraQty, objOp.UserID, strXML, ref res);
                   obj.NEW_INSERT_OrderProduct_Q_LATEST_SP(objOp.OPID, objOp.PINO, objOp.Description, objOp.Notes, objOp.Weight,objOp.WeightOption, objOp.Width, objOp.WidthOption, objOp.Unit,objOp.FRNO, Convert.ToDecimal(objOp.PerUnitKG), Convert.ToDecimal(objOp.UPDown), objOp.ISExtraQty, objOp.UserID, strXML, ref res);

                   obj.SubmitChanges();
                   if (res == "-1")
                   {
                       return false;
                   }
                   else
                   {
                       return true;
                   }
             
              

           }
           catch
           {
               return false;
           }
       }
       public bool OrderProductDetailsOthersUpdate( List<EOrderProductDetails> liopd)
       {
           try
           {
               DBFactoryDataContext obj = new DBFactoryDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionOfFactoryForLINQ();


               string res = "";
               string strXML = DataSerialization.SerializeObject<List<EOrderProductDetails>>(liopd);
               // strXML = strXML.Substring(39, (strXML.Length - 39));

               // obj.NEW_INSERT_OrderProduct_SP(objOp.OPID, objOp.PINO, objOp.Description, objOp.Notes, objOp.Weight, objOp.Width, objOp.Unit, Convert.ToDecimal(objOp.PerUnitKG), Convert.ToDecimal(objOp.UPDown), objOp.ISExtraQty, objOp.UserID, strXML, ref res);
               obj.NEW_INSERT_OrderProductLDF_FRNO_UPDATE_SP( strXML, ref res);

               obj.SubmitChanges();
               if (res == "-1")
               {
                   return false;
               }
               else
               {
                   return true;
               }



           }
           catch
           {
               return false;
           }
       }
       public bool InsertUpdateCustomer(ECustomer objDes, ref string sid)
       {


           try
           {
               DBFactoryDataContext obj = new DBFactoryDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionOfFactoryForLINQ();
               obj.CommandTimeout = 20000;
               string StrID = "";
               obj.INSERT_CUSTOMER_SP_NEW_WITHNO(Convert.ToInt32(objDes.ID), objDes.CustomerCode, objDes.CustomerName, objDes.CustomerAddress, objDes.Telephone, objDes.Email, objDes.FAX, objDes.ContactPerson, objDes.MainBuyer, objDes.BuyingHouse, objDes.UserID, ref StrID);//(objDes.ID, objDes.DesignationTitle, objDes.DesignationShort, Convert.ToDecimal(objDes.StartingBasic), Convert.ToDecimal(objDes.Increment), Convert.ToDecimal(objDes.MaxBasic), objDes.Rank, ref res);
               sid = StrID;
               return true;


           }
           catch
           {
               return false;
           }


       }

       public bool UpdateDeliveryAddress(string PINO,string Add,string ConPer,string Contact)
       {


           try
           {
               DBFactoryDataContext obj = new DBFactoryDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionOfFactoryForLINQ();
               obj.CommandTimeout = 20000;
              
               obj.Order_Delivery_Info_Update(PINO, Add, ConPer, Contact);
             
               return true;


           }
           catch
           {
               return false;
           }


       }

      
    }
}
