﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Services;
namespace DataLayer
{
   public  class LC_DL
   {

       #region LC_INFORMATION

       public ELCInfo LC_Info_GET(string lc)
       {

           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var k = from p in obj.LC_INFO_GET( lc)

                   select p;

           ELCInfo o  = new ELCInfo();
           foreach (var ob in k)
           {

              
               o.LCID = ob.ID;
               o.Customer = ob.CName;
               o.CustomerID = ob.CustomerID??0;
               o.BankBranchID = ob.BankBranchID??0;
               o.LCTypeID = ob.LCTypeID;
               o.LCNo = ob.LCNo;
               o.LCDate =Convert.ToDateTime( ob.LCDate);
               o.LCExpirydate = ob.LCExpiryDate;
               o.FileNo = ob.FileNo;
               o.NoOfDaysLC =Convert.ToInt16( ob.NoOfDaysLC);
               o.ShipmentDate = ob.ShipmentDate;
               o.MasterLC = ob.MasterLC;
               o.TINNO = ob.TINNo;
               o.HSCode = ob.HSCode;
               o.BankBranch = ob.BankBranch;
               o.LCValue = ob.LCValueUDS??0;
               o.IsBankVATShow = ob.IsBankVATShow ?? false;
               o.IsBankTINShow = ob.IsBankTINShow??false;
               o.Notes = ob.ClosingNotes;
               o.ShippingFrom = ob.ShippingFrom;
               o.ShippingTo = ob.ShippingTo;
               o.ForwardingNo = ob.ForwardingNO;
               o.ForwardingDate = Convert.ToDateTime(ob.ForwardingDate);
               o.AddlNotes = ob.Addl_info;
               o.AdviceNo = ob.LCAdviceNo;
           }
           return o;
       }
       public bool SaveLCInformation(ELCInfo ob)
       {
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


               string res = "";
             

               obj.LC_Information_INS_UPD(ob.LCID,ob.LCNo,ob.LCDate,ob.LCExpirydate,ob.LCTypeID,ob.FileNo,ob.ShipmentDate,ob.LCValue,ob.BankBranchID,ob.IsBankTINShow,ob.IsBankVATShow, ob.CustomerID,ob.MasterLC,ob.AdviceNo,ob.HSCode,ob.NoOfDaysLC,ob.Notes,ob.ShippingFrom,ob.ShippingTo,ob.ForwardingNo,ob.ForwardingDate,ob.AddlNotes,  ob.EntryID, ref res);

               obj.SubmitChanges();
               if (res.Substring(0, 1) == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }



           }
           catch
           {
               return false;
           }
       }

       public List<LC_Invoice_Summary_GETResult> LC_Invoice_Summary(Int32 LCID)
       {
           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           return (from p in obj.LC_Invoice_Summary_GET(LCID)

                   select p).ToList();
       }

       public List<LC_Shipment_Info> LC_Shipping()
       {
           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           return (from p in obj.LC_Shipment_Infos

                   select p).ToList();
       }


       public List<LC_UP_UD> LC_UP_UD_GET(string lc)
       {

           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var k = from p in obj.LC_UP_UD_GET(lc)

                   select p;

           List<LC_UP_UD> li = new List<LC_UP_UD>();
           LC_UP_UD o ;
           foreach (var ob in k)
           {

               o = new LC_UP_UD();
               o.ID = ob.ID;
               o.TypeID = ob.TypeID;
               o.TypeNo = ob.TypeNo;
               o.TypeAmount = ob.TypeAmount ?? 0;

               o.SubDate = ob.SubDate;
               o.RcvDate = ob.RcvDate;


               li.Add(o);

           }
           return li;
       }
       public List<EOrderList> LC_Order_Get(string LC)
       {
           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var k = from p in obj.LC_GET_ORDER_PRODUCT(LC)

                   select p;

           List<EOrderList> li = new List<EOrderList>();
           EOrderList o;
           foreach (var ob in k)
           {

               o = new EOrderList();
               o.PINO = ob.PINO;
               o.PIValue =Convert.ToDouble( ob.TotalPrice);
             

               li.Add(o);

           }
           return li;
       }
       public bool SaveLCOrder(String LCNo, List<EOrderList> li)
       {
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


               string res = "";
               string strXML = DataSerialization.SerializeObject<List<EOrderList>>(li);

               obj.LC_ORDER_INS_UPD(LCNo,UserSession.CurrentUser,strXML, ref res);

               obj.SubmitChanges();
               if (res.Substring(0, 1) == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }



           }
           catch
           {
               return false;
           }
       }
       public string SaveUP_UD(LC_UP_UD ob)
       {
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


               string res = "";


               obj.LC_UP_UD_INS_UPD(ob.ID,ob.LC_ID,ob.TypeID,ob.TypeNo,ob.TypeAmount,ob.SubDate,ob.RcvDate,ob.EntryID,ref res);

               obj.SubmitChanges();
               return res;



           }
           catch
           {
               return "0-operation failed";
           }
       }

       #endregion

       #region Commercial_Information

       public List<LC_COM_INV> LC_CI_GET(string LC, string file, Int32 CI_ID)
       {

           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var k = from p in obj.LC_CI_List(LC,file,CI_ID)

                   select p;

           List<LC_COM_INV> li=new List<LC_COM_INV>();
           LC_COM_INV o; 
           foreach (var ob in k)
           {

               o = new LC_COM_INV();
               o.ID = ob.ID;
               o.LC_ID = ob.LC_ID;
               o.CI_Master_INV_No = ob.CI_Master_INV_No;
               o.CI_NetAmountUSD = ob.LCValueUDS ?? 0;
               o.CI_SubAmountUSD = ob.AllInvoiveValue ;
               o.CI_Date = ob.CI_Date;
               o.CI_Product_List = ob.CI_Product_List;
               o.CI_Mushok_Date = Convert.ToDateTime(ob.CI_Mushok_Date);
               o.CI_Mushok_No = ob.CI_Mushok_No;
               o.CI_TruckNo = ob.CI_TruckNo;
               o.CI_DriverName = ob.CI_DriverName;
               o.CI_BTMA_ISS_Date = ob.CI_BTMA_ISS_Date;
               o.CI_DOC_SUBMIT_DATE = ob.CI_DOC_SUBMIT_DATE;
               o.CI_DOC_RCV_DATE = ob.CI_DOC_RCV_DATE;
 
               //o.BA_LDBC_NO = ob.BA_LDBC_NO;
               //o.BA_LDBC_DATE = ob.BA_LDBC_DATE;
               //o.BA_Date = ob.BA_Date;
               //o.BA_DocAcceptAmountUSD = ob.BA_DocAcceptAmountUSD;
               //o.BA_ConvertionRateBDT = ob.BA_ConvertionRateBDT;
               //o.BA_DOC_MaturityDate = ob.BA_DOC_MaturityDate;


               //o.AR_RealizedDate = ob.AR_RealizedDate;
               //o.AR_RealizedAmountUSD = ob.AR_RealizedAmountUSD;
               //o.AR_ConvertionRateBDT = ob.AR_ConvertionRateBDT;
               //o.AR_ShortFallUSD = ob.AR_ShortFallUSD;

               li.Add(o);
           }
           return li;
       }


       public ELC_COM_INV LC_CI_GetOthers(Int32 CI_ID)
       {

           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var k = from p in obj.LC_CI_Others_Info_GET( CI_ID)

                   select p;


           ELC_COM_INV o = new ELC_COM_INV();
           foreach (var ob in k)
           {

              
               o.ID = ob.ID;
               o.InvoiceSL = ob.InvoiceSL??0;
               o.CI_INV_NO_SUB = ob.CI_INV_NO_SUB;
               o.INV_Date = ob.INV_Date;
               o.Notes = ob.Notes;
               o.IsMain = ob.IsMain??false;
               o.IsSubmited = ob.IsSubmited ?? false;
               o.BA_LDBC_NO = ob.BA_LDBC_NO;
               o.BA_LDBC_DATE = ob.BA_LDBC_DATE;
               o.BA_Date = ob.BA_Date;
               o.BA_DocAcceptAmountUSD = ob.BA_DocAcceptAmountUSD;
               o.BA_ConvertionRateBDT = ob.BA_ConvertionRateBDT;
               o.BA_DOC_MaturityDate = ob.BA_DOC_MaturityDate;

               o.PurchaseRate = ob.DocPurchaseRate;
               o.PurchasePercent = ob.DocPurchasePercent;
               o.AR_RealizedDate = ob.AR_RealizedDate;
               o.AR_RealizedAmountUSD = ob.AR_RealizedAmountUSD;
               o.AR_ConvertionRateBDT = ob.AR_ConvertionRateBDT;
             

              
           }
           return o;
       }
      
       public string SaveCIInformation(LC_COM_INV ob,List<EOrderProductLC> li,Int32 CI_M_ID)
       {
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


               string res = "";
                string strXML = DataSerialization.SerializeObject<List<EOrderProductLC>>(li);


                obj.LC_CI_INS_UPD(ob.ID, ob.LC_ID, ob.CI_Master_INV_No, ob.CI_Date, ob.CI_Product_List, ob.CI_NetAmountUSD, ob.CI_Mushok_No, ob.CI_Mushok_Date, ob.CI_TruckNo, ob.CI_DriverName, ob.CI_BTMA_ISS_Date, ob.CI_DOC_SUBMIT_DATE, ob.CI_DOC_RCV_DATE, strXML, ob.CI_EntryID, CI_M_ID, ref res);

               obj.SubmitChanges();

              
               return res;
               //if (sp[0] == "1")
               //{

               //    return sp[1];
               //}
               //else
               //{
               //    return "Operation Failed";
               //}



           }
           catch(Exception ex)
           {
               return ex.Message.ToString();
           }
       }
       public bool SaveCI_AR(ELC_COM_INV ob)
       {
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


               string res = "";


               obj.LC_AR_INS_UPD(ob.ID,ob.AR_RealizedDate,ob.AR_RealizedAmountUSD,ob.AR_ConvertionRateBDT,0,ob.AR_EntryID,ref res);

               obj.SubmitChanges();
               return true;
               
               
           }
           catch
           {
               return false;
           }
       }
       public string SaveCI_BA(ELC_COM_INV ob)
       {
           string res = "";
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


              


               obj.LC_BA_INS_UPD(ob.ID,ob.BA_LDBC_NO,ob.BA_LDBC_DATE,ob.BA_Date,ob.BA_DocAcceptAmountUSD,ob.BA_ConvertionRateBDT,ob.BA_DOC_MaturityDate, ob.PurchasePercent,ob.PurchaseRate, ob.BA_EntryID,ref res);

               obj.SubmitChanges();

               return res;
               

           }
           catch (Exception ex)
           {
               return "0"+ ex.Message.ToString();
           }
       }

       public bool DeleteInvoice(Int32  ID,string Type)
       {
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


              


               obj.LC_DELETE(ID,Type);

               obj.SubmitChanges();

               return true;


           }
           catch
           {
               return false;
           }
       }
       public string SaveCI_SUB_INS(LC_COM_INV_Product_Master ob,List<ELCProduct> li)
       {
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                string res = "";
               string strXML = DataSerialization.SerializeObject<List<ELCProduct>>(li);
              


               obj.LC_CI_Sub_INS(ob.InvoiceInfoID, ob.ID, ob.CI_INV_NO_SUB,ob.INV_Date,strXML,ob.EntryID, ref res);

               obj.SubmitChanges();

               return res;


           }
           catch(Exception ex)
           {
               return "O-Operation Failed" + ex.Message.ToString();
           }
       }
       public bool SaveCI_Product(Int32 ID,Int32 InvoiceInfoID,string INVNo,DateTime invDate, double Amout,string notes,Int32 SLID, List<EOrderProductLC> li)
       {
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


               string res = "";
               string strXML = DataSerialization.SerializeObject<List<EOrderProductLC>>(li);

               obj.LC_CI_Product_INS_UPD(ID,InvoiceInfoID,INVNo,invDate,Convert.ToDecimal( Amout),notes,UserSession.CurrentUser,strXML,SLID,  ref res);

               obj.SubmitChanges();
               if (res.Substring(0, 1) == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }



           }
           catch
           {
               return false;
           }
       }
       public double CI_ORDER_PRODUCT_BAL_GET(Int32 OPID)
       {
           try
           {
               DBProcessDataContext obj = new DBProcessDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
               return Convert.ToDouble((from p in obj.LC_GET_ORDER_PRODUCT_DETAILS_BAL(OPID)

                                        select p).SingleOrDefault().PriceSubmitted);

           }
           catch
           {
               return 0;
           }

       }
       public List<ECI_OrderProduct> CI_ORDER_PRODUCT_GET(string PINO)
       {

           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var k = from p in obj.LC_GET_ORDER_PRODUCT_DETAILS(PINO)

                   select p;

           List<ECI_OrderProduct> li = new List<ECI_OrderProduct>();
           ECI_OrderProduct o;
           foreach (var ob in k)
           {

               o = new ECI_OrderProduct();
               o.OPID =Convert.ToInt32( ob.OPID);
               o.Product = ob.Product;
               o.Balance =Convert.ToDouble( ob.TotalPrice);
               li.Add(o);
           }
           return li;
       }

       public List<LC_COM_INV_Product_Master> CI_PRODUCT_MASTER_GET(Int32 invID)
       {

           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var k = from p in obj.LC_CI_GET_ORDER_MASTER(invID)

                   select p;

           List<LC_COM_INV_Product_Master> li = new List<LC_COM_INV_Product_Master>();
           LC_COM_INV_Product_Master o;
           foreach (var ob in k)
           {

               o = new LC_COM_INV_Product_Master();
               o.ID = Convert.ToInt32(ob.ID);
               o.CI_INV_NO_SUB = ob.CI_INV_NO_SUB;
               o.INV_Date = ob.INV_Date;
               o.InvoiceSL = ob.InvoiceSL;
               o.InvoiceInfoID = ob.InvoiceInfoID;
               o.ID = ob.ID;
               li.Add(o);
           }
           return li;
       }

       public List<ELCProduct> CI_PRODUCT_LIST_GET(Int32 invID,Int32 invMasterID)
       {

           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var k = from p in obj.LC_CI_ORDER_PRORDUCT_GET(invMasterID,invID)

                   select p;

           List<ELCProduct> li = new List<ELCProduct>();
           ELCProduct o;
           foreach (var ob in k)
           {
     

               o = new ELCProduct();
               o.ID = Convert.ToInt32(ob.ID);
               o.MasterID = ob.ProductMasterID??0;
               o.PINO = ob.PINO;
               o.Product = ob.Product;
               o.Unit = ob.UNIT;
               o.Qty =ob.Qty.Value.ToString("0.00");
               o.UnitPrice = (ob.UnitPrice ?? 0).ToString("0.00");
               o.Discount = (ob.Discount ?? 0).ToString("0.00");

               o.TotalPrice = (ob.UnitPrice  * ob.Qty ).Value.ToString("0.00");
               o.NetTotal =((ob.UnitPrice  * ob.Qty )-(ob.Discount)).Value.ToString("0.00");
               li.Add(o);
           }
           return li;
       }

       #endregion
   }
}
