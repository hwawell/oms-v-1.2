﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Services;
using System.Reflection;

namespace DataLayer
{
   public class Order_DL
    {
        public List<EOrder> GetAllOrder(string UserID)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER()
                    where (EPermission.IsAccessAllOrder==true || p.EntryID==UserSession.CurrentUser)
                    orderby p.PINo
                    select p;

            EOrder objc;
            List<EOrder> liob = new List<EOrder>();
            //objc = new EOrder();
            //objc.ID = 0;
            //objc.ColorName = "--Select Color--";
            //liob.Add(objc);
            foreach (var ob in v)
            {
                objc = new EOrder();
                objc.ID = Convert.ToInt32(ob.SL);
                objc.PINo = ob.PINo;
                objc.CustID = Convert.ToInt32(ob.CustID);
                 objc.CustomerName = ob.CName;
                 objc.PDate =Convert.ToDateTime(ob.PDate);
                 objc.RcvDate =Convert.ToDateTime( ob.RcvDate);
                 objc.ShpDate = Convert.ToDateTime(ob.ShpDate);
                 objc.LCNo = ob.LCNo;
                 objc.MainBuyer = ob.MainBuyer;
                 objc.OrderTypeID = ob.OrderTypeID??0;
                 objc.OrderTypeName = ob.OrderTypeName;
                 objc.ProductionType = ob.ProductionType;
                 //objc.EMode = ob.PINo;
                 objc.UserID = ob.EntryID;
                 objc.ISAuthorized = ob.IsAuthorized??false;
                 objc.BuyerRef = ob.BuyerRef;
                 objc.Notes = ob.Notes;
               

                liob.Add(objc);

               
            }
            return liob;
        }
        public EOrder GetAllOrderBYPINO(string PINO)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER()
                    where p.PINo==PINO
                    orderby p.PINo
                    select p;

            EOrder objc;
            objc = new EOrder();
            foreach (var ob in v)
            {
                
                objc.ID = Convert.ToInt32(ob.SL);
                objc.PINo = ob.PINo;
                objc.CustID = Convert.ToInt32(ob.CustID);
                objc.CustomerName = ob.CName;
                objc.PDate = Convert.ToDateTime(ob.PDate);
                objc.RcvDate = Convert.ToDateTime(ob.RcvDate);
                objc.ShpDate = Convert.ToDateTime(ob.ShpDate);
                objc.LCNo = ob.LCNo;
                objc.MainBuyer = ob.MainBuyer;
                objc.OrderTypeID = ob.OrderTypeID ?? 0;
                objc.OrderTypeName = ob.OrderTypeName;
                objc.ProductionType = ob.ProductionType;
                objc.MEmail = ob.MEmail;
                objc.UserID = ob.EntryID;
                objc.ISAuthorized = ob.IsAuthorized ?? false;
                objc.BuyerRef = ob.BuyerRef;
                objc.Notes = ob.Notes;
                objc.RevisedNotes = ob.RevisedNotes;

               


            }
            return objc;
        }


        public List<EOrderType> GetAllOrderType()
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_TYPE()
                    orderby p.OrderTypeName
                    select p;

            EOrderType objc;
            List<EOrderType> liob = new List<EOrderType>();
            objc = new EOrderType();
            objc.ID = 0;
            objc.OrderTypeName = "All Type Of Order";
            liob.Add(objc);
            foreach (var ob in v)
            {
                objc = new EOrderType();
                objc.ID = ob.ID;
                objc.OrderTypeName = ob.OrderTypeName;
                

                liob.Add(objc);


            }
            return liob;
        }

        //public List<LC_COM_INV> LC_CI_GET(DateTime frm, DateTime to, string LCNo, Int32 CINO)
        //{

        //    DBProcessDataContext obj = new DBProcessDataContext();
        //    DBConnect objdbc = new DBConnect();
        //    obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
        //    var v = from p in obj.LC_CI_List(frm,to,LCNo,CINO)
                  
        //            select p;

        //    LC_COM_INV objc;
        //    List<LC_COM_INV> liob = new List<LC_COM_INV>();
           
        //    foreach (var ob in v)
        //    {
        //        objc = new LC_COM_INV();
        //        objc.ID  = ob.ID;
        //        objc.COM_INV_No = ob.COM_INV_No;
        //        objc.COM_INV_Date = ob.COM_INV_Date;
        //        objc.COM_INV_LCNo = ob.COM_INV_LCNo;
      
        //          objc.COM_INV_Date=ob.COM_INV_Date;
        //          objc.COM_INVProduct_List = ob.COM_INVProduct_List;
        //          objc.COM_INV_PRICE = ob.COM_INV_PRICE;
        //          objc.COM_INV_BANK = ob.COM_INV_BANK;
        //          objc.COM_INV_BRANCH_ADD = ob.COM_INV_BRANCH_ADD;
        //          objc.COM_INV_IRC_NO = ob.COM_INV_IRC_NO;
        //          objc.COM_INV_HS_CODE = ob.COM_INV_HS_CODE;
        //          objc.COM_INV_ExportLCNoDate = ob.COM_INV_ExportLCNoDate;

        //          objc.Mushok_No = ob.Mushok_No;
        //          objc.Mushok_Date = ob.Mushok_Date;
        //          objc.TruckNo = ob.TruckNo;
        //          objc.DriverName = ob.DriverName;
        //          objc.BTMA_ISS_Date = ob.BTMA_ISS_Date;
        //          objc.DOC_SUBMIT_DATE = ob.DOC_SUBMIT_DATE;
        //          objc.DOC_RCV_DATE = ob.DOC_RCV_DATE;
        //          objc.LDBC_NO = ob.LDBC_NO;
        //          objc.LDBC_DATE = ob.LDBC_DATE;
        //          objc.BankAcceptanceDate = ob.BankAcceptanceDate;
 

               
        //        liob.Add(objc);


        //    }
        //    return liob;
        //}
      
       
       public bool InsertUpdateOrder(EOrder ob,ref string id1)
        {

            try
            {
                string id = "-1";
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.INSERT_ORDER_SP(ob.PINo, ob.CustID, ob.PDate, ob.RcvDate, ob.ShpDate, ob.LCNo, ob.MainBuyer, ob.BuyerRef, ob.FRNO, ob.OrderTypeID, ob.ProductionType, ob.Notes, ob.RevisedNotes, ob.UserID, ref id);


                id1 = id;
                return true;


            }
            catch
            {
                return false;
            }


        }
        //public List<EOrderApproval> GetAllOrderApproval()
        //{

        //    DBProcessDataContext obj = new DBProcessDataContext();
        //    DBConnect objdbc = new DBConnect();
        //    obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
        //    var v = from p in obj.GET_ORDER_APPROVAL()
        //            orderby p.Approve_date descending
        //            select p;

        //    EOrderApproval objc;
        //    List<EOrderApproval> liob = new List<EOrderApproval>();
        //    foreach (var ob in v)
        //    {
        //        objc = new EOrderApproval();
        //        objc.ID = ob.ID;
        //        objc.ApprovalDate = ob.Approve_date?? System.DateTime.Now.Date;
        //        objc.ApproveFor = ob.APPROVE_FOR;
        //        objc.PINO = ob.PINo;
        //        objc.Notes = ob.Description;
        //        objc.IsApproved = ob.IsApproved?? false;



        //        liob.Add(objc);


        //    }
        //    return liob;
        //}
        public bool InsertUpdateOrderApproval(EOrderApproval ob)
        {

            try
            {
                
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.Insert_Order_Approval_SP( ob.ID,ob.PINO,ob.ApproveFor,ob.ApprovalDate,ob.Notes,ob.IsApproved,ob.UserID);


               
                return true;


            }
            catch
            {
                return false;
            }


        }
        public bool InsertUpdateOrderProduct(EOrderProduct ob, ref string id1)
        {

            try
            {
                string id = "-1";
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.INSERT_OrderProduct_SP(Convert.ToInt32(ob.OPID),ob.PINO,ob.Description,ob.Notes,ob.FRNO,ob.Weight,ob.WeightOption,ob.Width,ob.OpenTube,ob.Unit,Convert.ToDecimal(ob.UPDown),ob.ISExtraQty,ob.UserID, ref id);


                id1 = id;
                return true;


            }
            catch
            {
                return false;
            }


        }

        public List<EOrderProduct> GetAllOrderProduct(string PINO)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_PRODUCT(PINO)
                    orderby p.OPID 
                    select p;

            EOrderProduct objc;
            List<EOrderProduct> liob = new List<EOrderProduct>();
            
            foreach (var ob in v)
            {
                objc = new EOrderProduct();
                objc.OPID = Convert.ToInt32(ob.OPID);
                objc.PINO = ob.PINO;
                objc.Description =ob.Description;
                objc.Notes= ob.Notes;
                objc.ISExtraQty =Convert.ToBoolean( ob.IsExtraQty);
                objc.Weight = ob.Weight;
              
                objc.Width = ob.Width;
                objc.WidthOption = ob.WidthOption;
                objc.UPDown =Convert.ToDouble(ob.UPDown);
                objc.FRNO = ob.FRNo;
                objc.Unit = ob.Unit;
                objc.OpenTube = ob.WidthOption;
                objc.PerUnitKG =Convert.ToDouble(ob.PerUnitKG);
                objc.WeightOption = ob.WeightOption??"";
                objc.ItemDesc = ob.Description + "  " + ob.Notes;



                liob.Add(objc);


            }
            return liob;
        }


        public List<EOrderProduct> GetAllOrderProductLC(string PINO)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_PRODUCT(PINO)
                    where p.LCNo == null || p.LCNo.Length<2
                    orderby p.OPID
                    select p;

            EOrderProduct objc;
            List<EOrderProduct> liob = new List<EOrderProduct>();

            foreach (var ob in v)
            {
                objc = new EOrderProduct();
                objc.OPID = Convert.ToInt32(ob.OPID);
                objc.PINO = ob.PINO;
                objc.Description = ob.Description;
                objc.Notes = ob.Notes;
                objc.ISExtraQty = Convert.ToBoolean(ob.IsExtraQty);
                objc.Weight = ob.Weight;

                objc.Width = ob.Width;
                objc.WidthOption = ob.WidthOption;
                objc.UPDown = Convert.ToDouble(ob.UPDown);
                objc.FRNO = ob.FRNo;
                objc.Unit = ob.Unit;
                objc.OpenTube = ob.WidthOption;
                objc.PerUnitKG = Convert.ToDouble(ob.PerUnitKG);
                objc.WeightOption = ob.WeightOption ?? "";
                objc.ItemDesc =ob.PINO + ">"+ ob.Description + ",Weight:" + ob.Weight + ",Width:" + ob.Width + "," + ob.Notes;



                liob.Add(objc);


            }
            return liob;
        }


        //public List<EOrderProductLC> LCGetAllOrderProduct(string LCNO)
        //{

        //    DBProcessDataContext obj = new DBProcessDataContext();
        //    DBConnect objdbc = new DBConnect();
        //    obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
        //    var v = from p in obj.LC_GET_ORDER_PRODUCT(LCNO)
                  
        //            orderby p.OPID
        //            select p;

        //    EOrderProductLC objc;
        //    List<EOrderProductLC> liob = new List<EOrderProductLC>();

        //    foreach (var ob in v)
        //    {
        //        objc = new EOrderProductLC();
        //        objc.OPID = Convert.ToInt32(ob.OPID);
        //        objc.PINO = ob.PINO;
        //        objc.Description = ob.Description;
        //        objc.Notes = ob.Notes;
                
        //        objc.Weight = ob.Weight;

        //        objc.Width = ob.Width;
        //        //objc.WidthOption = ob.WidthOption;
               
        //        //objc.FRNO = ob.FRNo;
        //        objc.Unit = ob.Unit;
        //        //objc.OpenTube = ob.WidthOption;
               
              
        //        objc.ItemDesc =ob.PINO + " > GSM:" + ob.Weight + " ,Width"+ ob.Width + " > " +ob.Description + " > " + ob.Notes;

        //        objc.Qty = ob.BalanceQty??0;
        //        objc.UnitPrice = ob.UnitPrice??0;

        //        liob.Add(objc);


        //    }
        //    return liob;
        //}
       
       
       
       public List<EOrderList> LC_OrderList(string LCNO)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           var dataList= from p in obj.Orders
                    where p.AssignLCNo==LCNO && p.EMode=="E"

                    orderby p.PINo select p;
           EOrderList objc;
           List<EOrderList> liob = new List<EOrderList>();

            foreach (var ob in dataList)
            {
                objc = new EOrderList();
                objc.PINO = ob.PINo;
                liob.Add(objc);
            }
            return liob;
        }
       
       //public List<EOrderProduct> LC_AllOrderProduct(string LCNO)
       // {

       //     DBProcessDataContext obj = new DBProcessDataContext();
       //     DBConnect objdbc = new DBConnect();
       //     obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
       //     var v = from p in obj.LC_GET_ORDER_PRODUCT(LCNO)

       //             orderby p.OPID
       //             select p;

       //     EOrderProduct objc;
       //     List<EOrderProduct> liob = new List<EOrderProduct>();

       //     foreach (var ob in v)
       //     {
       //         objc = new EOrderProduct();
       //         objc.OPID = Convert.ToInt32(ob.OPID);
       //         objc.PINO = ob.PINO;
       //         objc.Description = ob.Description;
       //         objc.Notes = ob.Notes;

       //         objc.Weight = ob.Weight;

       //         objc.Width = ob.Width;
       //         //objc.WidthOption = ob.WidthOption;

       //         //objc.FRNO = ob.FRNo;
       //         objc.Unit = ob.Unit;
       //         //objc.OpenTube = ob.WidthOption;


       //         objc.ItemDesc = ob.Description + ",Weight:" + ob.Weight + ",Width:" + ob.Width + "," + ob.Notes;

       //        // objc.Qty = ob.BalanceQty ?? 0;
       //         //objc.UnitPrice = ob.UnitPrice ?? 0;

       //         liob.Add(objc);


       //     }
       //     return liob;
       // }
        
        public List<EOrderProductLC> LC_CI_GetAllOrderProduct(string LCNo,Int32 InvID)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.LC_CI_GET_ORDER_PRODUCT(LCNo, InvID)

                    orderby p.OPID
                    select p;

            EOrderProductLC objc;
            List<EOrderProductLC> liob = new List<EOrderProductLC>();

            foreach (var ob in v)
            {
                objc = new EOrderProductLC();
                
                objc.OPID = Convert.ToInt32(ob.OPID);
                objc.PINO = ob.PINO;
                objc.Description = ob.Description;
                objc.Notes = ob.Notes;

                objc.Weight = ob.Weight;

                objc.Width = ob.Width;
                //objc.WidthOption = ob.WidthOption;

                //objc.FRNO = ob.FRNo;
                objc.Unit = ob.Unit;
                //objc.OpenTube = ob.WidthOption;


                objc.ItemDesc =ob.PINO + " > "+ ob.Description + ",Weight:" + ob.Weight + ",Width:" + ob.Width + "," + ob.Notes;

                objc.AvailableQty = (Convert.ToDecimal( ob.BookingQty) - (Convert.ToDecimal(ob.tot) -Convert.ToDecimal( ob.Qty)));
                objc.Qty = ob.Qty ?? 0;
                objc.UnitPrice = ob.UnitPrice ?? 0;
                objc.Total = objc.Qty * objc.UnitPrice;
                objc.Discount = ob.Discount??0;
                objc.NetTotal = (objc.Qty * objc.UnitPrice) - objc.Discount;
                objc.Booking = ob.BookingQty??0;
                objc.IsNotShow = ob.IsNotShow ?? false;
                liob.Add(objc);


            }
            return liob;
        }

        public bool InsertUpdateOrderProductDetails(EOrderProductDetails ob, ref string id1)
        {

            try
            {
                string id = "-1";
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.INSERT_OrderProductDetails_SP(Convert.ToInt32(ob.OPDID), Convert.ToInt32(ob.OPID),ob.PINO,ob.BaseColor, ob.Color, ob.ColorNo, ob.LDFRef,ob.FRNo,ob.ColorTone, Convert.ToDecimal(ob.Qty), ob.Size, Convert.ToDecimal(ob.UnitPrice),ob.UserID,ob.ISUnAuthorizedSave, ref id);


                id1 = id;
                return true;


            }
            catch
            {
                return false;
            }


        }
        public bool EditPermission(string pinO)
        {

            try
            {
                string id = "-1";
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.EditPermission(pinO);


              
                return true;


            }
            catch
            {
                return false;
            }


        }

        public List<EOrderProductDetails> GetAllOrderProductDetailsBY_PRICE(int OPID)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_PRODUCT_DETAILS(OPID)
                    group p by p.UnitPrice into g

                    select new { UnitPrice = g.Key };
                   

            EOrderProductDetails objc;
            List<EOrderProductDetails> liob = new List<EOrderProductDetails>();

            foreach (var ob in v)
            {
                objc = new EOrderProductDetails();
                objc.UnitPrice = Convert.ToDouble(ob.UnitPrice);
                liob.Add(objc);


            }

            return liob;
        }

        public List<EOrderProductDetails> GetAllOrderProductDetails(int OPID)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_PRODUCT_DETAILS(OPID)
                    orderby p.OPDID
                    select p;

            EOrderProductDetails objc;
            List<EOrderProductDetails> liob = new List<EOrderProductDetails>();
           
            foreach (var ob in v)
            {
                objc = new EOrderProductDetails();
                objc.OPID = Convert.ToInt32(ob.OPID);
                objc.OPDID = Convert.ToInt32(ob.OPDID);
                objc.BaseColor = ob.BaseColor;
                objc.Color = ob.Color;
                objc.ColorNo = ob.ColorNo;
                objc.ColorTone = ob.ColorTone;
                objc.LDFRef = ob.Idesc;
                objc.FRNo = ob.FRNo;
                objc.Size = ob.Size;
                objc.Qty = Convert.ToDouble(ob.Qty);
                objc.UnitPrice = Convert.ToDouble(ob.UnitPrice);
                






                liob.Add(objc);


            }

            return liob;
        }

        public List<EOrderProductDetails> GetAllOrderProductDetailsBYPINO(string PINO)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_PRODUCT_DETAILS_BYPINO(PINO)
                    orderby p.OPDID
                    select p;

            EOrderProductDetails objc;
            List<EOrderProductDetails> liob = new List<EOrderProductDetails>();

            foreach (var ob in v)
            {
                objc = new EOrderProductDetails();
                objc.OPID = Convert.ToInt32(ob.OPID);
                objc.OPDID = Convert.ToInt32(ob.OPDID);
                objc.BaseColor = ob.BaseColor;
                objc.Color = ob.Color;
                objc.ColorNo = ob.ColorNo;
                objc.ColorTone = ob.ColorTone;
                objc.LDFRef = ob.Idesc;
                objc.FRNo = ob.FRNo;
                objc.Size = ob.Size;
                objc.Qty = Convert.ToDouble(ob.Qty);
                objc.UnitPrice = Convert.ToDouble(ob.UnitPrice);







                liob.Add(objc);


            }
            //objc = new EOrderProductDetails();
            //liob.Add(objc);
            return liob;
        }

        public List<EOrderClause> GetAllOrderClause(string pino)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_OrderClauses(pino)
                    orderby p.ClauseSequence
                    select p;

            EOrderClause objc;
            List<EOrderClause> liob = new List<EOrderClause>();
         
            foreach (var ob in v)
            {
                objc = new EOrderClause();
                if (ob.CNO != null && ob.CNO > 0)
                {
                    objc.Select = true;
                }
                objc.clauseID = ob.ID;
                objc.ClauseName = ob.ClauseName;
                objc.Sequence = ob.ClauseSequence??0;

                liob.Add(objc);


            }
            return liob;
        }
        public List<EOrderClause> GetAllOrderClauseNew(string pino)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_OrderClauses_NEW(pino)
                    orderby p.ClauseSequence
                    select p;

            EOrderClause objc;
            List<EOrderClause> liob = new List<EOrderClause>();

            foreach (var ob in v)
            {
                objc = new EOrderClause();
                if (ob.ID != null && ob.ID > 0)
                {
                    objc.Select = true;
                }
                objc.clauseID = ob.ClauseID??0;
                objc.ClauseName = ob.ClauseName;
                objc.Sequence = ob.ClauseSequence ?? 0;
                objc.PINO = pino;
                liob.Add(objc);


            }
            return liob;
        }
        public List<EOrderApprovalView> GetAllOrderListForApproval(int OrderTyeID,int Year)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_APPROVAL(OrderTyeID)
                    where p.PDate.Value.Year==Year && (OrderTyeID == 0 || p.orderTypeID == OrderTyeID)
                    orderby p.orderTypeID, p.SL descending
                    select p;

            EOrderApprovalView objc;
            List<EOrderApprovalView> liob = new List<EOrderApprovalView>();

            foreach (var ob in v)
            {
                objc = new EOrderApprovalView();
                
                objc.PINO = ob.PINo;
                objc.Customer = ob.CustomerName;
                if (ob.Approve_date != null)
                {
                    objc.ApprovalDate = ob.Approve_date.Value.Date.ToString("dd-MM-yyyy") ?? "";
                }
                objc.ApproveFor = ob.APPROVE_FOR;
                objc.Notes = ob.Description;
                objc.IsApproved = ob.IsApproved;
                objc.OrderType = ob.OrderType;
                
                objc.RevisedNotes = ob.RevisedNotes;
                objc.MerchandiserName = ob.EmployeeName;
                objc.MerchandiserCode = ob.EntryID;
                liob.Add(objc);


            }
            return liob;
        }

        public List<EOrderApprovalView> GetAllOrderList(int OrderTyeID,int PIYear)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_APPROVAL(OrderTyeID)
                    where (p.PDate.Value.Year==PIYear) && (OrderTyeID==0 ||  p.orderTypeID == OrderTyeID) && (EPermission.IsAccessAllOrder == true || p.EntryID == UserSession.CurrentUser)
                    orderby p.orderTypeID, p.SL descending
                    select p;

            EOrderApprovalView objc;
            List<EOrderApprovalView> liob = new List<EOrderApprovalView>();

            foreach (var ob in v)
            {
                objc = new EOrderApprovalView();

                objc.PINO = ob.PINo;
                objc.Customer = ob.CustomerName;
                if (ob.Approve_date != null)
                {
                    objc.ApprovalDate = ob.Approve_date.Value.Date.ToString("dd-MM-yyyy") ?? "";
                }
                objc.ApproveFor = ob.APPROVE_FOR;
                objc.Notes = ob.Description;
                objc.IsApproved = ob.IsApproved;
                objc.OrderType = ob.OrderType;
                objc.RevisedNotes = ob.RevisedNotes;
                objc.MerchandiserName = ob.EmployeeName;
                objc.MerchandiserCode = ob.EntryID;
                liob.Add(objc);


            }
            return liob;
        }
       
       
       public bool HasOrderApproved(string PINO)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_APPROVAL(0)
                    where p.PINo == PINO && p.IsApproved==true
                    
                    select p;

            bool result = false;
            if (v.Count() > 0)
            {
                result = true;
            }


            return result;
        }

        public bool InsertOrderClauses(EOrderClause ob)
        {

            try
            {
             
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.Insert_OrderClauses_sp(ob.PINO,ob.clauseID,ob.Sequence,ob.ClauseName);



         
                return true;


            }
            catch
            {
                return false;
            }


        }
        public bool DeleteOrderClauses(EOrderClause ob)
        {

            try
            {

                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.DELETE_OrderClauses_sp(ob.PINO);




                return true;


            }
            catch
            {
                return false;
            }


        }
        public bool DeleteOrderProductDetails(int ID)
        {

            try
            {

                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();

                obj.DELETE_ORDER_PRODUCT_DETAILS_sp(ID);




                return true;


            }
            catch
            {
                return false;
            }


        }


       


        public string DeleteLCorCI(string LCNO, Int32 CINo)
        {

            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


                string res = "";

                obj.LC_CI_DELETE(LCNO, CINo, ref res);

                obj.SubmitChanges();
                return res;
            }
            catch
            {
                return "";
            }
        }
       
       public string  SaveLC_CI_Information(LC_COM_INV ob, List<EOrderProductLC> li, bool isItem)
        {
            try
            {
                DBProcessDataContext obj = new DBProcessDataContext();
                DBConnect objdbc = new DBConnect();
                obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();


                string res = "";
                string strXML = DataSerialization.SerializeObject<List<EOrderProductLC>>(li);
                obj.LC_CI_INS_UPD(ob.ID, ob.LC_ID, ob.CI_Master_INV_No,ob.CI_Date,ob.CI_Product_List,ob.CI_NetAmountUSD,ob.CI_Mushok_No,ob.CI_Mushok_Date,ob.CI_TruckNo,ob.CI_DriverName,ob.CI_BTMA_ISS_Date,ob.CI_DOC_SUBMIT_DATE,ob.CI_DOC_RCV_DATE, strXML,UserSession.CurrentUser,0, ref res);
                 obj.SubmitChanges();
                 return res;



            }
            catch
            {
                return "0";
            }
        }
       
       
       public List<EOrderList> GetOrderByCustomer(int CustomerID)
        {

            DBProcessDataContext obj = new DBProcessDataContext();
            DBConnect objdbc = new DBConnect();
            obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
            var v = from p in obj.GET_ORDER_CUSTOMER(CustomerID)
                     orderby p.PINO
                    select p;

            EOrderList objc;
            List<EOrderList> liob = new List<EOrderList>();

            foreach (var ob in v)
            {
                objc = new EOrderList();

                objc.PINO = ob.PINO;
                objc.PIValue =Convert.ToDouble( ob.TotalPrice ?? 0);
                liob.Add(objc);


            }
            return liob;
        }

       public List<Order_Delivery_Info_GetResult> GetOrderDeliveryInfo(string pino)
       {
           DBProcessDataContext obj = new DBProcessDataContext();
           DBConnect objdbc = new DBConnect();
           obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
           return (from p in obj.Order_Delivery_Info_Get(pino)
                   select p).ToList();
                   
                  

       }

       public bool UpdateDeliveryAddress(string PINO, string Add, string ConPer, string Contact)
       {


           try
           {
               DBFactoryDataContext obj = new DBFactoryDataContext();
               DBConnect objdbc = new DBConnect();
               obj.Connection.ConnectionString = objdbc.GetConnectionForLINQ();
       
               obj.Order_Delivery_Info_Update(PINO, Add, ConPer, Contact);

               return true;


           }
           catch
           {
               return false;
           }


       }
       
    }
}
