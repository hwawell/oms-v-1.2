﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class EColor
    {
        public int ID { get; set; }
        public string ColorCode { get; set; }
        public string ColorName { get; set; }
        public bool IsActive { get; set; }
       
    }
    public class EClasue
    {
        public int ID { get; set; }
      
        public string ClauseName { get; set; }
        public bool IsActive { get; set; }

    }
    public class EDescription
    {
        public int ID { get; set; }
        public string DescriptionName { get; set; }
        public string TypeID { get; set; }
       
        public bool IsActive { get; set; }
       
    }
    public class EDescriptionType
    {
        public int ID { get; set; }
        public string TypeName { get; set; }
    

    }
    public class EDescriptionNotes
    {
        public int ID { get; set; }
        public string DescriptionName { get; set; }

       

    }

    public class EUnit
    {
       
        public string UnitName { get; set; }
       
        
    }

    public class EObject
    {

        public Int64 ID { get; set; }
        public string Mode { get; set; }
        public string UserID { get; set; }
        public string FormID { get; set; }
    }

    

    public class ECustomer
    {
        public Int64 ID { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string FAX { get; set; }
        public string ContactPerson { get; set; }
        public string MainBuyer { get; set; }
        public string BuyingHouse { get; set; }

        public string TIN { get; set; }
        public string BIN { get; set; }
        public string ERC { get; set; }
        public string IRC { get; set; }
        public string BOND { get; set; }
        public string VAT { get; set; }
        public string BOI { get; set; }
        public bool IsActive { get; set; }
        public string UserID { get; set; }
        public string CustomerAdd { get; set; }
    }

    
}
