﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class EEmployees
    {
        public int ID { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string EmpTypeName { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        
        public DateTime ConfirmationDate { get; set; }
        public DateTime PromotionDate { get; set; }
        public string BranchID { get; set; }
        public int DesignationID { get; set; }
        public int DepartmentID { get; set; }

        public int JobResponsibilityID { get; set; }

        public DateTime JoiningDate { get; set; }

        public DateTime PresentPlaceJoiningDate { get; set; }

        public int EmpTypeID { get; set; }

        public DateTime LastPromtionDate { get; set; }

        public DateTime LastIncrementDate { get; set; }

        public bool IsActive { get; set; }

        public string EntryUserID { get; set; }

        public DateTime EntryTime { get; set; }




       
    }
    public class EEmployeesParam
    {
        public int DesignationID { get; set; }
        public string BranchID { get; set; }
        public int EmpID { get; set; }

    }
}
