﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public static class EGlobal
    {

        public static string DBServerName { get; set; }
        public static string DBDatabase { get; set; }
        public static string DBUserID { get; set; }
        public static string DBPassword { get; set; }
        public static string FactoryDBServerName { get; set; }
        public static string FactoryDBDatabase { get; set; }
        public static string FactoryDBUserID { get; set; }
        public static string FactoryDBPassword { get; set; }
        public static string ApplicationPath { get; set; }
    }

    public static class EPermission
    {

        public static bool Order_Making { get; set; }
        public static bool Order_Edit { get; set; }
        public static bool View_Print { get; set; }
        public static bool Knitting_Approval { get; set; }
        public static bool User_Creation { get; set; }
        public static bool Deying_Approval { get; set; }
        public static bool Customer { get; set; }
        public static bool Clause { get; set; }
        public static bool User_Permission { get; set; }
        public static bool IsAccessAllOrder { get; set; }

     
       









    }
}
