﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public    class ELCInfo
    {

            public Int32 LCID { get; set; }
            public Int32 CustomerID { get; set; }
            public Int32 BankBranchID { get; set; }
            public Int32 LCTypeID { get; set; }
           public string LCNo { get; set; }
           public string Customer { get; set; }
           public DateTime LCDate { get; set; }
           public DateTime LCExpirydate { get; set; }
           public decimal LCValue { get; set; }
         
           public string FileNo { get; set; }
           public string AdviceNo { get; set; }
           public Int16 NoOfDaysLC { get; set; }
           public DateTime? ShipmentDate { get; set; }
           public bool IsBankTINShow { get; set; }
           public bool IsBankVATShow { get; set; }
           public string MasterLC { get; set; }
           public string TINNO { get; set; }
           public string HSCode { get; set; }
           public string EntryID { get; set; }
           public string BankBranch { get; set; }
           public string ShippingFrom { get; set; }
           public string ForwardingNo { get; set; }
           public DateTime ForwardingDate { get; set; }
           public string ShippingTo { get; set; }
           public string Notes { get; set; }
           public string AddlNotes { get; set; }
            
    }

   public class ELC_COM_INV
   {
       public Int32 ID { get; set; }
       public string InvoiceInfoID { get; set; }
       public string CI_INV_NO_SUB { get; set; }
       public Int32 InvoiceSL { get; set; }
       public DateTime? INV_Date { get; set; }
       public string Notes { get; set; }

       public bool IsMain { get; set; }
       public bool IsSubmited { get; set; }
       public string BA_LDBC_NO { get; set; }
       public DateTime? BA_LDBC_DATE { get; set; }
       public DateTime? BA_Date { get; set; }
       public decimal? BA_DocAcceptAmountUSD { get; set; }
       public decimal? BA_ConvertionRateBDT { get; set; }

       public decimal? PurchasePercent { get; set; }
       public decimal? PurchaseRate { get; set; }


       public DateTime? BA_DOC_MaturityDate { get; set; }
       public string BA_EntryID { get; set; }


       public DateTime? AR_RealizedDate { get; set; }
       public decimal? AR_RealizedAmountUSD { get; set; }
       public decimal? AR_ConvertionRateBDT { get; set; }
       public string AR_EntryID { get; set; }
       public Int32 CustomerID { get; set; }
       



   }
   public class ELCOrder
   {

       public string PINO { get; set; }
       public decimal PIValue { get; set; }
      
   }
   public class ELCProduct
   {

       public Int32 ID { get; set; }
       public Int32 MasterID { get; set; }
      
       public string PINO { get; set; }
       public string Product { get; set; }
       public string Unit { get; set; }
       public string Qty { get; set; }

       public string UnitPrice { get; set; }
       public string TotalPrice { get; set; }
       public string Discount { get; set; }
       public string NetTotal { get; set; }
      


   }
}
