﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
  public  class EOrder
    { 
        public Int64 ID { get; set; }
        public string PINo { get; set; }
        public int CustID { get; set; }
        public string CustomerName { get; set; }
        public DateTime PDate { get; set; }
        public DateTime RcvDate { get; set; }
        public DateTime ShpDate { get; set; }
        public string LCNo { get; set; }
        public string FRNO { get; set; }
        public string MainBuyer { get; set; }
        public string MEmail { get; set; }
        
        public int OrderTypeID { get; set; }
        public string OrderTypeName { get; set; }
        public string ProductionType { get; set; }
        public string EMode { get; set; }
        public string UserID { get; set; }
        public bool ISAuthorized { get; set; }
        public string BuyerRef { get; set; }
        public string Notes { get; set; }
        public string RevisedNotes { get; set; }

    }
  public class EOrderType
  {
      public int ID { get; set; }
      public string OrderTypeName { get; set; }

  }
  public class EOrderReport
  {
      
      public string PINO { get; set; }
      public bool ExtraQty { get; set; }

  }
  public class EOrderProduct
  {
      public int OPID { get; set; }
      public string PINO { get; set; }


      public string Description { get; set; }
      public string Notes { get; set; }
      public string FRNO { get; set; }
      public string Weight { get; set; }
      public string WeightOption { get; set; }
      public double UPDown { get; set; }
      public string Width { get; set; }
      public string WidthOption { get; set; }
      public string Unit { get; set; }
      public double PerUnitKG { get; set; }
      public bool ISExtraQty { get; set; }
      public string OpenTube { get; set; }
      public string EMode { get; set; }
      public string UserID { get; set; }
      public string ItemDesc { get; set; }
      
    


  }
  public class ECIList
  {
      public Int32 InvoiceID { get; set; }
      public string LCNo { get; set; }


      public string InvoiceNo { get; set; }
      public string InvoiceDate { get; set; }

      public string InvoicePrice { get; set; }

  }
  public class ECI_OrderProduct
  {
      public int OPID { get; set; }
      public string Product { get; set; }
      public double Balance { get; set; }


  }
  public class EOrderProductLC
  {
      public int OPID { get; set; }
      public string PINO { get; set; }


      public string Description { get; set; }
      public string Notes { get; set; }
     
      public string Weight { get; set; }
   
      public string Width { get; set; }
    
      public string Unit { get; set; }
   
      public string ItemDesc { get; set; }
      public decimal Booking { get; set; }
      public decimal AvailableQty { get; set; }
      public decimal Qty { get; set; }
      public decimal UnitPrice { get; set; }
      public decimal Total { get; set; }
      public decimal Discount { get; set; }
      public decimal NetTotal { get; set; }

      public bool IsNotShow { get; set; }


  }
  public class ELCNoFileNo
  {
      public Int32 LCID { get; set; }
      public Int32 NoOfDays { get; set; }
      public string LCNo { get; set; }
      public string FileNo { get; set; }
      public string Customer { get; set; }
      public string Notes { get; set; }
      public string LcDate { get; set; }

      public decimal TotalLC { get; set; }
      public decimal TotalCI { get; set; }

  }
  public class ELCOrderProduct
  {
      public int OPID { get; set; }

  }
  public class EOrderProductDetails
  {
      public int OPDID { get; set; }
      public int OPID { get; set; }
      public string PINO { get; set; }
      public string BaseColor { get; set; }
      public string FRNo { get; set; }
      public string Color { get; set; }

      public string ColorNo { get; set; }
      public string ColorTone { get; set; }
      public string LDFRef { get; set; }

      public string Weight { get; set; }
      public string Size { get; set; }
      public double Qty { get; set; }
      public double UnitPrice { get; set; }
      public double TotalPrice { get; set; }
      public bool ISUnAuthorizedSave { get; set; }
     
    
      public string EMode { get; set; }
      public string UserID { get; set; }

    

  }
  public class EOrderClause
  {
      public int clauseID { get; set; }
      public bool Select { get; set; }
      public string ClauseName { get; set; }
      public string PINO { get; set; }
      public int Sequence { get; set; }

  }
  public class EOrderApproval
  {
      public Int32 ID { get; set; }
      public string PINO  { get; set; }
      public DateTime ApprovalDate { get; set; }
      public string ApproveFor { get; set; }
      public string Notes { get; set; }
      public bool IsApproved { get; set; }
      public string UserID { get; set; }

  }
  public class EOrderList
  {

      public string PINO { get; set; }
      public double PIValue { get; set; }

  }
  public class EOrderApprovalView
  {
     
      public string PINO { get; set; }
      public string Customer { get; set; }
      public string ApproveFor { get; set; }
      public string ApprovalDate { get; set; }
      public string Notes { get; set;}
      public string RevisedNotes { get; set; }
      public string MerchandiserName { get; set; }
      public string OrderType { get; set; }
      public string MerchandiserCode { get; set; }
     
      public bool IsApproved { get; set; }
     

  }
}


