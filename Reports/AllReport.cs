﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;
using System.Reflection;

using DataLayer;

namespace Reports
{
    public class AllReport
    {
        string db;
        string server;
        string uid;
        string pass;
        bool windowsAuthentication = false;
        public AllReport()
        {
            try
            {
                DBConnect obj = new DBConnect();
                Dictionary<string, string> ob = obj.ReadConnectionStringParts();
                db = ob["Database"];
                server = ob["Server"];
                uid = ob["UserId"];
                pass = ob["Password"];
               // windowsAuthentication =Convert.ToBoolean(ob["Authentication"]);                
            }
            catch (Exception ex)
            {
                //IMSLog.Debug("IMSAllReport : " + "IMSAllReport() :: " + ex.Message);
            }
        }
        //////////////////////////public ReportDocument GetDataEntryReport(string className, string fm, string to, string ads, string User)
        //////////////////////////{
        //////////////////////////    ReportDocument ob = new ReportDocument();

        //////////////////////////    //Get the current assembly object
        //////////////////////////    Assembly assembly = Assembly.GetExecutingAssembly();
           
        //////////////////////////    //Get the name of the assembly (this will include the public token and version number
        //////////////////////////    AssemblyName assemblyName = assembly.GetName();
           
        //////////////////////////    //Use just the name concat to the class chosen to get the type of the object
        //////////////////////////    Type t = assembly.GetType(className);
        //////////////////////////    //Type t = assembly.GetType("RPT_Balance_Tk_ENTRY");

        //////////////////////////    //Create the object, cast it and return it to the caller
        //////////////////////////    ob = (ReportDocument)Activator.CreateInstance(t);
        //////////////////////////    return ob;
        //////////////////////////    //return GetExportDataEntryReport(ob, fm, to, ads, User);
        //////////////////////////}

        //////////////////////////public ReportDocument GetScheduleStatementReport(string className, string fy, string fp, string ads, string ReportPar)
        //////////////////////////{
        //////////////////////////    ReportDocument ob = new ReportDocument();
            

        //////////////////////////    //Get the current assembly object
        //////////////////////////    Assembly assembly = Assembly.GetExecutingAssembly();

        //////////////////////////    //Get the name of the assembly (this will include the public token and version number
        //////////////////////////    AssemblyName assemblyName = assembly.GetName();

        //////////////////////////    //Use just the name concat to the class chosen to get the type of the object
        //////////////////////////    Type t = assembly.GetType(className);
        //////////////////////////    //Type t = assembly.GetType("RPT_Balance_Tk_ENTRY");



        //////////////////////////    //Create the object, cast it and return it to the caller
        //////////////////////////    ob = (ReportDocument)Activator.CreateInstance(t);
        //////////////////////////    //FxFlow_RL.Reports.Statement.Statement_S1 ob1 = new FxFlow_RL.Reports.Statement.Statement_S1();
        //////////////////////////    //return GetScheduleStatementReport1(ob, fy, fp, ads, ReportPar);
        //////////////////////////    return ob;
        //////////////////////////}
        public  ReportDocument GetReport(ReportDocument ReportObject)
        {

            try
            {
                ConnectionInfo connectionInfo = new ConnectionInfo();

                connectionInfo.ServerName = server;
                connectionInfo.DatabaseName = db;
                //if (windowsAuthentication == true)
                //{
                //    connectionInfo.IntegratedSecurity = true;
                //}
                //else
                //{
                    connectionInfo.UserID = uid;
                    connectionInfo.Password = pass;
                //}
                //////////////////////////////////////////ReportObject.SetParameterValue("@FromDate", fm);
                //////////////////////////////////////////ReportObject.SetParameterValue("@ToDate", to);
                //////////////////////////////////////////ReportObject.RecordSelectionFormula = "{RPT_EXPORT_SP;1.AdsCode}='" + ads + "'";
                //////////////////////////////////////////ReportObject.RecordSelectionFormula = "{RPT_EXPORT_SP;1.EntryUserId}='" + User + "'";


                Tables tables = ReportObject.Database.Tables;

                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;

                    tableLogonInfo.ConnectionInfo = connectionInfo;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }
            }
            catch (Exception ex)
            {
               
            }
            return ReportObject;
        }


       

    }
}
