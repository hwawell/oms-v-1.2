﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
namespace OMS
{
    public partial class ApprovalView : Form
    {
        bool IsNotApprove;
        List<EOrderApprovalView> liOrder;
        List<EOrderApprovalView> liOrderSelected;
        public ApprovalView()
        {
            InitializeComponent();
        }

        private void ApprovalView_Load(object sender, EventArgs e)
        {
            cboOrderType.DataSource = new Order_DL().GetAllOrderType();
            cboOrderType.DisplayMember = "OrderTypeName";
            cboOrderType.ValueMember = "ID";

            List<string> li = new List<string>();
            int year = System.DateTime.Now.Year;
            li.Add(year.ToString());
            li.Add((year-  1).ToString());
            li.Add((year - 2).ToString());
            li.Add((year - 3).ToString());
            li.Add((year - 4).ToString());
            li.Add((year - 5).ToString());

            cboYear.DataSource = li;
            //cboYear.Text = "Select Year";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            IsNotApprove = false;
            liOrder = new Order_DL().GetAllOrderListForApproval(Convert.ToInt32(cboOrderType.SelectedValue), Convert.ToInt32(cboYear.Text)).FindAll(o => o.IsApproved == true);
            dgvData.DataSource = liOrder;
            lblStatus.Text = "List of Approve order . Total :" + dgvData.Rows.Count.ToString();
        }

        private void btnNotApprove_Click(object sender, EventArgs e)
        {
            IsNotApprove = true;
            liOrder = new Order_DL().GetAllOrderListForApproval(Convert.ToInt32(cboOrderType.SelectedValue), Convert.ToInt32(cboYear.Text)).FindAll(o => o.IsApproved == false);
            dgvData.DataSource = liOrder;
            lblStatus.Text = "List of Pending order For Approval . Total :" + dgvData.Rows.Count.ToString();

        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditOrder();
        }

        
        private void EditOrder()
        {
            if (EPermission.Deying_Approval == true || EPermission.Knitting_Approval == true)
            {
                if (IsNotApprove == false)
                {
                    MessageBox.Show("Order Already Approved");
                }
                else
                {
                    if (dgvData.SelectedRows.Count > 0)
                    {
                        EOrderApproval obj = new EOrderApproval();
                        DataGridViewRow currentRow = dgvData.SelectedRows[0];
                        obj.PINO = currentRow.Cells["PINO"].Value.ToString();
                        OrderApproval objDesg = new OrderApproval(obj);
                        objDesg.ShowDialog();

                    }
                    else
                    {
                        MessageBox.Show("Select Order For Approval");
                    }
                }
            }
            else
            {
                MessageBox.Show("You dont Have Enough Permission To Approve this Order");
            }
           
            
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            EditOrder();

        }

        private void miClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void viewOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                DataGridViewRow currentRow = dgvData.SelectedRows[0];
                string PINO = currentRow.Cells["PINO"].Value.ToString();
                EOrder objOrder = new Order_DL().GetAllOrderBYPINO(PINO);
                Order objDesg = new Order(null, objOrder,true);
                objDesg.ShowDialog();

            }
        }

        private void dgvData_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string strColumnName = dgvData.Columns[e.ColumnIndex].Name;
            SortOrder strSortOrder = getSortOrder(e.ColumnIndex);

            liOrder.Sort(new StudentComparer(strColumnName, strSortOrder));
            dgvData.DataSource = null;
            dgvData.DataSource = liOrder;
            //customizeDataGridView();
            dgvData.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = strSortOrder;
        }
        private SortOrder getSortOrder(int columnIndex)
        {
            if (dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection == SortOrder.None ||
                dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection == SortOrder.Descending)
            {
                dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                return SortOrder.Ascending;
            }
            else
            {
                dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                return SortOrder.Descending;
            }
        }
    }



}
