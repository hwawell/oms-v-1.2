﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;

namespace OMS.Basic_Operation
{
    public partial class BrusingInfo : Form
    {
        public BrusingInfo()
        {
            InitializeComponent();
        }
          List<EDescriptionNotes> _liDes;
        EDescriptionNotes _objDes;

        public BrusingInfo(List<EDescriptionNotes> lid, EDescriptionNotes objDe)
        {
            InitializeComponent();
            _liDes = lid;
            _objDes = objDe;
            if (_objDes != null)
            {

                txtName.Text = _objDes.DescriptionName;
               
                

            }
            
        }
        private void Clear()
        {
            txtName.Text = string.Empty;
            // txtDescription.Text = string.Empty;

        }





        private void Save()
        {
            EDescriptionNotes objDes = new EDescriptionNotes();
            if (_objDes != null)
            {
                objDes = _liDes.Find(o => o.ID == _objDes.ID);

            }
            objDes.DescriptionName = txtName.Text;



            bool Res = new Common_DL().InsertUpdateDescriptionNotes(objDes);
            if (Res == true)
            {

                if (objDes == null)
                {

                    if (_liDes == null)
                    {
                        _liDes = new List<EDescriptionNotes>();
                    }
                    _liDes.Add(objDes);

                }
                MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Clear();
                txtName.Focus();
            }
            else
            {

                MessageBox.Show("Data not Saved", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtName.Focus();

            }
        }




        private void miSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
