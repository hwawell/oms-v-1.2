﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;

namespace OMS.Basic_Operation
{
    public partial class BuyerBank : Form
    {
        List<ECustomer> _liData = new List<ECustomer>();

        List<CustomerBank> liob;
        CustomerBank _objDes;
        public BuyerBank()
        {
            InitializeComponent();
        }

        private void BuyerBank_Load(object sender, EventArgs e)
        {
            _liData = new Common_DL().GetAllCustomer().FindAll(o => o.ID > 0);
            cboCustomer.DataSource = _liData;
            cboCustomer.DisplayMember = "CustomerName";
            cboCustomer.ValueMember = "ID";
            
        }

        private void cboCustomer_SelectionChangeCommitted(object sender, EventArgs e)
        {
            LoadCustomerBank();
        }

        private void LoadCustomerBank()
        {

            liob = new Common_DL().GetCustomerBank(Convert.ToInt32(cboCustomer.SelectedValue.ToString()));
            dataGridView1.DataSource = liob;
            if (liob != null)
            {
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].Visible = false;
            }
        }

        private void miSave_Click(object sender, EventArgs e)
        {
           
            if (txtBank.Text.Length < 4)
            {
                MessageBox.Show("Bank Name must be given");
                return;
            }
            if (txtBranch.Text.Length < 2)
            {
                MessageBox.Show("Bank Branch Name must be given");
                return;
            }
            if (txtAddress.Text.Length < 4)
            {
                MessageBox.Show("Addres must be given");
                return;
            }
            Save();
        }
        private void Clear()
        {
            txtBank.Text=string.Empty;
            txtBranch.Text=string.Empty;
            txtTIN.Text=string.Empty;
            txtAddress.Text=string.Empty;
            txtVAT.Text=string.Empty;
            txtBankBIN.Text = string.Empty;
            _objDes = null;
        }
        private void Save()
        {
            try
            {
                if (cboCustomer.SelectedIndex > 0)
                {
                    CustomerBank objDes = new CustomerBank();
                    if (_objDes != null)
                    {
                        objDes = liob.Find(o => o.ID == _objDes.ID);

                    }
                    objDes.BankName = txtBank.Text;
                    objDes.BranchName = txtBranch.Text;
                    objDes.BranchAddress = txtAddress.Text;
                    objDes.TINNO = txtTIN.Text;
                    objDes.RegNo = txtVAT.Text;
                    objDes.CustomerID = Convert.ToInt32(cboCustomer.SelectedValue.ToString());

                    objDes.BankBIN = txtBankBIN.Text;


                    if (new Common_DL().InsertUpdateCustomerBank(objDes) == true)
                    {
                        LoadCustomerBank();
                        MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Clear();
                        txtBank.Focus();

                    }
                    else
                    {

                        MessageBox.Show("Operation Failed.", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtBank.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Select a Customer.", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
               
            }
            catch
            {
                MessageBox.Show(" Please Try Again.", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Int32 id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["ID"].Value.ToString());
                _objDes = liob.SingleOrDefault(o => o.ID == id);

                if (_objDes != null)
                {

                    txtBank.Text = _objDes.BankName;
                    txtBranch.Text = _objDes.BranchName;
                    txtAddress.Text = _objDes.BranchAddress;
                    txtTIN.Text = _objDes.TINNO;
                    txtVAT.Text = _objDes.RegNo;
                    txtBankBIN.Text = _objDes.BankBIN;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Int32 id = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["ID"].Value.ToString());
            }
        }
        

    }
}
