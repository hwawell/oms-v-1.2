﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Entities;
namespace OMS.Basic_Operation
{
    public partial class Clause : Form
    {
        public Clause()
        {
            InitializeComponent();
        }
        List<EClasue> _liDes;
        EClasue _objDes;

        public Clause(List<EClasue> lid, EClasue objDe)
        {
            InitializeComponent();
            _liDes = lid;
            _objDes = objDe;
            if (_objDes != null)
            {

              
                txtColorName.Text = _objDes.ClauseName;
                

            }
            
        }


        

        

     
        private void Clear()
        {
           
            txtColorName.Text = string.Empty;
            
        }

     
        

       
        private void Save()
        {
            EClasue objDes = new EClasue();
            if (_objDes != null)
            {
                objDes = _liDes.Find(o => o.ID == _objDes.ID);

            }
          
            objDes.ClauseName = txtColorName.Text;
            objDes.IsActive = true;

            bool Res = new Common_DL().InsertUpdateClasue(objDes);
            if (Res == true)
            {

                if (objDes == null)
                {

                    if (_liDes == null)
                    {
                        _liDes = new List<EClasue>();
                    }
                    _liDes.Add(objDes);

                }
                MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Clear();
                txtColorName.Focus();
            }
            else
            {

                MessageBox.Show("Data not Saved", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtColorName.Focus();

            }
        }
        

        

        private void miSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
