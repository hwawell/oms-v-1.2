﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
using Services;
namespace OMS.Basic_Operation
{
    public partial class ClauseView : Form
    {
        List<EClasue> _liData = new List<EClasue>();
          public ClauseView()
        {
            InitializeComponent();
        }
        

       

       
        private void LoadDesignation()
        {
            _liData = new Common_DL().GetAllClasue().FindAll(o=> o.ID>0);
            LoadGridView();
            
        }
        private void LoadGridView()
        {
              
            dgvData.AutoGenerateColumns = false;
            dgvData.DataSource = _liData;

            
                       dgvData.Columns["ID"].Visible = false;
            dgvData.Refresh();
          
        }

      
        private void AddNew()
        {
            Clause objDesg = new Clause(_liData, null);
            objDesg.ShowDialog();
            LoadGridView();
        }

      

       
        private void EditData()
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                DataGridViewRow currentRow = dgvData.SelectedRows[0];
                int ID = Convert.ToInt32(currentRow.Cells["id"].Value);
                Clause objDesg = new Clause(_liData, _liData.SingleOrDefault(o => o.ID == ID));
                objDesg.ShowDialog();
                LoadGridView();
            }
        }

        private void DeleteData()
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Do you want to Delete this Record", "Result", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    DataGridViewRow currentRow = dgvData.SelectedRows[0];
                    int ID = Convert.ToInt32(currentRow.Cells["id"].Value);
                    EObject objUp = new EObject();
                    objUp.ID = ID;
                    objUp.Mode = "D";
                    objUp.FormID = "Clause";
                    objUp.UserID = UserSession.CurrentUser;
                    bool res = new Common_DL().DeleteObject(objUp);
                    if (res == true)
                    {
                        LoadDesignation();

                    }
                    else
                    {
                        MessageBox.Show("Delete Operation Failed", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
            }
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void miEdit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            DeleteData();
        }

        private void miLoad_Click(object sender, EventArgs e)
        {
            LoadDesignation();
        }

        private void miClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void ColorView_Load(object sender, EventArgs e)
        {

        }

        private void ClauseView_Load(object sender, EventArgs e)
        {

        }

       
    }
}
