﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Entities;
namespace OMS.Basic_Operation
{
    public partial class Colors : Form
    {
        public Colors()
        {
            InitializeComponent();
        }

        List<EColor> _liDes;
        EColor _objDes;

        public Colors(List<EColor> lid, EColor objDe)
        {
            InitializeComponent();
            _liDes = lid;
            _objDes = objDe;
            if (_objDes != null)
            {

                txtColor.Text = _objDes.ColorCode;
                txtColorName.Text = _objDes.ColorName;
                

            }
            
        }


        

        

     
        private void Clear()
        {
            txtColor.Text = string.Empty;
            txtColorName.Text = string.Empty;
            
        }

     
        

       
        private void Save()
        {
            EColor objDes = new EColor();
            if (_objDes != null)
            {
                objDes = _liDes.Find(o => o.ID == _objDes.ID);

            }
            objDes.ColorCode = txtColor.Text;
            objDes.ColorName = txtColorName.Text;
            objDes.IsActive = true;

            bool Res = new Common_DL().InsertUpdateColor(objDes);
            if (Res == true)
            {

                if (objDes == null)
                {

                    if (_liDes == null)
                    {
                        _liDes = new List<EColor>();
                    }
                    _liDes.Add(objDes);

                }
                MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Clear();
                txtColor.Focus();
            }
            else
            {

                MessageBox.Show("Data not Saved", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtColor.Focus();

            }
        }
        

        

        private void miSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
