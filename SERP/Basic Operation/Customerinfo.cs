﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
namespace OMS.Basic_Operation
{
    public partial class Customerinfo : Form
    {
        public Customerinfo()
        {
            InitializeComponent();
        }
          List<ECustomer> _liDes;
         ECustomer _objDes;

         public Customerinfo(List<ECustomer> lid, ECustomer objDe)
        {
            InitializeComponent();
            _liDes = lid;
            _objDes = objDe;
            if (_objDes != null)
            {
                txtCode.Text = objDe.CustomerCode;
                txtName.Text = _objDes.CustomerName;
                txtTelephone.Text = _objDes.Telephone;
                txtAddress.Text = _objDes.CustomerAddress;
                txtEmail.Text = _objDes.Email;
                txtFax.Text = _objDes.FAX;
                txtContactPerson.Text = _objDes.ContactPerson;
                txtTIN.Text=_objDes.TIN;
                txtIRC.Text = _objDes.IRC;
                txtVAT.Text = _objDes.VAT;
                txtBond.Text = _objDes.BOND;
                txtERC.Text = _objDes.ERC;
           

            }
            
        }


        

        

     
        private void Clear()
        {
            txtCode.Text = string.Empty;
            txtName.Text = string.Empty;
            txtTelephone.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtContactPerson.Text = string.Empty;
            
        }

     
        

       
        private void Save()
        {
            try
            {
                ECustomer objDes = new ECustomer();
                if (_objDes != null)
                {
                    objDes = _liDes.Find(o => o.ID == _objDes.ID);

                }
                objDes.CustomerName = txtName.Text;
                objDes.CustomerCode = txtCode.Text;
                objDes.Telephone = txtTelephone.Text;
                objDes.CustomerAddress = txtAddress.Text;
                objDes.Email = txtEmail.Text;
                objDes.FAX = txtFax.Text;
                objDes.ContactPerson = txtContactPerson.Text;
                objDes.TIN = txtTIN.Text;
                objDes.IRC = txtIRC.Text;
                objDes.VAT = txtVAT.Text;
                objDes.BOND = txtBond.Text;
                objDes.ERC = txtERC.Text;
                objDes.BOI = txtBOINo.Text;

                objDes.IsActive = true;
                string sid = "";
                
                if ( (new FactoryOperation_DL().InsertUpdateCustomer(objDes, ref sid) == true))
                {
                    objDes.ID = Convert.ToInt32(sid);
                    if (new Common_DL().InsertUpdateCustomer(objDes, ref sid)==true)
                    {
                        if (objDes == null)
                        {

                            if (_liDes == null)
                            {
                                _liDes = new List<ECustomer>();
                            }
                            _liDes.Add(objDes);

                        }
                        MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Clear();
                        txtName.Focus();

                    }
                    else
                    {
                        Int64   ID = objDes.ID;
                        EObject objUp = new EObject();
                        objUp.ID = ID;
                        objUp.Mode = "D";
                        objUp.FormID = "Customer";

                        bool res = new Common_DL().DeleteObject(objUp);

                        MessageBox.Show("Failed To Connect Factory Server! Please Try Again.", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtName.Focus();
                    }
                }
                else
                {

                    MessageBox.Show("Data not Saved", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtName.Focus();

                }
            }
            catch
            {
                MessageBox.Show("Failed To Connect Factory Server! Please Try Again.", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        

        

        private void miSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Customer_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
