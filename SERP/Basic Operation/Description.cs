﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;

namespace OMS.Basic_Operation
{
    public partial class Description : Form
    {
        public Description()
        {
            InitializeComponent();
        }
        List<EDescription> _liDes;
        EDescription _objDes;

        public Description(List<EDescription> lid, EDescription objDe)
        {
            InitializeComponent();
            _liDes = lid;
            _objDes = objDe;
            if (_objDes != null)
            {

                txtName.Text = _objDes.DescriptionName;
                cboType.SelectedValue = _objDes.TypeID;
                

            }
            
        }


        

        

     
        private void Clear()
        {
            txtName.Text = string.Empty;
           // txtDescription.Text = string.Empty;
            
        }

     
        

       
        private void Save()
        {
            EDescription objDes = new EDescription();
            if (_objDes != null)
            {
                objDes = _liDes.Find(o => o.ID == _objDes.ID);

            }
            objDes.DescriptionName = txtName.Text;
            objDes.TypeID = cboType.SelectedValue.ToString();
            objDes.IsActive = true;

            bool Res = new Common_DL().InsertUpdateDescription(objDes);
            if (Res == true)
            {

                if (objDes == null)
                {

                    if (_liDes == null)
                    {
                        _liDes = new List<EDescription>();
                    }
                    _liDes.Add(objDes);

                }
                MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Clear();
                txtName.Focus();
            }
            else
            {

                MessageBox.Show("Data not Saved", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtName.Focus();

            }
        }
        

        

        private void miSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Description_Load(object sender, EventArgs e)
        {
            cboType.DataSource = new Common_DL().GetAllDescriptionType();
            cboType.DisplayMember = "TypeName";
            cboType.ValueMember = "ID";
        }
    }
}
