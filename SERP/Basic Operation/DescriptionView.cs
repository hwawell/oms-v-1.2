﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
using Services;
namespace OMS.Basic_Operation
{
    public partial class DescriptionView : Form
    {
        public DescriptionView()
        {
            InitializeComponent();
        }
        List<EDescription> _liData = new List<EDescription>();






        private void LoadDesignation()
        {
            _liData = new Common_DL().GetAllDescription().FindAll(o => o.ID > 0);
            LoadGridView();

        }
        private void LoadGridView()
        {
            dgvData.DataSource = _liData;
            dgvData.Columns["ID"].Visible = false;
         
            dgvData.Refresh();
            txtStatus.Text = "Total Record : " + _liData.Count.ToString();
        }


        private void AddNew()
        {
            Description objDesg = new Description(_liData, null);
            objDesg.ShowDialog();
            LoadGridView();
        }




        private void EditData()
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                DataGridViewRow currentRow = dgvData.SelectedRows[0];
                int ID = Convert.ToInt32(currentRow.Cells["id"].Value);
                Description objDesg = new Description(_liData, _liData.SingleOrDefault(o => o.ID == ID));
                objDesg.ShowDialog();
                LoadGridView();
            }
        }

        private void DeleteData()
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Do you want to Delete this Record", "Result", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    DataGridViewRow currentRow = dgvData.SelectedRows[0];
                    int ID = Convert.ToInt32(currentRow.Cells["id"].Value);
                    EObject objUp = new EObject();
                    objUp.ID = ID;
                    objUp.Mode = "D";
                    objUp.FormID = "Description";
                    objUp.UserID = UserSession.CurrentUser;
                    bool res = new Common_DL().DeleteObject(objUp);
                    if (res == true)
                    {
                        LoadGridView();

                    }
                    else
                    {
                        MessageBox.Show("Delete Operation Failed", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
            }
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void miEdit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            DeleteData();
        }

        private void miLoad_Click(object sender, EventArgs e)
        {
            LoadDesignation();
        }

        private void miClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
