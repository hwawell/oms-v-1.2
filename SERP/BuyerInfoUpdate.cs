﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;

namespace OMS
{
    public partial class BuyerInfoUpdate : Form
    {
        ECustomer _objDes;
        public BuyerInfoUpdate()
        {
            InitializeComponent();
        }

         public BuyerInfoUpdate(ECustomer CustomerID)
        {
            InitializeComponent();
           
             _objDes=CustomerID;

              if (_objDes != null)
            {
                txtCode.Text = _objDes.CustomerCode;
                txtName.Text = _objDes.CustomerName;
                txtTelephone.Text = _objDes.Telephone;
                txtAddress.Text = _objDes.CustomerAddress;
                txtEmail.Text = _objDes.Email;
                txtFax.Text = _objDes.FAX;
                txtContactPerson.Text = _objDes.ContactPerson;
                txtTIN.Text=_objDes.TIN;
                txtIRC.Text = _objDes.IRC;
                txtVAT.Text = _objDes.VAT;
                txtBond.Text = _objDes.BOND;
                txtERC.Text = _objDes.ERC;
                txtBINNO.Text = _objDes.BIN;
                txtBOINo.Text = _objDes.BOI;

            }
        }

        private void BuyerInfoUpdate_Load(object sender, EventArgs e)
        {

        }

        private void miSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        private void Save()
        {
            try
            {


                _objDes.CustomerName = txtName.Text;
                _objDes.CustomerCode = txtCode.Text;
                _objDes.Telephone = txtTelephone.Text;
                _objDes.CustomerAddress = txtAddress.Text;
                _objDes.Email = txtEmail.Text;
                _objDes.FAX = txtFax.Text;
                _objDes.ContactPerson = txtContactPerson.Text;
                _objDes.TIN = txtTIN.Text;
                _objDes.IRC = txtIRC.Text;
                _objDes.VAT = txtVAT.Text;
                _objDes.BOND = txtBond.Text;
                _objDes.ERC = txtERC.Text;
                _objDes.BIN = txtBINNO.Text;
                _objDes.BOI = txtBOINo.Text;
                _objDes.IsActive = true;
                string sid = "";



                if (ReportCalling.ExecuteQuery(" update Customer set TINNo='" + txtTIN.Text + "', VATRegNo='" + txtVAT.Text + "', IRCNo='" + txtIRC.Text + "', ERCNo='" + txtERC.Text + "', BondLicenseNo='" + txtBond.Text + "', BIN='" + txtBINNO.Text + "', BOI_NO='" + txtBOINo.Text + "' where CustID=" + _objDes.ID + " ") == true)
                {

                    MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        
    }
}
