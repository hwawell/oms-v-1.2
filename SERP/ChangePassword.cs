﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using DataLayer;
namespace OMS
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void btnSavePassword_Click(object sender, EventArgs e)
        {
            if (UserSession.CurrentPassword == txtOld.Text.Trim())
            {

                if (txtNew.Text.Trim() == txtConfirm.Text.Trim())
                {
                    bool res = new Common_DL().ChangePassword(UserSession.CurrentUser, txtConfirm.Text.Trim());
                    MessageBox.Show("Password Changed.");
                    txtConfirm.Text = string.Empty;
                    txtNew.Text = string.Empty;
                    txtOld.Text = string.Empty;
                }
                else
                {
                    MessageBox.Show("Password dont match,Please check both the password are same");
                    txtConfirm.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please Give the current Password Correctly");
                txtOld.Focus();
            }
        }
    }
}
