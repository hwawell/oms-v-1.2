﻿namespace OMS
{
    partial class CompanySetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.txtOffAdd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOfficeCont = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFacAdd = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtInvoiceInitial = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtLienBank = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtFacCont = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(103, 42);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(266, 20);
            this.textBox1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(375, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(132, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Browse Logo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(294, 290);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Upload Logo";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Comapany";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(103, 84);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(266, 20);
            this.txtCompany.TabIndex = 5;
            // 
            // txtOffAdd
            // 
            this.txtOffAdd.Location = new System.Drawing.Point(103, 110);
            this.txtOffAdd.Name = "txtOffAdd";
            this.txtOffAdd.Size = new System.Drawing.Size(266, 20);
            this.txtOffAdd.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Office Add";
            // 
            // txtOfficeCont
            // 
            this.txtOfficeCont.Location = new System.Drawing.Point(104, 162);
            this.txtOfficeCont.Name = "txtOfficeCont";
            this.txtOfficeCont.Size = new System.Drawing.Size(266, 20);
            this.txtOfficeCont.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Office Cont";
            // 
            // txtFacAdd
            // 
            this.txtFacAdd.Location = new System.Drawing.Point(104, 136);
            this.txtFacAdd.Name = "txtFacAdd";
            this.txtFacAdd.Size = new System.Drawing.Size(266, 20);
            this.txtFacAdd.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Factory Add";
            // 
            // txtInvoiceInitial
            // 
            this.txtInvoiceInitial.Location = new System.Drawing.Point(104, 243);
            this.txtInvoiceInitial.Name = "txtInvoiceInitial";
            this.txtInvoiceInitial.Size = new System.Drawing.Size(266, 20);
            this.txtInvoiceInitial.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Invoice Initial";
            // 
            // txtLienBank
            // 
            this.txtLienBank.Location = new System.Drawing.Point(104, 217);
            this.txtLienBank.Name = "txtLienBank";
            this.txtLienBank.Size = new System.Drawing.Size(266, 20);
            this.txtLienBank.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "lien Bank";
            // 
            // txtFacCont
            // 
            this.txtFacCont.Location = new System.Drawing.Point(103, 188);
            this.txtFacCont.Name = "txtFacCont";
            this.txtFacCont.Size = new System.Drawing.Size(266, 20);
            this.txtFacCont.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Factory Contact";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Logo";
            // 
            // CompanySetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 335);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtFacCont);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtInvoiceInitial);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtLienBank);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtOfficeCont);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFacAdd);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtOffAdd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Name = "CompanySetup";
            this.Text = "CompanySetup";
            this.Load += new System.EventHandler(this.CompanySetup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.TextBox txtOffAdd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOfficeCont;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFacAdd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtInvoiceInitial;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtLienBank;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtFacCont;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}