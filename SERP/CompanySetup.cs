﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DataLayer;

namespace OMS
{
    public partial class CompanySetup : Form
    {
        public CompanySetup()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();

            openFileDialog1.Filter = "Image Files(*.jpeg;*.bmp;*.png;*.jpg)|*.jpeg;*.bmp;*.png;*.jpg";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string image = textBox1.Text;
            Bitmap bmp = new Bitmap(image);
            FileStream fs = new FileStream(image, FileMode.Open, FileAccess.Read);
            byte[] bimage = new byte[fs.Length];
            fs.Read(bimage, 0, Convert.ToInt32(fs.Length));
            fs.Close();

            CompanyInfo ob=new CompanyInfo();

           
             ob.CompanyName=txtCompany.Text;
            ob.FactoryAdd= txtFacAdd.Text ;
           ob.FactoryContact=  txtFacCont.Text ;
             ob.OfficeAdd=txtOffAdd.Text ;
             ob.officeContact=txtOfficeCont.Text ;
             ob.LienBank=txtLienBank.Text;
            ob.COM_INVOICE_START= txtInvoiceInitial.Text ;
           
            new Common_DL().SetupCompany(ob, bimage);
        }

        private void CompanySetup_Load(object sender, EventArgs e)
        {
            CompanyInfo ob = new Common_DL().GetCompanyInfo();

            if (ob != null)
            {

                txtCompany.Text = ob.CompanyName;
                txtFacAdd.Text = ob.FactoryAdd;
                txtFacCont.Text = ob.FactoryContact;
                txtOffAdd.Text = ob.OfficeAdd;
                txtOfficeCont.Text = ob.officeContact;
                txtLienBank.Text = ob.LienBank;
                txtInvoiceInitial.Text = ob.COM_INVOICE_START;
                

               
            }
        }
    }
}
