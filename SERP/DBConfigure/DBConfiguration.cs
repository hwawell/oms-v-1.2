﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace OMS
{
    public class DBConfiguration
    {
        private static StreamWriter _streamWriter;

        private static StreamReader _streamReader;
        private static StreamReader STRREDDB;
        public DBConfiguration()
        {
            //this.DBFN = Application.StartupPath + "\\RESOURCE_INFORMATION.txt";
        }
        /// <summary>
        /// For DB Connection String
        /// </summary>
        /// <returns></returns>
        private static string getConnForDBconfig()
        {
            string strPath;      
            strPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\IMSConfig.dll";
            strPath = strPath.Remove(0, 6);
            return strPath;
        }
        public static string getConnPathDBconfig()
        {
            string strPath;
            strPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\";
            strPath = strPath.Remove(0, 6);
            return strPath;
        }

        /// <summary>
        /// For WCF Secutity Info
        /// </summary>
        /// <returns></returns>
        private static string getWCFSecurityconfig()
        {
            string strPath;
            strPath = System.Environment.GetEnvironmentVariable("SystemRoot") + "\\system32\\IMSSecurityConfig.dll";
            
            return strPath;
        }

//      AUTHOR          :   EHSAN
//      DATE            :   SEPTEMBER 9,2009      
//      RETURNS         :   NONE//      
        /// <summary>
        /// SAVES THE CONNECTIN STRING PARAMETERS TO 
        /// A PRELOCATED TEXT FILE IN ENCRYPTE FORMAT
        /// </summary>
        /// <param name="ServerName">HOSTNAME OF THE SERVER</param>
        /// <param name="DbName">DATABASE NAME OF A PARTICULAR HOST</param>
        /// <param name="DbUserId">DATABASE USER ID</param>
        /// <param name="DbPassword">DATABSE PASSWORD</param>

        public static bool  SaveConnectionString(string ServerName, string DbName, string DbUserId, string DbPassword,string Authentication)
        {
            try
            {
                write(getConnForDBconfig(),"Write");
                WriteLine(ServerName);
                WriteLine(DbName);
                WriteLine(DbUserId);
                WriteLine(DbPassword);
                WriteLine(Authentication);
                DBConnectionString = null;
                return true; 
            }
            catch (Exception ex)
            {
               // IMSLog.Debug("DBConfiguration : " + "SaveConnectionString() :: " + ex.Message);
                return false;
            }
            finally
                {
                    _streamWriter.Close();
                }

        }

        /// <summary>
        /// Write WCF security Info
        /// </summary>
        /// <param name="ServerName"></param>
        /// <param name="DbName"></param>
        /// <param name="DbUserId"></param>
        /// <param name="DbPassword"></param>
        /// <returns></returns>
        public static bool SaveWCFSecurityInfo(string UserName, string PassWord)
        {
            try
            {
                write(getWCFSecurityconfig(), "Write");
                WriteLine(ToEncrypt(UserName, "E"));
                WriteLine(ToEncrypt(PassWord, "E"));
                return true;
            }
            catch (Exception ex)
            {
                //IMSLog.Debug("DBConfiguration : " + "SaveWCFSecurityInfo() :: " + ex.Message);
                return false;
            }
            finally
            {
                _streamWriter.Close();
            }

        }


        public static string ToEncrypt(string input, string Type)
        {
            string passPhrase = "SD345@UgISuccess";        // can be any string
            string saltValue = "s@1tUGIFailure";        // can be any string
            string hashAlgorithm = "MD5";             // can be "MD5"
            int passwordIterations = 7;                  // can be any number
            string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
            int keySize = 256;                // can be 192 or 128
            IMSEncrypt obj = new IMSEncrypt();
            string cipherText;
            if (Type == "E")
                cipherText = obj.EncryptString(input, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
            else
                cipherText = obj.Decrypt(input, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);

            return cipherText;
        }
        
        private static void write(string _strFileName, string _strAction)
        {
            try
            {

                if ((_strAction == "Write"))
                {
                    // _gblAction = _strAction;
                    _streamWriter = new StreamWriter(_strFileName);
                }

                if ((_strAction == "Read"))
                {
                    //_gblAction = _strAction;
                    _streamReader = new StreamReader(_strFileName);

                }
            }
            catch (Exception ex)
            {
                //IMSLog.Error ("DBConfiguration : " + "write() :: " + ex.Message);
            }
        }
        private static void WriteLine(string _strLine)
        {
            try
            {
                _streamWriter.WriteLine(_strLine);
            }
            catch (Exception ex)
            {
               // IMSLog.Error("DBConfiguration : " + "WriteLine() :: " + ex.Message);
            }
        }



        /// <summary>
        /// Read DB conneting info
        /// </summary>
        /// <returns></returns>
        public static void ReadConnectionString()
        {

            string _db, _server, _uid, _password, _strConn,_strAuthetication;

            try
            {
               
                //EncryptionDecryptionClass mEncrypt = new EncryptionDecryptionClass();
                STRREDDB = new StreamReader(getConnForDBconfig());
                _server = STRREDDB.ReadLine();
                _db = STRREDDB.ReadLine();
                _uid = STRREDDB.ReadLine();
                _password = STRREDDB.ReadLine();
                _strAuthetication = STRREDDB.ReadLine();
                //_mdlStrDBServerName = mEncrypt.Crypt(STRREDDB.ReadLine);

                STRREDDB.Close();
                _strAuthetication = IMSEncrypt.DecryptText(_strAuthetication);
                if (_strAuthetication.Trim() == "True")
                {
                  _strConn = "Data Source=" + IMSEncrypt.DecryptText(_server) + ";Initial Catalog=" + IMSEncrypt.DecryptText(_db) + ";Trusted_Connection=Yes;Integrated Security=SSPI;";
                  
                }
                else
                {
                    _strConn = "Data Source=" + IMSEncrypt.DecryptText(_server) + ";Initial Catalog=" + IMSEncrypt.DecryptText(_db) + ";Persist Security Info=True;User ID=" + IMSEncrypt.DecryptText(_uid) + ";Password=" + IMSEncrypt.DecryptText(_password);
                }
            }
            catch (Exception ex)
            {

                //IMSLog.Debug("DBConfiguration : " + "ReadConnectionString() :: " + ex.Message);
                _strConn = string.Empty;
            }
            DBConnectionString = _strConn;            
        }


        /// <summary>
        /// Read DB conneting info
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string,string> ReadConnectionStringParts()
        {
            Dictionary<string, string> dbParts = new Dictionary<string, string>();
            string _db, _server, _uid, _password, _strConn, _strAuthetication;

            try
            {

                //EncryptionDecryptionClass mEncrypt = new EncryptionDecryptionClass();
                STRREDDB = new StreamReader(getConnForDBconfig());
                _server = IMSEncrypt.DecryptText(STRREDDB.ReadLine());
                _db = IMSEncrypt.DecryptText(STRREDDB.ReadLine());
                _uid = IMSEncrypt.DecryptText(STRREDDB.ReadLine());
                _password = IMSEncrypt.DecryptText(STRREDDB.ReadLine());
                _strAuthetication = IMSEncrypt.DecryptText(STRREDDB.ReadLine());

                //_mdlStrDBServerName = mEncrypt.Crypt(STRREDDB.ReadLine);

                STRREDDB.Close();
                dbParts.Add("Server", _server);
                dbParts.Add("Database", _db);
                dbParts.Add("UserId", _uid);
                dbParts.Add("Password", _password);
                dbParts.Add("Authentication", _strAuthetication);
                //_strConn = "Data Source=" + IMSEncrypt.DecryptText(_server) + ";Initial Catalog=" + IMSEncrypt.DecryptText(_db) + ";Persist Security Info=True;User ID=" + IMSEncrypt.DecryptText(_uid) + ";Password=" + IMSEncrypt.DecryptText(_password);
            }
            catch (Exception ex)
            {

               // IMSLog.Debug("DBConfiguration : " + "ReadConnectionString() :: " + ex.Message);
                _strConn = string.Empty;
            }
            return dbParts;
        }




        /// <summary>
        /// Read WCF security info
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> ReadWCFSecurityInfo()
        {

            string strUserName, strPassWord;
            Dictionary<string, string> liWCFinfo = new Dictionary<string, string>();

            try
            {
                //EncryptionDecryptionClass mEncrypt = new EncryptionDecryptionClass();
                STRREDDB = new StreamReader(getWCFSecurityconfig());
                strUserName = STRREDDB.ReadLine();
                strPassWord = STRREDDB.ReadLine();

                liWCFinfo.Add("UserName", strUserName);
                liWCFinfo.Add("PassWord", strPassWord);
                              

                STRREDDB.Close();
                
            }
            catch (Exception ex)
            {
               // IMSLog.Debug("DBConfiguration : " + "ReadWCFSecurityInfo() :: " + ex.Message); 
            }
            return liWCFinfo;
        }
        public static string  DBConnectionString { get; set; }

    }

}