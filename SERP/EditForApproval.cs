﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
namespace OMS
{
    public partial class EditForApproval : Form
    {
        List<EOrderApprovalView> liOrder;
        List<EOrderApprovalView> liOrderSelected;
        bool IsNotApprove;
        public EditForApproval()
        {
            InitializeComponent();
        }

        private void EditForApproval_Load(object sender, EventArgs e)
        {
            cboOrderType.DataSource = new Order_DL().GetAllOrderType();
            cboOrderType.DisplayMember = "OrderTypeName";
            cboOrderType.ValueMember = "ID";

            List<string> li = new List<string>();
            int year = System.DateTime.Now.Year;
            li.Add(year.ToString());
            li.Add((year - 1).ToString());
            li.Add((year - 2).ToString());
            li.Add((year - 3).ToString());
            li.Add((year - 4).ToString());
            li.Add((year - 5).ToString());

            cboYear.DataSource = li;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IsNotApprove = false;
            liOrder = new Order_DL().GetAllOrderListForApproval(Convert.ToInt32(cboOrderType.SelectedValue), Convert.ToInt32(cboYear.Text)).FindAll(o => o.IsApproved == true);
            dgvData.DataSource = liOrder;
            lblStatus.Text = "List of Approve order . Total :" + dgvData.Rows.Count.ToString();
        }

        private void btnNotApprove_Click(object sender, EventArgs e)
        {

        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditOrder();
        }

        
        private void EditOrder()
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                DataGridViewRow currentRow = dgvData.SelectedRows[0];
                string PINO = currentRow.Cells["PINO"].Value.ToString();

                if (MessageBox.Show("Do You want to cancel Approval and give permission to Edit Order :" + PINO, "Permission Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {

                    bool res = new Order_DL().EditPermission(PINO);
                    if (res == true)
                    {
                        MessageBox.Show("Permitted to Edit PINO: " + PINO);
                    }
                    else
                    {
                        MessageBox.Show("Permission Failed. ");
                    }
                }

            }
           
            
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            EditOrder();

        }

        private void miClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void viewOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                DataGridViewRow currentRow = dgvData.SelectedRows[0];
                string PINO = currentRow.Cells["PINO"].Value.ToString();

                if (MessageBox.Show("Do You want to cancel Approval and give permission to Edit Order :" + PINO, "Permission Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
             
                    bool res=new Order_DL().EditPermission(PINO);
                      if (res==true)
                      {
                          MessageBox.Show("Permitted to Edit PINO: "+ PINO);
                      }
                      else
                      {
                           MessageBox.Show("Permission Failed. ");
                      }
                }

            }
        }

        private void dgvData_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string strColumnName = dgvData.Columns[e.ColumnIndex].Name;
            SortOrder strSortOrder = getSortOrder(e.ColumnIndex);

            liOrder.Sort(new StudentComparer(strColumnName, strSortOrder));
            dgvData.DataSource = null;
            dgvData.DataSource = liOrder;
            //customizeDataGridView();
            dgvData.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = strSortOrder;
        }
        private SortOrder getSortOrder(int columnIndex)
        {
            if (dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection == SortOrder.None ||
                dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection == SortOrder.Descending)
            {
                dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                return SortOrder.Ascending;
            }
            else
            {
                dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                return SortOrder.Descending;
            }
        }
    }
}
