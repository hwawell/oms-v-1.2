﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using DataLayer;
namespace OMS
{
    public partial class Employee : Form
    {
        public Employee()
        {
            InitializeComponent();
        }
          List<NSEC_USER> _liDes;
          NSEC_USER _objDes;

          public Employee(List<NSEC_USER> lid, NSEC_USER objDe)
        {
            InitializeComponent();
            _liDes = lid;
            _objDes = objDe;
            if (_objDes != null)
            {
                txtCode.Text = objDe.UserCode;
                txtName.Text = _objDes.EmployeeName;
                txtContact.Text = _objDes.Phone;
                txtAddress.Text = _objDes.Address;
                txtEmail.Text = _objDes.Email;
                txtDesignation.Text = _objDes.Designation;
                txtPassword.Text = _objDes.Password;
                

            }
            
        }


        

        

     
        private void Clear()
        {
            txtCode.Text = string.Empty;
            txtName.Text = string.Empty;
            txtContact.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtEmail.Text = string.Empty;
            
        }
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void miSave_Click(object sender, EventArgs e)
        {
            Save();

        }
        private void Save()
        {
            NSEC_USER objDes = new NSEC_USER();
            if (_objDes != null)
            {
                objDes = _liDes.Find(o => o.ID == _objDes.ID);

            }
            objDes.EmployeeName = txtName.Text;
            objDes.Designation = txtDesignation.Text;
            objDes.Address = txtAddress.Text;
            objDes.Phone = txtContact.Text;
            objDes.Email = txtEmail.Text;

            objDes.UserCode = txtCode.Text;
            objDes.Password = txtPassword.Text;



            bool Res = new Common_DL().InsertUpdateEmployee(objDes);
            if (Res == true)
            {

                if (objDes == null)
                {

                    if (_liDes == null)
                    {
                        _liDes = new List<NSEC_USER>();
                    }
                    _liDes.Add(objDes);

                }
                MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Clear();
                txtName.Focus();
            }
            else
            {

                MessageBox.Show("Data not Saved", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtName.Focus();

            }
        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
    }
}
