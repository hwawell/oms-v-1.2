﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OMS
{
    public partial class ForwardingLetter : Form
    {
        public Int32 IDS;
        public ForwardingLetter( Int32 ID)
        {
            InitializeComponent();

            IDS = ID;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] param = { textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, textBox6.Text, textBox7.Text, textBox8.Text, textBox9.Text, textBox10.Text };
            ReportCalling.PrintForwarding(IDS, param);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string[] param = { textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, textBox6.Text, textBox7.Text, textBox8.Text, textBox9.Text, textBox10.Text };
            ReportCalling.PrintForwardingAdd(IDS, param);
        }
    }
}
