﻿namespace OMS
{
    partial class LC_CI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LC_CI));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miSave = new System.Windows.Forms.ToolStripMenuItem();
            this.miClear = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtExportLC = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBankAddress = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBank = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHSCode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtIRC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.dtpCIDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cboDescription = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.txtLCNO = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dtpBanckAccpt = new System.Windows.Forms.DateTimePicker();
            this.dtpLDBCDate = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.txtInvoiceTotal = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpDocRcvDate = new System.Windows.Forms.DateTimePicker();
            this.label22 = new System.Windows.Forms.Label();
            this.dtpDocSubDate = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpBTMADate = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMushok = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTruckNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLDBCNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDriverName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpMushokDate = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.txtitemQty = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cboOrderNo = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miClear,
            this.miExit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(862, 24);
            this.menuStrip1.TabIndex = 61;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miSave
            // 
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.Name = "miSave";
            this.miSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.miSave.Size = new System.Drawing.Size(59, 20);
            this.miSave.Text = "&Save";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // miClear
            // 
            this.miClear.Image = ((System.Drawing.Image)(resources.GetObject("miClear.Image")));
            this.miClear.Name = "miClear";
            this.miClear.Size = new System.Drawing.Size(62, 20);
            this.miClear.Text = "&Clear";
            // 
            // miExit
            // 
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(53, 20);
            this.miExit.Text = "E&xit";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtExportLC);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtBankAddress);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtBank);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtHSCode);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtIRC);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(832, 86);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Commercial Invoice Information";
            // 
            // txtExportLC
            // 
            this.txtExportLC.Location = new System.Drawing.Point(111, 23);
            this.txtExportLC.Name = "txtExportLC";
            this.txtExportLC.Size = new System.Drawing.Size(323, 20);
            this.txtExportLC.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Export LC No & Date";
            // 
            // txtBankAddress
            // 
            this.txtBankAddress.Location = new System.Drawing.Point(393, 52);
            this.txtBankAddress.Name = "txtBankAddress";
            this.txtBankAddress.Size = new System.Drawing.Size(430, 20);
            this.txtBankAddress.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(313, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Bank Addres :";
            // 
            // txtBank
            // 
            this.txtBank.Location = new System.Drawing.Point(111, 49);
            this.txtBank.Name = "txtBank";
            this.txtBank.Size = new System.Drawing.Size(196, 20);
            this.txtBank.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Bank  Name:";
            // 
            // txtHSCode
            // 
            this.txtHSCode.Location = new System.Drawing.Point(701, 19);
            this.txtHSCode.Name = "txtHSCode";
            this.txtHSCode.Size = new System.Drawing.Size(122, 20);
            this.txtHSCode.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(639, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "HS Code :";
            // 
            // txtIRC
            // 
            this.txtIRC.Location = new System.Drawing.Point(486, 23);
            this.txtIRC.Name = "txtIRC";
            this.txtIRC.Size = new System.Drawing.Size(135, 20);
            this.txtIRC.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(440, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "IRC NO :";
            // 
            // txtInvNo
            // 
            this.txtInvNo.Location = new System.Drawing.Point(308, 34);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(135, 20);
            this.txtInvNo.TabIndex = 23;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(237, 37);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 22;
            this.label20.Text = "Invoice No :";
            // 
            // dtpCIDate
            // 
            this.dtpCIDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpCIDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCIDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCIDate.Location = new System.Drawing.Point(124, 34);
            this.dtpCIDate.Name = "dtpCIDate";
            this.dtpCIDate.Size = new System.Drawing.Size(103, 23);
            this.dtpCIDate.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Invoice Date";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(752, 333);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(45, 23);
            this.button1.TabIndex = 18;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cboDescription
            // 
            this.cboDescription.BackColor = System.Drawing.Color.White;
            this.cboDescription.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDescription.FormattingEnabled = true;
            this.cboDescription.Location = new System.Drawing.Point(308, 300);
            this.cboDescription.Name = "cboDescription";
            this.cboDescription.Size = new System.Drawing.Size(536, 24);
            this.cboDescription.TabIndex = 16;
            this.cboDescription.SelectedIndexChanged += new System.EventHandler(this.cboDescription_SelectedIndexChanged);
            this.cboDescription.SelectionChangeCommitted += new System.EventHandler(this.cboDescription_SelectionChangeCommitted);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label10.Location = new System.Drawing.Point(260, 303);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 16);
            this.label10.TabIndex = 63;
            this.label10.Text = "Item";
            // 
            // dgvData
            // 
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvData.BackgroundColor = System.Drawing.Color.SlateGray;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvData.Location = new System.Drawing.Point(12, 362);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Navy;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.Size = new System.Drawing.Size(838, 228);
            this.dgvData.TabIndex = 19;
            // 
            // txtLCNO
            // 
            this.txtLCNO.Location = new System.Drawing.Point(529, 33);
            this.txtLCNO.Name = "txtLCNO";
            this.txtLCNO.ReadOnly = true;
            this.txtLCNO.Size = new System.Drawing.Size(170, 20);
            this.txtLCNO.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(474, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "LC NO :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.dtpBanckAccpt);
            this.groupBox2.Controls.Add(this.dtpLDBCDate);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtInvoiceTotal);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.dtpDocRcvDate);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.dtpDocSubDate);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.dtpBTMADate);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtMushok);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtTruckNo);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtLDBCNo);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtDriverName);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.dtpMushokDate);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(12, 155);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(832, 139);
            this.groupBox2.TabIndex = 61;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Commercial Invoice Additional Information";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(514, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(125, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Bank Acceptance Date :";
            // 
            // dtpBanckAccpt
            // 
            this.dtpBanckAccpt.Checked = false;
            this.dtpBanckAccpt.CustomFormat = "dd-MMM-yyyy";
            this.dtpBanckAccpt.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpBanckAccpt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBanckAccpt.Location = new System.Drawing.Point(651, 66);
            this.dtpBanckAccpt.Name = "dtpBanckAccpt";
            this.dtpBanckAccpt.ShowCheckBox = true;
            this.dtpBanckAccpt.Size = new System.Drawing.Size(119, 23);
            this.dtpBanckAccpt.TabIndex = 13;
            this.dtpBanckAccpt.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // dtpLDBCDate
            // 
            this.dtpLDBCDate.Checked = false;
            this.dtpLDBCDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpLDBCDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLDBCDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLDBCDate.Location = new System.Drawing.Point(368, 68);
            this.dtpLDBCDate.Name = "dtpLDBCDate";
            this.dtpLDBCDate.ShowCheckBox = true;
            this.dtpLDBCDate.Size = new System.Drawing.Size(119, 23);
            this.dtpLDBCDate.TabIndex = 12;
            this.dtpLDBCDate.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(296, 71);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "LDBC Date :";
            // 
            // txtInvoiceTotal
            // 
            this.txtInvoiceTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceTotal.Location = new System.Drawing.Point(651, 100);
            this.txtInvoiceTotal.Name = "txtInvoiceTotal";
            this.txtInvoiceTotal.ReadOnly = true;
            this.txtInvoiceTotal.Size = new System.Drawing.Size(147, 21);
            this.txtInvoiceTotal.TabIndex = 71;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(262, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Doc Receive Date :";
            // 
            // dtpDocRcvDate
            // 
            this.dtpDocRcvDate.Checked = false;
            this.dtpDocRcvDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDocRcvDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDocRcvDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDocRcvDate.Location = new System.Drawing.Point(368, 97);
            this.dtpDocRcvDate.Name = "dtpDocRcvDate";
            this.dtpDocRcvDate.ShowCheckBox = true;
            this.dtpDocRcvDate.Size = new System.Drawing.Size(119, 23);
            this.dtpDocRcvDate.TabIndex = 15;
            this.dtpDocRcvDate.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(534, 103);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(111, 17);
            this.label22.TabIndex = 70;
            this.label22.Text = "Invoice Total :";
            // 
            // dtpDocSubDate
            // 
            this.dtpDocSubDate.Checked = false;
            this.dtpDocSubDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDocSubDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDocSubDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDocSubDate.Location = new System.Drawing.Point(111, 94);
            this.dtpDocSubDate.Name = "dtpDocSubDate";
            this.dtpDocSubDate.ShowCheckBox = true;
            this.dtpDocSubDate.Size = new System.Drawing.Size(119, 23);
            this.dtpDocSubDate.TabIndex = 14;
            this.dtpDocSubDate.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 98);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Doc Submit Date :";
            // 
            // dtpBTMADate
            // 
            this.dtpBTMADate.Checked = false;
            this.dtpBTMADate.CustomFormat = "dd-MMM-yyyy";
            this.dtpBTMADate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpBTMADate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBTMADate.Location = new System.Drawing.Point(651, 15);
            this.dtpBTMADate.Name = "dtpBTMADate";
            this.dtpBTMADate.ShowCheckBox = true;
            this.dtpBTMADate.Size = new System.Drawing.Size(119, 23);
            this.dtpBTMADate.TabIndex = 8;
            this.dtpBTMADate.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(554, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "BTMA Issue Date";
            // 
            // txtMushok
            // 
            this.txtMushok.Location = new System.Drawing.Point(111, 20);
            this.txtMushok.Name = "txtMushok";
            this.txtMushok.Size = new System.Drawing.Size(179, 20);
            this.txtMushok.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(39, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Mushok No :";
            // 
            // txtTruckNo
            // 
            this.txtTruckNo.Location = new System.Drawing.Point(111, 44);
            this.txtTruckNo.Name = "txtTruckNo";
            this.txtTruckNo.Size = new System.Drawing.Size(179, 20);
            this.txtTruckNo.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Truck No";
            // 
            // txtLDBCNo
            // 
            this.txtLDBCNo.Location = new System.Drawing.Point(111, 68);
            this.txtLDBCNo.Name = "txtLDBCNo";
            this.txtLDBCNo.Size = new System.Drawing.Size(179, 20);
            this.txtLDBCNo.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(49, 71);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "LDBC No :";
            // 
            // txtDriverName
            // 
            this.txtDriverName.Location = new System.Drawing.Point(368, 42);
            this.txtDriverName.Name = "txtDriverName";
            this.txtDriverName.Size = new System.Drawing.Size(219, 20);
            this.txtDriverName.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(296, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Driver Name";
            // 
            // dtpMushokDate
            // 
            this.dtpMushokDate.Checked = false;
            this.dtpMushokDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpMushokDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpMushokDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMushokDate.Location = new System.Drawing.Point(369, 15);
            this.dtpMushokDate.Name = "dtpMushokDate";
            this.dtpMushokDate.ShowCheckBox = true;
            this.dtpMushokDate.Size = new System.Drawing.Size(119, 23);
            this.dtpMushokDate.TabIndex = 7;
            this.dtpMushokDate.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(295, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Mushok Date";
            // 
            // txtitemQty
            // 
            this.txtitemQty.Location = new System.Drawing.Point(477, 332);
            this.txtitemQty.Name = "txtitemQty";
            this.txtitemQty.Size = new System.Drawing.Size(66, 20);
            this.txtitemQty.TabIndex = 17;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(442, 332);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 13);
            this.label23.TabIndex = 72;
            this.label23.Text = "Qty :";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(797, 333);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(53, 23);
            this.button2.TabIndex = 20;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(328, 330);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(95, 20);
            this.textBox1.TabIndex = 73;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(269, 335);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 74;
            this.label5.Text = "Available";
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.White;
            this.comboBox1.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(612, 330);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(129, 24);
            this.comboBox1.TabIndex = 75;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(546, 335);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 13);
            this.label21.TabIndex = 76;
            this.label21.Text = "Unit Price :";
            // 
            // cboOrderNo
            // 
            this.cboOrderNo.FormattingEnabled = true;
            this.cboOrderNo.Location = new System.Drawing.Point(84, 298);
            this.cboOrderNo.Name = "cboOrderNo";
            this.cboOrderNo.Size = new System.Drawing.Size(170, 21);
            this.cboOrderNo.TabIndex = 78;
           
            this.cboOrderNo.SelectionChangeCommitted += new System.EventHandler(this.cboOrderNo_SelectionChangeCommitted);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label24.Location = new System.Drawing.Point(15, 301);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 16);
            this.label24.TabIndex = 77;
            this.label24.Text = "Order No";
            // 
            // LC_ComInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 602);
            this.Controls.Add(this.cboOrderNo);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtInvNo);
            this.Controls.Add(this.txtitemQty);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtLCNO);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cboDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpCIDate);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "LC_ComInvoice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LC_ComInvoice";
            this.Load += new System.EventHandler(this.LC_ComInvoice_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miSave;
        private System.Windows.Forms.ToolStripMenuItem miClear;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtBankAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBank;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtHSCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtIRC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpCIDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtExportLC;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cboDescription;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.TextBox txtLCNO;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtTruckNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLDBCNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDriverName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtpMushokDate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtMushok;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dtpBanckAccpt;
        private System.Windows.Forms.DateTimePicker dtpLDBCDate;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpDocRcvDate;
        private System.Windows.Forms.DateTimePicker dtpDocSubDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpBTMADate;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtInvoiceTotal;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtitemQty;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cboOrderNo;
        private System.Windows.Forms.Label label24;
    }
}