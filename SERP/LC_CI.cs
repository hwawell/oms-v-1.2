﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Entities;
namespace OMS
{
    public partial class LC_CI : Form
    {
        List<EOrderProductLC> li;
        List<EOrderProductLC> liSelect;
        public List<EOrderList> liorder;
        LC_COM_INV ci;
        bool isItemIssue = false;
        private void LoadItem()
        {
            if (li != null)
            {
                cboDescription.DataSource = null;
                cboDescription.Refresh();

                EOrderList ob =(EOrderList) cboOrderNo.SelectedItem;
                if (ob != null)
                {
                    List<EOrderProductLC> litemp = li.FindAll(o => o.PINO == ob.PINO);

                    
                    EOrderProductLC obt = new EOrderProductLC();
                    obt.OPID = 0;
                    obt.ItemDesc = "--Select a Product--";
                    litemp.Insert(0, obt);

                    cboDescription.DataSource = litemp;
                    cboDescription.DisplayMember = "ItemDesc";
                    cboDescription.ValueMember = "OPID";
                }
            }
        }

        private void LoadOrder()
        {
            if (liorder != null)
            {
                cboOrderNo.DataSource = null;
                cboOrderNo.Refresh();
                EOrderList obj = new EOrderList();
                obj.PINO = "--Select an Order--";
                liorder.Insert(0, obj);
                cboOrderNo.DataSource = liorder;
                cboOrderNo.DisplayMember = "PINO";
                cboOrderNo.ValueMember = "PINO";
            }
        }
        public LC_CI(string LCNo,string CINO,LC_COM_INV o)
        {
            InitializeComponent();

            txtLCNO.Text = LCNo;
            liorder = new Order_DL().LC_OrderList(LCNo);

           
            //li = new Order_DL().LCGetAllOrderProduct(LCNo);
            //if (liorder != null)
            //{
            //    LoadOrder();
            //}
            ci = o;

            if (ci != null)
            {

                //liSelect = new Order_DL().LC_CI_GetAllOrderProduct(ci.ID);
                //LoadGrid();
                //txtInvNo.Text=ci.CI_Master_INV_No;
                //setDate(dtpMushokDate, ci.CI_Mushok_Date);
                //setDate(dtpBTMADate, ci.CI_BTMA_ISS_Date);
                //setDate(dtpDocSubDate, ci.CI_DOC_SUBMIT_DATE);
                //setDate(dtpDocRcvDate, ci.CI_DOC_RCV_DATE);
               


                //dtpCIDate.Value = ci.CI_Date.Value;
                //txtBank.Text = ci.COM_INV_BANK;
                //txtBankAddress.Text = ci.COM_INV_BRANCH_ADD;
                //txtIRC.Text = ci.COM_INV_IRC_NO;
                //txtHSCode.Text = ci.COM_INV_HS_CODE;
                //txtExportLC.Text = ci.COM_INV_ExportLCNoDate;


                //txtMushok.Text = ci.CI_Mushok_No;
                //txtInvNo.Text = ci.CI_Master_INV_No;
                //txtTruckNo.Text = ci.CI_TruckNo;
                //txtDriverName.Text = ci.CI_DriverName;
                //txtLDBCNo.Text = ci.BA_LDBC_NO;

             
              
            }
        }

        private void LC_ComInvoice_Load(object sender, EventArgs e)
        {
          
        }

        private void miSave_Click(object sender, EventArgs e)
        {
           
            LC_COM_INV obj = new LC_COM_INV();

            if (ci != null)
            {
                obj.ID = ci.ID;
            }
            else
            {
                isItemIssue = true;
            }
            //obj.COM_INV_No = txtInvNo.Text;
            //obj.COM_INV_LCNo = txtLCNO.Text;
            //obj.COM_INV_Date = dtpCIDate.Value;
            //obj.COM_INV_PRICE =Convert.ToDecimal(txtInvoiceTotal.Text);
            //obj.COM_INV_BANK = txtBank.Text;
            //obj.COM_INV_BRANCH_ADD = txtBankAddress.Text;
            //obj.COM_INV_IRC_NO = txtIRC.Text;

            //obj.COM_INV_HS_CODE = txtHSCode.Text;
            //obj.COM_INV_ExportLCNoDate = txtExportLC.Text;
            //obj.Mushok_No = txtMushok.Text;
            //obj.Mushok_Date = UtilityService.GetDate(dtpMushokDate);
            //obj.TruckNo = txtTruckNo.Text;
            //obj.DriverName = txtDriverName.Text;
            //obj.BTMA_ISS_Date = UtilityService.GetDate(dtpBTMADate);
            //obj.DOC_SUBMIT_DATE = UtilityService.GetDate(dtpDocSubDate);
            //obj.DOC_RCV_DATE = UtilityService.GetDate(dtpDocRcvDate);
            //obj.LDBC_NO = txtLDBCNo.Text;
            //obj.LDBC_DATE = UtilityService.GetDate(dtpLDBCDate);
            //obj.BankAcceptanceDate = UtilityService.GetDate(dtpBanckAccpt);

            string res = new Order_DL().SaveLC_CI_Information(obj, liSelect, isItemIssue);
            if (res == "0")
            {
                MessageBox.Show("Data is not saved. Operation Failed");
            }
            else
            {
                MessageBox.Show("Commercial Invoice:"+ res+" saved Successfully");
                this.Close();
            }
        }

        private void setDate(DateTimePicker dt, DateTime? val)
        {

            if (val == null)
            {
                dt.Checked = false;
            }
            else
            {
                dt.Checked = true;
                dt.Value = val.Value;
            }
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            if (liSelect == null)
            {
                liSelect = new List<EOrderProductLC>();
            }
            EOrderProductLC obj = (EOrderProductLC)cboDescription.SelectedItem;
            if (obj != null)
            {
                EOrderProductDetails obPrice=(EOrderProductDetails)comboBox1.SelectedItem;
                if (obPrice != null)
                {

                    if (obj.Qty >= Convert.ToDecimal(txtitemQty.Text))
                    {
                        if (liSelect.Exists(o => o.OPID == obj.OPID && o.UnitPrice.ToString() == obPrice.UnitPrice.ToString()) == true)
                        {
                            liSelect.SingleOrDefault(o => o.OPID == obj.OPID).Qty = liSelect.SingleOrDefault(o => o.OPID == obj.OPID).Qty + Convert.ToDecimal(txtitemQty.Text);
                        }
                        else
                        {

                            EOrderProductLC objC = new EOrderProductLC();
                            objC.OPID = obj.OPID;
                            objC.PINO = obj.PINO;
                            objC.Description = obj.Description;
                            objC.Notes = obj.Notes;
                            objC.Weight = obj.Width;
                            objC.Unit = obj.Unit;
                            objC.ItemDesc = obj.ItemDesc;
                            objC.Width = obj.Width;
                            objC.UnitPrice = Convert.ToDecimal(obPrice.UnitPrice);

                            objC.Qty = Convert.ToDecimal(txtitemQty.Text);

                            liSelect.Add(objC);
                        }
                        li[cboDescription.SelectedIndex].Qty = li[cboDescription.SelectedIndex].Qty - Convert.ToDecimal(txtitemQty.Text);

                        LoadItem();
                        isItemIssue = true;
                    }
                    else
                    {
                        MessageBox.Show("Sorry, Available Qty:" + obj.Qty.ToString());
                    }

                }
                else
                {
                    MessageBox.Show("Please Select a Price" );
                }
               
            }

            LoadGrid();
        }
        private void LoadGrid()
        {
            dgvData.DataSource = null;
            dgvData.Refresh();
            dgvData.DataSource = liSelect;
            dgvData.Columns["OPID"].Visible = false;


            dgvData.Columns["ItemDesc"].Visible = false;
          
            dgvData.Columns["Notes"].Visible = false;

            if (liSelect != null)
            {
                txtInvoiceTotal.Text = liSelect.Sum(o => o.Qty * o.UnitPrice).ToString("0.00");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            isItemIssue = true;

            if (dgvData.SelectedRows.Count > 0)
            {
                EOrderProductLC obj=liSelect[dgvData.SelectedRows[0].Index];
                if (obj != null)
                {
                    liSelect.RemoveAt(dgvData.SelectedRows[0].Index);
                    LoadGrid();
                    li.SingleOrDefault(o => o.OPID == obj.OPID).Qty = li.SingleOrDefault(o => o.OPID == obj.OPID).Qty + obj.Qty;
                    LoadItem();
                }

            }
        }

        private void cboDescription_SelectedIndexChanged(object sender, EventArgs e)
        {
              
        }

        private void cboDescription_SelectionChangeCommitted(object sender, EventArgs e)
        {
            EOrderProductLC obj = (EOrderProductLC)cboDescription.SelectedItem;
            if (obj != null)
            {
                textBox1.Text = obj.Qty.ToString();

                List<EOrderProductDetails> li = new Order_DL().GetAllOrderProductDetails(obj.OPID);
                comboBox1.DataSource = li;
                comboBox1.DisplayMember = "UnitPrice";
                comboBox1.ValueMember = "OPDID";

            }
        }

        private void cboOrderNo_SelectionChangeCommitted(object sender, EventArgs e)
        {


            if (cboOrderNo.SelectedIndex >= 0)
            {
                
                if (li != null)
                {
                    LoadItem();
                }
            }
        }

      
    }
}
