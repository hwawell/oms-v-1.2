﻿namespace OMS
{
    partial class LC_CI_ALL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.buyerAcceptanceInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankAcceptanceInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acceptanceRealizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateLDBCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.txtLCNO = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtlcvalue = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMushokDate = new System.Windows.Forms.TextBox();
            this.txtDocRcv = new System.Windows.Forms.TextBox();
            this.txtInvoiceDate = new System.Windows.Forms.TextBox();
            this.txtDocSub = new System.Windows.Forms.TextBox();
            this.txtBTMAIssDate = new System.Windows.Forms.TextBox();
            this.txtInvoiceTotal = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMushok = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTruckNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDriverName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDocReal = new System.Windows.Forms.TextBox();
            this.txtSFall = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDocValueUSD = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtConRate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDocValueTK = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtDocAccpDate = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtDOCMatDate = new System.Windows.Forms.TextBox();
            this.txtLDBCDate = new System.Windows.Forms.TextBox();
            this.txtDocAccpUSD = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBAConRate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDOCAccpBDT = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtLDBCNo = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button14 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.txtsearch = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.cboMaster = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.cboCI = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cboFile = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtBuyer = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtLCDate = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buyerAcceptanceInformationToolStripMenuItem,
            this.bankAcceptanceInformationToolStripMenuItem,
            this.acceptanceRealizationToolStripMenuItem,
            this.updateLDBCToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(959, 24);
            this.menuStrip1.TabIndex = 61;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // buyerAcceptanceInformationToolStripMenuItem
            // 
            this.buyerAcceptanceInformationToolStripMenuItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buyerAcceptanceInformationToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.buyerAcceptanceInformationToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.buyerAcceptanceInformationToolStripMenuItem.Name = "buyerAcceptanceInformationToolStripMenuItem";
            this.buyerAcceptanceInformationToolStripMenuItem.Size = new System.Drawing.Size(231, 20);
            this.buyerAcceptanceInformationToolStripMenuItem.Text = "New/Edit Commercial Invoice Basic Info";
            this.buyerAcceptanceInformationToolStripMenuItem.Click += new System.EventHandler(this.buyerAcceptanceInformationToolStripMenuItem_Click);
            // 
            // bankAcceptanceInformationToolStripMenuItem
            // 
            this.bankAcceptanceInformationToolStripMenuItem.BackColor = System.Drawing.Color.Gainsboro;
            this.bankAcceptanceInformationToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bankAcceptanceInformationToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.bankAcceptanceInformationToolStripMenuItem.Name = "bankAcceptanceInformationToolStripMenuItem";
            this.bankAcceptanceInformationToolStripMenuItem.Size = new System.Drawing.Size(176, 20);
            this.bankAcceptanceInformationToolStripMenuItem.Text = "Bank Acceptance Information";
            this.bankAcceptanceInformationToolStripMenuItem.Click += new System.EventHandler(this.bankAcceptanceInformationToolStripMenuItem_Click);
            // 
            // acceptanceRealizationToolStripMenuItem
            // 
            this.acceptanceRealizationToolStripMenuItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.acceptanceRealizationToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.acceptanceRealizationToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.acceptanceRealizationToolStripMenuItem.Name = "acceptanceRealizationToolStripMenuItem";
            this.acceptanceRealizationToolStripMenuItem.Size = new System.Drawing.Size(141, 20);
            this.acceptanceRealizationToolStripMenuItem.Text = "Acceptance Realization";
            this.acceptanceRealizationToolStripMenuItem.Click += new System.EventHandler(this.acceptanceRealizationToolStripMenuItem_Click);
            // 
            // updateLDBCToolStripMenuItem
            // 
            this.updateLDBCToolStripMenuItem.Name = "updateLDBCToolStripMenuItem";
            this.updateLDBCToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.updateLDBCToolStripMenuItem.Text = "Update LDBC";
            this.updateLDBCToolStripMenuItem.Click += new System.EventHandler(this.updateLDBCToolStripMenuItem_Click);
            // 
            // txtInvNo
            // 
            this.txtInvNo.Location = new System.Drawing.Point(484, 75);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(122, 20);
            this.txtInvNo.TabIndex = 23;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(416, 79);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 22;
            this.label20.Text = "Invoice No :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(251, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Invoice Date";
            // 
            // dgvData
            // 
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvData.BackgroundColor = System.Drawing.Color.SlateGray;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.Location = new System.Drawing.Point(14, 355);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Navy;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.Size = new System.Drawing.Size(616, 194);
            this.dgvData.TabIndex = 19;
            // 
            // txtLCNO
            // 
            this.txtLCNO.Location = new System.Drawing.Point(124, 19);
            this.txtLCNO.Name = "txtLCNO";
            this.txtLCNO.ReadOnly = true;
            this.txtLCNO.Size = new System.Drawing.Size(112, 20);
            this.txtLCNO.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(73, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "LC NO :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtLCDate);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.txtNotes);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.txtBuyer);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.txtlcvalue);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.txtMushokDate);
            this.groupBox2.Controls.Add(this.txtDocRcv);
            this.groupBox2.Controls.Add(this.txtInvoiceDate);
            this.groupBox2.Controls.Add(this.txtDocSub);
            this.groupBox2.Controls.Add(this.txtBTMAIssDate);
            this.groupBox2.Controls.Add(this.txtInvoiceTotal);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtInvNo);
            this.groupBox2.Controls.Add(this.txtLCNO);
            this.groupBox2.Controls.Add(this.txtMushok);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtTruckNo);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtDriverName);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(12, 94);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(618, 238);
            this.groupBox2.TabIndex = 61;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Buyer Acceptance Information";
            // 
            // txtlcvalue
            // 
            this.txtlcvalue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlcvalue.Location = new System.Drawing.Point(124, 210);
            this.txtlcvalue.Name = "txtlcvalue";
            this.txtlcvalue.ReadOnly = true;
            this.txtlcvalue.Size = new System.Drawing.Size(147, 21);
            this.txtlcvalue.TabIndex = 79;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(32, 213);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(86, 17);
            this.label26.TabIndex = 78;
            this.label26.Text = "LC value : ";
            // 
            // txtMushokDate
            // 
            this.txtMushokDate.Location = new System.Drawing.Point(325, 109);
            this.txtMushokDate.Name = "txtMushokDate";
            this.txtMushokDate.ReadOnly = true;
            this.txtMushokDate.Size = new System.Drawing.Size(87, 20);
            this.txtMushokDate.TabIndex = 77;
            // 
            // txtDocRcv
            // 
            this.txtDocRcv.Location = new System.Drawing.Point(325, 178);
            this.txtDocRcv.Name = "txtDocRcv";
            this.txtDocRcv.ReadOnly = true;
            this.txtDocRcv.Size = new System.Drawing.Size(87, 20);
            this.txtDocRcv.TabIndex = 76;
            // 
            // txtInvoiceDate
            // 
            this.txtInvoiceDate.Location = new System.Drawing.Point(325, 75);
            this.txtInvoiceDate.Name = "txtInvoiceDate";
            this.txtInvoiceDate.ReadOnly = true;
            this.txtInvoiceDate.Size = new System.Drawing.Size(86, 20);
            this.txtInvoiceDate.TabIndex = 75;
            // 
            // txtDocSub
            // 
            this.txtDocSub.Location = new System.Drawing.Point(124, 174);
            this.txtDocSub.Name = "txtDocSub";
            this.txtDocSub.ReadOnly = true;
            this.txtDocSub.Size = new System.Drawing.Size(87, 20);
            this.txtDocSub.TabIndex = 74;
            // 
            // txtBTMAIssDate
            // 
            this.txtBTMAIssDate.Location = new System.Drawing.Point(518, 105);
            this.txtBTMAIssDate.Name = "txtBTMAIssDate";
            this.txtBTMAIssDate.ReadOnly = true;
            this.txtBTMAIssDate.Size = new System.Drawing.Size(88, 20);
            this.txtBTMAIssDate.TabIndex = 73;
            // 
            // txtInvoiceTotal
            // 
            this.txtInvoiceTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceTotal.Location = new System.Drawing.Point(459, 207);
            this.txtInvoiceTotal.Name = "txtInvoiceTotal";
            this.txtInvoiceTotal.ReadOnly = true;
            this.txtInvoiceTotal.Size = new System.Drawing.Size(147, 21);
            this.txtInvoiceTotal.TabIndex = 71;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(217, 181);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Doc Receive Date :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(333, 210);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(111, 17);
            this.label22.TabIndex = 70;
            this.label22.Text = "Invoice Total :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(30, 178);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Doc Submit Date :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(418, 109);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "BTMA Issue Date";
            // 
            // txtMushok
            // 
            this.txtMushok.Location = new System.Drawing.Point(124, 112);
            this.txtMushok.Name = "txtMushok";
            this.txtMushok.ReadOnly = true;
            this.txtMushok.Size = new System.Drawing.Size(112, 20);
            this.txtMushok.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(50, 115);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Mushok No :";
            // 
            // txtTruckNo
            // 
            this.txtTruckNo.Location = new System.Drawing.Point(124, 147);
            this.txtTruckNo.Name = "txtTruckNo";
            this.txtTruckNo.ReadOnly = true;
            this.txtTruckNo.Size = new System.Drawing.Size(112, 20);
            this.txtTruckNo.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Truck No";
            // 
            // txtDriverName
            // 
            this.txtDriverName.Location = new System.Drawing.Point(325, 145);
            this.txtDriverName.Name = "txtDriverName";
            this.txtDriverName.ReadOnly = true;
            this.txtDriverName.Size = new System.Drawing.Size(281, 20);
            this.txtDriverName.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(242, 150);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Driver Name";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(248, 112);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Mushok Date";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDocReal);
            this.groupBox1.Controls.Add(this.txtSFall);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtDocValueUSD);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtConRate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtDocValueTK);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Location = new System.Drawing.Point(640, 332);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(309, 193);
            this.groupBox1.TabIndex = 65;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Notes:";
            // 
            // txtDocReal
            // 
            this.txtDocReal.Location = new System.Drawing.Point(157, 42);
            this.txtDocReal.Name = "txtDocReal";
            this.txtDocReal.ReadOnly = true;
            this.txtDocReal.Size = new System.Drawing.Size(119, 20);
            this.txtDocReal.TabIndex = 78;
            // 
            // txtSFall
            // 
            this.txtSFall.Location = new System.Drawing.Point(157, 150);
            this.txtSFall.Name = "txtSFall";
            this.txtSFall.ReadOnly = true;
            this.txtSFall.Size = new System.Drawing.Size(119, 20);
            this.txtSFall.TabIndex = 40;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(80, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Short Fall ($) :";
            // 
            // txtDocValueUSD
            // 
            this.txtDocValueUSD.Location = new System.Drawing.Point(157, 72);
            this.txtDocValueUSD.Name = "txtDocValueUSD";
            this.txtDocValueUSD.ReadOnly = true;
            this.txtDocValueUSD.Size = new System.Drawing.Size(119, 20);
            this.txtDocValueUSD.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "DOC Realized Value (BDT) :";
            // 
            // txtConRate
            // 
            this.txtConRate.Location = new System.Drawing.Point(157, 98);
            this.txtConRate.Name = "txtConRate";
            this.txtConRate.ReadOnly = true;
            this.txtConRate.Size = new System.Drawing.Size(73, 20);
            this.txtConRate.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Convertion Rate (BDT) :";
            // 
            // txtDocValueTK
            // 
            this.txtDocValueTK.Location = new System.Drawing.Point(157, 124);
            this.txtDocValueTK.Name = "txtDocValueTK";
            this.txtDocValueTK.ReadOnly = true;
            this.txtDocValueTK.Size = new System.Drawing.Size(119, 20);
            this.txtDocValueTK.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "DOC Realized Value ($) :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(38, 45);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(114, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "Doc Realization Date :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtDocAccpDate);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.txtDOCMatDate);
            this.groupBox3.Controls.Add(this.txtLDBCDate);
            this.groupBox3.Controls.Add(this.txtDocAccpUSD);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtBAConRate);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtDOCAccpBDT);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtLDBCNo);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Location = new System.Drawing.Point(640, 96);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(309, 232);
            this.groupBox3.TabIndex = 64;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Bank Acceptance Information";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // txtDocAccpDate
            // 
            this.txtDocAccpDate.Location = new System.Drawing.Point(176, 83);
            this.txtDocAccpDate.Name = "txtDocAccpDate";
            this.txtDocAccpDate.ReadOnly = true;
            this.txtDocAccpDate.Size = new System.Drawing.Size(119, 20);
            this.txtDocAccpDate.TabIndex = 81;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(11, 86);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(156, 13);
            this.label27.TabIndex = 80;
            this.label27.Text = "Bank Acceptance Issue  Date :";
            // 
            // txtDOCMatDate
            // 
            this.txtDOCMatDate.Location = new System.Drawing.Point(176, 108);
            this.txtDOCMatDate.Name = "txtDOCMatDate";
            this.txtDOCMatDate.ReadOnly = true;
            this.txtDOCMatDate.Size = new System.Drawing.Size(119, 20);
            this.txtDOCMatDate.TabIndex = 79;
            // 
            // txtLDBCDate
            // 
            this.txtLDBCDate.Location = new System.Drawing.Point(176, 58);
            this.txtLDBCDate.Name = "txtLDBCDate";
            this.txtLDBCDate.ReadOnly = true;
            this.txtLDBCDate.Size = new System.Drawing.Size(119, 20);
            this.txtLDBCDate.TabIndex = 78;
            // 
            // txtDocAccpUSD
            // 
            this.txtDocAccpUSD.Location = new System.Drawing.Point(176, 136);
            this.txtDocAccpUSD.Name = "txtDocAccpUSD";
            this.txtDocAccpUSD.ReadOnly = true;
            this.txtDocAccpUSD.Size = new System.Drawing.Size(119, 20);
            this.txtDocAccpUSD.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 191);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(158, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "DOC Acceptance Value (BDT) :";
            // 
            // txtBAConRate
            // 
            this.txtBAConRate.Location = new System.Drawing.Point(176, 162);
            this.txtBAConRate.Name = "txtBAConRate";
            this.txtBAConRate.ReadOnly = true;
            this.txtBAConRate.Size = new System.Drawing.Size(73, 20);
            this.txtBAConRate.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(51, 165);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 13);
            this.label9.TabIndex = 37;
            this.label9.Text = "Convertion Rate (BDT) :";
            // 
            // txtDOCAccpBDT
            // 
            this.txtDOCAccpBDT.Location = new System.Drawing.Point(176, 188);
            this.txtDOCAccpBDT.Name = "txtDOCAccpBDT";
            this.txtDOCAccpBDT.ReadOnly = true;
            this.txtDOCAccpBDT.Size = new System.Drawing.Size(119, 20);
            this.txtDOCAccpBDT.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(30, 139);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(142, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "DOC Acceptance Value ($) :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(71, 111);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "DOC Maturity Date :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(100, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "LDBC  Date :";
            // 
            // txtLDBCNo
            // 
            this.txtLDBCNo.Location = new System.Drawing.Point(176, 30);
            this.txtLDBCNo.Name = "txtLDBCNo";
            this.txtLDBCNo.ReadOnly = true;
            this.txtLDBCNo.Size = new System.Drawing.Size(119, 20);
            this.txtLDBCNo.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(114, 33);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 14;
            this.label21.Text = "LDBC No :";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(604, 555);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(87, 23);
            this.button8.TabIndex = 86;
            this.button8.Text = "Cert. of Origin";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(697, 555);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(80, 23);
            this.button7.TabIndex = 85;
            this.button7.Text = "Prod. Certi.";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(513, 555);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(85, 23);
            this.button6.TabIndex = 84;
            this.button6.Text = "Truck Receipt";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(410, 555);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(97, 23);
            this.button5.TabIndex = 83;
            this.button5.Text = "Delivery Challan";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(229, 555);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(91, 23);
            this.button4.TabIndex = 82;
            this.button4.Text = "Packing List";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 555);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(102, 23);
            this.button3.TabIndex = 81;
            this.button3.Text = "Bill of Exchange";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(120, 555);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 23);
            this.button2.TabIndex = 80;
            this.button2.Text = "Comm. Invoice";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox4.Controls.Add(this.button14);
            this.groupBox4.Controls.Add(this.button11);
            this.groupBox4.Controls.Add(this.txtsearch);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.button10);
            this.groupBox4.Controls.Add(this.cboMaster);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.cboCI);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.cboFile);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox4.Location = new System.Drawing.Point(12, 29);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(935, 61);
            this.groupBox4.TabIndex = 87;
            this.groupBox4.TabStop = false;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(820, 40);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(104, 21);
            this.button14.TabIndex = 93;
            this.button14.Text = "Delete Invoice";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(278, 12);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 92;
            this.button11.Text = "Search";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // txtsearch
            // 
            this.txtsearch.Location = new System.Drawing.Point(152, 14);
            this.txtsearch.Name = "txtsearch";
            this.txtsearch.Size = new System.Drawing.Size(120, 20);
            this.txtsearch.TabIndex = 91;
            this.txtsearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsearch_KeyDown);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(139, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 13);
            this.label23.TabIndex = 90;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(725, 40);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(91, 21);
            this.button10.TabIndex = 89;
            this.button10.Text = "Refresh";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // cboMaster
            // 
            this.cboMaster.BackColor = System.Drawing.Color.White;
            this.cboMaster.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMaster.FormattingEnabled = true;
            this.cboMaster.Location = new System.Drawing.Point(725, 11);
            this.cboMaster.Name = "cboMaster";
            this.cboMaster.Size = new System.Drawing.Size(198, 24);
            this.cboMaster.TabIndex = 88;
            this.cboMaster.SelectedIndexChanged += new System.EventHandler(this.cboMaster_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(634, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 13);
            this.label28.TabIndex = 87;
            this.label28.Text = "Sub Com. Invoice";
            // 
            // cboCI
            // 
            this.cboCI.BackColor = System.Drawing.Color.White;
            this.cboCI.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCI.FormattingEnabled = true;
            this.cboCI.Location = new System.Drawing.Point(464, 11);
            this.cboCI.Name = "cboCI";
            this.cboCI.Size = new System.Drawing.Size(167, 24);
            this.cboCI.TabIndex = 84;
            this.cboCI.SelectedIndexChanged += new System.EventHandler(this.cboCI_SelectedIndexChanged);
            this.cboCI.SelectionChangeCommitted += new System.EventHandler(this.cboCI_SelectionChangeCommitted);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(359, 17);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(99, 13);
            this.label24.TabIndex = 83;
            this.label24.Text = "Commercial Invoice";
            // 
            // cboFile
            // 
            this.cboFile.BackColor = System.Drawing.Color.White;
            this.cboFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFile.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFile.FormattingEnabled = true;
            this.cboFile.Items.AddRange(new object[] {
            "FILE NO",
            "LC NO"});
            this.cboFile.Location = new System.Drawing.Point(78, 10);
            this.cboFile.Name = "cboFile";
            this.cboFile.Size = new System.Drawing.Size(67, 24);
            this.cboFile.TabIndex = 82;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(13, 15);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(61, 13);
            this.label30.TabIndex = 81;
            this.label30.Text = "Search by :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(16, 335);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(141, 17);
            this.label25.TabIndex = 72;
            this.label25.Text = "Order Information ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(767, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 23);
            this.button1.TabIndex = 88;
            this.button1.Text = "New Sub Invoice";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(866, 0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(93, 23);
            this.button9.TabIndex = 89;
            this.button9.Text = "Edit Sub Invoice";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(783, 555);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(80, 23);
            this.button12.TabIndex = 90;
            this.button12.Text = "Ben. Certi.";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(869, 555);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(80, 23);
            this.button13.TabIndex = 91;
            this.button13.Text = "For. Letter";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(327, 555);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(71, 17);
            this.checkBox1.TabIndex = 92;
            this.checkBox1.Text = "With Msg";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // txtBuyer
            // 
            this.txtBuyer.Location = new System.Drawing.Point(325, 19);
            this.txtBuyer.Name = "txtBuyer";
            this.txtBuyer.ReadOnly = true;
            this.txtBuyer.Size = new System.Drawing.Size(287, 20);
            this.txtBuyer.TabIndex = 81;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(282, 19);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(37, 13);
            this.label29.TabIndex = 80;
            this.label29.Text = "Buyer:";
            // 
            // txtNotes
            // 
            this.txtNotes.Location = new System.Drawing.Point(123, 47);
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.ReadOnly = true;
            this.txtNotes.Size = new System.Drawing.Size(489, 20);
            this.txtNotes.TabIndex = 83;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(80, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 13);
            this.label31.TabIndex = 82;
            this.label31.Text = "Notes :";
            // 
            // txtLCDate
            // 
            this.txtLCDate.Location = new System.Drawing.Point(124, 75);
            this.txtLCDate.Name = "txtLCDate";
            this.txtLCDate.ReadOnly = true;
            this.txtLCDate.Size = new System.Drawing.Size(86, 20);
            this.txtLCDate.TabIndex = 85;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(73, 79);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(46, 13);
            this.label32.TabIndex = 84;
            this.label32.Text = "LC Date";
            // 
            // LC_CI_ALL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 588);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LC_CI_ALL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Commercial Information";
            this.Activated += new System.EventHandler(this.LC_CI_ALL_Activated);
            this.Load += new System.EventHandler(this.LC_ComInvoice_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.TextBox txtLCNO;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtTruckNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDriverName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtMushok;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtInvoiceTotal;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSFall;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDocValueUSD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtConRate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDocValueTK;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ToolStripMenuItem buyerAcceptanceInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankAcceptanceInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acceptanceRealizationToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtDocAccpUSD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBAConRate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDOCAccpBDT;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtLDBCNo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cboCI;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cboFile;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtMushokDate;
        private System.Windows.Forms.TextBox txtDocRcv;
        private System.Windows.Forms.TextBox txtInvoiceDate;
        private System.Windows.Forms.TextBox txtDocSub;
        private System.Windows.Forms.TextBox txtBTMAIssDate;
        private System.Windows.Forms.TextBox txtDocReal;
        private System.Windows.Forms.TextBox txtDOCMatDate;
        private System.Windows.Forms.TextBox txtLDBCDate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtDocAccpDate;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cboMaster;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox txtsearch;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtlcvalue;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.ToolStripMenuItem updateLDBCToolStripMenuItem;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtBuyer;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtLCDate;
        private System.Windows.Forms.Label label32;
    }
}