﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Entities;
namespace OMS
{
    public partial class LC_CI_ALL : Form
    {
        List<EOrderProductLC> li;
        List<EOrderProductLC> liSelect;
        public List<EOrderList> liorder;
        LC_COM_INV ci;

        ELC_COM_INV cio;

        List<LC_COM_INV> liCi;

        List<ELCProduct> lipro;

        ELCNoFileNo selectedlc;
        public LC_CI_ALL()
        {
            InitializeComponent();

            //txtLCNO.Text = LCNo;
            //liorder = new Order_DL().LC_OrderList(LCNo);

           
            //li = new Order_DL().LCGetAllOrderProduct(LCNo);
          
            //ci = o;

            //if (ci != null)
            //{

         


            //    dtpCIDate.Value = ci.COM_INV_Date.Value;
              
            //    txtMushok.Text = ci.Mushok_No;
            //    txtInvNo.Text = ci.COM_INV_No;
            //    txtTruckNo.Text = ci.TruckNo;
            //    txtDriverName.Text = ci.DriverName;
            //    txtLDBCNo.Text = ci.LDBC_NO;

             
              
            //}
        }

        private void LC_ComInvoice_Load(object sender, EventArgs e)
        {

            LoadInitial();
        }

        private void LoadInitial()
        {

           // List<ELCNoFileNo> li = new Common_DL().GetActiveLC();
            cboFile.SelectedIndex = 0;
           
        }

  
        private void buyerAcceptanceInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ELCNoFileNo ob1 = selectedlc;
            if (ob1 != null)
            {
                Int32 ID = 0;
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
                if (o != null)
                {
                    ID = o.ID;
                }
                string LCNo = selectedlc.LCNo;

                LC_CI_Info ob = new LC_CI_Info(selectedlc, "", ci, ob1.LCID, ID);
                ob.ShowDialog();
            }
        }

        private void bankAcceptanceInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cio != null)
            {
               
                LC_CI_BA ob = new LC_CI_BA(txtLCNO.Text,cio,selectedlc );
                ob.ShowDialog();
            }
        }

        private void acceptanceRealizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cio != null)
            {
                LC_CI_BR ob = new LC_CI_BR(txtLCNO.Text, cio);
                ob.ShowDialog();
            }
          
        }
        private void deleteCommInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
               

                if (ci != null)
                {
                    if (MessageBox.Show("Do You want to Delete Commercial Invoice: " + ci.CI_Master_INV_No, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        string mess = new Order_DL().DeleteLCorCI("", ci.ID);
                        MessageBox.Show(mess);
                    }
                }

            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
               
                Int32 ID=Convert.ToInt32( cboMaster.SelectedValue.ToString());
                if (ci != null)
                {
                    ReportCalling.PrintCI(ID);
                }

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
                if (o != null)
                {
                    ReportCalling.PrintBE(o.ID);
                }

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
                if (o != null)
                {
                    ReportCalling.PrintPL(o.ID);
                }

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
                if (o != null)
                {
                    ReportCalling.PrintDC(o.ID,checkBox1.Checked);
                }

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
                if (o != null)
                {
                    ReportCalling.PrintTR(o.ID);
                }

            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
                if (o != null)
                {
                    ReportCalling.PrintPC(o.ID);
                }

            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
                if (o != null)
                {
                    ReportCalling.PrintCO(o.ID);
                }

            }
        }

        private void cboLCList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            LoadCI_List();
        }
       

        private void cboFile_SelectionChangeCommitted(object sender, EventArgs e)
        {
            LoadCI_List();
        }

        private void LoadCI_List()
        {
            ELCNoFileNo ob1 = selectedlc;
            if (ob1 != null)
            {
                cboCI.DataSource = null;
                cboCI.Refresh();
                liCi = new LC_DL().LC_CI_GET(ob1.LCNo, "", 0);

                LC_COM_INV ob = new LC_COM_INV();
                ob.ID = 0;
                ob.CI_Master_INV_No = "--Select a Com. Invoice--";
             
                liCi.Insert(0, ob);
                cboCI.DataSource = liCi;
                cboCI.DisplayMember = "CI_Master_INV_No";
                cboCI.ValueMember = "ID";

                //txtBuyer.Text=ob1.cu
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void cboMaster_SelectionChangeCommitted(object sender, EventArgs e)
        {
            LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
            if (o != null)
            {
                dgvData.DataSource = lipro.FindAll(p => p.MasterID == o.ID);
                dgvData.Columns[0].Visible = false;
                dgvData.Columns[1].Visible = false;
               
                txtInvoiceTotal.Text = lipro.FindAll(p => p.MasterID == o.ID).Sum(i => Convert.ToDouble(i.NetTotal)).ToString("0.00");
                txtlcvalue.Text = selectedlc.TotalLC.ToString();
            }
        }

        private void cboCI_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboCI.SelectedIndex > 0)
            {
                ci = (LC_COM_INV)cboCI.SelectedItem;

             
                if (ci != null)
                {
                    ci = new LC_DL().LC_CI_GET("", "", ci.ID)[0];
                    txtLCNO.Text = selectedlc.LCNo;
                    txtInvNo.Text = ci.CI_Master_INV_No;
                    txtBuyer.Text = selectedlc.Customer;
                    txtNotes.Text = selectedlc.Notes;
                    txtLCDate.Text = selectedlc.LcDate;
                    txtlcvalue.Text = selectedlc.TotalLC.ToString();
                    if (ci.CI_Date != null) txtInvoiceDate.Text = ci.CI_Date.Value.ToString("dd-MMM-yy");
                    txtMushok.Text = ci.CI_Mushok_No;
                    if (ci.CI_Mushok_Date != null) txtMushokDate.Text = ci.CI_Mushok_Date.Value.ToString("dd-MMM-yy");
                    if (ci.CI_BTMA_ISS_Date != null) txtBTMAIssDate.Text = ci.CI_BTMA_ISS_Date.Value.ToString("dd-MMM-yy");
                    txtTruckNo.Text = ci.CI_TruckNo;
                    txtDriverName.Text = ci.CI_DriverName;
                    if (ci.CI_DOC_RCV_DATE != null) txtDocRcv.Text = ci.CI_DOC_RCV_DATE.Value.ToString("dd-MMM-yy");
                    if (ci.CI_DOC_SUBMIT_DATE != null) txtDocSub.Text = ci.CI_DOC_SUBMIT_DATE.Value.ToString("dd-MMM-yy");

                    
                    LC_DL ob = new LC_DL();
                    lipro = ob.CI_PRODUCT_LIST_GET(ci.ID, 0);


                    cboMaster.DataSource = ob.CI_PRODUCT_MASTER_GET(ci.ID);
                    cboMaster.DisplayMember = "CI_INV_NO_SUB";
                    cboMaster.ValueMember = "ID";

                   
                }
            }
        }

        private void cboLCList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (cboMaster.SelectedIndex == 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;

                LC_CI_SUB ob = new LC_CI_SUB(o, true);
                ob.ShowDialog();
            }
            else
            {
                cboMaster.Focus();
                MessageBox.Show("Select Master Invoice No first");

            }
        }


        private void Load_LDBC_REALIZATION_Product()
        {
            LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;

            if (o != null)
            {
                dgvData.DataSource = lipro.FindAll(p => p.MasterID == o.ID && Convert.ToDouble(p.Qty) > 0);
                dgvData.Columns[0].Visible = false;
                dgvData.Columns[1].Visible = false;
               
                txtInvoiceTotal.Text = lipro.FindAll(p => p.MasterID == o.ID).Sum(i => Convert.ToDouble(i.NetTotal)).ToString("0.00");
            }
            cio = new LC_DL().LC_CI_GetOthers(o.ID);

            if (cio != null)
            {
                txtLDBCNo.Text = cio.BA_LDBC_NO;
                cio.BA_DocAcceptAmountUSD = Convert.ToDecimal(txtInvoiceTotal.Text);
                if (cio.BA_LDBC_DATE != null) txtLDBCDate.Text = cio.BA_LDBC_DATE.Value.ToString("dd-MMM-yyyy");
                if (cio.BA_DocAcceptAmountUSD != null) txtDocAccpUSD.Text = cio.BA_DocAcceptAmountUSD.Value.ToString("0.00");
                if (cio.BA_ConvertionRateBDT != null) txtBAConRate.Text = cio.BA_ConvertionRateBDT.Value.ToString("0.00");
                if (cio.BA_ConvertionRateBDT != null) txtDOCAccpBDT.Text = (cio.BA_DocAcceptAmountUSD * cio.BA_ConvertionRateBDT).Value.ToString("0.00");
                if (cio.BA_Date != null) txtDocAccpDate.Text = cio.BA_Date.Value.ToString("dd-MMM-yyyy");
                if (cio.BA_DOC_MaturityDate != null) txtDOCMatDate.Text = cio.BA_DOC_MaturityDate.Value.ToString("dd-MMM-yyyy");
                if (cio.BA_DOC_MaturityDate != null)
                {
                    txtDOCMatDate.Text = cio.BA_DOC_MaturityDate.Value.ToString("dd-MMM-yyyy");
                }
                
                if (cio.AR_RealizedDate != null) txtDocReal.Text = cio.AR_RealizedDate.Value.ToString("dd-MMM-yyyy");
                if (cio.AR_RealizedAmountUSD != null) txtDocValueUSD.Text = cio.AR_RealizedAmountUSD.Value.ToString("0.00");

                if (cio.AR_ConvertionRateBDT != null) txtConRate.Text = cio.AR_ConvertionRateBDT.Value.ToString("0.00");
                if (cio.AR_RealizedAmountUSD != null) txtDocValueTK.Text = (cio.AR_RealizedAmountUSD * cio.AR_ConvertionRateBDT).Value.ToString("0.00");
                // if (ci.AR_RealizedAmountUSD != null) txtSFall.Text = cio.AR_ShortFallUSD.Value.ToString("0.00");
                if (cio.AR_RealizedAmountUSD != null) txtSFall.Text = (cio.BA_DocAcceptAmountUSD - cio.AR_RealizedAmountUSD).Value.ToString("0.00");
                else
                {
                    if (cio.BA_DocAcceptAmountUSD != null) txtSFall.Text = cio.BA_DocAcceptAmountUSD.Value.ToString();

                }

                
                
            }
           
        }
        private void cboMaster_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboMaster.SelectedIndex >= 0)
            {
                Load_LDBC_REALIZATION_Product();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (cboMaster.SelectedIndex > 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;

                LC_CI_SUB ob = new LC_CI_SUB(o,false);
                ob.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please select a Sub Invoice No to Edit");
                
            }
        }

        private void cboCI_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            Load_LDBC_REALIZATION_Product();
        }

        private void button11_Click(object sender, EventArgs e)
        {

            LoadData();
        }
        private void LoadData()
        {
            if (cboFile.SelectedIndex == 0)
            {
                selectedlc = new Common_DL().Getbyfileno(txtsearch.Text);
            }
            else
            {
                selectedlc = new Common_DL().GetbyLC(txtsearch.Text);
            }

            if (selectedlc != null)
            {
                LoadCI_List();
            }
            else
            {
                MessageBox.Show("Search not found");
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
                if (o != null)
                {
                    ForwardingLetter obj = new ForwardingLetter(o.ID);
                    obj.ShowDialog();
                    
                }

            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
                if (o != null)
                {
                    ReportCalling.PrintBC(o.ID);
                }

            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void buyerInfoUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ci != null)
            {
                ECustomer objDes = new Common_DL().GetACustomer(Convert.ToInt32( ci.LC_ID));
                if (objDes != null && objDes.ID>0)
                {
                    BuyerInfoUpdate objDesg = new BuyerInfoUpdate(objDes);
                    objDesg.ShowDialog();

                }

            }
        }

        private void updateLDBCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
            if (o != null)
            {
                LDBC_Update ob = new LDBC_Update(txtInvNo.Text, o.ID,txtLDBCNo.Text);
                ob.ShowDialog();
            }
        }

        private void txtsearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadData();

            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
           LC_COM_INV_Product_Master o = (LC_COM_INV_Product_Master)cboMaster.SelectedItem;
           if (o != null)
           {
               if (MessageBox.Show("Do you want to delete this Invoice :" + o.CI_INV_NO_SUB, "Delete Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
               {
                   if (new LC_DL().DeleteInvoice(o.ID, "COM_INV") == false )
                   {
                       MessageBox.Show("Delete Operation Failed");
                   }
               }
              
           }
        }

        private void LC_CI_ALL_Activated(object sender, EventArgs e)
        {
            txtsearch.Focus();
        }

       
      
    }
}
