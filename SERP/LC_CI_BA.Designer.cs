﻿namespace OMS
{
    partial class LC_CI_BA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LC_CI_BA));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miSave = new System.Windows.Forms.ToolStripMenuItem();
            this.miClear = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpLDBCDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpMaturity = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDocValueUSD = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtConRate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDocValueTK = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpBanckAccpt = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.txtLDBCNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPurchasePercent = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPurchaseRate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miClear,
            this.miExit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(422, 24);
            this.menuStrip1.TabIndex = 62;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miSave
            // 
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.Name = "miSave";
            this.miSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.miSave.Size = new System.Drawing.Size(59, 20);
            this.miSave.Text = "&Save";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // miClear
            // 
            this.miClear.Image = ((System.Drawing.Image)(resources.GetObject("miClear.Image")));
            this.miClear.Name = "miClear";
            this.miClear.Size = new System.Drawing.Size(62, 20);
            this.miClear.Text = "&Clear";
            // 
            // miExit
            // 
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(53, 20);
            this.miExit.Text = "E&xit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtPurchaseRate);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtPurchasePercent);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.dtpLDBCDate);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.dtpMaturity);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtDocValueUSD);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtConRate);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtDocValueTK);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dtpBanckAccpt);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtLDBCNo);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Location = new System.Drawing.Point(12, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(399, 273);
            this.groupBox2.TabIndex = 63;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bank Acceptance Information";
            // 
            // dtpLDBCDate
            // 
            this.dtpLDBCDate.Checked = false;
            this.dtpLDBCDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpLDBCDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLDBCDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLDBCDate.Location = new System.Drawing.Point(187, 56);
            this.dtpLDBCDate.Name = "dtpLDBCDate";
            this.dtpLDBCDate.ShowCheckBox = true;
            this.dtpLDBCDate.Size = new System.Drawing.Size(179, 23);
            this.dtpLDBCDate.TabIndex = 2;
            this.dtpLDBCDate.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(117, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 43;
            this.label5.Text = "LDBC Date:";
            // 
            // dtpMaturity
            // 
            this.dtpMaturity.Checked = false;
            this.dtpMaturity.CustomFormat = "dd-MMM-yyyy";
            this.dtpMaturity.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpMaturity.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMaturity.Location = new System.Drawing.Point(187, 113);
            this.dtpMaturity.Name = "dtpMaturity";
            this.dtpMaturity.ShowCheckBox = true;
            this.dtpMaturity.Size = new System.Drawing.Size(179, 23);
            this.dtpMaturity.TabIndex = 4;
            this.dtpMaturity.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(74, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 41;
            this.label4.Text = "Bank Maturity  Date :";
            // 
            // txtDocValueUSD
            // 
            this.txtDocValueUSD.Location = new System.Drawing.Point(187, 142);
            this.txtDocValueUSD.Name = "txtDocValueUSD";
            this.txtDocValueUSD.ReadOnly = true;
            this.txtDocValueUSD.Size = new System.Drawing.Size(119, 20);
            this.txtDocValueUSD.TabIndex = 5;
            this.txtDocValueUSD.TextChanged += new System.EventHandler(this.txtDocValueUSD_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "DOC Acceptance Value (BDT) :";
            // 
            // txtConRate
            // 
            this.txtConRate.Location = new System.Drawing.Point(187, 168);
            this.txtConRate.Name = "txtConRate";
            this.txtConRate.Size = new System.Drawing.Size(73, 20);
            this.txtConRate.TabIndex = 6;
            this.txtConRate.TextChanged += new System.EventHandler(this.txtConRate_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 171);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Convertion Rate (BDT) :";
            // 
            // txtDocValueTK
            // 
            this.txtDocValueTK.Location = new System.Drawing.Point(187, 194);
            this.txtDocValueTK.Name = "txtDocValueTK";
            this.txtDocValueTK.Size = new System.Drawing.Size(119, 20);
            this.txtDocValueTK.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "DOC Acceptance Value ($) :";
            // 
            // dtpBanckAccpt
            // 
            this.dtpBanckAccpt.Checked = false;
            this.dtpBanckAccpt.CustomFormat = "dd-MMM-yyyy";
            this.dtpBanckAccpt.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpBanckAccpt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBanckAccpt.Location = new System.Drawing.Point(187, 86);
            this.dtpBanckAccpt.Name = "dtpBanckAccpt";
            this.dtpBanckAccpt.ShowCheckBox = true;
            this.dtpBanckAccpt.Size = new System.Drawing.Size(179, 23);
            this.dtpBanckAccpt.TabIndex = 3;
            this.dtpBanckAccpt.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(55, 86);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(128, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "Bank Acceptance  Date :";
            // 
            // txtLDBCNo
            // 
            this.txtLDBCNo.Location = new System.Drawing.Point(187, 30);
            this.txtLDBCNo.Name = "txtLDBCNo";
            this.txtLDBCNo.ReadOnly = true;
            this.txtLDBCNo.Size = new System.Drawing.Size(179, 20);
            this.txtLDBCNo.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(125, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "LDBC No :";
            // 
            // txtInvNo
            // 
            this.txtInvNo.Location = new System.Drawing.Point(92, 28);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(249, 20);
            this.txtInvNo.TabIndex = 42;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(21, 31);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "Invoice No :";
            // 
            // txtPurchasePercent
            // 
            this.txtPurchasePercent.Location = new System.Drawing.Point(187, 220);
            this.txtPurchasePercent.Name = "txtPurchasePercent";
            this.txtPurchasePercent.Size = new System.Drawing.Size(73, 20);
            this.txtPurchasePercent.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(62, 223);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "Doc Purchase (%) :";
            // 
            // txtPurchaseRate
            // 
            this.txtPurchaseRate.Location = new System.Drawing.Point(187, 246);
            this.txtPurchaseRate.Name = "txtPurchaseRate";
            this.txtPurchaseRate.Size = new System.Drawing.Size(73, 20);
            this.txtPurchaseRate.TabIndex = 46;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(62, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "Purchase Rate (BDT) :";
            // 
            // LC_CI_BA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 370);
            this.Controls.Add(this.txtInvNo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LC_CI_BA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bank Acceptance Information";
            this.Load += new System.EventHandler(this.LC_CI_BA_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miSave;
        private System.Windows.Forms.ToolStripMenuItem miClear;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDocValueUSD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtConRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDocValueTK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpBanckAccpt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtLDBCNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DateTimePicker dtpMaturity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpLDBCDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPurchaseRate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPurchasePercent;
        private System.Windows.Forms.Label label6;
    }
}