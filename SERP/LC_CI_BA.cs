﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Entities;
namespace OMS
{
    public partial class LC_CI_BA : Form
    {
        ELC_COM_INV c;
        ELCNoFileNo invC;
        public LC_CI_BA()
        {
            InitializeComponent();
        }

        public LC_CI_BA(string LC,ELC_COM_INV ci,ELCNoFileNo inv )
        {
            InitializeComponent();
            c = ci;
            invC = inv;
            if (c != null)
            {

                if (ci.BA_LDBC_DATE != null) dtpLDBCDate.Value = ci.BA_LDBC_DATE.Value;
                if (ci.BA_DOC_MaturityDate != null)
                {
                    dtpMaturity.Value = ci.BA_DOC_MaturityDate.Value;

                    dtpMaturity.Checked = true;
                }
                else
                {
                    if (ci.BA_Date != null && invC !=null)
                    {
                        dtpMaturity.Value = ci.BA_Date.Value.AddDays(invC.NoOfDays);
                        dtpMaturity.Checked = true;
                    }
                
                }
                if (ci.BA_DocAcceptAmountUSD != null) txtDocValueUSD.Text = (ci.BA_DocAcceptAmountUSD??0).ToString("0");
                if (ci.BA_ConvertionRateBDT != null) txtConRate.Text = (ci.BA_ConvertionRateBDT??0).ToString("0");

                if (ci.PurchasePercent != null) txtPurchasePercent.Text = (ci.PurchasePercent ?? 0).ToString("0");
                if (ci.PurchaseRate != null) txtPurchaseRate.Text = (ci.PurchaseRate ?? 0).ToString("0");


                if (ci.BA_DocAcceptAmountUSD != null) txtDocValueTK.Text = ((ci.BA_DocAcceptAmountUSD ?? 0) * (ci.BA_ConvertionRateBDT??0)).ToString("0");
                if (ci.BA_Date != null) dtpBanckAccpt.Value = ci.BA_Date.Value;
                txtLDBCNo.Text = ci.BA_LDBC_NO;
                txtInvNo.Text = ci.CI_INV_NO_SUB;

            }
        }

        private void LC_CI_BA_Load(object sender, EventArgs e)
        {

        }

        private void miSave_Click(object sender, EventArgs e)
        {
            c.BA_LDBC_DATE =UtilityService.GetDate(dtpLDBCDate);
            c.BA_Date = UtilityService.GetDate(dtpBanckAccpt);
            
            c.BA_DocAcceptAmountUSD =UtilityService.ConvertToDecimal( txtDocValueUSD.Text);
            c.BA_ConvertionRateBDT = UtilityService.ConvertToDecimal(txtConRate.Text);
            c.BA_LDBC_NO = txtLDBCNo.Text;
            c.BA_DOC_MaturityDate = UtilityService.GetDate(dtpMaturity);
            c.PurchasePercent =UtilityService.ConvertToDecimal( txtPurchasePercent.Text);
            c.PurchaseRate = UtilityService.ConvertToDecimal(txtPurchaseRate.Text);
            string res = new LC_DL().SaveCI_BA(c);
            MessageBox.Show(res);
            if (res.Substring(0,1) == "1")
            {
               
                this.Close();
            }
           
       
        }

        private void txtConRate_TextChanged(object sender, EventArgs e)
        {
            Calculate();
        }

        private void txtDocValueUSD_TextChanged(object sender, EventArgs e)
        {
            Calculate();
        }
        private void Calculate()
        {

            txtDocValueTK.Text = (UtilityService.ConvertToDouble(txtDocValueUSD.Text) * UtilityService.ConvertToDouble(txtConRate.Text)).ToString("0.00");
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
