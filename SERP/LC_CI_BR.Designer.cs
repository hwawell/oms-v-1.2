﻿namespace OMS
{
    partial class LC_CI_BR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LC_CI_BR));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDOCValue = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSF = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDocValueUSD = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtConRate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDocValueTK = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpLDBCDate = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miSave = new System.Windows.Forms.ToolStripMenuItem();
            this.miClear = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtLDBCNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtDOCValue);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtSF);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtDocValueUSD);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtConRate);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtDocValueTK);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dtpLDBCDate);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Location = new System.Drawing.Point(22, 67);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(399, 214);
            this.groupBox2.TabIndex = 64;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Acceptance Realization Information";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // txtDOCValue
            // 
            this.txtDOCValue.Location = new System.Drawing.Point(191, 36);
            this.txtDOCValue.Name = "txtDOCValue";
            this.txtDOCValue.ReadOnly = true;
            this.txtDOCValue.Size = new System.Drawing.Size(119, 20);
            this.txtDOCValue.TabIndex = 44;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(142, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "DOC Acceptance Value ($) :";
            // 
            // txtSF
            // 
            this.txtSF.Location = new System.Drawing.Point(191, 171);
            this.txtSF.Name = "txtSF";
            this.txtSF.ReadOnly = true;
            this.txtSF.Size = new System.Drawing.Size(119, 20);
            this.txtSF.TabIndex = 40;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(114, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Short Fall ($) :";
            // 
            // txtDocValueUSD
            // 
            this.txtDocValueUSD.Location = new System.Drawing.Point(191, 93);
            this.txtDocValueUSD.Name = "txtDocValueUSD";
            this.txtDocValueUSD.Size = new System.Drawing.Size(119, 20);
            this.txtDocValueUSD.TabIndex = 2;
            this.txtDocValueUSD.TextChanged += new System.EventHandler(this.txtDocValueTK_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "DOC Realized Value (BDT) :";
            // 
            // txtConRate
            // 
            this.txtConRate.Location = new System.Drawing.Point(191, 119);
            this.txtConRate.Name = "txtConRate";
            this.txtConRate.Size = new System.Drawing.Size(73, 20);
            this.txtConRate.TabIndex = 3;
            this.txtConRate.TextChanged += new System.EventHandler(this.txtConRate_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Convertion Rate (BDT) :";
            // 
            // txtDocValueTK
            // 
            this.txtDocValueTK.Location = new System.Drawing.Point(191, 145);
            this.txtDocValueTK.Name = "txtDocValueTK";
            this.txtDocValueTK.ReadOnly = true;
            this.txtDocValueTK.Size = new System.Drawing.Size(119, 20);
            this.txtDocValueTK.TabIndex = 5;
            this.txtDocValueTK.TextChanged += new System.EventHandler(this.txtDocValueTK_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "DOC Realized Value ($) :";
            // 
            // dtpLDBCDate
            // 
            this.dtpLDBCDate.Checked = false;
            this.dtpLDBCDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpLDBCDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLDBCDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLDBCDate.Location = new System.Drawing.Point(191, 62);
            this.dtpLDBCDate.Name = "dtpLDBCDate";
            this.dtpLDBCDate.ShowCheckBox = true;
            this.dtpLDBCDate.Size = new System.Drawing.Size(119, 23);
            this.dtpLDBCDate.TabIndex = 1;
            this.dtpLDBCDate.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(72, 66);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(114, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "Doc Realization Date :";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miClear,
            this.miExit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(440, 24);
            this.menuStrip1.TabIndex = 65;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miSave
            // 
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.Name = "miSave";
            this.miSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.miSave.Size = new System.Drawing.Size(59, 20);
            this.miSave.Text = "&Save";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // miClear
            // 
            this.miClear.Image = ((System.Drawing.Image)(resources.GetObject("miClear.Image")));
            this.miClear.Name = "miClear";
            this.miClear.Size = new System.Drawing.Size(62, 20);
            this.miClear.Text = "&Clear";
            // 
            // miExit
            // 
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(53, 20);
            this.miExit.Text = "E&xit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // txtInvNo
            // 
            this.txtInvNo.Location = new System.Drawing.Point(94, 33);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(121, 20);
            this.txtInvNo.TabIndex = 68;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(25, 33);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 67;
            this.label20.Text = "Invoice No :";
            // 
            // txtLDBCNo
            // 
            this.txtLDBCNo.Location = new System.Drawing.Point(282, 33);
            this.txtLDBCNo.Name = "txtLDBCNo";
            this.txtLDBCNo.ReadOnly = true;
            this.txtLDBCNo.Size = new System.Drawing.Size(139, 20);
            this.txtLDBCNo.TabIndex = 70;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(221, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 69;
            this.label4.Text = "LDBC No:";
            // 
            // LC_CI_BR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 299);
            this.Controls.Add(this.txtLDBCNo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtInvNo);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LC_CI_BR";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Acceptance Realization Information";
            this.Load += new System.EventHandler(this.LC_CI_BR_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDocValueUSD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtConRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDocValueTK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpLDBCDate;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miSave;
        private System.Windows.Forms.ToolStripMenuItem miClear;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtSF;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLDBCNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDOCValue;
        private System.Windows.Forms.Label label7;
    }
}