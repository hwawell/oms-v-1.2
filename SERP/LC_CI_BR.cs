﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Entities;
namespace OMS
{
    public partial class LC_CI_BR : Form
    {
        ELC_COM_INV c;
        public LC_CI_BR()
        {
            InitializeComponent();
        }
        public LC_CI_BR(string LC, ELC_COM_INV ci)
        {
            InitializeComponent();
            c = ci;

            if (c != null)
            {

                if (ci.AR_RealizedDate != null)
                {
                   
                    dtpLDBCDate.Checked = true;
                    dtpLDBCDate.Value = ci.AR_RealizedDate.Value;
                }
                if (ci.AR_RealizedAmountUSD != null) txtDocValueUSD.Text = ci.AR_RealizedAmountUSD.Value.ToString("0");
                if (ci.AR_ConvertionRateBDT != null) txtConRate.Text = ci.AR_ConvertionRateBDT.Value.ToString("0");
                if (ci.AR_RealizedAmountUSD != null) txtDocValueTK.Text = (ci.AR_RealizedAmountUSD * ci.AR_ConvertionRateBDT).Value.ToString("0");

               // if (ci.AR_ShortFallUSD != null) txtSF.Text = ci.AR_ShortFallUSD.Value.ToString("0.00");
                txtInvNo.Text = ci.CI_INV_NO_SUB;
                if (ci.BA_LDBC_NO != null) txtLDBCNo.Text = ci.BA_LDBC_NO;
                if (ci.BA_DocAcceptAmountUSD != null) txtDOCValue.Text = ci.BA_DocAcceptAmountUSD.Value.ToString("0.00");
            }
        }
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void txtConRate_TextChanged(object sender, EventArgs e)
        {
            Calculate();
        }

        private void txtDocValueTK_TextChanged(object sender, EventArgs e)
        {

            Calculate();
            txtSF.Text = (UtilityService.ConvertToDouble(txtDOCValue.Text) - UtilityService.ConvertToDouble(txtDocValueUSD.Text)).ToString("0.00");
        }
        private void Calculate()
        {

            txtDocValueTK.Text = (UtilityService.ConvertToDouble(txtDocValueUSD.Text) * UtilityService.ConvertToDouble(txtConRate.Text)).ToString("0.00");

           
        }

        private void miSave_Click(object sender, EventArgs e)
        {
          
            c.AR_RealizedDate = UtilityService.GetDate(dtpLDBCDate);
            c.AR_RealizedAmountUSD = UtilityService.ConvertToDecimal(txtDocValueUSD.Text);
            c.AR_ConvertionRateBDT = UtilityService.ConvertToDecimal(txtConRate.Text);
           // c.AR_ShortFallUSD = UtilityService.ConvertToDecimal(txtSF.Text);
            bool res = new LC_DL().SaveCI_AR(c);
            if (res == true)
            {
                MessageBox.Show("Data Saved Successfully");
                this.Close();
            }
            else
            {
                MessageBox.Show("Try Again!");
            }
        }

        private void LC_CI_BR_Load(object sender, EventArgs e)
        {

        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
