﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Entities;
namespace OMS
{
    public partial class LC_CI_Info : Form
    {
        ELCNoFileNo eLC;
        List<EOrderProductLC> li;
        List<EOrderProductLC> liSelect;
        public List<EOrderList> liorder;
        LC_COM_INV ci;
        bool isItemIssue = false;
        Int32 LC_ID=0;
        Int32 CI_M_ID=0;

        decimal balance;
        public LC_CI_Info()
        {
            InitializeComponent();

        }
        public LC_CI_Info(ELCNoFileNo LCNo, string CINO, LC_COM_INV o, Int32 LCID,Int32 CIM_ID)
        {
            InitializeComponent();
            eLC = LCNo;
            txtLCNO.Text = eLC.LCNo;
            liorder = new Order_DL().LC_OrderList(eLC.LCNo);
            LC_ID = LCID;
           
           // li = new Order_DL().LCGetAllOrderProduct(LCNo);
          
            ci = o;
            CI_M_ID = CIM_ID;
            if (ci != null)
            {
                balance = ci.CI_NetAmountUSD ?? 0 - ci.CI_SubAmountUSD ?? 0;

                liSelect = new Order_DL().LC_CI_GetAllOrderProduct(eLC.LCNo, CI_M_ID);
                LoadGrid();
                txtInvNo.Text = ci.CI_Master_INV_No;
                setDate(dtpMushokDate, ci.CI_Mushok_Date);
                setDate(dtpBTMADate, ci.CI_BTMA_ISS_Date);
                setDate(dtpDocSubDate, ci.CI_DOC_SUBMIT_DATE);
                setDate(dtpDocRcvDate, ci.CI_DOC_RCV_DATE);


                dtpCIDate.Value = ci.CI_Date.Value;

                txtMushok.Text = ci.CI_Mushok_No;

                txtTruckNo.Text = ci.CI_TruckNo;
                txtDriverName.Text = ci.CI_DriverName;



            }
            else
            {
                liSelect = new Order_DL().LC_CI_GetAllOrderProduct(eLC.LCNo, 0);
                LoadGrid();
            }

        }

        private void LC_ComInvoice_Load(object sender, EventArgs e)
        {
          
        }

        private void miSave_Click(object sender, EventArgs e)
        {
           
            LC_COM_INV obj = new LC_COM_INV();

            if (Convert.ToDecimal(txtlcvalue.Text) <Convert.ToDecimal(txtInvoiceTotal.Text))
            {
                if (MessageBox.Show("Commercial Info Value is greater than Available LC Value,Do You want to Continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    return;
                }
            }

            if (ci != null)
            {
                obj.ID = ci.ID;
            }
            else
            {
                isItemIssue = true;
            }
            obj.CI_Master_INV_No = txtInvNo.Text;
            obj.LC_ID = LC_ID;
            obj.CI_Date = dtpCIDate.Value.Date;
                    
            obj.CI_NetAmountUSD =UtilityService.ConvertToDecimal( txtInvoiceTotal.Text);
            obj.CI_Mushok_No = txtMushok.Text;
            obj.CI_Mushok_Date = GetDate(dtpMushokDate);
            obj.CI_TruckNo = txtTruckNo.Text;
            obj.CI_DriverName = txtDriverName.Text;
            obj.CI_BTMA_ISS_Date = GetDate(dtpBTMADate);
            obj.CI_DOC_SUBMIT_DATE = GetDate(dtpDocSubDate);
            obj.CI_DOC_RCV_DATE = GetDate(dtpDocRcvDate);

            string  res = new LC_DL().SaveCIInformation(obj, liSelect.FindAll(o=> o.Qty>0),CI_M_ID);
            string[] sp = res.Split('|');
            if (sp[0]!="1")
            {
                MessageBox.Show("Data is not saved. Operation Failed");
            }
            else
            {
                MessageBox.Show("Commercial Invoice: " + sp[1] + " saved Successfully");
                this.Close();
            }
        }

        private void setDate(DateTimePicker dt, DateTime? val)
        {

            if (val == null || val.Value<Convert.ToDateTime( "1/1/1900"))
            {
                dt.Checked = false;
            }
            else
            {
                dt.Checked = true;
                dt.Value = val.Value;
            }
        }
        private DateTime? GetDate(DateTimePicker dt)
        {

            try
            {
                if (dt.Checked == false)
                {
                    return null;
                }
                else
                {
                    return dt.Value.Date;
                }
            }
            catch
            {
                return null;
            }
        }
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    if (liSelect == null)
        //    {
        //        liSelect = new List<EOrderProductLC>();
        //    }
        //    ECI_OrderProduct obj = (ECI_OrderProduct)cboDescription.SelectedItem;
        //    if (obj != null)
        //    {
        //        EOrderProductDetails obPrice=(EOrderProductDetails)comboBox1.SelectedItem;
        //        if (obPrice != null)
        //        {

        //            if (Convert.ToDecimal(txtBalance.Text) < Convert.ToDecimal(txtIssuedQty.Text))
        //            {
        //                if (MessageBox.Show("Do you want to Issue More Qty than Ordered Qty?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
        //                {
        //                    return;
        //                }
        //            }
        //            if (liSelect.Exists(o => o.OPID == obj.OPID && o.UnitPrice.ToString() == obPrice.UnitPrice.ToString()) == true)
        //            {
        //                liSelect.SingleOrDefault(o => o.OPID == obj.OPID).Qty = liSelect.SingleOrDefault(o => o.OPID == obj.OPID).Qty + Convert.ToDecimal(txtIssuedQty.Text);
        //            }
        //            else
        //            {

        //                EOrderProductLC objC = new EOrderProductLC();
        //                objC.OPID = obj.OPID;
        //                objC.PINO = cboOrderNo.SelectedValue.ToString();
        //                objC.Description = obj.Product;
   
        //                objC.UnitPrice = UtilityService.ConvertToDecimal(obPrice.UnitPrice.ToString());

        //                objC.Qty = UtilityService.ConvertToDecimal(txtIssuedQty.Text);
        //                objC.Total = objC.UnitPrice * objC.Qty;
        //                objC.Unit = obj.Product.Substring(obj.Product.LastIndexOf(",Unit:")).Replace(",Unit:","");
        //                objC.Discount = UtilityService.ConvertToDecimal(txtDiscount.Text);
        //                objC.NetTotal = objC.Total - objC.Discount;

        //                liSelect.Add(objC);
        //            }
                     
                                    

        //        }
        //        else
        //        {
        //            MessageBox.Show("Please Select a Price" );
        //        }
               
        //    }

        //    LoadGrid();

        //    cboDescription.SelectedIndex = 0;
        //    txtDiscount.Text = string.Empty;
        //    txtBalance.Text = string.Empty;
        //    txtNet.Text = string.Empty;
        //    txtIssuedQty.Text = string.Empty;
        //    comboBox1.DataSource = null;
        //    comboBox1.Refresh();
        //}
        private void LoadGrid()
        {
            dgvData.DataSource = null;
            dgvData.Refresh();
            dgvData.DataSource = liSelect;
            dgvData.Columns["OPID"].Visible = false;


            dgvData.Columns["ItemDesc"].Visible = false;
            dgvData.Columns["Weight"].Visible = true;
            dgvData.Columns["Width"].Visible = true;
            dgvData.Columns["Unit"].Visible = false;
            dgvData.Columns["Notes"].Visible = false;

            dgvData.Columns["PINO"].ReadOnly = true;
            dgvData.Columns["Description"].ReadOnly = true;
            dgvData.Columns["Weight"].ReadOnly = true;
            dgvData.Columns["Width"].ReadOnly = true;
            dgvData.Columns["Booking"].ReadOnly = true;
            dgvData.Columns["UnitPrice"].ReadOnly = true;
            dgvData.Columns["Total"].ReadOnly = true;
            dgvData.Columns["NetTotal"].ReadOnly = true;

            dgvData.Columns["IsNotShow"].ReadOnly = false;
            dgvData.Columns["Discount"].DefaultCellStyle.BackColor = System.Drawing.Color.White;
         
            dgvData.Columns["Discount"].ReadOnly = false;
            dgvData.Columns["Discount"].DefaultCellStyle.BackColor = System.Drawing.Color.White;

            dgvData.Columns["Qty"].HeaderText = "Assign Qty";
            dgvData.Columns["Qty"].ReadOnly = false;
            dgvData.Columns["Qty"].DefaultCellStyle.BackColor = System.Drawing.Color.White;
            txtlcvalue.Text = (eLC.TotalLC - eLC.TotalCI + liSelect.Sum(o => o.NetTotal)).ToString("0.00");
            Calculation();
        }

        private void Calculation()
        {
            if (liSelect != null)
            {



        

                txtTotal.Text = liSelect.Sum(o => o.Total).ToString("0.00");
                txtDiscount.Text = liSelect.Sum(o => o.Discount).ToString("0.00");
                txtInvoiceTotal.Text = liSelect.Sum(o => o.NetTotal).ToString("0.00");
                txtBalance.Text = (Convert.ToDecimal(txtlcvalue.Text) - Convert.ToDecimal(txtInvoiceTotal.Text)).ToString("0.00");
            }
        }
        private void dgvData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void txtDiscount_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvData_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                
                string qty = dgvData.Rows[e.RowIndex].Cells["Qty"].Value.ToString();
                double unitprice = Convert.ToDouble(dgvData.Rows[e.RowIndex].Cells["UnitPrice"].Value.ToString());


                double dis = Convert.ToDouble(dgvData.Rows[e.RowIndex].Cells["Discount"].Value);
                dgvData.Rows[e.RowIndex].Cells["Total"].Value = (Convert.ToDouble(qty) * unitprice).ToString();
                dgvData.Rows[e.RowIndex].Cells["NetTotal"].Value = ((Convert.ToDouble(qty) * unitprice) - dis).ToString();



                Calculation();
            }
        }

        private void dgvData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex==10)
            {
                string qty = e.FormattedValue.ToString();
                int x;
                if (int.TryParse(qty,out x)==true  )
                {
                    if (Convert.ToInt32(dgvData.Rows[e.RowIndex].Cells["AvailableQty"].Value) < x)
                    {
                        MessageBox.Show("Qty is not available");
                        dgvData.CancelEdit();
                    }
                }
                else
                {
                    MessageBox.Show("Enter a valid Qty");
                   // e.Cancel = true;
                    dgvData.CancelEdit();
                }
            }
        }

        private void dgvData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       

      
    }
}
