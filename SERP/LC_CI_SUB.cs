﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Entities;
namespace OMS
{
    public partial class LC_CI_SUB : Form
    {
        LC_COM_INV_Product_Master o;

        Int32 ID = 0;
        List<ELCProduct> li;
        public LC_CI_SUB( LC_COM_INV_Product_Master ob,bool IsNew)
        {
            InitializeComponent();

            o = ob;
            if (o != null)
            {
                if (IsNew == true)
                {
                    txtInvNo.Text = "New Sub Invoice";
                    ID = 0;
                }
                else
                {
                    txtInvNo.Text = ob.CI_INV_NO_SUB;
                    ID = ob.ID;
                }
                li = new LC_DL().CI_PRODUCT_LIST_GET(0, o.ID);
                dgvData.DataSource = li;


                dgvData.Columns[0].Visible = false;
                dgvData.Columns[1].Visible = false;
                dgvData.Columns["ID"].ReadOnly = true;
                dgvData.Columns["MasterID"].ReadOnly = true;
                dgvData.Columns["PINO"].ReadOnly = true;
                dgvData.Columns["Product"].ReadOnly = true;
                dgvData.Columns["Unit"].ReadOnly = true;
                dgvData.Columns["Qty"].ReadOnly = false;
                dgvData.Columns["Qty"].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                dgvData.Columns["UnitPrice"].ReadOnly = false;
                dgvData.Columns["UnitPrice"].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                dgvData.Columns["TotalPrice"].ReadOnly = true;
                dgvData.Columns["Discount"].ReadOnly = false;
                dgvData.Columns["Discount"].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                dgvData.Columns["NetTotal"].ReadOnly = true;
                


                //txtInvoiceTotal.Text = lipro.FindAll(p => p.MasterID == o.ID).Sum(i => Convert.ToDouble(i.NetTotal)).ToString("0.00");
            }
        }

        private void LC_CI_SUB_Load(object sender, EventArgs e)
        {

        }

        private void dgvData_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgvData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            double total=Convert.ToDouble(dgvData.Rows[e.RowIndex].Cells["Qty"].Value) * Convert.ToDouble(dgvData.Rows[e.RowIndex].Cells["UnitPrice"].Value);

            double dis = Convert.ToDouble(dgvData.Rows[e.RowIndex].Cells["Discount"].Value);
            dgvData.Rows[e.RowIndex].Cells["TotalPrice"].Value = (total).ToString();
            dgvData.Rows[e.RowIndex].Cells["NetTotal"].Value = (total-dis).ToString();

           // MessageBox.Show("df" + dgvData.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()); 
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            if (li.Count > 0)
            {
                o.ID = ID;
                o.INV_Date = dtpLCDate.Value.Date;
                string res = new LC_DL().SaveCI_SUB_INS(o, li);
                MessageBox.Show(res);
            }
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
