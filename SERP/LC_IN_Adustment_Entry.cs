﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
using Services;

namespace OMS
{
    public partial class LC_IN_Adustment_Entry : Form
    {
        List<EOrderList> liorder;

    
       
        public LC_IN_Adustment_Entry()
        {
            InitializeComponent();
          
        }
        public LC_IN_Adustment_Entry(string LCNo, string LCValue,Int32 CusID)
        {
            InitializeComponent();
            LoadLCOrder(LCNo);
            txtLCNo.Text = LCNo;
            txtLCValue.Text = LCValue;
            LoadOrder(CusID);
            LoadGrid();
        }
        private void LoadLCOrder(string LC)
        {
            liorder = new LC_DL().LC_Order_Get(LC);
           
            
        }


    

        private void button1_Click(object sender, EventArgs e)
        {
            EOrderList obj = (EOrderList)cboOrderNo.SelectedItem;
            if (obj != null)
            {

                obj.PIValue =Convert.ToDouble( txtAdjustedValue.Text);


                if (liorder == null)
                {
                    liorder = new List<EOrderList>();

                }

                if (liorder.Exists(o => o.PINO == obj.PINO) == true)
                {
                    MessageBox.Show(" This Order already Exists");
                }
                else
                {
                    liorder.Add(obj);
                    LoadGrid();

                }


                //}
                //else
                //{
                //    cboDescription.DataSource = null;
                //    cboDescription.Refresh();
                //    MessageBox.Show(" Product you selected is not matched with this PINO ");
                //}
            }

        }
        private void LoadOrder(Int32 CID)
        {
            cboOrderNo.DataSource = new Order_DL().GetOrderByCustomer(CID);
            cboOrderNo.DisplayMember = "PINO";
            cboOrderNo.ValueMember = "PINO";
     
        }
        private void LoadGrid()
        {
            try
            {
                dgvData.DataSource = null;
                dgvData.DataSource = liorder;
                dgvData.AutoGenerateColumns = true;

                txtPIValue.Text = liorder.Sum(o => o.PIValue).ToString("0.00");
                txtLCBalance.Text = (Convert.ToDecimal(txtLCValue.Text) - Convert.ToDecimal(txtPIValue.Text)).ToString("0.00");
            }
            catch
            {

            }
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            bool res = new LC_DL().SaveLCOrder(txtLCNo.Text, liorder);
            if (res == true)
            {
                MessageBox.Show("Data Saved Successfully");
                this.Close();
            }
            else
            {
                MessageBox.Show("Failed! LC informaiton not Saved.");
            }
        }

        private void LC_Adustment_Entry_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (dgvData.SelectedRows.Count > 0)
            {
                EOrderList oo=liorder.SingleOrDefault(o=> o.PINO==dgvData.SelectedRows[0].Cells["PINO"].Value.ToString());

                if (oo !=null)
                {
                    liorder.Remove(oo);
                    LoadGrid();
                }
              
            }
        }

        private void dgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex != 3)  // ignore header row and any column
                return;                                  //  that doesn't have a file name

           
        }

        private void dgvData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex != 3)  // ignore header row and any column
                return;                                  //  that doesn't have a file name

           
        }

        private void dgvData_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                dgvData.DataSource = null;
                dgvData.DataSource = liorder;
            }
            catch
            {

            }
        }

        private void cboOrderNo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            EOrderList ob = (EOrderList)cboOrderNo.SelectedItem;

            if (ob != null)
            {
                txtAdjustedValue.Text = ob.PIValue.ToString();
            }
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
