﻿namespace OMS
{
    partial class LC_IN_Basic_Entry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LC_IN_Basic_Entry));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miSave = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboShippingTo = new System.Windows.Forms.ComboBox();
            this.cboShippingFrom = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpforwarding = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.txtforwarding = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chkVAT = new System.Windows.Forms.CheckBox();
            this.chkTIN = new System.Windows.Forms.CheckBox();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboBank = new System.Windows.Forms.ComboBox();
            this.txtHSCode = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtLCValue = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cboCustomer = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDaysLC = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMasterLC = new System.Windows.Forms.TextBox();
            this.dtpShipmentDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFileNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpLCExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.dtpLCDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLCNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAdviceNo = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSave,
            this.miExit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(589, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miSave
            // 
            this.miSave.Image = ((System.Drawing.Image)(resources.GetObject("miSave.Image")));
            this.miSave.Name = "miSave";
            this.miSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.miSave.Size = new System.Drawing.Size(59, 20);
            this.miSave.Text = "&Save";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // miExit
            // 
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(53, 20);
            this.miExit.Text = "E&xit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAdviceNo);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.cboShippingTo);
            this.groupBox1.Controls.Add(this.cboShippingFrom);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.dtpforwarding);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtforwarding);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.chkVAT);
            this.groupBox1.Controls.Add(this.chkTIN);
            this.groupBox1.Controls.Add(this.txtNotes);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cboBank);
            this.groupBox1.Controls.Add(this.txtHSCode);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.txtLCValue);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.cboCustomer);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtDaysLC);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.txtMasterLC);
            this.groupBox1.Controls.Add(this.dtpShipmentDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtFileNo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dtpLCExpiryDate);
            this.groupBox1.Controls.Add(this.dtpLCDate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtLCNo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(565, 435);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic LC Information";
            // 
            // cboShippingTo
            // 
            this.cboShippingTo.FormattingEnabled = true;
            this.cboShippingTo.Location = new System.Drawing.Point(360, 261);
            this.cboShippingTo.Name = "cboShippingTo";
            this.cboShippingTo.Size = new System.Drawing.Size(186, 21);
            this.cboShippingTo.TabIndex = 14;
            // 
            // cboShippingFrom
            // 
            this.cboShippingFrom.FormattingEnabled = true;
            this.cboShippingFrom.Location = new System.Drawing.Point(126, 262);
            this.cboShippingFrom.Name = "cboShippingFrom";
            this.cboShippingFrom.Size = new System.Drawing.Size(186, 21);
            this.cboShippingFrom.TabIndex = 13;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(126, 379);
            this.textBox1.MaxLength = 1000;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(420, 48);
            this.textBox1.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(31, 385);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 55;
            this.label14.Text = "Additional Notes :";
            // 
            // dtpforwarding
            // 
            this.dtpforwarding.CustomFormat = "dd-MMM-yyyy";
            this.dtpforwarding.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpforwarding.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpforwarding.Location = new System.Drawing.Point(443, 291);
            this.dtpforwarding.Name = "dtpforwarding";
            this.dtpforwarding.Size = new System.Drawing.Size(103, 23);
            this.dtpforwarding.TabIndex = 54;
            this.dtpforwarding.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(349, 298);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 13);
            this.label13.TabIndex = 53;
            this.label13.Text = "Forwarding Date :";
            this.label13.Visible = false;
            // 
            // txtforwarding
            // 
            this.txtforwarding.Location = new System.Drawing.Point(127, 291);
            this.txtforwarding.Name = "txtforwarding";
            this.txtforwarding.Size = new System.Drawing.Size(171, 20);
            this.txtforwarding.TabIndex = 52;
            this.txtforwarding.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 294);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 13);
            this.label12.TabIndex = 51;
            this.label12.Text = "Bank Forwarding No";
            this.label12.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(326, 264);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 13);
            this.label11.TabIndex = 50;
            this.label11.Text = "To:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(41, 264);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 48;
            this.label10.Text = "Shipping From:";
            // 
            // chkVAT
            // 
            this.chkVAT.AutoSize = true;
            this.chkVAT.Location = new System.Drawing.Point(476, 162);
            this.chkVAT.Name = "chkVAT";
            this.chkVAT.Size = new System.Drawing.Size(70, 17);
            this.chkVAT.TabIndex = 22;
            this.chkVAT.Text = "VAT Reg";
            this.chkVAT.UseVisualStyleBackColor = true;
            // 
            // chkTIN
            // 
            this.chkTIN.AutoSize = true;
            this.chkTIN.Location = new System.Drawing.Point(423, 162);
            this.chkTIN.Name = "chkTIN";
            this.chkTIN.Size = new System.Drawing.Size(44, 17);
            this.chkTIN.TabIndex = 21;
            this.chkTIN.Text = "TIN";
            this.chkTIN.UseVisualStyleBackColor = true;
            // 
            // txtNotes
            // 
            this.txtNotes.Location = new System.Drawing.Point(126, 291);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(420, 82);
            this.txtNotes.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(69, 325);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 44;
            this.label8.Text = "Notes :";
            // 
            // cboBank
            // 
            this.cboBank.FormattingEnabled = true;
            this.cboBank.Location = new System.Drawing.Point(128, 162);
            this.cboBank.Name = "cboBank";
            this.cboBank.Size = new System.Drawing.Size(288, 21);
            this.cboBank.TabIndex = 10;
            // 
            // txtHSCode
            // 
            this.txtHSCode.Location = new System.Drawing.Point(128, 235);
            this.txtHSCode.Name = "txtHSCode";
            this.txtHSCode.Size = new System.Drawing.Size(171, 20);
            this.txtHSCode.TabIndex = 12;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(71, 238);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 13);
            this.label27.TabIndex = 41;
            this.label27.Text = "HS Code:";
            // 
            // txtLCValue
            // 
            this.txtLCValue.Location = new System.Drawing.Point(128, 133);
            this.txtLCValue.Name = "txtLCValue";
            this.txtLCValue.Size = new System.Drawing.Size(171, 20);
            this.txtLCValue.TabIndex = 8;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(57, 136);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "LC Value ($)";
            // 
            // cboCustomer
            // 
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.Location = new System.Drawing.Point(127, 19);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(419, 21);
            this.cboCustomer.TabIndex = 1;
            this.cboCustomer.SelectionChangeCommitted += new System.EventHandler(this.cboCustomer_SelectionChangeCommitted);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(70, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Customer:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(53, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Bank  Branch";
            // 
            // txtDaysLC
            // 
            this.txtDaysLC.Location = new System.Drawing.Point(405, 77);
            this.txtDaysLC.Name = "txtDaysLC";
            this.txtDaysLC.Size = new System.Drawing.Size(141, 20);
            this.txtDaysLC.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(316, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "No of Days LC :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 189);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(120, 13);
            this.label21.TabIndex = 26;
            this.label21.Text = "Master LC/Contact No :";
            // 
            // txtMasterLC
            // 
            this.txtMasterLC.Location = new System.Drawing.Point(128, 189);
            this.txtMasterLC.Multiline = true;
            this.txtMasterLC.Name = "txtMasterLC";
            this.txtMasterLC.Size = new System.Drawing.Size(418, 40);
            this.txtMasterLC.TabIndex = 11;
            // 
            // dtpShipmentDate
            // 
            this.dtpShipmentDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpShipmentDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpShipmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpShipmentDate.Location = new System.Drawing.Point(127, 102);
            this.dtpShipmentDate.Name = "dtpShipmentDate";
            this.dtpShipmentDate.Size = new System.Drawing.Size(172, 23);
            this.dtpShipmentDate.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Shipment Date :";
            // 
            // txtFileNo
            // 
            this.txtFileNo.Location = new System.Drawing.Point(128, 76);
            this.txtFileNo.Name = "txtFileNo";
            this.txtFileNo.Size = new System.Drawing.Size(102, 20);
            this.txtFileNo.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(78, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "File No :";
            // 
            // dtpLCExpiryDate
            // 
            this.dtpLCExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpLCExpiryDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLCExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLCExpiryDate.Location = new System.Drawing.Point(405, 103);
            this.dtpLCExpiryDate.Name = "dtpLCExpiryDate";
            this.dtpLCExpiryDate.Size = new System.Drawing.Size(141, 23);
            this.dtpLCExpiryDate.TabIndex = 7;
            // 
            // dtpLCDate
            // 
            this.dtpLCDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpLCDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLCDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLCDate.Location = new System.Drawing.Point(405, 50);
            this.dtpLCDate.Name = "dtpLCDate";
            this.dtpLCDate.Size = new System.Drawing.Size(141, 23);
            this.dtpLCDate.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(322, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "LC Expiry Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(347, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "LC Date :";
            // 
            // txtLCNo
            // 
            this.txtLCNo.Location = new System.Drawing.Point(127, 50);
            this.txtLCNo.Name = "txtLCNo";
            this.txtLCNo.Size = new System.Drawing.Size(170, 20);
            this.txtLCNo.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(78, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "LC No :";
            // 
            // txtAdviceNo
            // 
            this.txtAdviceNo.Location = new System.Drawing.Point(405, 129);
            this.txtAdviceNo.Name = "txtAdviceNo";
            this.txtAdviceNo.Size = new System.Drawing.Size(141, 20);
            this.txtAdviceNo.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(326, 132);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 57;
            this.label15.Text = "LC Advice No:";
            // 
            // LC_IN_Basic_Entry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 483);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LC_IN_Basic_Entry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LC Basic Information Entry";
            this.Load += new System.EventHandler(this.LC_BASIC_ENTRY_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miSave;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtHSCode;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtLCValue;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cboCustomer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDaysLC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMasterLC;
        private System.Windows.Forms.DateTimePicker dtpShipmentDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFileNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpLCExpiryDate;
        private System.Windows.Forms.DateTimePicker dtpLCDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLCNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboBank;
        private System.Windows.Forms.CheckBox chkVAT;
        private System.Windows.Forms.CheckBox chkTIN;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtpforwarding;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtforwarding;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cboShippingTo;
        private System.Windows.Forms.ComboBox cboShippingFrom;
        private System.Windows.Forms.TextBox txtAdviceNo;
        private System.Windows.Forms.Label label15;
    }
}