﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
using Services;

namespace OMS
{
    public partial class LC_IN_Basic_Entry : Form
    {
        //List<EOrderList> liorder;

        //LC_Information objLCInfo;
        List<CustomerBank> lib;
        List<ECustomer> li;
        ELCInfo obLCB;
        public LC_IN_Basic_Entry()
        {
            InitializeComponent();
            LoadCustomer();
           
        }
        public LC_IN_Basic_Entry(string LCno,bool val)
        {
            InitializeComponent();
           
            LoadCustomer();
            LoadBasicLCInfo(LCno);
            if (val == true)
            {
                cboCustomer.Enabled = true;
            }
            else
            {
                cboCustomer.Enabled = false;
            }
        }
        private void LoadCustomer()
        {
            List<LC_Shipment_Info> lis = new LC_DL().LC_Shipping();
            cboShippingFrom.DataSource = lis.FindAll(o => o.IsFrom == true);
            cboShippingFrom.DisplayMember = "SName";
            cboShippingTo.DataSource = lis.FindAll(o => o.IsFrom == false);
            cboShippingTo.DisplayMember = "SName";
            li = new Common_DL().GetAllCustomer().FindAll(o => o.ID > 0);
            cboCustomer.DataSource = li;
            cboCustomer.DisplayMember = "CustomerName";
            cboCustomer.ValueMember = "ID";
        }
        private void LC_BASIC_ENTRY_Load(object sender, EventArgs e)
        {
            //li = new Common_DL().GetAllCustomer();
            //cboCustomer.DataSource = li;
            //cboCustomer.DisplayMember = "CustomerName";
            //cboCustomer.ValueMember = "ID";
        }
        private void LoadBasicLCInfo(string LCNo)
        {
            obLCB = new LC_DL().LC_Info_GET(LCNo);
            if (obLCB != null)
            {

                
                cboCustomer.SelectedItem = li.SingleOrDefault(o=> o.ID== obLCB.CustomerID);
                txtLCNo.Text = obLCB.LCNo;
                txtLCValue.Text = obLCB.LCValue.ToString();
                txtDaysLC.Text = obLCB.NoOfDaysLC.ToString();
                txtFileNo.Text = obLCB.FileNo;

                LoadCustomerBank(obLCB.CustomerID.ToString());
                if (lib.SingleOrDefault(o=> o.ID==obLCB.BankBranchID) !=null)
                cboBank.SelectedItem=lib.SingleOrDefault(o=> o.ID==obLCB.BankBranchID);
                txtMasterLC.Text = obLCB.MasterLC;
                txtHSCode.Text = obLCB.HSCode;
                txtNotes.Text = obLCB.Notes;
                txtAdviceNo.Text = obLCB.AdviceNo;
                chkVAT.Checked = obLCB.IsBankVATShow;
                chkTIN.Checked = obLCB.IsBankTINShow;
                dtpLCDate.Value = obLCB.LCDate.Date;
                cboShippingFrom.Text = obLCB.ShippingFrom;
                cboShippingTo.Text = obLCB.ShippingTo;
                txtforwarding.Text = obLCB.ForwardingNo;
                textBox1.Text = obLCB.AddlNotes;
                if (obLCB.ForwardingDate>Convert.ToDateTime( "1/1/2010"))
                dtpforwarding.Value = obLCB.ForwardingDate;
                    if (obLCB.ShipmentDate != null)
                {
                    dtpShipmentDate.Value = obLCB.ShipmentDate.Value;
                }
                dtpLCExpiryDate.Value = obLCB.LCExpirydate.Date;

                cboCustomer.Enabled = false;
                //txtLCNo.Enabled = false;
            }


        }
        private void LoadCustomerBank(string CustomerID)
        {
            lib = new Common_DL().GetCustomerBankShort(Convert.ToInt32(CustomerID));
            cboBank.DataSource = lib;
            cboBank.DisplayMember = "BankName";
            cboBank.ValueMember = "ID";
          
        }

        private void cboCustomer_SelectionChangeCommitted(object sender, EventArgs e)
        {
            LoadCustomerBank(cboCustomer.SelectedValue.ToString());
        }

        private void miSave_Click(object sender, EventArgs e)
        {

            try
            {
                ELCInfo obj = new ELCInfo();

                if (obLCB != null)
                {
                    obj.LCID = obLCB.LCID;
                }
                //obj.CustomerID = Convert.ToInt32(cboCustomer.SelectedValue.ToString());
                obj.LCNo = txtLCNo.Text;
                obj.LCDate = dtpLCDate.Value.Date;
                obj.LCExpirydate = dtpLCExpiryDate.Value.Date;
                obj.ShipmentDate = dtpShipmentDate.Value.Date;
                obj.FileNo = txtFileNo.Text;
                obj.CustomerID = Convert.ToInt32(cboCustomer.SelectedValue.ToString());
                obj.BankBranchID = Convert.ToInt32(cboBank.SelectedValue.ToString());
                //obj.BranchDetails = txtBankAddress.Text;
                obj.NoOfDaysLC = Convert.ToInt16(txtDaysLC.Text);
                obj.IsBankTINShow = chkTIN.Checked;
                obj.IsBankVATShow = chkVAT.Checked;
                obj.MasterLC = txtMasterLC.Text;
                obj.HSCode = txtHSCode.Text;
                obj.LCValue = Convert.ToDecimal(txtLCValue.Text);
                obj.AdviceNo = txtAdviceNo.Text;
                obj.EntryID = UserSession.CurrentUser;
                obj.Notes = txtNotes.Text;
                obj.ShippingFrom = cboShippingFrom.Text;
                obj.ShippingTo = cboShippingTo.Text;
                obj.ForwardingNo = txtforwarding.Text;
                obj.ForwardingDate = dtpforwarding.Value.Date;
                obj.AddlNotes = textBox1.Text;
                bool res = new LC_DL().SaveLCInformation(obj);

                if (res == true)
                {
                    MessageBox.Show("Data Saved Successfully");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Failed! LC informaiton not Saved.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error :" + ex.Message.ToString());
            }
           
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
