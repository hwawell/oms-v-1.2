﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;

namespace OMS
{
    public partial class LC_IN_UP_UD_Entry : Form
    {
        List<LC_UP_UD> li;
        LC_UP_UD objEdit;
        Int32 LC_ID;
        public LC_IN_UP_UD_Entry()
        {
            InitializeComponent();
        }
        public LC_IN_UP_UD_Entry(string LC, Int32 LCID)
        {
            InitializeComponent();
            LC_ID = LCID;
            txtLC.Text = LC;
            LoadUPUD();
        }

        private void LC_UP_UD_Entry_Load(object sender, EventArgs e)
        {

        }
        private void LoadUPUD()
        {
                string uPUD = comboBox1.Text.ToString();
                if (uPUD != null && uPUD.Length > 0)
                {
                    li = new LC_DL().LC_UP_UD_GET(txtLC.Text).FindAll(o => o.TypeID == uPUD);

                    dgvData.DataSource = li;
                    dgvData.Columns[0].Visible = false;
                    dgvData.Columns[1].Visible = false;
                    dgvData.Columns[7].Visible = false;
                    dgvData.Columns[8].Visible = false;
                    //dgvData.Columns[2].Visible = false;
                    dgvData.Columns[2].HeaderText = "UP/UD";
                    dgvData.Columns[3].HeaderText = "UP/UD No";
                    dgvData.Columns[4].HeaderText = "UP/UD Value";

                    txtTotUD.Text = li.FindAll(o => o.TypeID == uPUD).Sum(o => o.TypeAmount).ToString();
                    // txtTotUP.Text = li.FindAll(o => o.TypeID == "UP").Sum(o => o.TypeAmount).ToString();
                }
          
   
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {

            LoadUPUD();
            //if (comboBox1.SelectedIndex > 0)
            //{
            //   dgvData.DataSource
            //}
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            LC_UP_UD obj = new LC_UP_UD();
            if (objEdit != null)
            {
                obj.ID = objEdit.ID;
            }
            obj.LC_ID = LC_ID;
            obj.TypeID = comboBox1.Text;
            obj.TypeNo = txtUDNo.Text;
           
            obj.SubDate =UtilityService.GetDate(dtpUPdate); 
            obj.RcvDate=UtilityService.GetDate(dtpUDRcvDate);
            obj.TypeAmount = UtilityService.ConvertToDecimal(txtAmount.Text);
            string res = new LC_DL().SaveUP_UD(obj);

            if (res.Substring(0, 1) == "1")
            {
                MessageBox.Show("Data Saved Successfully");
                LoadUPUD();
            }
            else
            {
                MessageBox.Show(res);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {

                Int32 ID =Convert.ToInt32( dgvData.SelectedRows[0].Cells["ID"].Value.ToString());
                objEdit = li.SingleOrDefault(o => o.ID == ID);
                if (objEdit != null)
                {
                    comboBox1.Text = objEdit.TypeID;
                    txtUDNo.Text = objEdit.TypeNo;
                    txtAmount.Text = objEdit.TypeAmount.ToString();
                    if (objEdit.SubDate != null)
                    {
                        dtpUPdate.Value = objEdit.SubDate.Value.Date;
                    }
                    if (objEdit.RcvDate != null)
                    {
                        dtpUDRcvDate.Value = objEdit.RcvDate.Value.Date;
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {

                Int32 ID = Convert.ToInt32(dgvData.SelectedRows[0].Cells["ID"].Value.ToString());
                ReportCalling.ExecuteQuery(" Delete from LC_UP_UD where ID="+ ID+"");
                LoadUPUD();
            }
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
