﻿namespace OMS
{
    partial class LC_IN_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LC_IN_View));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.txtHSCode = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBankName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDaysLC = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMasterLC = new System.Windows.Forms.TextBox();
            this.dtpShipmentDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFileNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpLCExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.dtpLCDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLCNo = new System.Windows.Forms.TextBox();
            this.txtLCValue = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nEWLCEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.basicLCInformationEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lCPIAdjustmentEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uPUDInformatinEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteThisLCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buyerInfoUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button11 = new System.Windows.Forms.Button();
            this.txtsearch = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cboFileno = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtUDBalance = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtUDTotal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUPBalance = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtUPTotal = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.gvUP = new System.Windows.Forms.DataGridView();
            this.gvUD = new System.Windows.Forms.DataGridView();
            this.txtPIValue = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtLCBalance = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dgInvoice = new System.Windows.Forms.DataGridView();
            this.txtInvoiceTotal = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtLCNet = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvUP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgInvoice)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(486, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "LC No :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.txtNotes);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtCustomer);
            this.groupBox1.Controls.Add(this.txtHSCode);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtBankName);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtDaysLC);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.txtMasterLC);
            this.groupBox1.Controls.Add(this.dtpShipmentDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtFileNo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dtpLCExpiryDate);
            this.groupBox1.Controls.Add(this.dtpLCDate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtLCNo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Enabled = false;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(11, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(711, 185);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic LC Information";
            // 
            // txtNotes
            // 
            this.txtNotes.Location = new System.Drawing.Point(135, 148);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(570, 25);
            this.txtNotes.TabIndex = 43;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(88, 148);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "Notes :";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(134, 15);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(341, 20);
            this.txtCustomer.TabIndex = 42;
            // 
            // txtHSCode
            // 
            this.txtHSCode.Location = new System.Drawing.Point(349, 71);
            this.txtHSCode.Name = "txtHSCode";
            this.txtHSCode.Size = new System.Drawing.Size(142, 20);
            this.txtHSCode.TabIndex = 18;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(290, 75);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 13);
            this.label27.TabIndex = 41;
            this.label27.Text = "HS Code:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(74, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Customer:";
            // 
            // txtBankName
            // 
            this.txtBankName.Location = new System.Drawing.Point(135, 96);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(570, 20);
            this.txtBankName.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(60, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Bank  Name:";
            // 
            // txtDaysLC
            // 
            this.txtDaysLC.Location = new System.Drawing.Point(597, 72);
            this.txtDaysLC.Name = "txtDaysLC";
            this.txtDaysLC.Size = new System.Drawing.Size(108, 20);
            this.txtDaysLC.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(514, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "No of Days LC :";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(9, 119);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(123, 26);
            this.label21.TabIndex = 26;
            this.label21.Text = "Master LC/Contact No :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMasterLC
            // 
            this.txtMasterLC.Location = new System.Drawing.Point(135, 122);
            this.txtMasterLC.Name = "txtMasterLC";
            this.txtMasterLC.Size = new System.Drawing.Size(570, 20);
            this.txtMasterLC.TabIndex = 12;
            // 
            // dtpShipmentDate
            // 
            this.dtpShipmentDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpShipmentDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpShipmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpShipmentDate.Location = new System.Drawing.Point(597, 41);
            this.dtpShipmentDate.Name = "dtpShipmentDate";
            this.dtpShipmentDate.Size = new System.Drawing.Size(109, 23);
            this.dtpShipmentDate.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(509, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Shipment Date :";
            // 
            // txtFileNo
            // 
            this.txtFileNo.Location = new System.Drawing.Point(135, 70);
            this.txtFileNo.Name = "txtFileNo";
            this.txtFileNo.Size = new System.Drawing.Size(144, 20);
            this.txtFileNo.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(83, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "File No :";
            // 
            // dtpLCExpiryDate
            // 
            this.dtpLCExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpLCExpiryDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLCExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLCExpiryDate.Location = new System.Drawing.Point(134, 41);
            this.dtpLCExpiryDate.Name = "dtpLCExpiryDate";
            this.dtpLCExpiryDate.Size = new System.Drawing.Size(145, 23);
            this.dtpLCExpiryDate.TabIndex = 5;
            // 
            // dtpLCDate
            // 
            this.dtpLCDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpLCDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLCDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLCDate.Location = new System.Drawing.Point(349, 41);
            this.dtpLCDate.Name = "dtpLCDate";
            this.dtpLCDate.Size = new System.Drawing.Size(142, 23);
            this.dtpLCDate.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(49, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "LC Expiry Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(291, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "LC Date :";
            // 
            // txtLCNo
            // 
            this.txtLCNo.Location = new System.Drawing.Point(535, 14);
            this.txtLCNo.Name = "txtLCNo";
            this.txtLCNo.Size = new System.Drawing.Size(170, 20);
            this.txtLCNo.TabIndex = 3;
            // 
            // txtLCValue
            // 
            this.txtLCValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLCValue.Location = new System.Drawing.Point(844, 480);
            this.txtLCValue.Name = "txtLCValue";
            this.txtLCValue.Size = new System.Drawing.Size(133, 21);
            this.txtLCValue.TabIndex = 7;
            this.txtLCValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(841, 464);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "LC Value ($)";
            // 
            // dgvData
            // 
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvData.BackgroundColor = System.Drawing.Color.SlateGray;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.Location = new System.Drawing.Point(728, 42);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Navy;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.Size = new System.Drawing.Size(255, 159);
            this.dgvData.TabIndex = 9;
            this.dgvData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellContentClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nEWLCEntryToolStripMenuItem,
            this.buyerInfoUpdateToolStripMenuItem,
            this.basicLCInformationEntryToolStripMenuItem,
            this.lCPIAdjustmentEntryToolStripMenuItem,
            this.uPUDInformatinEntryToolStripMenuItem,
            this.deleteThisLCToolStripMenuItem,
            this.miExit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(992, 24);
            this.menuStrip1.TabIndex = 59;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nEWLCEntryToolStripMenuItem
            // 
            this.nEWLCEntryToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.nEWLCEntryToolStripMenuItem.Name = "nEWLCEntryToolStripMenuItem";
            this.nEWLCEntryToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.nEWLCEntryToolStripMenuItem.Text = "NEW LC Entry";
            this.nEWLCEntryToolStripMenuItem.Click += new System.EventHandler(this.nEWLCEntryToolStripMenuItem_Click);
            // 
            // basicLCInformationEntryToolStripMenuItem
            // 
            this.basicLCInformationEntryToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.basicLCInformationEntryToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.basicLCInformationEntryToolStripMenuItem.Name = "basicLCInformationEntryToolStripMenuItem";
            this.basicLCInformationEntryToolStripMenuItem.Size = new System.Drawing.Size(146, 20);
            this.basicLCInformationEntryToolStripMenuItem.Text = "LC Information Update";
            this.basicLCInformationEntryToolStripMenuItem.Click += new System.EventHandler(this.basicLCInformationEntryToolStripMenuItem_Click);
            // 
            // lCPIAdjustmentEntryToolStripMenuItem
            // 
            this.lCPIAdjustmentEntryToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lCPIAdjustmentEntryToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.lCPIAdjustmentEntryToolStripMenuItem.Name = "lCPIAdjustmentEntryToolStripMenuItem";
            this.lCPIAdjustmentEntryToolStripMenuItem.Size = new System.Drawing.Size(158, 20);
            this.lCPIAdjustmentEntryToolStripMenuItem.Text = "LC PI Adjustment Update";
            this.lCPIAdjustmentEntryToolStripMenuItem.Click += new System.EventHandler(this.lCPIAdjustmentEntryToolStripMenuItem_Click);
            // 
            // uPUDInformatinEntryToolStripMenuItem
            // 
            this.uPUDInformatinEntryToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.uPUDInformatinEntryToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.uPUDInformatinEntryToolStripMenuItem.Name = "uPUDInformatinEntryToolStripMenuItem";
            this.uPUDInformatinEntryToolStripMenuItem.Size = new System.Drawing.Size(165, 20);
            this.uPUDInformatinEntryToolStripMenuItem.Text = "UP/UD Informatin Update";
            this.uPUDInformatinEntryToolStripMenuItem.Click += new System.EventHandler(this.uPUDInformatinEntryToolStripMenuItem_Click);
            // 
            // deleteThisLCToolStripMenuItem
            // 
            this.deleteThisLCToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.deleteThisLCToolStripMenuItem.Name = "deleteThisLCToolStripMenuItem";
            this.deleteThisLCToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.deleteThisLCToolStripMenuItem.Text = "Delete This LC";
            this.deleteThisLCToolStripMenuItem.Visible = false;
            this.deleteThisLCToolStripMenuItem.Click += new System.EventHandler(this.deleteThisLCToolStripMenuItem_Click);
            // 
            // buyerInfoUpdateToolStripMenuItem
            // 
            this.buyerInfoUpdateToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.buyerInfoUpdateToolStripMenuItem.Name = "buyerInfoUpdateToolStripMenuItem";
            this.buyerInfoUpdateToolStripMenuItem.Size = new System.Drawing.Size(122, 20);
            this.buyerInfoUpdateToolStripMenuItem.Text = "Buyer Info Update";
            this.buyerInfoUpdateToolStripMenuItem.Click += new System.EventHandler(this.buyerInfoUpdateToolStripMenuItem_Click);
            // 
            // miExit
            // 
            this.miExit.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(56, 20);
            this.miExit.Text = "E&xit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(2)))));
            this.groupBox2.Controls.Add(this.button11);
            this.groupBox2.Controls.Add(this.txtsearch);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.cboFileno);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(11, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(515, 35);
            this.groupBox2.TabIndex = 81;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search an LC";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(425, 6);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 97;
            this.button11.Text = "Search";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // txtsearch
            // 
            this.txtsearch.Location = new System.Drawing.Point(233, 7);
            this.txtsearch.Name = "txtsearch";
            this.txtsearch.Size = new System.Drawing.Size(186, 20);
            this.txtsearch.TabIndex = 1;
            this.txtsearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsearch_KeyDown);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(220, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 13);
            this.label23.TabIndex = 95;
            // 
            // cboFileno
            // 
            this.cboFileno.BackColor = System.Drawing.Color.White;
            this.cboFileno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFileno.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFileno.FormattingEnabled = true;
            this.cboFileno.Items.AddRange(new object[] {
            "FILE NO",
            "LC NO"});
            this.cboFileno.Location = new System.Drawing.Point(159, 7);
            this.cboFileno.Name = "cboFileno";
            this.cboFileno.Size = new System.Drawing.Size(67, 24);
            this.cboFileno.TabIndex = 94;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(94, 11);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 13);
            this.label17.TabIndex = 93;
            this.label17.Text = "Search by :";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox3.Controls.Add(this.txtUDBalance);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtUDTotal);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.txtUPBalance);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtUPTotal);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.gvUP);
            this.groupBox3.Controls.Add(this.gvUD);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(11, 261);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(972, 142);
            this.groupBox3.TabIndex = 83;
            this.groupBox3.TabStop = false;
            // 
            // txtUDBalance
            // 
            this.txtUDBalance.Location = new System.Drawing.Point(349, 3);
            this.txtUDBalance.Name = "txtUDBalance";
            this.txtUDBalance.Size = new System.Drawing.Size(140, 20);
            this.txtUDBalance.TabIndex = 94;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(781, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 13);
            this.label13.TabIndex = 95;
            this.label13.Text = "Balance";
            // 
            // txtUDTotal
            // 
            this.txtUDTotal.Location = new System.Drawing.Point(150, 3);
            this.txtUDTotal.Name = "txtUDTotal";
            this.txtUDTotal.Size = new System.Drawing.Size(141, 20);
            this.txtUDTotal.TabIndex = 92;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(551, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(93, 13);
            this.label14.TabIndex = 93;
            this.label14.Text = "Total Adjust Value";
            // 
            // txtUPBalance
            // 
            this.txtUPBalance.Location = new System.Drawing.Point(833, 6);
            this.txtUPBalance.Name = "txtUPBalance";
            this.txtUPBalance.Size = new System.Drawing.Size(133, 20);
            this.txtUPBalance.TabIndex = 90;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(297, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 13);
            this.label12.TabIndex = 91;
            this.label12.Text = "Balance";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 89;
            this.label11.Text = "UD Info";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(495, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 43;
            this.label10.Text = "UP Info";
            // 
            // txtUPTotal
            // 
            this.txtUPTotal.Location = new System.Drawing.Point(650, 6);
            this.txtUPTotal.Name = "txtUPTotal";
            this.txtUPTotal.Size = new System.Drawing.Size(127, 20);
            this.txtUPTotal.TabIndex = 43;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(57, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 44;
            this.label8.Text = "Total Adjust Value";
            // 
            // gvUP
            // 
            this.gvUP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.gvUP.BackgroundColor = System.Drawing.Color.SlateGray;
            this.gvUP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvUP.DefaultCellStyle = dataGridViewCellStyle2;
            this.gvUP.Location = new System.Drawing.Point(497, 30);
            this.gvUP.Name = "gvUP";
            this.gvUP.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Navy;
            this.gvUP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvUP.Size = new System.Drawing.Size(469, 101);
            this.gvUP.TabIndex = 87;
            // 
            // gvUD
            // 
            this.gvUD.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.gvUD.BackgroundColor = System.Drawing.Color.SlateGray;
            this.gvUD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvUD.DefaultCellStyle = dataGridViewCellStyle3;
            this.gvUD.Location = new System.Drawing.Point(6, 30);
            this.gvUD.Name = "gvUD";
            this.gvUD.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Navy;
            this.gvUD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvUD.Size = new System.Drawing.Size(485, 101);
            this.gvUD.TabIndex = 88;
            // 
            // txtPIValue
            // 
            this.txtPIValue.Location = new System.Drawing.Point(839, 204);
            this.txtPIValue.Name = "txtPIValue";
            this.txtPIValue.Size = new System.Drawing.Size(144, 20);
            this.txtPIValue.TabIndex = 25;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(738, 208);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(95, 13);
            this.label28.TabIndex = 26;
            this.label28.Text = "Total PI Value ($) :";
            // 
            // txtLCBalance
            // 
            this.txtLCBalance.Location = new System.Drawing.Point(838, 230);
            this.txtLCBalance.Name = "txtLCBalance";
            this.txtLCBalance.Size = new System.Drawing.Size(145, 20);
            this.txtLCBalance.TabIndex = 85;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(749, 231);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(83, 13);
            this.label29.TabIndex = 86;
            this.label29.Text = "LC Balance ($) :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(725, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 96;
            this.label15.Text = "Order Info";
            // 
            // dgInvoice
            // 
            this.dgInvoice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgInvoice.BackgroundColor = System.Drawing.Color.SlateGray;
            this.dgInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgInvoice.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgInvoice.Location = new System.Drawing.Point(11, 422);
            this.dgInvoice.Name = "dgInvoice";
            this.dgInvoice.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Navy;
            this.dgInvoice.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInvoice.Size = new System.Drawing.Size(824, 211);
            this.dgInvoice.TabIndex = 96;
            // 
            // txtInvoiceTotal
            // 
            this.txtInvoiceTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceTotal.Location = new System.Drawing.Point(844, 523);
            this.txtInvoiceTotal.Name = "txtInvoiceTotal";
            this.txtInvoiceTotal.Size = new System.Drawing.Size(133, 21);
            this.txtInvoiceTotal.TabIndex = 97;
            this.txtInvoiceTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(841, 507);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(114, 13);
            this.label18.TabIndex = 98;
            this.label18.Text = "Total Invoice Value ($)";
            // 
            // txtLCNet
            // 
            this.txtLCNet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLCNet.Location = new System.Drawing.Point(844, 569);
            this.txtLCNet.Name = "txtLCNet";
            this.txtLCNet.Size = new System.Drawing.Size(133, 21);
            this.txtLCNet.TabIndex = 99;
            this.txtLCNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(841, 553);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(114, 13);
            this.label20.TabIndex = 100;
            this.label20.Text = "Available LC Value ($):";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(8, 406);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(103, 13);
            this.label22.TabIndex = 98;
            this.label22.Text = "Invoice Summary";
            // 
            // LC_IN_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 641);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtLCNet);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtInvoiceTotal);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.dgInvoice);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtLCBalance);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.txtLCValue);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtPIValue);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LC_IN_View";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LC Information";
            this.Activated += new System.EventHandler(this.LC_IN_View_Activated);
            this.Load += new System.EventHandler(this.LCInformation_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvUP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgInvoice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLCNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBankName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDaysLC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpShipmentDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFileNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpLCExpiryDate;
        private System.Windows.Forms.DateTimePicker dtpLCDate;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.ToolStripMenuItem deleteThisLCToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtLCValue;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ToolStripMenuItem basicLCInformationEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lCPIAdjustmentEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uPUDInformatinEntryToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtHSCode;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtMasterLC;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPIValue;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtLCBalance;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.TextBox txtUDBalance;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtUDTotal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtUPBalance;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtUPTotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView gvUP;
        private System.Windows.Forms.DataGridView gvUD;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ToolStripMenuItem nEWLCEntryToolStripMenuItem;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox txtsearch;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cboFileno;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView dgInvoice;
        private System.Windows.Forms.TextBox txtInvoiceTotal;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtLCNet;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ToolStripMenuItem buyerInfoUpdateToolStripMenuItem;
    }
}