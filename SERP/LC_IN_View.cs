﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
using Services;

namespace OMS
{
    public partial class LC_IN_View : Form
    {

        List<EOrderList> liorder;

        LC_Information objLCInfo;
        List<ECustomer> li;

        ELCInfo obLCB;
        public LC_IN_View()
        {
            InitializeComponent();
            cboFileno.SelectedIndex = 0;
        }

       

        private void txtOrderNo_KeyDown(object sender, KeyEventArgs e)
        {
            
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
     
        }
      
       
       
        private void LCInformation_Load(object sender, EventArgs e)
        {
            LoadInitial();
            txtsearch.Focus();
        }

        private void LoadInitial()
        {

            //List<ELCNoFileNo> li = new Common_DL().GetActiveLC();

            //cboFile.DataSource = li;
            //cboFile.DisplayMember = "FileNo";
            //cboFile.ValueMember = "FileNo";


            //cboLCList.DataSource = li;
            //cboLCList.DisplayMember = "LCNo";
            //cboLCList.ValueMember = "LCNo";
        }

        private void cboCustomer_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
        }

       
     

        private void button2_Click(object sender, EventArgs e)
        {

          
            
        }
        private void LoadBasicLCInfo(string LCNo)
        {
            obLCB = new LC_DL().LC_Info_GET(LCNo);
            if (obLCB != null)
            {

                txtCustomer.Text = obLCB.Customer;
                txtLCNo.Text = obLCB.LCNo;
                txtLCValue.Text = obLCB.LCValue.ToString();
                txtDaysLC.Text = obLCB.NoOfDaysLC.ToString();
                txtFileNo.Text = obLCB.FileNo;
                txtBankName.Text = obLCB.BankBranch;
                txtMasterLC.Text = obLCB.MasterLC;
                txtHSCode.Text = obLCB.HSCode;
                txtNotes.Text = obLCB.Notes;

                dtpLCDate.Value = obLCB.LCDate.Date;
                if (obLCB.ShipmentDate != null)
                {
                    dtpShipmentDate.Value = obLCB.ShipmentDate.Value;
                }
                dtpLCExpiryDate.Value = obLCB.LCExpirydate.Date;


            }
                

        }

        private void LoadUPUD(string LCID)
        {

            List<LC_UP_UD> li = new LC_DL().LC_UP_UD_GET(LCID);

            
            gvUD.DataSource = li.FindAll(o => o.TypeID == "UD");
            gvUD.Columns[0].Visible = false;
            gvUD.Columns[1].Visible = false;
            gvUD.Columns[2].Visible = false;
            gvUD.Columns[3].HeaderText = "UD No";
            gvUD.Columns[4].HeaderText = "UD Value";

            gvUD.Refresh();
            txtUDTotal.Text = li.FindAll(o => o.TypeID == "UD").Sum(o => o.TypeAmount).ToString();
            txtUDBalance.Text = (UtilityService.ConvertToDouble(txtLCValue.Text) - UtilityService.ConvertToDouble(txtUDTotal.Text)).ToString("0.00");


            gvUP.DataSource = li.FindAll(o => o.TypeID == "UP");
            gvUP.Columns[0].Visible = false;
            gvUP.Columns[1].Visible = false;
            gvUP.Columns[2].Visible = false;
            gvUP.Columns[3].HeaderText = "UP No";
            gvUP.Columns[4].HeaderText = "UP Value";

            txtUPTotal.Text = li.FindAll(o => o.TypeID == "UP").Sum(o => o.TypeAmount).ToString();
            txtUPBalance.Text = (UtilityService.ConvertToDouble(txtLCValue.Text) - UtilityService.ConvertToDouble(txtUPTotal.Text)).ToString("0.00");
        }

        private void LoadLCOrder(string LC)
        {
            List<EOrderList> li=new LC_DL().LC_Order_Get(LC);
            dgvData.DataSource = li;

            txtLCBalance.Text=(UtilityService.ConvertToDouble(txtLCValue.Text)-li.Sum(o=> o.PIValue)).ToString("0.00");
            txtPIValue.Text=li.Sum(o=> o.PIValue).ToString("0.00");
            
        }


    
        private void deleteThisLCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (obLCB != null)
            {
                if (MessageBox.Show("Do You want to Delete LC NO: " + obLCB.LCNo, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string mess = new Order_DL().DeleteLCorCI(obLCB.LCNo, 0);
                    MessageBox.Show(mess);
                }
            }
        }

        private void dgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {

                if (liorder != null && liorder.Exists(o => o.PINO == dgvData.SelectedRows[0].Cells["PINO"].Value.ToString()) == true)
                {
                    liorder.RemoveAll(o => o.PINO == dgvData.SelectedRows[0].Cells["PINO"].Value.ToString());
                   
                }
             }
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            liorder = new List<EOrderList>();
           
            txtLCNo.Text = string.Empty;
           
            txtBankName.Text = string.Empty;
            txtDaysLC.Text = string.Empty;
            txtFileNo.Text = string.Empty;
          
            txtLCNo.Text = string.Empty;
            
          
        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void basicLCInformationEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (txtLCNo.Text.Length > 2)
            {
                bool canChange = false;
                if (dgvData.Rows.Count == 0)
                {
                    canChange = true;
                }
                LC_IN_Basic_Entry ob = new LC_IN_Basic_Entry(txtLCNo.Text,canChange);
                ob.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please search an LC Record First");
            }
        }

        private void lCPIAdjustmentEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (obLCB != null)
            {
                LC_IN_Adustment_Entry ob = new LC_IN_Adustment_Entry(txtLCNo.Text, txtLCValue.Text, Convert.ToInt32(obLCB.CustomerID));
                ob.Show();
            }
             
            else
            {
                MessageBox.Show("Please search an LC Record First");
            }
        }

        private void uPUDInformatinEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (obLCB != null)
            {
                //LC_Adustment_Entry ob = new LC_Adustment_Entry(txtLCNo.Text, txtLCValue.Text, Convert.ToInt32(obLCB.CustomerID));
                //ob.ShowDialog();
                LC_IN_UP_UD_Entry ob = new LC_IN_UP_UD_Entry(obLCB.LCNo,obLCB.LCID);
                ob.ShowDialog();
            }

            else
            {
                MessageBox.Show("Please search an LC Record First");
            }
       
        }

        private void nEWLCEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LC_IN_Basic_Entry ob = new LC_IN_Basic_Entry();
            ob.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            LoadInitial();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            //if (obLCB != null)
            //{
            //    ReportCalling.PrintForwarding(obLCB.LCNo);
            //}

        }

        private void button11_Click(object sender, EventArgs e)
        {
            LoadData();

          
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void txtsearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadData();

            }
        }

        private void LoadData()
        {
            ELCNoFileNo selectedlc;
            if (cboFileno.SelectedIndex == 0)
            {
                selectedlc = new Common_DL().Getbyfileno(txtsearch.Text);
            }
            else
            {
                selectedlc = new Common_DL().GetbyLC(txtsearch.Text);
            }

            if (selectedlc != null)
            {

                LoadBasicLCInfo(selectedlc.LCNo);
                LoadUPUD(selectedlc.LCNo);
                LoadLCOrder(selectedlc.LCNo);
                LoadInvoiceSummary(selectedlc.LCID);
            }
            else
            {

                MessageBox.Show("Search not found");
            }
        }
        private void LoadInvoiceSummary(Int32 ID)
        {
            try
            {
                List<LC_Invoice_Summary_GETResult> li = new LC_DL().LC_Invoice_Summary(ID);
                dgInvoice.DataSource = li;

                txtInvoiceTotal.Text = li.Sum(o => o.Invoice_Value).ToString();
                txtLCNet.Text = (Convert.ToDouble(txtLCValue.Text) - Convert.ToDouble(txtInvoiceTotal.Text)).ToString();
            }
            catch
            {
            }

        }

        private void LC_IN_View_Activated(object sender, EventArgs e)
        {
            txtsearch.Focus();
        }

        private void buyerInfoUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (obLCB != null)
            {
                ECustomer objDes = new Common_DL().GetACustomer(Convert.ToInt32(obLCB.LCID));
                if (objDes != null && objDes.ID > 0)
                {
                    BuyerInfoUpdate objDesg = new BuyerInfoUpdate(objDes);
                    objDesg.ShowDialog();

                }

            }
        }
       
        
    }
    
}
