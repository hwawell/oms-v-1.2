﻿namespace OMS
{
    partial class LDBC_Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtLDBCNo = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.dtpLDBCDate = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // txtInvNo
            // 
            this.txtInvNo.Location = new System.Drawing.Point(116, 49);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(122, 20);
            this.txtInvNo.TabIndex = 25;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(48, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "Invoice No :";
            // 
            // txtLDBCNo
            // 
            this.txtLDBCNo.Location = new System.Drawing.Point(116, 84);
            this.txtLDBCNo.Name = "txtLDBCNo";
            this.txtLDBCNo.Size = new System.Drawing.Size(119, 20);
            this.txtLDBCNo.TabIndex = 26;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(54, 87);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "LDBC No :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(116, 157);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(45, 119);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 13);
            this.label18.TabIndex = 35;
            this.label18.Text = "LDBC Date :";
            // 
            // dtpLDBCDate
            // 
            this.dtpLDBCDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpLDBCDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLDBCDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLDBCDate.Location = new System.Drawing.Point(116, 119);
            this.dtpLDBCDate.Name = "dtpLDBCDate";
            this.dtpLDBCDate.Size = new System.Drawing.Size(119, 23);
            this.dtpLDBCDate.TabIndex = 34;
            this.dtpLDBCDate.Value = new System.DateTime(2014, 9, 9, 13, 45, 37, 0);
            // 
            // LDBC_Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 261);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.dtpLDBCDate);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtLDBCNo);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtInvNo);
            this.Controls.Add(this.label20);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LDBC_Update";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LDBC_Update";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtLDBCNo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dtpLDBCDate;
    }
}