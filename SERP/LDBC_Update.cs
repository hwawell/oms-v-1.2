﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OMS
{
    public partial class LDBC_Update : Form
    {
        Int32 MID;
        public LDBC_Update()
        {
            InitializeComponent();
        }
        public LDBC_Update(string Invoice,Int32 ID,string ldbc)
        {
            InitializeComponent();

            txtInvNo.Text = Invoice;
            txtLDBCNo.Text = ldbc;
            dtpLDBCDate.Value = System.DateTime.Now.Date;
            MID = ID;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataSet ds = ReportCalling.GetQuery(" select * from LC_COM_INV_Product_Master where BA_LDBC_NO='" + txtLDBCNo.Text + "' and ID not in (" + MID + ") ");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                MessageBox.Show("Sorry , LDBC No already Used in Other Invoice", "Result", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                if (ReportCalling.ExecuteQuery(" update LC_COM_INV_Product_Master set BA_LDBC_NO='" + txtLDBCNo.Text + "' ,BA_LDBC_DATE ='" + dtpLDBCDate.Value.Date.ToString() + "' where ID=" + MID + " ") == true)
                {

                    MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }
            }
        }
    }
}
