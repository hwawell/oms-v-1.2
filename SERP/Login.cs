﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using DataLayer;
namespace OMS
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UserSession.IsQuit = true;
            this.Close();

        }

        private void Login_Load(object sender, EventArgs e)
        {
          
        }

        private void Login_Activated(object sender, EventArgs e)
        {
            txtID.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckValidation() == true)
            {


                this.Close();
            }
            else
            {
                MessageBox.Show("Login Failed");
            }
        }
        private bool CheckValidation()
        {
            bool res = false;
            string UserName = txtID.Text;
            string password = txtPassword.Text;
            Common_DL obj = new Common_DL();
            string EmpName = "";
            res = obj.IsAuthenticated(UserName, password, ref EmpName);
            if (res == true)
            {
                UserSession.CurrentUser = txtID.Text.Trim();
                UserSession.CurrentPassword = password;
                UserSession.IsValidUser = true;
            }
            else
            {
                UserSession.CurrentUser = string.Empty;
                UserSession.CurrentPassword = string.Empty;
                UserSession.IsValidUser = false;
            }
            return res;
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            UserSession.IsQuit = true;
        }
    }
}
