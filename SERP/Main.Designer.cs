﻿namespace OMS
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsUserSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.tsColors = new System.Windows.Forms.ToolStripMenuItem();
            this.Buyer = new System.Windows.Forms.ToolStripMenuItem();
            this.buyerBankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.description = new System.Windows.Forms.ToolStripMenuItem();
            this.Brushing = new System.Windows.Forms.ToolStripMenuItem();
            this.clausesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsOrdermaking = new System.Windows.Forms.ToolStripButton();
            this.tsNewOrder = new System.Windows.Forms.ToolStripButton();
            this.tsOrderApproval = new System.Windows.Forms.ToolStripButton();
            this.tsEditOrder = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.lCInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commercialInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripStatusLabel1});
            this.statusStrip.Location = new System.Drawing.Point(0, 551);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(803, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripStatusLabel.Image")));
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(55, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripStatusLabel1.Image")));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "a Net bee Product";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(42)))), ((int)(((byte)(72)))));
            this.toolStrip1.Font = new System.Drawing.Font("Tempus Sans ITC", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton2,
            this.toolStripDropDownButton1,
            this.tsOrdermaking,
            this.tsNewOrder,
            this.tsOrderApproval,
            this.tsEditOrder,
            this.toolStripButton5,
            this.toolStripDropDownButton3});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(803, 52);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsUserSetup,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.setupToolStripMenuItem});
            this.toolStripDropDownButton2.ForeColor = System.Drawing.Color.White;
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(65, 23);
            this.toolStripDropDownButton2.Text = "File";
            // 
            // tsUserSetup
            // 
            this.tsUserSetup.Name = "tsUserSetup";
            this.tsUserSetup.Size = new System.Drawing.Size(202, 24);
            this.tsUserSetup.Text = "User Group";
            this.tsUserSetup.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(202, 24);
            this.toolStripMenuItem2.Text = "Change Password";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(202, 24);
            this.toolStripMenuItem3.Text = "Exit";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsEmployee,
            this.tsColors,
            this.Buyer,
            this.buyerBankToolStripMenuItem,
            this.description,
            this.Brushing,
            this.clausesToolStripMenuItem1});
            this.toolStripDropDownButton1.ForeColor = System.Drawing.Color.White;
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(95, 23);
            this.toolStripDropDownButton1.Text = "Settings";
            // 
            // tsEmployee
            // 
            this.tsEmployee.Name = "tsEmployee";
            this.tsEmployee.Size = new System.Drawing.Size(254, 24);
            this.tsEmployee.Text = "Employee/Merchandiser";
            this.tsEmployee.Click += new System.EventHandler(this.tsEmployee_Click);
            // 
            // tsColors
            // 
            this.tsColors.Name = "tsColors";
            this.tsColors.Size = new System.Drawing.Size(254, 24);
            this.tsColors.Text = "Color";
            this.tsColors.Click += new System.EventHandler(this.tsColors_Click);
            // 
            // Buyer
            // 
            this.Buyer.Name = "Buyer";
            this.Buyer.Size = new System.Drawing.Size(254, 24);
            this.Buyer.Text = "Buyer";
            this.Buyer.Click += new System.EventHandler(this.Buyer_Click);
            // 
            // buyerBankToolStripMenuItem
            // 
            this.buyerBankToolStripMenuItem.Name = "buyerBankToolStripMenuItem";
            this.buyerBankToolStripMenuItem.Size = new System.Drawing.Size(254, 24);
            this.buyerBankToolStripMenuItem.Text = "BuyerBank";
            this.buyerBankToolStripMenuItem.Click += new System.EventHandler(this.buyerBankToolStripMenuItem_Click);
            // 
            // description
            // 
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(254, 24);
            this.description.Text = "Description";
            this.description.Click += new System.EventHandler(this.description_Click);
            // 
            // Brushing
            // 
            this.Brushing.Name = "Brushing";
            this.Brushing.Size = new System.Drawing.Size(254, 24);
            this.Brushing.Text = "Brushing";
            this.Brushing.Click += new System.EventHandler(this.Brushing_Click);
            // 
            // clausesToolStripMenuItem1
            // 
            this.clausesToolStripMenuItem1.Name = "clausesToolStripMenuItem1";
            this.clausesToolStripMenuItem1.Size = new System.Drawing.Size(254, 24);
            this.clausesToolStripMenuItem1.Text = "Clauses";
            this.clausesToolStripMenuItem1.Click += new System.EventHandler(this.clausesToolStripMenuItem1_Click);
            // 
            // tsOrdermaking
            // 
            this.tsOrdermaking.ForeColor = System.Drawing.Color.White;
            this.tsOrdermaking.Image = ((System.Drawing.Image)(resources.GetObject("tsOrdermaking.Image")));
            this.tsOrdermaking.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsOrdermaking.Name = "tsOrdermaking";
            this.tsOrdermaking.Size = new System.Drawing.Size(108, 23);
            this.tsOrdermaking.Text = "New Order";
            this.tsOrdermaking.Click += new System.EventHandler(this.tsOrdermaking_Click);
            // 
            // tsNewOrder
            // 
            this.tsNewOrder.ForeColor = System.Drawing.Color.White;
            this.tsNewOrder.Image = ((System.Drawing.Image)(resources.GetObject("tsNewOrder.Image")));
            this.tsNewOrder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsNewOrder.Name = "tsNewOrder";
            this.tsNewOrder.Size = new System.Drawing.Size(111, 23);
            this.tsNewOrder.Text = "View Order";
            this.tsNewOrder.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // tsOrderApproval
            // 
            this.tsOrderApproval.ForeColor = System.Drawing.Color.White;
            this.tsOrderApproval.Image = ((System.Drawing.Image)(resources.GetObject("tsOrderApproval.Image")));
            this.tsOrderApproval.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsOrderApproval.Name = "tsOrderApproval";
            this.tsOrderApproval.Size = new System.Drawing.Size(143, 23);
            this.tsOrderApproval.Text = "Order Approval";
            this.tsOrderApproval.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // tsEditOrder
            // 
            this.tsEditOrder.ForeColor = System.Drawing.Color.White;
            this.tsEditOrder.Image = ((System.Drawing.Image)(resources.GetObject("tsEditOrder.Image")));
            this.tsEditOrder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsEditOrder.Name = "tsEditOrder";
            this.tsEditOrder.Size = new System.Drawing.Size(177, 23);
            this.tsEditOrder.Text = "Edit Order Approval";
            this.tsEditOrder.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.ForeColor = System.Drawing.Color.White;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(57, 23);
            this.toolStripButton5.Text = "Exit";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lCInformationToolStripMenuItem,
            this.commercialInvoiceToolStripMenuItem,
            this.reportingToolStripMenuItem});
            this.toolStripDropDownButton3.ForeColor = System.Drawing.Color.Silver;
            this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(116, 23);
            this.toolStripDropDownButton3.Text = "LC Module";
            // 
            // lCInformationToolStripMenuItem
            // 
            this.lCInformationToolStripMenuItem.Name = "lCInformationToolStripMenuItem";
            this.lCInformationToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.lCInformationToolStripMenuItem.Text = "LC Information";
            this.lCInformationToolStripMenuItem.Click += new System.EventHandler(this.lCInformationToolStripMenuItem_Click);
            // 
            // commercialInvoiceToolStripMenuItem
            // 
            this.commercialInvoiceToolStripMenuItem.Name = "commercialInvoiceToolStripMenuItem";
            this.commercialInvoiceToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.commercialInvoiceToolStripMenuItem.Text = "Commercial Invoice";
            this.commercialInvoiceToolStripMenuItem.Click += new System.EventHandler(this.commercialInvoiceToolStripMenuItem_Click);
            // 
            // reportingToolStripMenuItem
            // 
            this.reportingToolStripMenuItem.Name = "reportingToolStripMenuItem";
            this.reportingToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.reportingToolStripMenuItem.Text = "Reporting";
            this.reportingToolStripMenuItem.Click += new System.EventHandler(this.reportingToolStripMenuItem_Click);
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(202, 24);
            this.setupToolStripMenuItem.Text = "Setup";
            this.setupToolStripMenuItem.Click += new System.EventHandler(this.setupToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(803, 573);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "Main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ERP -> Module -> Order Processing";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Main_Load);
            this.Click += new System.EventHandler(this.Main_Click);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsOrdermaking;
        private System.Windows.Forms.ToolStripButton tsNewOrder;
        private System.Windows.Forms.ToolStripButton tsOrderApproval;
        private System.Windows.Forms.ToolStripButton tsEditOrder;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem tsEmployee;
        private System.Windows.Forms.ToolStripMenuItem tsColors;
        private System.Windows.Forms.ToolStripMenuItem Buyer;
        private System.Windows.Forms.ToolStripMenuItem description;
        private System.Windows.Forms.ToolStripMenuItem Brushing;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem tsUserSetup;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem clausesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem buyerBankToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem lCInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commercialInvoiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
    }
}



