﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OMS.Basic_Operation;
using Entities;
using Services;
using DataLayer;
namespace OMS
{
    public partial class Main : Form
    {
        private int childFormNumber = 0;

        public Main()
        {
            InitializeComponent();
            EGlobal.ApplicationPath = Application.StartupPath;
            UserSession.IsValidUser = false;
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

       

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

       

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            
        }

        private void customerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomerView obj = new CustomerView();
            
            obj.Show();
        }

        private void gGToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void makeAOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrderViewNew obj = new OrderViewNew();
           
            obj.Show();
        }

        private void authorizeOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("You dont have Enough Permisstion to View This Form", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
            ApprovalView obj = new ApprovalView();
           
            obj.Show();

        }

        private void reportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditForApproval obj = new EditForApproval();
            
            obj.Show();
        }

        private void Main_Load(object sender, EventArgs e)
        {

            //while (UserSession.IsValidUser == false)
            //{
            //    if (UserSession.IsQuit == true)
            //    {
            //        this.Close();
            //        UserSession.IsValidUser = true;
            //    }
            //    else
            //    {
            //        Login obj = new Login();
            //        obj.ShowDialog();
            //    }
            //}
            
           
            if (UserSession.IsValidUser == true )
            {
                tsUserSetup.Enabled = false;
                tsEmployee.Enabled = false;
                tsOrdermaking.Enabled = false;
                tsNewOrder.Enabled = false;
                tsEditOrder.Enabled = false;
                tsOrderApproval.Enabled = false;
                List<NSEC_PERMISSION> li = new Common_DL().GetAllUserPermission(UserSession.CurrentUser);

                if (li != null && li.Count>0)
                {
                    EPermission.Order_Making = li.Exists(o => o.ModuleID == 1);
                    EPermission.Order_Edit = li.Exists(o => o.ModuleID == 2);
                    EPermission.View_Print = li.Exists(o => o.ModuleID == 3);
                    EPermission.Knitting_Approval = li.Exists(o => o.ModuleID == 4);
                    EPermission.Deying_Approval = li.Exists(o => o.ModuleID == 5);
                    EPermission.Customer = li.Exists(o => o.ModuleID == 6);
                    EPermission.Clause = li.Exists(o => o.ModuleID == 7);
                    EPermission.User_Permission = li.Exists(o => o.ModuleID == 8);
                    EPermission.User_Creation = li.Exists(o => o.ModuleID == 100);
                    EPermission.IsAccessAllOrder = li.Exists(o => o.ModuleID == 9);

                    if (EPermission.User_Creation == true)
                    {
                        tsEmployee.Enabled = true;
                    }
                    if (EPermission.User_Permission == true)
                    {
                        tsUserSetup.Enabled = true;
                    }
                    if (UserSession.CurrentUser=="311")
                    {
                        tsUserSetup.Enabled = true;
                    }
                    if (EPermission.Order_Making == true)
                    {
                        tsOrdermaking.Enabled = true;
                        tsNewOrder.Enabled = true;
                       
                    }
                    if (EPermission.Order_Edit == true)
                    {
                        tsEditOrder.Enabled = true;
                    }
                    if (EPermission.Knitting_Approval == true || EPermission.Deying_Approval == true)
                    {
                        tsOrderApproval.Enabled = true;
                    }
                  
                    


                }
             
            }
        }

        private void userSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserPermission obj = new UserPermission();
            obj.MdiParent = this;
            obj.Show();
        }

        private void merchandiserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeView obj = new EmployeeView();
            obj.MdiParent = this;
            obj.Show();
        }

        private void clausesToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword ob = new ChangePassword();
            ob.MdiParent = this;
            ob.Show();
        }

        private void descriptionNotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DescriptionNotesView obj = new DescriptionNotesView();
            obj.MdiParent = this;
            obj.ShowDialog();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            OrderViewNew obj = new OrderViewNew();
            obj.MdiParent = this;
            obj.Show();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            ApprovalView obj = new ApprovalView();
            obj.MdiParent = this;
            obj.Show();

        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            EditForApproval obj = new EditForApproval();
            obj.MdiParent = this;
            obj.Show();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            UserPermission obj = new UserPermission();
            obj.MdiParent = this;
            obj.Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ChangePassword ob = new ChangePassword();
            ob.MdiParent = this;
            ob.Show();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Main_Click(object sender, EventArgs e)
        {

        }

        private void tsEmployee_Click(object sender, EventArgs e)
        {
            EmployeeView obj = new EmployeeView();
            obj.MdiParent = this;
            obj.Show();
        }

        private void tsColors_Click(object sender, EventArgs e)
        {
            ColorView obj = new ColorView();
            obj.MdiParent = this;
            obj.Show();
        }

        private void Buyer_Click(object sender, EventArgs e)
        {
            CustomerView obj = new CustomerView();
            obj.MdiParent = this;
            obj.Show();
        }

        private void description_Click(object sender, EventArgs e)
        {
            DescriptionView obj = new DescriptionView();
            obj.MdiParent = this;
            obj.Show();
        }

        private void clausesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ClauseView obj = new ClauseView();
            obj.MdiParent = this;
            obj.Show();
        }

        private void Brushing_Click(object sender, EventArgs e)
        {
            DescriptionNotesView obj = new DescriptionNotesView();
            obj.MdiParent = this;
            obj.ShowDialog();
        }

        private void tsOrdermaking_Click(object sender, EventArgs e)
        {
            Order obj = new Order();
            obj.MdiParent = this;
            obj.Show();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
           
        }

        private void toolStripButton2_Click_1(object sender, EventArgs e)
        {
            
        }

        private void buyerBankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuyerBank obj = new BuyerBank();
            obj.MdiParent = this;
            obj.Show();
        }

        private void lCInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LC_IN_View obj = new LC_IN_View();
            obj.MdiParent = this;
            obj.Show();
        }

        private void commercialInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LC_CI_ALL obj = new LC_CI_ALL();
            obj.MdiParent = this;
            obj.Show();
        }

        private void reportingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LC_CI_Report obj = new LC_CI_Report();
            obj.MdiParent = this;
            obj.Show();
        }

        private void setupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CompanySetup obj = new CompanySetup();
            obj.MdiParent = this;
            obj.Show();
        }

     
       

       

      

      
    }
}
