﻿namespace OMS
{
    partial class Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Order));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.printViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printViewExtraQtyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateFactoryDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewClauseListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miClear = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtShipmentDate = new System.Windows.Forms.TextBox();
            this.txtRevision = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.cmdSaveOrder = new System.Windows.Forms.Button();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBuyerRef = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cboOrderType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpPIDATe = new System.Windows.Forms.DateTimePicker();
            this.txtMainBuyer = new System.Windows.Forms.TextBox();
            this.txtLCNo = new System.Windows.Forms.TextBox();
            this.cboCustomer = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOrder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.btnOrderProductSave = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblHead = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.txtWeightText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.txtOpenTube = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.chkExtra = new System.Windows.Forms.CheckBox();
            this.button6 = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.dgvProduct = new System.Windows.Forms.DataGridView();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFRNo = new System.Windows.Forms.TextBox();
            this.cboUnit = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUPDown = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.cboNotes = new System.Windows.Forms.ComboBox();
            this.cboDescription = new System.Windows.Forms.ComboBox();
            this.chkLDF = new System.Windows.Forms.CheckBox();
            this.button7 = new System.Windows.Forms.Button();
            this.chkServer = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.updateDeliveryAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miEdit,
            this.miDelete,
            this.printViewToolStripMenuItem,
            this.printViewExtraQtyToolStripMenuItem,
            this.updateFactoryDatabaseToolStripMenuItem,
            this.viewClauseListToolStripMenuItem,
            this.updateDeliveryAddToolStripMenuItem,
            this.miClear,
            this.miExit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1135, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miEdit
            // 
            this.miEdit.ForeColor = System.Drawing.Color.AliceBlue;
            this.miEdit.Image = ((System.Drawing.Image)(resources.GetObject("miEdit.Image")));
            this.miEdit.Name = "miEdit";
            this.miEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
            this.miEdit.Size = new System.Drawing.Size(56, 20);
            this.miEdit.Text = "Edit";
            this.miEdit.Visible = false;
            this.miEdit.Click += new System.EventHandler(this.miEdit_Click);
            // 
            // miDelete
            // 
            this.miDelete.Image = ((System.Drawing.Image)(resources.GetObject("miDelete.Image")));
            this.miDelete.Name = "miDelete";
            this.miDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D)));
            this.miDelete.Size = new System.Drawing.Size(72, 20);
            this.miDelete.Text = "Delete";
            this.miDelete.Visible = false;
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // printViewToolStripMenuItem
            // 
            this.printViewToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.printViewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printViewToolStripMenuItem.Image")));
            this.printViewToolStripMenuItem.Name = "printViewToolStripMenuItem";
            this.printViewToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.printViewToolStripMenuItem.Text = "PrintView";
            this.printViewToolStripMenuItem.Click += new System.EventHandler(this.printViewToolStripMenuItem_Click);
            // 
            // printViewExtraQtyToolStripMenuItem
            // 
            this.printViewExtraQtyToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.printViewExtraQtyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printViewExtraQtyToolStripMenuItem.Image")));
            this.printViewExtraQtyToolStripMenuItem.Name = "printViewExtraQtyToolStripMenuItem";
            this.printViewExtraQtyToolStripMenuItem.Size = new System.Drawing.Size(157, 20);
            this.printViewExtraQtyToolStripMenuItem.Text = "Print View (Extra Qty)";
            this.printViewExtraQtyToolStripMenuItem.Click += new System.EventHandler(this.printViewExtraQtyToolStripMenuItem_Click);
            // 
            // updateFactoryDatabaseToolStripMenuItem
            // 
            this.updateFactoryDatabaseToolStripMenuItem.Enabled = false;
            this.updateFactoryDatabaseToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.updateFactoryDatabaseToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("updateFactoryDatabaseToolStripMenuItem.Image")));
            this.updateFactoryDatabaseToolStripMenuItem.Name = "updateFactoryDatabaseToolStripMenuItem";
            this.updateFactoryDatabaseToolStripMenuItem.Size = new System.Drawing.Size(179, 20);
            this.updateFactoryDatabaseToolStripMenuItem.Text = "Update Factory Database";
            this.updateFactoryDatabaseToolStripMenuItem.Visible = false;
            this.updateFactoryDatabaseToolStripMenuItem.Click += new System.EventHandler(this.updateFactoryDatabaseToolStripMenuItem_Click);
            // 
            // viewClauseListToolStripMenuItem
            // 
            this.viewClauseListToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.viewClauseListToolStripMenuItem.Image = global::OMS.Properties.Resources.properties;
            this.viewClauseListToolStripMenuItem.Name = "viewClauseListToolStripMenuItem";
            this.viewClauseListToolStripMenuItem.Size = new System.Drawing.Size(124, 20);
            this.viewClauseListToolStripMenuItem.Text = "View Clause List";
            this.viewClauseListToolStripMenuItem.Click += new System.EventHandler(this.viewClauseListToolStripMenuItem_Click);
            // 
            // miClear
            // 
            this.miClear.ForeColor = System.Drawing.Color.Navy;
            this.miClear.Image = ((System.Drawing.Image)(resources.GetObject("miClear.Image")));
            this.miClear.Name = "miClear";
            this.miClear.Size = new System.Drawing.Size(64, 20);
            this.miClear.Text = "&Clear";
            this.miClear.Click += new System.EventHandler(this.miClear_Click);
            // 
            // miExit
            // 
            this.miExit.ForeColor = System.Drawing.Color.Navy;
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(56, 20);
            this.miExit.Text = "E&xit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.SlateGray;
            this.groupBox1.Controls.Add(this.txtShipmentDate);
            this.groupBox1.Controls.Add(this.txtRevision);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.cmdSaveOrder);
            this.groupBox1.Controls.Add(this.txtNotes);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtBuyerRef);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cboOrderType);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dtpPIDATe);
            this.groupBox1.Controls.Add(this.txtMainBuyer);
            this.groupBox1.Controls.Add(this.txtLCNo);
            this.groupBox1.Controls.Add(this.cboCustomer);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtOrder);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.groupBox1.ForeColor = System.Drawing.Color.AliceBlue;
            this.groupBox1.Location = new System.Drawing.Point(7, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1120, 144);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Order Information";
            // 
            // txtShipmentDate
            // 
            this.txtShipmentDate.BackColor = System.Drawing.Color.Gainsboro;
            this.txtShipmentDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtShipmentDate.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShipmentDate.Location = new System.Drawing.Point(1027, 19);
            this.txtShipmentDate.Name = "txtShipmentDate";
            this.txtShipmentDate.Size = new System.Drawing.Size(87, 23);
            this.txtShipmentDate.TabIndex = 103;
            // 
            // txtRevision
            // 
            this.txtRevision.BackColor = System.Drawing.Color.Gainsboro;
            this.txtRevision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRevision.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRevision.Location = new System.Drawing.Point(112, 111);
            this.txtRevision.MaxLength = 200;
            this.txtRevision.Name = "txtRevision";
            this.txtRevision.Size = new System.Drawing.Size(853, 23);
            this.txtRevision.TabIndex = 101;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label11.Location = new System.Drawing.Point(12, 114);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 16);
            this.label11.TabIndex = 102;
            this.label11.Text = "Revised Notes";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.LightCoral;
            this.lblStatus.Location = new System.Drawing.Point(162, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            this.lblStatus.TabIndex = 55;
            // 
            // cmdSaveOrder
            // 
            this.cmdSaveOrder.BackColor = System.Drawing.Color.ForestGreen;
            this.cmdSaveOrder.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSaveOrder.ForeColor = System.Drawing.Color.White;
            this.cmdSaveOrder.Location = new System.Drawing.Point(1015, 99);
            this.cmdSaveOrder.Name = "cmdSaveOrder";
            this.cmdSaveOrder.Size = new System.Drawing.Size(99, 36);
            this.cmdSaveOrder.TabIndex = 11;
            this.cmdSaveOrder.Text = "Save Order";
            this.cmdSaveOrder.UseVisualStyleBackColor = false;
            this.cmdSaveOrder.Click += new System.EventHandler(this.cmdSaveOrder_Click);
            // 
            // txtNotes
            // 
            this.txtNotes.BackColor = System.Drawing.Color.Gainsboro;
            this.txtNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNotes.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNotes.Location = new System.Drawing.Point(112, 81);
            this.txtNotes.MaxLength = 200;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(853, 23);
            this.txtNotes.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label19.Location = new System.Drawing.Point(36, 84);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 16);
            this.label19.TabIndex = 54;
            this.label19.Text = "Notes In PI";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label13.Location = new System.Drawing.Point(859, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 16);
            this.label13.TabIndex = 53;
            this.label13.Text = "Buyer Ref.";
            // 
            // txtBuyerRef
            // 
            this.txtBuyerRef.BackColor = System.Drawing.Color.Gainsboro;
            this.txtBuyerRef.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBuyerRef.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuyerRef.Location = new System.Drawing.Point(937, 52);
            this.txtBuyerRef.Name = "txtBuyerRef";
            this.txtBuyerRef.Size = new System.Drawing.Size(176, 23);
            this.txtBuyerRef.TabIndex = 9;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button5.ForeColor = System.Drawing.Color.Navy;
            this.button5.Location = new System.Drawing.Point(514, 50);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(63, 23);
            this.button5.TabIndex = 51;
            this.button5.Text = "Add New";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label8.Location = new System.Drawing.Point(434, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 16);
            this.label8.TabIndex = 15;
            this.label8.Text = "Order Type";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // cboOrderType
            // 
            this.cboOrderType.BackColor = System.Drawing.Color.Gainsboro;
            this.cboOrderType.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboOrderType.FormattingEnabled = true;
            this.cboOrderType.Location = new System.Drawing.Point(514, 20);
            this.cboOrderType.Name = "cboOrderType";
            this.cboOrderType.Size = new System.Drawing.Size(150, 23);
            this.cboOrderType.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label7.Location = new System.Drawing.Point(253, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "Revision";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label6.Location = new System.Drawing.Point(673, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "PI Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label5.Location = new System.Drawing.Point(834, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(189, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Shipment Date(dd-mmm-yyyy)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label4.Location = new System.Drawing.Point(576, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Main Buyer";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // dtpPIDATe
            // 
            this.dtpPIDATe.CustomFormat = "dd-MMM-yyyy";
            this.dtpPIDATe.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpPIDATe.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPIDATe.Location = new System.Drawing.Point(727, 19);
            this.dtpPIDATe.Name = "dtpPIDATe";
            this.dtpPIDATe.Size = new System.Drawing.Size(103, 23);
            this.dtpPIDATe.TabIndex = 7;
            // 
            // txtMainBuyer
            // 
            this.txtMainBuyer.BackColor = System.Drawing.Color.Gainsboro;
            this.txtMainBuyer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMainBuyer.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMainBuyer.Location = new System.Drawing.Point(657, 51);
            this.txtMainBuyer.Name = "txtMainBuyer";
            this.txtMainBuyer.Size = new System.Drawing.Size(196, 23);
            this.txtMainBuyer.TabIndex = 8;
            this.txtMainBuyer.TextChanged += new System.EventHandler(this.txtMainBuyer_TextChanged);
            // 
            // txtLCNo
            // 
            this.txtLCNo.BackColor = System.Drawing.Color.Gainsboro;
            this.txtLCNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLCNo.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLCNo.Location = new System.Drawing.Point(317, 19);
            this.txtLCNo.Name = "txtLCNo";
            this.txtLCNo.Size = new System.Drawing.Size(114, 23);
            this.txtLCNo.TabIndex = 3;
            // 
            // cboCustomer
            // 
            this.cboCustomer.BackColor = System.Drawing.Color.Gainsboro;
            this.cboCustomer.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.Location = new System.Drawing.Point(112, 51);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(396, 23);
            this.cboCustomer.TabIndex = 6;
            this.cboCustomer.SelectedIndexChanged += new System.EventHandler(this.cboCustomer_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label2.Location = new System.Drawing.Point(63, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Buyer";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtOrder
            // 
            this.txtOrder.BackColor = System.Drawing.Color.Gainsboro;
            this.txtOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOrder.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrder.Location = new System.Drawing.Point(112, 20);
            this.txtOrder.Name = "txtOrder";
            this.txtOrder.Size = new System.Drawing.Size(139, 25);
            this.txtOrder.TabIndex = 100;
            this.txtOrder.Text = "Auto Generate";
            this.txtOrder.TextChanged += new System.EventHandler(this.txtOrder_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label1.Location = new System.Drawing.Point(44, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Order No";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(377, 211);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(189, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "View Order Product";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // dgvData
            // 
            this.dgvData.BackgroundColor = System.Drawing.Color.SlateGray;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.Location = new System.Drawing.Point(7, 182);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Navy;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.Size = new System.Drawing.Size(1120, 190);
            this.dgvData.TabIndex = 8;
            this.dgvData.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellClick);
            this.dgvData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellContentClick);
            this.dgvData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellDoubleClick);
            this.dgvData.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvData_RowsAdded);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(14, 452);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(342, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "List of Order Product ( Double Click on Any Record to Edit)";
            // 
            // btnOrderProductSave
            // 
            this.btnOrderProductSave.BackColor = System.Drawing.Color.Green;
            this.btnOrderProductSave.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderProductSave.ForeColor = System.Drawing.Color.White;
            this.btnOrderProductSave.Location = new System.Drawing.Point(1004, 224);
            this.btnOrderProductSave.Name = "btnOrderProductSave";
            this.btnOrderProductSave.Size = new System.Drawing.Size(109, 35);
            this.btnOrderProductSave.TabIndex = 23;
            this.btnOrderProductSave.Text = "Save Product";
            this.btnOrderProductSave.UseVisualStyleBackColor = false;
            this.btnOrderProductSave.Click += new System.EventHandler(this.btnOrderProductSave_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.SlateGray;
            this.groupBox2.Controls.Add(this.lblHead);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.txtWeightText);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.txtOpenTube);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.cboType);
            this.groupBox2.Controls.Add(this.chkExtra);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.btnNew);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.dgvProduct);
            this.groupBox2.Controls.Add(this.btnOrderProductSave);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.txtFRNo);
            this.groupBox2.Controls.Add(this.cboUnit);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtUPDown);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtWidth);
            this.groupBox2.Controls.Add(this.txtWeight);
            this.groupBox2.Controls.Add(this.cboNotes);
            this.groupBox2.Controls.Add(this.cboDescription);
            this.groupBox2.Controls.Add(this.chkLDF);
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.groupBox2.ForeColor = System.Drawing.Color.AliceBlue;
            this.groupBox2.Location = new System.Drawing.Point(7, 380);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1120, 290);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Order Product";
            // 
            // lblHead
            // 
            this.lblHead.AutoSize = true;
            this.lblHead.Location = new System.Drawing.Point(444, 5);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(0, 16);
            this.lblHead.TabIndex = 63;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button4.ForeColor = System.Drawing.Color.Navy;
            this.button4.Location = new System.Drawing.Point(1004, 152);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(111, 27);
            this.button4.TabIndex = 24;
            this.button4.Text = "Clear Product";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // txtWeightText
            // 
            this.txtWeightText.BackColor = System.Drawing.Color.Gainsboro;
            this.txtWeightText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWeightText.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightText.Location = new System.Drawing.Point(139, 71);
            this.txtWeightText.Name = "txtWeightText";
            this.txtWeightText.Size = new System.Drawing.Size(77, 23);
            this.txtWeightText.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(451, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 60;
            this.label3.Text = "FRNo";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Green;
            this.button2.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(1004, 185);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 35);
            this.button2.TabIndex = 59;
            this.button2.Text = "Save Color LDF";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // txtOpenTube
            // 
            this.txtOpenTube.BackColor = System.Drawing.Color.Gainsboro;
            this.txtOpenTube.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOpenTube.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOpenTube.Location = new System.Drawing.Point(368, 72);
            this.txtOpenTube.Name = "txtOpenTube";
            this.txtOpenTube.Size = new System.Drawing.Size(77, 23);
            this.txtOpenTube.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(282, 41);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 16);
            this.label10.TabIndex = 55;
            this.label10.Text = "Description";
            // 
            // cboType
            // 
            this.cboType.BackColor = System.Drawing.Color.Gainsboro;
            this.cboType.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboType.FormattingEnabled = true;
            this.cboType.Location = new System.Drawing.Point(68, 37);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(203, 23);
            this.cboType.TabIndex = 12;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // chkExtra
            // 
            this.chkExtra.AutoSize = true;
            this.chkExtra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExtra.Location = new System.Drawing.Point(1005, 75);
            this.chkExtra.Name = "chkExtra";
            this.chkExtra.Size = new System.Drawing.Size(80, 20);
            this.chkExtra.TabIndex = 22;
            this.chkExtra.Text = "Extra Qty";
            this.chkExtra.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button6.ForeColor = System.Drawing.Color.Navy;
            this.button6.Location = new System.Drawing.Point(1004, 119);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(112, 27);
            this.button6.TabIndex = 52;
            this.button6.Text = "Add Color";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnNew.ForeColor = System.Drawing.Color.Navy;
            this.btnNew.Location = new System.Drawing.Point(777, 36);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(42, 27);
            this.btnNew.TabIndex = 49;
            this.btnNew.Text = "Add New";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(843, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 16);
            this.label18.TabIndex = 47;
            this.label18.Text = "Unit";
            // 
            // dgvProduct
            // 
            this.dgvProduct.BackgroundColor = System.Drawing.Color.SlateGray;
            this.dgvProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProduct.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvProduct.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvProduct.Location = new System.Drawing.Point(8, 107);
            this.dgvProduct.Name = "dgvProduct";
            this.dgvProduct.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Navy;
            this.dgvProduct.Size = new System.Drawing.Size(984, 171);
            this.dgvProduct.TabIndex = 21;
            this.dgvProduct.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduct_CellClick);
            this.dgvProduct.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvProduct_RowsAdded);
            this.dgvProduct.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvProduct_UserAddedRow);
            this.dgvProduct.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvProduct_UserDeletingRow);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(24, 37);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 16);
            this.label17.TabIndex = 45;
            this.label17.Text = "Type";
            // 
            // txtFRNo
            // 
            this.txtFRNo.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFRNo.Location = new System.Drawing.Point(496, 72);
            this.txtFRNo.Name = "txtFRNo";
            this.txtFRNo.Size = new System.Drawing.Size(175, 23);
            this.txtFRNo.TabIndex = 19;
            // 
            // cboUnit
            // 
            this.cboUnit.BackColor = System.Drawing.Color.Gainsboro;
            this.cboUnit.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboUnit.FormattingEnabled = true;
            this.cboUnit.Location = new System.Drawing.Point(880, 72);
            this.cboUnit.Name = "cboUnit";
            this.cboUnit.Size = new System.Drawing.Size(112, 23);
            this.cboUnit.TabIndex = 21;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(677, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 16);
            this.label14.TabIndex = 43;
            this.label14.Text = "Up/Down(%)";
            // 
            // txtUPDown
            // 
            this.txtUPDown.BackColor = System.Drawing.Color.Gainsboro;
            this.txtUPDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUPDown.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUPDown.Location = new System.Drawing.Point(770, 74);
            this.txtUPDown.Name = "txtUPDown";
            this.txtUPDown.Size = new System.Drawing.Size(56, 23);
            this.txtUPDown.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(236, 77);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 16);
            this.label15.TabIndex = 42;
            this.label15.Text = "Width";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(12, 76);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 16);
            this.label16.TabIndex = 41;
            this.label16.Text = "Weight";
            // 
            // txtWidth
            // 
            this.txtWidth.BackColor = System.Drawing.Color.Gainsboro;
            this.txtWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWidth.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWidth.Location = new System.Drawing.Point(285, 72);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(77, 23);
            this.txtWidth.TabIndex = 17;
            // 
            // txtWeight
            // 
            this.txtWeight.BackColor = System.Drawing.Color.Gainsboro;
            this.txtWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWeight.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeight.Location = new System.Drawing.Point(68, 71);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(65, 23);
            this.txtWeight.TabIndex = 15;
            // 
            // cboNotes
            // 
            this.cboNotes.BackColor = System.Drawing.Color.Gainsboro;
            this.cboNotes.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNotes.FormattingEnabled = true;
            this.cboNotes.Location = new System.Drawing.Point(825, 36);
            this.cboNotes.Name = "cboNotes";
            this.cboNotes.Size = new System.Drawing.Size(286, 23);
            this.cboNotes.TabIndex = 14;
            // 
            // cboDescription
            // 
            this.cboDescription.BackColor = System.Drawing.Color.White;
            this.cboDescription.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDescription.FormattingEnabled = true;
            this.cboDescription.Location = new System.Drawing.Point(368, 37);
            this.cboDescription.Name = "cboDescription";
            this.cboDescription.Size = new System.Drawing.Size(403, 23);
            this.cboDescription.TabIndex = 13;
            // 
            // chkLDF
            // 
            this.chkLDF.AutoSize = true;
            this.chkLDF.Checked = true;
            this.chkLDF.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLDF.Font = new System.Drawing.Font("Cambria", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLDF.Location = new System.Drawing.Point(733, 98);
            this.chkLDF.Name = "chkLDF";
            this.chkLDF.Size = new System.Drawing.Size(196, 16);
            this.chkLDF.TabIndex = 58;
            this.chkLDF.Text = "Update Factory DB For Color ,LDF,FR";
            this.chkLDF.UseVisualStyleBackColor = true;
            this.chkLDF.Visible = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.LightGray;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button7.Location = new System.Drawing.Point(808, 107);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(109, 28);
            this.button7.TabIndex = 56;
            this.button7.Text = "Clear";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // chkServer
            // 
            this.chkServer.AutoSize = true;
            this.chkServer.BackColor = System.Drawing.Color.Silver;
            this.chkServer.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkServer.ForeColor = System.Drawing.Color.Navy;
            this.chkServer.Location = new System.Drawing.Point(827, 1);
            this.chkServer.Name = "chkServer";
            this.chkServer.Size = new System.Drawing.Size(211, 20);
            this.chkServer.TabIndex = 53;
            this.chkServer.Text = "Also Update Factory Database";
            this.chkServer.UseVisualStyleBackColor = false;
            this.chkServer.Visible = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button3.ForeColor = System.Drawing.Color.Navy;
            this.button3.Location = new System.Drawing.Point(1022, 173);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(98, 27);
            this.button3.TabIndex = 62;
            this.button3.Text = "Copy Item";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // updateDeliveryAddToolStripMenuItem
            // 
            this.updateDeliveryAddToolStripMenuItem.Name = "updateDeliveryAddToolStripMenuItem";
            this.updateDeliveryAddToolStripMenuItem.Size = new System.Drawing.Size(135, 20);
            this.updateDeliveryAddToolStripMenuItem.Text = "Update Delivery Add";
            this.updateDeliveryAddToolStripMenuItem.Click += new System.EventHandler(this.updateDeliveryAddToolStripMenuItem_Click);
            // 
            // Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1135, 674);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.chkServer);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Order";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order";
            this.Load += new System.EventHandler(this.Order_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miClear;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.ToolStripMenuItem miEdit;
        private System.Windows.Forms.ToolStripMenuItem miDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboOrderType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpPIDATe;
        private System.Windows.Forms.TextBox txtMainBuyer;
        private System.Windows.Forms.ComboBox cboCustomer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOrder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnOrderProductSave;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvProduct;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtFRNo;
        private System.Windows.Forms.ComboBox cboUnit;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtUPDown;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.ComboBox cboNotes;
        private System.Windows.Forms.ComboBox cboDescription;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.CheckBox chkExtra;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.ToolStripMenuItem printViewToolStripMenuItem;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBuyerRef;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtOpenTube;
        private System.Windows.Forms.Button cmdSaveOrder;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.CheckBox chkServer;
        private System.Windows.Forms.ToolStripMenuItem updateFactoryDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewClauseListToolStripMenuItem;
        private System.Windows.Forms.TextBox txtRevision;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkLDF;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtLCNo;
        private System.Windows.Forms.TextBox txtWeightText;
        private System.Windows.Forms.ToolStripMenuItem printViewExtraQtyToolStripMenuItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label lblHead;
        private System.Windows.Forms.TextBox txtShipmentDate;
        private System.Windows.Forms.ToolStripMenuItem updateDeliveryAddToolStripMenuItem;
    }
}