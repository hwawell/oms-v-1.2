﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Services;
using DataLayer;
using OMS.Basic_Operation;
namespace OMS
{
    public partial class Order : Form
    {
        EOrder objOrder;
        bool Select;
        public Order()
        {
            InitializeComponent();
        }
        List<EOrder> _liOrder;
        List<EOrderProduct> liOrderProduct;
        List<EDescription> liDes;
        EOrderProduct objOrderProduct;
        EOrderProduct objOP;
        List<EOrderProductDetails> liOPD;
        bool IsReadOnlyForm=false;
        bool ISOrderApprove=false;
        public Order(List<EOrder> lid, EOrder objDe,bool IsReadOnly)
        {
            InitializeComponent();
            _liOrder = lid;
            objOrder = objDe;

            IsReadOnlyForm = IsReadOnly;
            if (new Order_DL().HasOrderApproved(objOrder.PINo) == true)
            {
                lblStatus.Text = "Order Already Approved , Cannot Edit ";
                ISOrderApprove = true;
            }
            //if (IsReadOnlyForm == true)
            if (ISOrderApprove == true)
            {
                cmdSaveOrder.Enabled = false;
                
                miEdit.Enabled = false;
                miDelete.Enabled = false;
               
                dgvData.ReadOnly = true;
                dgvProduct.ReadOnly = false;
                btnOrderProductSave.Enabled = false;
            }
            else
            {
                cmdSaveOrder.Enabled = true;
              
                miEdit.Enabled = true;
                miDelete.Enabled = true;
              
                dgvData.ReadOnly = false;
                dgvProduct.ReadOnly = false;
                btnOrderProductSave.Enabled = true;
            }
        }

        private void miSave_Click(object sender, EventArgs e)
        {
           
        }
        private void Save()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                EOrder objc = new EOrder();
                if (Convert.ToInt16(cboOrderType.SelectedValue) == 0)
                {
                    MessageBox.Show("Select Order Type", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cboOrderType.Focus();
                    this.Cursor = Cursors.Default;
                    return;
                }

                if (Convert.ToInt32(cboCustomer.SelectedValue) == 0)
                {
                    MessageBox.Show("Select Customer", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cboCustomer.Focus();
                    this.Cursor = Cursors.Default;
                    return;
                }

                if (objOrder != null && objOrder.ID > 0)
                {
                    objc.ID = objOrder.ID;
                }
                objc.PINo = txtOrder.Text.Trim();
                objc.CustID = Convert.ToInt32(cboCustomer.SelectedValue);
                objc.CustomerName = cboCustomer.Text;
                objc.PDate = dtpPIDATe.Value.Date;
                DateTime? shpDate = UtilityService.ConvertFormmatedDate(txtShipmentDate.Text.ToString());

                if (shpDate ==null)
                {
                    MessageBox.Show("Valid Shipment Date Must be Given(Format Like 'dd-MMM-yyyy') Ex: "+ System.DateTime.Now.Date.ToString("dd-MMM-yyyy") , "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtShipmentDate.Focus();
                    this.Cursor = Cursors.Default;
                    return;
                }
                objc.RcvDate =Convert.ToDateTime( shpDate);
                objc.ShpDate = Convert.ToDateTime(shpDate);
                objc.LCNo = txtLCNo.Text.Trim();
                objc.MainBuyer = txtMainBuyer.Text;
                objc.OrderTypeID = Convert.ToInt16(cboOrderType.SelectedValue);
                objc.OrderTypeName = cboOrderType.Text;
                objc.BuyerRef = txtBuyerRef.Text;
                objc.EMode = "E";
                objc.UserID = UserSession.CurrentUser;
                objc.Notes = txtNotes.Text;
                objc.RevisedNotes = txtRevision.Text;
                string orderID = "0";

                bool res = new Order_DL().InsertUpdateOrder(objc, ref orderID);

                if (res == true)
                {
                    objOrder = objc;
                    //objOrder.ID = Convert.ToInt16(orderID);
                    txtOrder.Text = orderID;
                    if (chkServer.Checked == true)
                    {
                        bool facRes = new FactoryOperation_DL().InsertApprovedOrder(objOrder);
                        if (facRes == true)
                        {
                            MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            cboType.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Order has not updated in Factory Server But Order saved successfully in Headoffice", "Result", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                else
                {

                    MessageBox.Show("Data not Saved", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);


                }
            }
            catch
            {

            }
            this.Cursor = Cursors.Default;
        }

        private void Order_Load(object sender, EventArgs e)
        {
            cboCustomer.DataSource = new Common_DL().GetAllCustomer();
            cboCustomer.DisplayMember = "CustomerName";
            cboCustomer.ValueMember = "ID";

            cboOrderType.DataSource = new Order_DL().GetAllOrderType();
            cboOrderType.DisplayMember = "OrderTypeName";
            cboOrderType.ValueMember = "ID";


            liDes = new Common_DL().GetAllDescription();
            cboDescription.DataSource = liDes;
            cboDescription.DisplayMember = "DescriptionName";
            cboDescription.ValueMember = "ID";

            cboType.DataSource = new Common_DL().GetAllDescriptionType();
            cboType.DisplayMember = "TypeName";
            cboType.ValueMember = "ID";


            cboNotes.DataSource = new Common_DL().GetAllDescriptionNotes();
            cboNotes.DisplayMember = "DescriptionName";
            cboUnit.DataSource = new Common_DL().GetAllUnit();
            cboUnit.DisplayMember = "UnitName";


            if (objOrder != null)
            {
                txtOrder.Text = objOrder.PINo;
                txtLCNo.Text = objOrder.LCNo;
                txtMainBuyer.Text = objOrder.MainBuyer;
                txtNotes.Text = objOrder.Notes;
              
               // cboCustomer.SelectedValue = objOrder.CustID.ToString();
                cboCustomer.Text = objOrder.CustomerName;
                dtpPIDATe.Value = objOrder.PDate;
                txtShipmentDate.Text = objOrder.ShpDate.ToString("dd-MMM-yyyy");
                cboOrderType.SelectedValue = objOrder.OrderTypeID;
                txtBuyerRef.Text = objOrder.BuyerRef;
                txtRevision.Text = objOrder.RevisedNotes;
                OrderLoad();
              
            }
            
            
            LoadGridSetting();
        }
       
        private void LoadGridProductSetting()
        {
            dgvData.Columns.Clear();

            dgvData.AutoGenerateColumns = false;
            dgvData.AllowUserToDeleteRows = true;
            dgvData.AllowUserToAddRows = true;
        
            DataGridViewTextBoxColumn obj0 = new DataGridViewTextBoxColumn();
            obj0.DataPropertyName = "OPID";
            obj0.HeaderText = "OPID";
            obj0.Width = 150;
            obj0.Visible = false;
            dgvData.Columns.Add(obj0);


           

            DataGridViewTextBoxColumn obj21 = new DataGridViewTextBoxColumn();
            obj21.DataPropertyName = "Description";
            obj21.HeaderText = "Description";
            obj21.Width = 260;
            obj21.Visible = true;
            dgvData.Columns.Add(obj21);

            DataGridViewTextBoxColumn obj22 = new DataGridViewTextBoxColumn();
            obj22.DataPropertyName = "Notes";
            obj22.HeaderText = "Notes";
            obj22.Width = 200;
            dgvData.Columns.Add(obj22);
            DataGridViewTextBoxColumn obj2 = new DataGridViewTextBoxColumn();
            obj2.DataPropertyName = "Weight";
            obj2.HeaderText = "Weight";
            obj2.Width = 80;
            dgvData.Columns.Add(obj2);

            DataGridViewTextBoxColumn obj3 = new DataGridViewTextBoxColumn();
            obj3.DataPropertyName = "Width";
            obj3.HeaderText = "Width";
            obj3.Width = 80;
            dgvData.Columns.Add(obj3);


            DataGridViewTextBoxColumn obj4 = new DataGridViewTextBoxColumn();
            obj4.DataPropertyName = "UPDown";
            obj4.HeaderText = "UPDown";
            obj4.Width = 50;
            dgvData.Columns.Add(obj4);


            DataGridViewTextBoxColumn obj5 = new DataGridViewTextBoxColumn();
            obj5.DataPropertyName = "FRNO";
            obj5.HeaderText = "FR No/Style";
            obj5.Width = 70;
            dgvData.Columns.Add(obj5);

            DataGridViewTextBoxColumn obj6 = new DataGridViewTextBoxColumn();
            obj6.DataPropertyName = "Unit";
            obj6.HeaderText = "Unit";
            obj6.Width = 70;
            dgvData.Columns.Add(obj6);

            DataGridViewTextBoxColumn obj7 = new DataGridViewTextBoxColumn();
            obj7.DataPropertyName = "ISExtraQty";
            obj7.HeaderText = "Extra Qty";
            obj7.Width = 70;
            dgvData.Columns.Add(obj7);

            DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
            dgvData.Columns.Add(btn);
            btn.HeaderText = "Delete";
            btn.Text = "Delete";
            btn.Name = "btn";
            btn.Width = 80;
            btn.UseColumnTextForButtonValue = true;

          
           
           
        }
        private void LoadGridSetting()
        {
            dgvProduct.Columns.Clear();

            dgvProduct.AutoGenerateColumns = false;
            dgvProduct.AllowUserToDeleteRows = true;
            dgvProduct.AllowUserToAddRows = true;

            DataGridViewTextBoxColumn obj0 = new DataGridViewTextBoxColumn();
            obj0.DataPropertyName = "OPDID";
            obj0.HeaderText = "OPDID";
            obj0.Width = 150;
            obj0.Visible = false;
            dgvProduct.Columns.Add(obj0);


            List<EColor> li = new Common_DL().GetAllColor();
            // EOrderProductDetails
            DataGridViewComboBoxColumn obj1 = new DataGridViewComboBoxColumn();
            obj1.DataPropertyName = "ColorName";
            obj1.HeaderText = "Base Color";
            obj1.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;
            obj1.Width = 110;
            obj1.DataSource = li;
            obj1.DisplayMember = "ColorName";
            dgvProduct.Columns.Add(obj1);

            DataGridViewTextBoxColumn obj2111 = new DataGridViewTextBoxColumn();
            obj2111.DataPropertyName = "FRNo";
            obj2111.HeaderText = "FRNo";
            obj2111.Width = 85;
            
            dgvProduct.Columns.Add(obj2111);

            DataGridViewTextBoxColumn obj211 = new DataGridViewTextBoxColumn();
            obj211.DataPropertyName = "Color";
            obj211.HeaderText = "Color";
            obj211.Width = 105;
            dgvProduct.Columns.Add(obj211);


            DataGridViewTextBoxColumn obj21 = new DataGridViewTextBoxColumn();
            obj21.DataPropertyName = "ColorNo";
            obj21.HeaderText = "Color No";
            obj21.Width = 105;
            dgvProduct.Columns.Add(obj21);

            DataGridViewTextBoxColumn obj22 = new DataGridViewTextBoxColumn();
            obj22.DataPropertyName = "Idesc";
            obj22.HeaderText = "L/D Ref";
            obj22.Width = 105;
            dgvProduct.Columns.Add(obj22);
            DataGridViewTextBoxColumn obj2 = new DataGridViewTextBoxColumn();
            obj2.DataPropertyName = "ColorTone";
            obj2.HeaderText = "Color Tone";
            obj2.Width = 105;
            dgvProduct.Columns.Add(obj2);

            DataGridViewTextBoxColumn obj3 = new DataGridViewTextBoxColumn();
            obj3.DataPropertyName = "Size";
            obj3.HeaderText = "Size";
            obj3.Width = 70;
            dgvProduct.Columns.Add(obj3);


            DataGridViewTextBoxColumn obj4 = new DataGridViewTextBoxColumn();
            obj4.DataPropertyName = "Qty";
            obj4.HeaderText = "Qty";
            obj4.Width = 60;
            dgvProduct.Columns.Add(obj4);


            DataGridViewTextBoxColumn obj5 = new DataGridViewTextBoxColumn();
            obj5.DataPropertyName = "UnitPrice";
            obj5.HeaderText = "UnitPrice";
            obj5.Width = 80;
            dgvProduct.Columns.Add(obj5);


            DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
            dgvProduct.Columns.Add(btn);
            btn.HeaderText = "Delete";
            btn.Text = "Delete";
            btn.Name = "btn";
            btn.Width = 55;
            btn.UseColumnTextForButtonValue = true;


        }
        private void OrderLoad()
        {
            if (objOrder != null)
            {
                LoadGridProductSetting();
                liOrderProduct = new Order_DL().GetAllOrderProduct(txtOrder.Text);

                dgvData.DataSource = liOrderProduct;
                

                
            }

        }
        private void btnOrder_Click(object sender, EventArgs e)
        {

           
        }

        private void miEdit_Click(object sender, EventArgs e)
        {
            EditData();
        }
        private void LoadProductData()
        {
            if (objOrderProduct != null)
            {


                //  liOPD = new Order_DL().GetAllOrderProductDetails(objOrderProduct.OPID);
                LoadGridSetting();
                List<EOrderProductDetails> liob = new Order_DL().GetAllOrderProductDetails(objOrderProduct.OPID);
                if (liob.Count == 0)
                {
                    dgvProduct.Rows.Add(1);
                }
                else
                {
                    dgvProduct.Rows.Add(liob.Count);
                }
                int i = 0;
                foreach (EOrderProductDetails ob in liob)
                {
                    
                    dgvProduct.Rows[i].Cells[0].Value = ob.OPDID.ToString();

                    if (ob.BaseColor != null)
                    {
                        dgvProduct.Rows[i].Cells[1].Value = ob.BaseColor.ToString();
                    }
                    if (ob.FRNo != null)
                    {
                        dgvProduct.Rows[i].Cells[2].Value = ob.FRNo.ToString();
                    }
                    if (ob.Color != null)
                    {
                        dgvProduct.Rows[i].Cells[3].Value = ob.Color.ToString();
                    }
                    if (ob.ColorNo != null)
                    {
                        dgvProduct.Rows[i].Cells[4].Value = ob.ColorNo.ToString();
                    }
                    if (ob.LDFRef != null)
                    {
                        dgvProduct.Rows[i].Cells[5].Value = ob.LDFRef.ToString();
                    }
                    if (ob.ColorTone != null)
                    {
                        dgvProduct.Rows[i].Cells[6].Value = ob.ColorTone.ToString();
                    }
                    if (ob.Size != null)
                    {
                        dgvProduct.Rows[i].Cells[7].Value = ob.Size.ToString();
                    }
                    //if (ob.Qty != null)
                    //{
                        dgvProduct.Rows[i].Cells[8].Value = ob.Qty.ToString();
                    //}
                    //if (ob.UnitPrice != null)
                    //{
                        dgvProduct.Rows[i].Cells[9].Value = ob.UnitPrice.ToString();
                    //}

                    i++;
                }

            }
        }
        private void EditData()
        {
            try
            {
                if (dgvData.SelectedRows.Count > 0)
                {
                    DataGridViewRow currentRow = dgvData.SelectedRows[0];
                    int ID = Convert.ToInt32(currentRow.Cells[0].Value);
                    objOrderProduct = liOrderProduct.SingleOrDefault(o => o.OPID == ID);
                    if (objOrderProduct != null)
                    {
                        cboDescription.Text = objOrderProduct.Description;
                        cboNotes.Text = objOrderProduct.Notes;
                        cboUnit.Text = objOrderProduct.Unit;
                        txtUPDown.Text = objOrderProduct.UPDown.ToString();
                        txtWeight.Text = objOrderProduct.Weight;
                        txtWidth.Text = objOrderProduct.Width;
                        txtWeightText.Text = objOrderProduct.WeightOption;
                        txtFRNo.Text = objOrderProduct.FRNO;
                        txtOpenTube.Text = objOrderProduct.OpenTube;
                        chkExtra.Checked = objOrderProduct.ISExtraQty;

                        //  liOPD = new Order_DL().GetAllOrderProductDetails(objOrderProduct.OPID);
                       LoadProductData();

                    ////       objc.OPDID = UtilityService.ConvertToNumber(ob.Cells[0].Value.ToString());
                    ////}
                    ////objc.Color = ob.Cells[1].Value.ToString();
                    ////if (ob.Cells[2].Value != null)
                    ////{
                    ////    objc.ColorNo = ob.Cells[2].Value.ToString();
                    ////}
                    ////if (ob.Cells[4].Value != null)
                    ////{
                    ////    objc.ColorTone = ob.Cells[4].Value.ToString();
                    ////}
                    ////if (ob.Cells[3].Value != null)
                    ////{
                    ////    objc.Idesc = ob.Cells[3].Value.ToString();
                    ////}
                    
                    ////objc.OPID = OPID;
                    ////if (ob.Cells[5].Value != null)
                    ////{
                    ////    objc.Size = ob.Cells[5].Value.ToString();
                    ////}
                    ////objc.Qty = UtilityService.ConvertToDouble(ob.Cells[6].Value.ToString());
                    ////objc.UnitPrice = UtilityService.ConvertToDouble(ob.Cells[7].Value.ToString());



                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OrderLoad();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if (objOrder != null)
            //{
            //    EOrderClause objP = new EOrderClause();
            //    objP.PINO = objOrder.PINo;
            //    new Order_DL().DeleteOrderClauses(objP);

            //    foreach (DataGridViewRow ob in dgvClause.Rows)
            //    {
            //        bool res = Convert.ToBoolean(ob.Cells["Select"].Value);
            //        if (res == true)
            //        {
            //            objP = new EOrderClause();
            //            objP.PINO = objOrder.PINo;
            //            objP.clauseID = Convert.ToInt32(ob.Cells["clauseID"].Value);
            //            objP.Sequence = UtilityService.ConvertToNumber(ob.Cells["Sequence"].Value.ToString());
            //            objP.ClauseName = ob.Cells["ClauseName"].Value.ToString();
            //            new Order_DL().InsertOrderClauses(objP);
            //        }
            //    }
            //    MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
           
            //}
        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditData();
            lblHead.Text = "Update Product Info";
        }

        private void dgvProduct_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (IsReadOnlyForm == true)
            {
                MessageBox.Show("You can not delete this Record, You Only View this Reord");
            }
            else
            {
                if (dgvProduct.CurrentRow.Cells[1].Value != null)
                {
                    string s = dgvProduct.CurrentRow.Cells[1].Value.ToString();
                    if (MessageBox.Show("Do You want to Delete Color :" + s, "Delete Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        dgvProduct.Rows.Remove(dgvProduct.CurrentRow);
                    }
                }
            }
        }

        private void dgvProduct_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
           // e.Row.Cells[0].Value = dgvProduct.Rows.Count.ToString();
        }

        private void btnOrderProductSave_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (objOrder != null)
                {
                    OrderProductSave();
                }
                else
                {
                    MessageBox.Show("Save order First, Then Details", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {


            }
            this.Cursor = Cursors.Default;
        }
        private Int64 ToNumber(string Val)
        {
            Int64 value;

            try
            {
                value = Convert.ToInt64(Val);
                return value;

            }
            catch
            {
                return 0;
            }

        }
        private void OrderProductSave()
        {
            EOrderProduct objc = new EOrderProduct();

            if (ToNumber(txtWeight.Text) == 0)
            {
                MessageBox.Show("Weight must be Given");
                txtWeight.Focus();
                return;
            }
            if (ToNumber(txtWidth.Text) == 0)
            {
                MessageBox.Show("Width must be Given");
                txtWidth.Focus();
                return;
            }
            if (dgvProduct.Rows.Count == 0)
            {
                MessageBox.Show("Item Color has not given");
                dgvData.Focus();
                return;
            }
            if (dgvProduct.Rows[0].Cells[8].Value == null)
            {
                MessageBox.Show("Item Qty has not given");
                dgvData.Focus();
                return;
            }
            objc.PINO = txtOrder.Text.Trim();
            
            if (objOrderProduct != null && objOrderProduct.OPID > 0)
            {
                objc.OPID = objOrderProduct.OPID;
            }
            objc.Description = cboDescription.Text.Trim();

            objc.Notes = cboNotes.Text;
            objc.Width = txtWidth.Text;
            objc.WeightOption = txtWeightText.Text;
            objc.FRNO = txtFRNo.Text;
            objc.OpenTube = txtOpenTube.Text;
            objc.UPDown = UtilityService.ConvertToDouble(txtUPDown.Text);
            objc.Weight = txtWeight.Text.Trim();
            objc.Unit = cboUnit.Text;
            objc.ISExtraQty = chkExtra.Checked;
            objc.EMode = "E";
            objc.UserID = UserSession.CurrentUser;
            string orderID = "0";

            bool res = new Order_DL().InsertUpdateOrderProduct(objc, ref orderID);

            if (res == true)
            {
                
                if (objOrderProduct != null && objOrderProduct.OPID > 0)
                {
                    objc.OPID = objOrderProduct.OPID;
                }
                else
                {
                    objc.OPID = Convert.ToInt32(orderID);
                }
                //objOrderProduct = objc;
                objOP = objc;
                liOPD = new List<EOrderProductDetails>();
                SaveOPD(objc.OPID);

               
                ////bool result=true;
                //if (liOPD.Count > 0)
                //{
                //    if (chkServer.Checked == true)
                //    {
                //        result = new FactoryOperation_DL().OrderProductDetails(objOP, liOPD);
                //    }
                   
                    
                //}
                //if (result == true)
                //{
                    dgvData.DataSource = null;
                    objOrderProduct = null;
                    OrderLoad();
                    //LoadGridSetting();
                    txtWeight.Text = string.Empty;
                    txtWidth.Text = string.Empty;
                    txtFRNo.Text = string.Empty;
                    txtUPDown.Text = string.Empty;
                    ClearProduct();
                    MessageBox.Show("Product Save Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ////}
                ////else
                ////{
                ////    MessageBox.Show("Order Product has not updated on Factory Server.But Data Save Succcessfully in Head Office", "Result", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ////}
            }
            else
            {

                MessageBox.Show("Data not Saved", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);


            }


        }
        private void OrderProductDetailsOnlySave()
        {
            EOrderProduct objc = new EOrderProduct();

            
            
            
            
                if (objOrderProduct != null && objOrderProduct.OPID > 0)
                {
                    objc.OPID = objOrderProduct.OPID;
                    objOP = objc;
                    liOPD = new List<EOrderProductDetails>();
                    SaveOPDOnlyDetails(objc.OPID);


                    dgvData.DataSource = null;
                    objOrderProduct = null;
                    OrderLoad();
                    LoadGridSetting();
                    txtWeight.Text = string.Empty;
                    txtWidth.Text = string.Empty;
                    txtFRNo.Text = string.Empty;
                    txtUPDown.Text = string.Empty;

                    MessageBox.Show("Product Save Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
               
                //objOrderProduct = objc;
               
               
           


        }
        private void Clear()
        {
            //LoadGridSetting();
            dgvData.DataSource = null;
            dgvProduct.DataSource = null;
            objOrder = null;
            objOrderProduct = null;
            txtWeightText.Text = string.Empty;
            txtFRNo.Text = string.Empty;
            txtUPDown.Text = string.Empty;
            txtWeight.Text = string.Empty;
            txtWidth.Text = string.Empty;
            cboDescription.Text= string.Empty;
            cboNotes.Text = string.Empty;
            chkExtra.Checked = false;

        }
        private void ClearProduct()
        {
            //LoadGridSetting();
           
            dgvProduct.DataSource = null;
            LoadGridSetting();
            objOrderProduct = null;
            txtWeightText.Text = string.Empty;
            txtFRNo.Text = string.Empty;
            txtUPDown.Text = string.Empty;
            txtWeight.Text = string.Empty;
            txtWidth.Text = string.Empty;
            cboDescription.Text = string.Empty;
            cboNotes.Text = string.Empty;
            chkExtra.Checked = false;

        }
        private void SaveOPD(int OPID)
        {

            //new Order_DL().DeleteOrderProductDetails(OPID);
            EOrderProductDetails objc = new EOrderProductDetails();
          
            foreach (DataGridViewRow ob in dgvProduct.Rows)
            {

                string orderID = "0";
                if (ob.Cells[8].Value != null)
                {
                    objc = new EOrderProductDetails();
                    if (ob.Cells[0].Value != null && UtilityService.ConvertToNumber(ob.Cells[0].Value.ToString()) > 0)
                    {
                        objc.OPDID = UtilityService.ConvertToNumber(ob.Cells[0].Value.ToString());
                    }
                    if (ob.Cells[1].Value != null)
                    {
                        objc.BaseColor = ob.Cells[1].Value.ToString();
                    }
                    if (ob.Cells[2].Value != null)
                    {
                        objc.FRNo = ob.Cells[2].Value.ToString();
                    }

                    if (ob.Cells[3].Value != null)
                    {
                        objc.Color = ob.Cells[3].Value.ToString();
                    }

                    if (ob.Cells[4].Value != null)
                    {
                        objc.ColorNo = ob.Cells[4].Value.ToString();
                    }
                    if (ob.Cells[6].Value != null)
                    {
                        objc.ColorTone = ob.Cells[6].Value.ToString();
                    }
                    if (ob.Cells[5].Value != null)
                    {
                        objc.LDFRef = ob.Cells[5].Value.ToString();
                    }

                    objc.OPID = OPID;
                    if (ob.Cells[7].Value != null)
                    {
                        objc.Size = ob.Cells[7].Value.ToString();
                    }
                    objc.PINO = txtOrder.Text.Trim();
                    objc.Qty = UtilityService.ConvertToDouble(ob.Cells[8].Value.ToString());
                    objc.UnitPrice = UtilityService.ConvertToDouble(ob.Cells[9].Value.ToString());
                    objc.EMode = "E";
                    objc.UserID = UserSession.CurrentUser;

                    if (ob.Cells[3].Value != null && objc.Qty > 0)
                    {
                        bool res = new Order_DL().InsertUpdateOrderProductDetails(objc, ref orderID);

                        if (res == true)
                        {
                            objc.OPDID = Convert.ToInt32(orderID);
                            liOPD.Add(objc);
                        }
                    }
                }
            }

           
          



        }
        private void SaveOPDOnlyDetails(int OPID)
        {

            //new Order_DL().DeleteOrderProductDetails(OPID);
            EOrderProductDetails objc = new EOrderProductDetails();

            foreach (DataGridViewRow ob in dgvProduct.Rows)
            {

                string orderID = "0";
                if (ob.Cells[8].Value != null && UtilityService.ConvertToDouble(ob.Cells[8].Value.ToString())>0)
                {

                    objc = new EOrderProductDetails();
                    if (ob.Cells[0].Value != null && UtilityService.ConvertToNumber(ob.Cells[0].Value.ToString()) > 0)
                    {
                        objc.OPDID = UtilityService.ConvertToNumber(ob.Cells[0].Value.ToString());
                    }
                    if (ob.Cells[1].Value != null)
                    {
                        objc.BaseColor = ob.Cells[1].Value.ToString();
                    }
                    if (ob.Cells[2].Value != null)
                    {
                        objc.FRNo = ob.Cells[2].Value.ToString();
                    }

                    if (ob.Cells[3].Value != null)
                    {
                        objc.Color = ob.Cells[3].Value.ToString();
                    }

                    if (ob.Cells[4].Value != null)
                    {
                        objc.ColorNo = ob.Cells[4].Value.ToString();
                    }
                    if (ob.Cells[6].Value != null)
                    {
                        objc.ColorTone = ob.Cells[6].Value.ToString();
                    }
                    if (ob.Cells[5].Value != null)
                    {
                        objc.LDFRef = ob.Cells[5].Value.ToString();
                    }

                    objc.OPID = OPID;
                    if (ob.Cells[7].Value != null)
                    {
                        objc.Size = ob.Cells[7].Value.ToString();
                    }
                    objc.PINO = txtOrder.Text.Trim();
                    objc.Qty = UtilityService.ConvertToDouble(ob.Cells[8].Value.ToString());
                    objc.UnitPrice = UtilityService.ConvertToDouble(ob.Cells[9].Value.ToString());
                    objc.EMode = "E";
                    objc.UserID = UserSession.CurrentUser;
                    objc.ISUnAuthorizedSave = true;
                    if (ob.Cells[3].Value != null && objc.Qty > 0)
                    {
                        bool res = new Order_DL().InsertUpdateOrderProductDetails(objc, ref orderID);

                        if (res == true)
                        {
                            objc.OPDID = Convert.ToInt32(orderID);
                            liOPD.Add(objc);
                        }
                    }
                }
                
            }

           
             new FactoryOperation_DL().OrderProductDetailsOthersUpdate(liOPD);
            




        }
        private void dgvClause_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtOrder_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtMainBuyer_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dgvProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if ((e.RowIndex > -1) && (e.ColumnIndex > -1))
                {
                    if ((dgvProduct.Columns[e.ColumnIndex].GetType().Equals(typeof(DataGridViewButtonColumn))) ||
                        (dgvProduct.Columns[e.ColumnIndex].GetType().Equals(typeof(DataGridViewButtonColumn))))
                    {
                        if (IsReadOnlyForm == true)
                        {
                            MessageBox.Show("You can not delete this Record, You Only View this Reord");
                        }
                        else
                        {
                            if (dgvProduct.CurrentRow.Cells != null)
                            {
                                string s = dgvProduct.CurrentRow.Cells[3].Value.ToString();
                                if (MessageBox.Show("Do You want to Delete Color :" + s, "Delete Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                                {
                                    if (dgvProduct.CurrentRow.Cells[0].Value == null)
                                    {
                                        dgvProduct.Rows.Remove(dgvProduct.CurrentRow);
                                    }
                                    else
                                    {
                                        EObject obj = new EObject();
                                        obj.ID = Convert.ToInt32(dgvProduct.CurrentRow.Cells[0].Value.ToString());
                                        obj.FormID = "OrderProductDetails";
                                        bool res = new Common_DL().DeleteObject(obj);
                                        if (res == true)
                                        {
                                            LoadProductData();
                                        }
                                        else
                                        {
                                            MessageBox.Show("Delete operation Failed");
                                        }
                                    }

                                    //dgvProduct.Rows.Remove(dgvProduct.CurrentRow);
                                }
                            }
                        }
                    }

                }
            }
            catch
            {

            }
       }


        private void dgvProduct_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvProduct_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

            //StringFormat sf=new StringFormat();
            //sf.Alignment=StringAlignment.Center;
            //if (e.ColumnIndex < 0 && e.RowIndex >= 0 && e.RowIndex < dgvProduct.Rows.Count)
            //{

            //    e.PaintBackground(e.ClipBounds, true);
            //    e.Graphics.DrawString((e.RowIndex + 1).ToString, this.Font, Brushes.Black,System.Drawing.Rectangle);
            //    e.Handled = true;

            //}
        //    Dim sf As New StringFormat
        //sf.Alignment = StringAlignment.Center
        //If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < dt.Rows.Count Then
        //    e.PaintBackground(e.ClipBounds, True)
        //    e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
        //    e.Handled = True
        //End If
        }

        private void dgvProduct_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgvProduct.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            int rowNumber = 1;
            foreach (DataGridViewRow row in dgvProduct.Rows)
            {
                if (row.IsNewRow) continue;
                row.HeaderCell.Value = "P:" + rowNumber;
                rowNumber = rowNumber + 1;
            }
            dgvProduct.AutoResizeRowHeadersWidth(
                DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Description obj = new Description();
            obj.ShowDialog();
            cboDescription.DataSource = new Common_DL().GetAllDescription();
            cboDescription.DisplayMember = "DescriptionName";
            cboDescription.ValueMember = "ID";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Customerinfo obj = new Customerinfo();
            obj.ShowDialog();
            cboCustomer.DataSource = new Common_DL().GetAllCustomer();
            cboCustomer.DisplayMember = "CustomerName";
            cboCustomer.ValueMember = "ID";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Colors obj = new Colors();
            obj.ShowDialog();
            LoadGridSetting();
        }

        private void dgvData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        
                if ((e.RowIndex > -1) && (e.ColumnIndex > -1))
                {
                    if ((dgvData.Columns[e.ColumnIndex].GetType().Equals(typeof(DataGridViewButtonColumn))) ||
                        (dgvData.Columns[e.ColumnIndex].GetType().Equals(typeof(DataGridViewButtonColumn))))
                    {
                        if (IsReadOnlyForm == true)
                        {
                            MessageBox.Show("You can not delete this Record, You Only View this Reord");
                        }
                        else
                        {
                            if (dgvData.CurrentRow.Cells[1].Value != null)
                            {
                                string s = dgvData.CurrentRow.Cells[1].Value.ToString();
                                if (MessageBox.Show("Do You want to Delete Product :" + s, "Delete Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                                {
                                    EObject obj = new EObject();
                                    obj.ID = Convert.ToInt32(dgvData.CurrentRow.Cells[0].Value.ToString());
                                    obj.FormID = "ORDER_PRODUCT";
                                    bool res = new Common_DL().DeleteObject(obj);
                                    if (res == true)
                                    {
                                        LoadProductData();
                                    }
                                    else
                                    {
                                        MessageBox.Show("Delete operation Failed");
                                    }
                                }
                            }
                        }
                    }

                }
           
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string val =cboType.SelectedValue.ToString();
            if (val != "0")
            {
                cboDescription.DataSource = liDes.FindAll(o => o.TypeID == val);
                
            }
            else
            {
                cboDescription.DataSource = liDes;
            }
            cboDescription.DisplayMember = "DescriptionName";
            cboDescription.ValueMember = "ID";
        }

        private void printViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (EPermission.View_Print == true)
            {
                EOrderReport obj = new EOrderReport();
                obj.PINO = txtOrder.Text.Trim();
                obj.ExtraQty = false;


                ReportCalling.ProformaInvoice(obj);
            }
            else
            {

                MessageBox.Show("You dont Have Enough Permission To View and Print this Order");
            }
        }

        private void dgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void miDelete_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow ob in dgvClause.Rows)
            //{
            //    ob.Cells["Select"].Value = Select;
            //    //bool res = Convert.ToBoolean(ob.Cells["Select"].Value);

            //}
            //if (Select == false)
            //{
            //    Select = true;
            //}
            //else
            //{
            //    Select = false;
            //}
        }

        private void cmdSaveOrder_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void miClear_Click(object sender, EventArgs e)
        {
            Clear();
            txtOrder.Text = string.Empty;
            txtLCNo.Text = string.Empty;
            txtMainBuyer.Text = string.Empty;

            
            cboCustomer.SelectedValue = string.Empty;
         
            cboOrderType.SelectedValue = string.Empty;
            txtBuyerRef.Text = string.Empty;
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Clause obj = new Clause();
            //obj.ShowDialog();
            //dgvClause.DataSource = new Order_DL().GetAllOrderClause(txtOrder.Text);
            //dgvClause.Columns[0].Visible = false;
            //dgvClause.Columns[3].Visible = false;
        }

        private void updateFactoryDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                string PINO = txtOrder.Text;
                EOrder _objOrder = new Order_DL().GetAllOrderBYPINO(PINO);
                List<EOrderProduct> li = new List<EOrderProduct>();
                int count = 0;
                int totalRec = 1;
                if (_objOrder != null)
                {
                    bool facRes = new FactoryOperation_DL().InsertApprovedOrder(objOrder);
                    if (facRes == true)
                    {
                        count = 1;
                        li = new Order_DL().GetAllOrderProduct(_objOrder.PINo);
                        totalRec = totalRec + li.Count;
                        bool res;
                        foreach (EOrderProduct obj in li)
                        {

                            List<EOrderProductDetails> liob = new Order_DL().GetAllOrderProductDetails(obj.OPID);

                            if (liob != null)
                            {

                                res = new FactoryOperation_DL().OrderProductDetails(obj, liob);
                                if (res == true)
                                {
                                    count = count + 1;
                                }
                            }
                        }
                    }
                }
                if (count == totalRec)
                {
                    MessageBox.Show("Factory Database updated Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Update Failed ! Please Try Again", "Result", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch
            {

            }
            this.Cursor = Cursors.Default;

        }

        private void viewClauseListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (txtOrder.Text.Length > 6 && dgvData.Rows.Count > 0)
            {
                OrderClause obj = new OrderClause(txtOrder.Text, cboCustomer.Text);
                obj.ShowDialog();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (objOrder != null)
                {
                    OrderProductDetailsOnlySave();
                }
                else
                {
                    MessageBox.Show("Save order First, Then Details", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {


            }
            this.Cursor = Cursors.Default;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvData.CurrentRow.Cells[1].Value != null)
            {
                string s = dgvData.CurrentRow.Cells[1].Value.ToString();
                if (MessageBox.Show("Do You want to Copy Product :" + s, "Copy Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    DataGridViewRow currentRow = dgvData.SelectedRows[0];
                    int ID = Convert.ToInt32(currentRow.Cells[0].Value);
                    objOrderProduct = liOrderProduct.SingleOrDefault(o => o.OPID == ID);
                    
                    //objOrderProduct = objLocal;
                    if (objOrderProduct != null)
                    {
                       // cboDescription.Text = objOrderProduct.Description;
                        cboNotes.Text = objOrderProduct.Notes;
                        cboUnit.Text = objOrderProduct.Unit;
                        txtUPDown.Text = objOrderProduct.UPDown.ToString();
                        txtWeightText.Text = objOrderProduct.WeightOption.ToString();
                        txtWeight.Text = objOrderProduct.Weight;
                        txtWidth.Text = objOrderProduct.Width;
                        txtFRNo.Text = objOrderProduct.FRNO;
                        txtOpenTube.Text = objOrderProduct.OpenTube;
                        chkExtra.Checked = objOrderProduct.ISExtraQty;
                    }
                    LoadProductData();
                    objOrderProduct = null;
                    foreach (DataGridViewRow ob in dgvProduct.Rows)
                    {

                       
                        if (ob.Cells[8].Value != null)
                        {
                           
                            if (ob.Cells[0].Value != null )
                            {
                                ob.Cells[0].Value="0";
                            }

                        }

                    }
                }
            }
            lblHead.Text = "New Product Entry";
        }

        private void dgvData_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (liOrderProduct != null)
            {
                dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
                int rowNumber = 1;
                foreach (DataGridViewRow row in dgvData.Rows)
                {
                    if (row.IsNewRow) continue;
                    row.HeaderCell.Value = "" + rowNumber;
                    rowNumber = rowNumber + 1;
                }
                dgvData.AutoResizeRowHeadersWidth(
                    DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);
            }
        }

        private void printViewExtraQtyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (EPermission.View_Print == true)
            {
                EOrderReport obj = new EOrderReport();
                obj.PINO = txtOrder.Text.Trim();
                obj.ExtraQty = true;


                ReportCalling.ProformaInvoice(obj);
            }
            else
            {

                MessageBox.Show("You dont Have Enough Permission To View and Print this Order");
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            ClearProduct();
        }

        private void updateDeliveryAddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (txtOrder.Text.Trim().Length > 3 && cboCustomer.SelectedIndex>0)
            {
                Order_Delivery_Info o = new Order_Delivery_Info(txtOrder.Text.Trim(), cboCustomer.Text);
                o.ShowDialog();
            }
        }

       
       
        //end OnRowPostPaint method
    }
}
