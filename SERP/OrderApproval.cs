﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
using Services;
namespace OMS
{
    public partial class OrderApproval : Form
    {
        EOrderApproval objApprove;
        EOrder objO;
        List<EOrderProduct> liOP;
        List<EOrderProductDetails> liOPD;
        public OrderApproval()
        {
            InitializeComponent();
        }

        public OrderApproval(EOrderApproval objAp)
        {
            InitializeComponent();
            objApprove = objAp;
            if (objApprove != null)
            {
                this.Cursor = Cursors.WaitCursor;
                try
                {
                    txtPIN.Text = objApprove.PINO;
                    objO = new Order_DL().GetAllOrderBYPINO(objApprove.PINO);
                    liOP = new Order_DL().GetAllOrderProduct(objApprove.PINO);
                    liOPD = new Order_DL().GetAllOrderProductDetailsBYPINO(objApprove.PINO);
                }
                catch
                {
                }
                this.Cursor = Cursors.Default;

            }
        }

        private void SaveFactory()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                string PINO = txtPIN.Text;
                EOrder _objOrder = new Order_DL().GetAllOrderBYPINO(PINO);
                List<EOrderProduct> li = new List<EOrderProduct>();
                int count = 0;
                int totalRec = 1;
                if (_objOrder != null)
                {
                    bool facRes = new FactoryOperation_DL().InsertApprovedOrder(_objOrder);
                    if (facRes == true)
                    {
                        count = 1;
                        li = new Order_DL().GetAllOrderProduct(_objOrder.PINo);
                        totalRec = totalRec + li.Count;
                        bool res;
                        foreach (EOrderProduct obj in li)
                        {

                            List<EOrderProductDetails> liob = new Order_DL().GetAllOrderProductDetails(obj.OPID);

                            if (liob != null)
                            {

                                res = new FactoryOperation_DL().OrderProductDetails(obj, liob);
                                if (res == true)
                                {
                                    count = count + 1;
                                }
                            }
                        }
                    }
                }
                if (count == totalRec)
                {

                    EOrderApproval obj = new EOrderApproval();
                    if (objApprove != null && objApprove.ID > 0)
                    {
                        obj.ID = objApprove.ID;
                    }
                    obj.PINO = txtPIN.Text;
                    obj.ApproveFor = cboApproveFor.Text;
                    obj.ApprovalDate = dtpApproveDate.Value;
                    obj.Notes = txtNotes.Text;
                    obj.IsApproved = true;
                    obj.UserID = UserSession.CurrentUser;
                    bool res = false;
                    if (objO != null && liOPD != null && liOP != null)
                    {
                        res = new Order_DL().InsertUpdateOrderApproval(obj);

                    }


                    if (res == true)
                    {
                        bool result = new FactoryOperation_DL().SaveApprovedOrder(obj);
                        if (result == true)
                        {
                            MessageBox.Show("Order Approved and Data Updated in Factory Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Order Not Approved! May Be Factory Server Connection is Failed", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);


                        }
                    }
                    else
                    {
                        MessageBox.Show("Factory Database Operation Failed" , "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                   
                }
                else
                {
                    MessageBox.Show("Update Failed ! Please Try Again", "Result", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Update Failed ! Please Try Again. Error: " + ex.Message.ToString() , "Result", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            this.Cursor = Cursors.Default;
        }
        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                EOrderApproval obj = new EOrderApproval();
                if (objApprove != null && objApprove.ID > 0)
                {
                    obj.ID = objApprove.ID;
                }
                obj.PINO = txtPIN.Text;
                obj.ApproveFor = cboApproveFor.Text;
                obj.ApprovalDate = dtpApproveDate.Value;
                obj.Notes = txtNotes.Text;
                obj.IsApproved = true;
                obj.UserID = UserSession.CurrentUser;
                bool res = false;
                if (objO != null && liOPD != null && liOP != null)
                {
                    res = new Order_DL().InsertUpdateOrderApproval(obj);

                }


                if (res == true)
                {
                    bool result = new FactoryOperation_DL().SaveApprovedOrder(obj);
                    if (result == true)
                    {
                        MessageBox.Show("Order Approve Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    else
                    {
                        MessageBox.Show("Order Not Approved! May Be Factory Server Connection is Failed", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);


                    }
                }
                else
                {


                }
            }
            catch
            {

            }
            this.Cursor = Cursors.Default;
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void miClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            txtPIN.Text = String.Empty;
            objApprove = null;
            txtNotes.Text = string.Empty;

        }

        private void OrderApproval_Load(object sender, EventArgs e)
        {

        }

        private void updateFactoryDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            

        }

        private void btnOrderProductSave_Click(object sender, EventArgs e)
        {
            SaveFactory();
        }
    }
}
