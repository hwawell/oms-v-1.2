﻿namespace OMS
{
    partial class OrderClause
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderClause));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dgvClause = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtClause = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtHead = new System.Windows.Forms.TextBox();
            this.updateFactoryDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miClear = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.button9 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClause)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateFactoryDatabaseToolStripMenuItem,
            this.miClear,
            this.miExit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(932, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dgvClause
            // 
            this.dgvClause.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgvClause.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClause.Location = new System.Drawing.Point(12, 54);
            this.dgvClause.Name = "dgvClause";
            this.dgvClause.Size = new System.Drawing.Size(908, 437);
            this.dgvClause.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 494);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "New Clause";
            // 
            // txtClause
            // 
            this.txtClause.Location = new System.Drawing.Point(12, 510);
            this.txtClause.Multiline = true;
            this.txtClause.Name = "txtClause";
            this.txtClause.Size = new System.Drawing.Size(827, 41);
            this.txtClause.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(845, 509);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 41);
            this.button1.TabIndex = 11;
            this.button1.Text = "Add Clause";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtHead
            // 
            this.txtHead.Location = new System.Drawing.Point(15, 28);
            this.txtHead.Name = "txtHead";
            this.txtHead.ReadOnly = true;
            this.txtHead.Size = new System.Drawing.Size(642, 20);
            this.txtHead.TabIndex = 13;
            // 
            // updateFactoryDatabaseToolStripMenuItem
            // 
            this.updateFactoryDatabaseToolStripMenuItem.Image = global::OMS.Properties.Resources.save;
            this.updateFactoryDatabaseToolStripMenuItem.Name = "updateFactoryDatabaseToolStripMenuItem";
            this.updateFactoryDatabaseToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.updateFactoryDatabaseToolStripMenuItem.Text = "Save Clause ";
            this.updateFactoryDatabaseToolStripMenuItem.Click += new System.EventHandler(this.updateFactoryDatabaseToolStripMenuItem_Click);
            // 
            // miClear
            // 
            this.miClear.Image = ((System.Drawing.Image)(resources.GetObject("miClear.Image")));
            this.miClear.Name = "miClear";
            this.miClear.Size = new System.Drawing.Size(60, 20);
            this.miClear.Text = "&Clear";
            // 
            // miExit
            // 
            this.miExit.Image = ((System.Drawing.Image)(resources.GetObject("miExit.Image")));
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(53, 20);
            this.miExit.Text = "E&xit";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(701, 26);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(105, 24);
            this.button9.TabIndex = 54;
            this.button9.Text = "Select /unselect";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(832, 26);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 24);
            this.button4.TabIndex = 53;
            this.button4.Text = "Add New";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // OrderClause
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 562);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtHead);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtClause);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvClause);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrderClause";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OrderClause";
            this.Load += new System.EventHandler(this.OrderClause_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClause)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem updateFactoryDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miClear;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.DataGridView dgvClause;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtClause;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtHead;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button4;
    }
}