﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Entities;
using OMS.Basic_Operation;

namespace OMS
{
    public partial class OrderClause : Form
    {
        bool Select=true;
        string OrderNo;
        public OrderClause()
        {
            InitializeComponent();
        }
        public OrderClause(string PINO,string Customer)
        {
            InitializeComponent();
            txtHead.Text = "Order No: " + PINO + "  Customer : " + Customer;
            OrderNo = PINO;
            LoadClause();

        }
        private void LoadClause()
        {
            dgvClause.DataSource = new Order_DL().GetAllOrderClauseNew(OrderNo);
            dgvClause.Columns[0].Visible = false;
            dgvClause.Columns[3].Visible = false;
            dgvClause.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;

        }
        private void OrderClause_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            EOrderClause objP = new EOrderClause();
            objP.PINO = OrderNo;
            objP.clauseID = 0;
            objP.Sequence = 0;
            objP.ClauseName = txtClause.Text;
            bool res =new Order_DL().InsertOrderClauses(objP);
            if (res == true)
            {
                txtClause.Text = string.Empty;
                    LoadClause();
                MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                MessageBox.Show("Data Failed", "Result", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

            }
        }

        private void updateFactoryDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                txtClause.Focus();
                EOrderClause objP = new EOrderClause();
                objP.PINO = OrderNo;
                new Order_DL().DeleteOrderClauses(objP);

                foreach (DataGridViewRow ob in dgvClause.Rows)
                {
                    bool res = Convert.ToBoolean(ob.Cells["Select"].Value);
                    if (res == true)
                    {
                        objP = new EOrderClause();
                        objP.PINO = OrderNo;
                        objP.clauseID = Convert.ToInt32(ob.Cells["clauseID"].Value);
                        objP.Sequence = UtilityService.ConvertToNumber(ob.Cells["Sequence"].Value.ToString());
                        objP.ClauseName = ob.Cells["ClauseName"].Value.ToString();
                        new Order_DL().InsertOrderClauses(objP);
                    }
                }
              
                MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch
            {

                MessageBox.Show("Save Failed", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow ob in dgvClause.Rows)
            {
                ob.Cells["Select"].Value = Select;
                //bool res = Convert.ToBoolean(ob.Cells["Select"].Value);

            }
            if (Select == false)
            {
                Select = true;
            }
            else
            {
                Select = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Clause obj = new Clause();
            obj.ShowDialog();
            LoadClause();
        }
    }
}
