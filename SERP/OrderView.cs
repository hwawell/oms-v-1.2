﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Services;
using Entities;
namespace OMS
{
    public partial class OrderView : Form
    {
        List<EOrder> liOrder;
        List<EOrder> liOrderSelected;
        public OrderView()
        {
            InitializeComponent();
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            Order obj = new Order();
            obj.ShowDialog();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
        }
        private void LoadOrder()
        {
            liOrder=new Order_DL().GetAllOrder(UserSession.CurrentUser).FindAll(o=> o.CustomerName !=null);

            if (cboSearch.Text== "Order No")

            {
                liOrderSelected=liOrder.FindAll(o => o.PINo.Contains(txtSearch.Text));
                dgvData.DataSource = liOrderSelected;
            }
            else if (cboSearch.Text == "Customer")
            {
                liOrderSelected = liOrder.FindAll(o => o.CustomerName !=null && o.CustomerName.Contains(txtSearch.Text));
                dgvData.DataSource = liOrderSelected;
            }
            else
            {
                liOrderSelected = liOrder;
                dgvData.DataSource = liOrderSelected;
            }

            dgvData.Columns[0].Visible = false;
            dgvData.Columns[2].Visible = false;

        }
        private void EditData()
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                DataGridViewRow currentRow = dgvData.SelectedRows[0];
                int ID = Convert.ToInt32(currentRow.Cells["ID"].Value);
                Order objDesg = new Order(liOrder, liOrder.SingleOrDefault(o => o.ID == ID),true);
                objDesg.ShowDialog();
               
            }
        }

        private void miEdit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void miDelete_Click(object sender, EventArgs e)
        {

        }

        private void dgvData_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditData();
        }

        private void dgvData_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //string strColumnName = dgvData.Columns[e.ColumnIndex].Name;
            //SortOrder strSortOrder = getSortOrder(e.ColumnIndex);

            //liOrder.Sort(new StudentComparer(strColumnName, strSortOrder));
            //dgvData.DataSource = null;
            //dgvData.DataSource = liOrderSelected;
            ////customizeDataGridView();
            //dgvData.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = strSortOrder;

        }
        //private SortOrder getSortOrder(int columnIndex)
        //{
        //    //if (dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection == SortOrder.None ||
        //    //    dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection == SortOrder.Descending)
        //    //{
        //    //    dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
        //    //    return SortOrder.Ascending;
        //    //}
        //    //else
        //    //{
        //    //    dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;
        //    //    return SortOrder.Descending;
        //    //}
        //}

        private void OrderView_Load(object sender, EventArgs e)
        {

        }

        private void dgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnNotApprove_Click(object sender, EventArgs e)
        {
            LoadOrder();

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
           // dgvData.DataSource = new Order_DL().GetAllOrderListForApproval(Convert.ToInt32(cboOrderType.SelectedValue)).FindAll(o => o.IsApproved == true);
           
        }
    }
  
}
