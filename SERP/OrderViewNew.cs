﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using DataLayer;
namespace OMS
{
      public partial class OrderViewNew : Form
    {
        List<EOrderApprovalView> liOrder;
        List<EOrderApprovalView> liOrderSelected;
         bool IsNotApprove;
         public OrderViewNew()
        {
            InitializeComponent();
        }

        private void ApprovalView_Load(object sender, EventArgs e)
        {
            cboOrderType.DataSource = new Order_DL().GetAllOrderType();
            cboOrderType.DisplayMember = "OrderTypeName";
            cboOrderType.ValueMember = "ID";
            List<string> li = new List<string>();
            int year = System.DateTime.Now.Year;
            li.Add(year.ToString());
            li.Add((year - 1).ToString());
            li.Add((year - 2).ToString());
            li.Add((year - 3).ToString());
            li.Add((year - 4).ToString());
            li.Add((year - 5).ToString());

            cboYear.DataSource = li;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IsNotApprove = false;
            liOrder = new Order_DL().GetAllOrderList(Convert.ToInt32(cboOrderType.SelectedValue), Convert.ToInt32(cboYear.Text)).FindAll(o => o.IsApproved == true);
            dgvData.DataSource = liOrder;
            lblStatus.Text = "List of Approve order . Total :" + dgvData.Rows.Count.ToString();
        }

        private void btnNotApprove_Click(object sender, EventArgs e)
        {
            IsNotApprove = true;
            liOrder = new Order_DL().GetAllOrderList(Convert.ToInt32(cboOrderType.SelectedValue), Convert.ToInt32(cboYear.Text)).FindAll(o => o.IsApproved == false);
            dgvData.DataSource = liOrder;
            //dgvData.DataSource = new Order_DL().GetAllOrderList(Convert.ToInt32(cboOrderType.SelectedValue)).FindAll(o => o.IsApproved == false);
            lblStatus.Text = "List of Pending order For Approval . Total :" + dgvData.Rows.Count.ToString();

        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditOrder();
        }

        
        private void EditOrder()
        {

            if (dgvData.SelectedRows.Count > 0)
            {
                DataGridViewRow currentRow = dgvData.SelectedRows[0];
                string PINO = currentRow.Cells["PINO"].Value.ToString();
                EOrder objOrder = new Order_DL().GetAllOrderBYPINO(PINO);
                Order objDesg = new Order(null, objOrder, false);
                objDesg.ShowDialog();

            }
                
          
           
            
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            EditOrder();

        }

        private void miClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void viewOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void miAdd_Click_1(object sender, EventArgs e)
        {
            Order obj = new Order();
            obj.ShowDialog();
        }

        private void viewOrderToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            EditOrder();
        }

        private void miClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dgvData_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            

        }
        private SortOrder getSortOrder(int columnIndex)
        {
            if (dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection == SortOrder.None ||
                dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection == SortOrder.Descending)
            {
                dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                return SortOrder.Ascending;
            }
            else
            {
                dgvData.Columns[columnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                return SortOrder.Descending;
            }
        }

        private void dgvData_ColumnHeaderMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            string strColumnName = dgvData.Columns[e.ColumnIndex].Name;
            SortOrder strSortOrder = getSortOrder(e.ColumnIndex);

            liOrder.Sort(new StudentComparer(strColumnName, strSortOrder));
            dgvData.DataSource = null;
            dgvData.DataSource = liOrder;
            //customizeDataGridView();
            dgvData.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = strSortOrder;
        }
    }

      class StudentComparer : IComparer<EOrderApprovalView>
      {
          string memberName = string.Empty; // specifies the member name to be sorted
          SortOrder sortOrder = SortOrder.None; // Specifies the SortOrder.

          /// <summary>
          /// constructor to set the sort column and sort order.
          /// </summary>
          /// <param name="strMemberName"></param>
          /// <param name="sortingOrder"></param>
          public StudentComparer(string strMemberName, SortOrder sortingOrder)
          {
              memberName = strMemberName;
              sortOrder = sortingOrder;
          }

          /// <summary>
          /// Compares two Students based on member name and sort order
          /// and return the result.
          /// </summary>
          /// <param name="Student1"></param>
          /// <param name="Student2"></param>
          /// <returns></returns>
          public int Compare(EOrderApprovalView Student1, EOrderApprovalView Student2)
          {
      //         
     
              int returnValue = 1;
              switch (memberName)
              {
                  case "ApproveFor":
                      if (sortOrder == SortOrder.Ascending)
                      {
                          returnValue = Student1.ApproveFor.CompareTo(Student2.ApproveFor);
                      }
                      else
                      {
                          returnValue = Student2.ApproveFor.CompareTo(Student1.ApproveFor);
                      }

                      break;

                  case "ApprovalDate":
                      if (sortOrder == SortOrder.Ascending)
                      {
                          returnValue = Student1.ApprovalDate.CompareTo(Student2.ApprovalDate);
                      }
                      else
                      {
                          returnValue = Student2.ApprovalDate.CompareTo(Student1.ApprovalDate);
                      }

                      break;
                  case "MerchandiserName":
                      if (sortOrder == SortOrder.Ascending)
                      {
                          returnValue = Student1.MerchandiserName.CompareTo(Student2.MerchandiserName);
                      }
                      else
                      {
                          returnValue = Student2.MerchandiserName.CompareTo(Student1.MerchandiserName);
                      }

                      break;

                  case "Notes":
                      if (sortOrder == SortOrder.Ascending)
                      {
                          returnValue = Student1.Notes.CompareTo(Student2.Notes);
                      }
                      else
                      {
                          returnValue = Student2.Notes.CompareTo(Student1.Notes);
                      }

                      break;
                  case "Customer":
                      if (sortOrder == SortOrder.Ascending)
                      {
                          returnValue = Student1.Customer.CompareTo(Student2.Customer);
                      }
                      else
                      {
                          returnValue = Student2.Customer.CompareTo(Student1.Customer);
                      }

                      break;
                  case "PINO":
                      if (sortOrder == SortOrder.Ascending)
                      {
                          returnValue = Student1.PINO.CompareTo(Student2.PINO);
                      }
                      else
                      {
                          returnValue = Student2.PINO.CompareTo(Student1.PINO);
                      }
                      break;
                  case "OrderType":
                      if (sortOrder == SortOrder.Ascending)
                      {
                          returnValue = Student1.OrderType.CompareTo(Student2.OrderType);
                      }
                      else
                      {
                          returnValue = Student2.OrderType.CompareTo(Student1.OrderType);
                      }
                      break;

                  default:
                      if (sortOrder == SortOrder.Ascending)
                      {
                          returnValue = Student1.PINO.CompareTo(Student2.PINO);
                      }
                      else
                      {
                          returnValue = Student2.PINO.CompareTo(Student1.PINO);
                      }
                      break;
              }
              return returnValue;
          }
      }

     
}
