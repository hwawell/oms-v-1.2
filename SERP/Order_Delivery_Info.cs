﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
namespace OMS
{
    public partial class Order_Delivery_Info : Form
    {
        public Order_Delivery_Info(string OrderNo,string CustomerName)
        {
            InitializeComponent();
            txtOrderNo.Text = OrderNo;
            txtName.Text = CustomerName;

            List<Order_Delivery_Info_GetResult> li = new Order_DL().GetOrderDeliveryInfo(OrderNo);
            if (li != null & li.Count>0)
            {
                txtAddress.Text = li[0].DeliveryAddress;
                txtContactPerson.Text = li[0].DeliveryContactPerson;
                txtTelephone.Text = li[0].DeliveryContactNo;
            }

        }

        private void miSave_Click(object sender, EventArgs e)
        {

            if (txtOrderNo.Text.Trim().Length > 3)
            {
                bool res = new FactoryOperation_DL().UpdateDeliveryAddress(txtOrderNo.Text.Trim(), txtAddress.Text, txtContactPerson.Text, txtTelephone.Text);

                if (res == true)
                {
                    bool final = new Order_DL().UpdateDeliveryAddress(txtOrderNo.Text.Trim(), txtAddress.Text, txtContactPerson.Text, txtTelephone.Text);
                    if (final == true)
                    {
                        MessageBox.Show("Data Updated Successfully");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Failed to Update Factory Database. Try Again");
                    }

                }
                else
                {
                    MessageBox.Show("Failed to Update Factory Database. Try Again");
                }
            }
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
