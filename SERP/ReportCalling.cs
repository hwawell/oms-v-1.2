﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services;
using Reports;
using Entities;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DataLayer;
namespace OMS
{
   public class ReportCalling
   {
       #region HR


       public static void ProformaInvoice(EOrderReport obj)
       {
           rptProformaInvoice objrptAttendenceAll = new rptProformaInvoice();


           int i=0;
           if (obj.ExtraQty == true)
           {
               i = 1;
           }
           else
           {
               i = 0;
           }

           string[,] ParamArray1 = new string[,] { { "@OrderNo", obj.PINO }, { "@IsExtraQty", i.ToString() } };
              
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rptProformaInvoice_ProductInfo_SP").Tables[0];

           DataSet ds = new DataSet();

           string[,] ParamArray2= new string[,] { { "@OrderNo", obj.PINO } };
           DataTable  dt1 = new DataTable();
           dt1 = GetTheReport(ParamArray2, "rptProformaInvoice_CustmerInfo_SP").Tables[0];
           objrptAttendenceAll.Database.Tables[0].SetDataSource(dt1);
           objrptAttendenceAll.Database.Tables[1].SetDataSource(dt);


           DataTable dt2 = new DataTable();
           dt2 = GetTheReport(ParamArray2, "rptProformaInvoice_clauses_SP").Tables[0];
          
           objrptAttendenceAll.Subreports[0].Database.Tables[0].SetDataSource(dt2);
           objrptAttendenceAll.Subreports[0].Database.Tables[1].SetDataSource(dt1);
          
           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptAttendenceAll, false);
           objViewer.Show();


       }
       public static void CommericalInvoice(Int32 CIID)
       {
           AllReport objAllReport = new AllReport();
           rptCI objrptAttendenceAll = new rptCI();
           objrptAttendenceAll.Refresh();
           objrptAttendenceAll.SetParameterValue(0, CIID);
          


           ReportViewer objViewer = new ReportViewer();



           objViewer.SetReportToViwer(objAllReport.GetReport(objrptAttendenceAll), false);

           objViewer.Show();
       }

       public static void PrintForwarding(Int32 No,string [] para)
       {

           rptForwarding objrptDeliveryChallan = new rptForwarding();

           string[,] ParamArray1 = new string[,] { { "@invID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_LC_CI_Forwarding").Tables[0];

      
           objrptDeliveryChallan.SetDataSource(dt);

           for (int i = 0; i < 10; i++)
           {
               ((CrystalDecisions.CrystalReports.Engine.TextObject)objrptDeliveryChallan.ReportDefinition.ReportObjects["Text3"+i.ToString() +""]).Text = para[i];
           }
           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan, false);
           objViewer.Show();
       }
       public static void PrintForwardingAdd(Int32 No, string[] para)
       {

           rptForwardingAdd objrptDeliveryChallan = new rptForwardingAdd();

           string[,] ParamArray1 = new string[,] { { "@invID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_COM_INV").Tables[0];


           objrptDeliveryChallan.SetDataSource(dt);

           //for (int i = 0; i < 10; i++)
           //{
           //    ((CrystalDecisions.CrystalReports.Engine.TextObject)objrptDeliveryChallan.ReportDefinition.ReportObjects["Text3" + i.ToString() + ""]).Text = para[i];
           //}
           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan, false);
           objViewer.Show();
       }

       public static void PrintCI(Int32 No)
       {

           rptLC_CI objrptDeliveryChallan = new rptLC_CI();

           string[,] ParamArray1 = new string[,] { { "@InvID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_COM_INV").Tables[0];

           DataTable dt1 = new DataTable();
           dt1 = GetTheReport(ParamArray1, "rpt_COM_INV_Product").Tables[0];
           objrptDeliveryChallan.SetDataSource(dt);
           objrptDeliveryChallan.Subreports["rptSubPI"].SetDataSource(dt1);

           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan,false);
           objViewer.Show();
       }
       public static void PrintBE(Int32 No)
       {

           rptBE objrptDeliveryChallan = new rptBE();

           string[,] ParamArray1 = new string[,] { { "@InvID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_COM_INV").Tables[0];

           //DataTable dt1 = new DataTable();
           //dt1 = GetTheReport(ParamArray1, "rpt_COM_INV_Product").Tables[0];
           objrptDeliveryChallan.SetDataSource(dt);
           //objrptDeliveryChallan.Subreports["rptCIP"].SetDataSource(dt1);

           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan, false);
           objViewer.Show();
       }
       public static void PrintCO(Int32 No)
       {

           rptLC_CI_CO objrptDeliveryChallan = new rptLC_CI_CO();

           string[,] ParamArray1 = new string[,] { { "@InvID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_COM_INV").Tables[0];

           //DataTable dt1 = new DataTable();
           //dt1 = GetTheReport(ParamArray1, "rpt_COM_INV_Product").Tables[0];
           objrptDeliveryChallan.SetDataSource(dt);
           //objrptDeliveryChallan.Subreports["rptSubPI"].SetDataSource(dt1);

           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan, false);
           objViewer.Show();
       }
       public static void PrintDC(Int32 No,bool IsShow)
       {

           rptLC_CI_DC objrptDeliveryChallan = new rptLC_CI_DC();

           CrystalDecisions.CrystalReports.Engine.TextObject txtReportHeader;
           txtReportHeader = objrptDeliveryChallan.ReportDefinition.ReportObjects["Text7"] as CrystalDecisions.CrystalReports.Engine.TextObject;

           if (IsShow==false)
           {
               txtReportHeader.Text = "";


               txtReportHeader = objrptDeliveryChallan.ReportDefinition.ReportObjects["Text22"] as CrystalDecisions.CrystalReports.Engine.TextObject;
               txtReportHeader.Text = "";

               txtReportHeader = objrptDeliveryChallan.ReportDefinition.ReportObjects["Text29"] as CrystalDecisions.CrystalReports.Engine.TextObject;
               txtReportHeader.ObjectFormat.EnableSuppress = true;

           }
           string[,] ParamArray1 = new string[,] { { "@InvID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_COM_INV").Tables[0];

           DataTable dt1 = new DataTable();
           dt1 = GetTheReport(ParamArray1, "rpt_COM_INV_Product").Tables[0];
           objrptDeliveryChallan.SetDataSource(dt);
           objrptDeliveryChallan.Subreports["rptSubPI"].SetDataSource(dt1);

           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan, false);
           objViewer.Show();
       }
       public static void PrintPC(Int32 No)
       {

           rptLC_CI_PC objrptDeliveryChallan = new rptLC_CI_PC();

           string[,] ParamArray1 = new string[,] { { "@InvID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_COM_INV").Tables[0];
           objrptDeliveryChallan.SetDataSource(dt);

           DataTable dt1 = new DataTable();
          // dt1 = GetTheReport(ParamArray1, "rpt_COM_INV_Product").Tables[0];
          // objrptDeliveryChallan.Subreports["rptSubPI"].SetDataSource(dt1);

           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan, false);
           objViewer.Show();
       }
       public static void PrintBC(Int32 No)
       {

           rptLC_CI_BC objrptDeliveryChallan = new rptLC_CI_BC();

           string[,] ParamArray1 = new string[,] { { "@InvID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_COM_INV").Tables[0];
           objrptDeliveryChallan.SetDataSource(dt);

           DataTable dt1 = new DataTable();
           // dt1 = GetTheReport(ParamArray1, "rpt_COM_INV_Product").Tables[0];
           // objrptDeliveryChallan.Subreports["rptSubPI"].SetDataSource(dt1);

           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan, false);
           objViewer.Show();
       }
       public static void PrintPL(Int32 No)
       {

           rptLC_CI_PI objrptDeliveryChallan = new rptLC_CI_PI();

           string[,] ParamArray1 = new string[,] { { "@InvID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_COM_INV").Tables[0];

           DataTable dt1 = new DataTable();
           dt1 = GetTheReport(ParamArray1, "rpt_COM_INV_Product").Tables[0];
           objrptDeliveryChallan.SetDataSource(dt);
           objrptDeliveryChallan.Subreports["rptSubPI"].SetDataSource(dt1);

           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan, false);
           objViewer.Show();
       }
       public static void PrintTR(Int32 No)
       {

           rptLC_CI_TR objrptDeliveryChallan = new rptLC_CI_TR();

           string[,] ParamArray1 = new string[,] { { "@InvID", No.ToString() }
               };
           DataTable dt = new DataTable();
           dt = GetTheReport(ParamArray1, "rpt_COM_INV").Tables[0];

           DataTable dt1 = new DataTable();
           dt1 = GetTheReport(ParamArray1, "rpt_COM_INV_Product").Tables[0];
           objrptDeliveryChallan.SetDataSource(dt);
           objrptDeliveryChallan.Subreports["rptSubPI"].SetDataSource(dt1);

           ReportViewer objViewer = new ReportViewer();

           objViewer.SetReportToViwer(objrptDeliveryChallan, false);
           objViewer.Show();
       }


       #endregion

       public static DataSet GetTheReport(string[,] List, string SpName)
       {
          
           // string sqlCon = "Data Source=192.168.101.17;Initial Catalog=HRMS;User ID=sa;Password=sa1234";// ConfigurationManager.AppSettings["DBConnectionString"].ToString();
           DBConnect objdbc = new DBConnect();
          
           string sqlCon = objdbc.GetConnectionForLINQ();

          
           SqlConnection Con = new SqlConnection(sqlCon);
           SqlCommand cmd = new SqlCommand();
           DataSet ds = null;
           SqlDataAdapter adapter;
           try
           {
               Con.Open();
               cmd.CommandText = SpName;
               cmd.CommandType = CommandType.StoredProcedure;



               for (int i = 0; i < List.GetLength(0); i++)
               {
                   cmd.Parameters.Add(new SqlParameter(List[i, 0], List[i, 1]));

               }


               cmd.Connection = Con;
               ds = new DataSet();
               adapter = new SqlDataAdapter(cmd);
               adapter.Fill(ds, SpName+"_ds");
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
           finally
           {
               cmd.Dispose();
               if (Con.State != ConnectionState.Closed)
                   Con.Close();
           }
           return ds;
       }

       public static bool ExecuteQuery(string sql)
       {
           bool res;

           // string sqlCon = "Data Source=192.168.101.17;Initial Catalog=HRMS;User ID=sa;Password=sa1234";// ConfigurationManager.AppSettings["DBConnectionString"].ToString();
           DBConnect objdbc = new DBConnect();

           string sqlCon = objdbc.GetConnectionForLINQ();


           SqlConnection Con = new SqlConnection(sqlCon);
           SqlCommand cmd = new SqlCommand();
          
           try
           {
               Con.Open();
               cmd.CommandText = sql;
               cmd.CommandType = CommandType.Text;




               cmd.Connection = Con;
               cmd.ExecuteNonQuery();
               res = true;
           }
           catch (Exception ex)
           {
               res = false;
               throw new Exception(ex.Message);
           }
           finally
           {
               cmd.Dispose();
               if (Con.State != ConnectionState.Closed)
                   Con.Close();
           }
           return res;
       }

       public static DataSet GetQuery(string sql)
       {
          

           // string sqlCon = "Data Source=192.168.101.17;Initial Catalog=HRMS;User ID=sa;Password=sa1234";// ConfigurationManager.AppSettings["DBConnectionString"].ToString();
           DBConnect objdbc = new DBConnect();

           string sqlCon = objdbc.GetConnectionForLINQ();

           DataSet ds = new DataSet();
           SqlConnection Con = new SqlConnection(sqlCon);
           SqlCommand cmd = new SqlCommand();

           try
           {
               Con.Open();
               cmd.CommandText = sql;
               cmd.CommandType = CommandType.Text;




               cmd.Connection = Con;
               SqlDataAdapter adapter = new SqlDataAdapter(cmd);
               adapter.Fill(ds, "Users");

               return ds;
              
           }
           catch (Exception ex)
           {
               return null;
           }
          
          
       }
   }

}
