﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OMS
{
    public partial class ReportViewer : Form
    {
        public ReportViewer()
        {
            InitializeComponent();
        }
        internal void SetReportToViwer(CrystalDecisions.CrystalReports.Engine.ReportDocument reportDocument,bool IsShowGroupTree)
        {
            crv.DisplayGroupTree = IsShowGroupTree;
            crv.ReportSource = reportDocument;
        }
    }
}
