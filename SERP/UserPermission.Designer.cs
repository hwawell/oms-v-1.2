﻿namespace OMS
{
    partial class UserPermission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkClause = new System.Windows.Forms.CheckBox();
            this.chkCustomer = new System.Windows.Forms.CheckBox();
            this.chkUserPermission = new System.Windows.Forms.CheckBox();
            this.chkNewUser = new System.Windows.Forms.CheckBox();
            this.chkKnitt = new System.Windows.Forms.CheckBox();
            this.chkDeying = new System.Windows.Forms.CheckBox();
            this.chkPrint = new System.Windows.Forms.CheckBox();
            this.chkEdit = new System.Windows.Forms.CheckBox();
            this.chkOrder = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.chkAccessAll = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkAccessAll);
            this.groupBox1.Controls.Add(this.chkClause);
            this.groupBox1.Controls.Add(this.chkCustomer);
            this.groupBox1.Controls.Add(this.chkUserPermission);
            this.groupBox1.Controls.Add(this.chkNewUser);
            this.groupBox1.Controls.Add(this.chkKnitt);
            this.groupBox1.Controls.Add(this.chkDeying);
            this.groupBox1.Controls.Add(this.chkPrint);
            this.groupBox1.Controls.Add(this.chkEdit);
            this.groupBox1.Controls.Add(this.chkOrder);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(474, 255);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "User Permission Setup";
            // 
            // chkClause
            // 
            this.chkClause.AutoSize = true;
            this.chkClause.Location = new System.Drawing.Point(75, 192);
            this.chkClause.Name = "chkClause";
            this.chkClause.Size = new System.Drawing.Size(85, 17);
            this.chkClause.TabIndex = 13;
            this.chkClause.Text = "Clause Entry";
            this.chkClause.UseVisualStyleBackColor = true;
            // 
            // chkCustomer
            // 
            this.chkCustomer.AutoSize = true;
            this.chkCustomer.Location = new System.Drawing.Point(334, 149);
            this.chkCustomer.Name = "chkCustomer";
            this.chkCustomer.Size = new System.Drawing.Size(97, 17);
            this.chkCustomer.TabIndex = 12;
            this.chkCustomer.Text = "Customer Entry";
            this.chkCustomer.UseVisualStyleBackColor = true;
            // 
            // chkUserPermission
            // 
            this.chkUserPermission.AutoSize = true;
            this.chkUserPermission.Location = new System.Drawing.Point(334, 192);
            this.chkUserPermission.Name = "chkUserPermission";
            this.chkUserPermission.Size = new System.Drawing.Size(101, 17);
            this.chkUserPermission.TabIndex = 11;
            this.chkUserPermission.Text = "User Permission";
            this.chkUserPermission.UseVisualStyleBackColor = true;
            // 
            // chkNewUser
            // 
            this.chkNewUser.AutoSize = true;
            this.chkNewUser.Location = new System.Drawing.Point(213, 192);
            this.chkNewUser.Name = "chkNewUser";
            this.chkNewUser.Size = new System.Drawing.Size(115, 17);
            this.chkNewUser.TabIndex = 10;
            this.chkNewUser.Text = "New User Creation";
            this.chkNewUser.UseVisualStyleBackColor = true;
            // 
            // chkKnitt
            // 
            this.chkKnitt.AutoSize = true;
            this.chkKnitt.Location = new System.Drawing.Point(75, 149);
            this.chkKnitt.Name = "chkKnitt";
            this.chkKnitt.Size = new System.Drawing.Size(106, 17);
            this.chkKnitt.TabIndex = 7;
            this.chkKnitt.Text = "Knitting Approval";
            this.chkKnitt.UseVisualStyleBackColor = true;
            // 
            // chkDeying
            // 
            this.chkDeying.AutoSize = true;
            this.chkDeying.Location = new System.Drawing.Point(213, 149);
            this.chkDeying.Name = "chkDeying";
            this.chkDeying.Size = new System.Drawing.Size(104, 17);
            this.chkDeying.TabIndex = 6;
            this.chkDeying.Text = "Deying Approval";
            this.chkDeying.UseVisualStyleBackColor = true;
            // 
            // chkPrint
            // 
            this.chkPrint.AutoSize = true;
            this.chkPrint.Location = new System.Drawing.Point(334, 109);
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Size = new System.Drawing.Size(76, 17);
            this.chkPrint.TabIndex = 5;
            this.chkPrint.Text = "View & Print";
            this.chkPrint.UseVisualStyleBackColor = true;
            // 
            // chkEdit
            // 
            this.chkEdit.AutoSize = true;
            this.chkEdit.Location = new System.Drawing.Point(213, 109);
            this.chkEdit.Name = "chkEdit";
            this.chkEdit.Size = new System.Drawing.Size(73, 17);
            this.chkEdit.TabIndex = 4;
            this.chkEdit.Text = "Order Edit";
            this.chkEdit.UseVisualStyleBackColor = true;
            // 
            // chkOrder
            // 
            this.chkOrder.AutoSize = true;
            this.chkOrder.Location = new System.Drawing.Point(75, 105);
            this.chkOrder.Name = "chkOrder";
            this.chkOrder.Size = new System.Drawing.Size(90, 17);
            this.chkOrder.TabIndex = 3;
            this.chkOrder.Text = "Order Making";
            this.chkOrder.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "User";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(339, 53);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(120, 53);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(197, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(330, 293);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(411, 293);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Exit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // chkAccessAll
            // 
            this.chkAccessAll.AutoSize = true;
            this.chkAccessAll.Location = new System.Drawing.Point(75, 222);
            this.chkAccessAll.Name = "chkAccessAll";
            this.chkAccessAll.Size = new System.Drawing.Size(104, 17);
            this.chkAccessAll.TabIndex = 14;
            this.chkAccessAll.Text = "Access All Order";
            this.chkAccessAll.UseVisualStyleBackColor = true;
            // 
            // UserPermission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 328);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox1);
            this.Name = "UserPermission";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UserPermission";
            this.Load += new System.EventHandler(this.UserPermission_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox chkUserPermission;
        private System.Windows.Forms.CheckBox chkNewUser;
        private System.Windows.Forms.CheckBox chkKnitt;
        private System.Windows.Forms.CheckBox chkDeying;
        private System.Windows.Forms.CheckBox chkPrint;
        private System.Windows.Forms.CheckBox chkEdit;
        private System.Windows.Forms.CheckBox chkOrder;
        private System.Windows.Forms.CheckBox chkClause;
        private System.Windows.Forms.CheckBox chkCustomer;
        private System.Windows.Forms.CheckBox chkAccessAll;
    }
}