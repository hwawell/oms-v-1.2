﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataLayer;
using Services;
namespace OMS
{
    public partial class UserPermission : Form
    {
        bool IsPostBack;
        public UserPermission()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "--Select User--")
            {
                List<NSEC_PERMISSION> li = new List<NSEC_PERMISSION>();
                string UserID=comboBox1.Text.Trim();
                NSEC_PERMISSION obj = new NSEC_PERMISSION();
                obj.ModuleID = 0;
                if (chkOrder.Checked == true)
                {
                    obj.ModuleID = 1;
                    li.Add(obj);
                }

                obj = new NSEC_PERMISSION();
                
                if (chkEdit.Checked == true)
                {
                    obj.ModuleID = 2;
                    li.Add(obj);
                }

                obj = new NSEC_PERMISSION();
               
                if (chkPrint.Checked == true)
                {
                    obj.ModuleID = 3;
                    li.Add(obj);
                }

                obj = new NSEC_PERMISSION();
                
                if (chkKnitt.Checked == true)
                {
                    obj.ModuleID = 4;
                    li.Add(obj);
                }

                obj = new NSEC_PERMISSION();
               
                if (chkDeying.Checked == true)
                {
                    obj.ModuleID = 5;
                    li.Add(obj);
                }
                obj = new NSEC_PERMISSION();
               
                if (chkCustomer.Checked == true)
                {
                    obj.ModuleID = 6;
                    li.Add(obj);
                }
                obj = new NSEC_PERMISSION();
               
                if (chkClause.Checked == true)
                {
                    obj.ModuleID = 7;
                    li.Add(obj);
                }

                obj = new NSEC_PERMISSION();
              
                if (chkUserPermission.Checked == true)
                {
                    obj.ModuleID = 8;
                    li.Add(obj);
                }
                obj = new NSEC_PERMISSION();

                if (chkAccessAll.Checked == true)
                {
                    obj.ModuleID = 9;
                    li.Add(obj);
                }

                obj = new NSEC_PERMISSION();
               
                if (chkNewUser.Checked == true)
                {
                    obj.ModuleID = 100;
                    li.Add(obj);
                }

                if (li != null && li.Count > 0)
                {
                    Common_DL objDl = new Common_DL();
                    objDl.DeleteUserPermission(UserID);
                    bool res = objDl.SaveUserPermission(li,UserID);
                    if (res == true)
                    {
                        MessageBox.Show("Data Saved Succcessfully", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Data not Saved", "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void UserPermission_Load(object sender, EventArgs e)
        {
            comboBox1.DataSource = new Common_DL().GetAllUser();
            comboBox1.DisplayMember = "UserCode";
            comboBox1.ValueMember = "UserCode";
            comboBox1.Text="--Select User--";
            IsPostBack = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                chkOrder.Checked = false;
                chkEdit.Checked = false;
                chkPrint.Checked = false;
                chkUserPermission.Checked = false;
                chkDeying.Checked = false;
                chkKnitt.Checked = false;
                chkNewUser.Checked = false;
                if (comboBox1.Text != "--Select User--")
                {
                    List<NSEC_PERMISSION> li = new Common_DL().GetAllUserPermission(comboBox1.Text);

                    


                    if (li != null && li.Count > 0)
                    {
                        if (li.Exists(o=> o.ModuleID==1))
                        {
                            chkOrder.Checked = true;
                        }
                        if (li.Exists(o => o.ModuleID == 2))
                        {
                            chkEdit.Checked = true;
                        }
                        if (li.Exists(o => o.ModuleID == 3))
                        {
                            chkPrint.Checked = true;
                        }

                        if (li.Exists(o => o.ModuleID == 4))
                        {
                            chkKnitt.Checked = true;
                        }

                        if (li.Exists(o => o.ModuleID ==5))
                        {
                            chkDeying.Checked = true;
                        }

                        if (li.Exists(o => o.ModuleID == 6))
                        {
                            chkUserPermission.Checked = true;
                        }
                        if (li.Exists(o => o.ModuleID == 100))
                        {
                            chkNewUser.Checked = true;
                        }
                        

                    }
                }
            }
        }
    }
}
