﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
//using BusinessLayer;
namespace OMS
{
   public static class UtilityService
    {
       //public static List<EEmployees> _listOfEmployees;
       //public static List<EBranch> _listOfBranch;
       //public static List<EDesignation> _listOfDesignation;
       //public static List<EAllowanceDeduction> _listOfAD;

       //public static List<EEmployees> ListOfEmployees()
       
       //{

       //    if (_listOfEmployees == null || _listOfEmployees.Count==0)
       //    {
       //        _listOfEmployees = new Employees_BL().GetAllEmpInfo(new EEmployeesParam());

       //        if (_listOfEmployees == null)
       //        {
       //            _listOfEmployees = new List<EEmployees>();
       //        }
       //        else
       //        {
       //            EEmployees obemp = new EEmployees();
       //            obemp.ID = 0;
       //            obemp.EmpName = "--Select an Employee--";
       //            _listOfEmployees.Add(obemp);
       //        }
       //    }

       //    return _listOfEmployees;

       //}
       //public static List<EBranch> ListOfBranch()
       //{

       //    if (_listOfBranch == null || _listOfBranch.Count == 0)
       //    {
       //        _listOfBranch = new Common_BL().GetAllBranch();

       //        if (_listOfBranch == null)
       //        {
       //            _listOfBranch = new List<EBranch>();
       //        }
       //        else
       //        {
                  
       //        }
       //    }

       //    return _listOfBranch;

       //}

       //public static List<EDesignation> ListOfDesignation()
       //{

       //    if (_listOfDesignation == null || _listOfDesignation.Count == 0)
       //    {
       //        _listOfDesignation = new Designation_BL().GetAllDesignation();
       //        if (_listOfDesignation == null)
       //        {
       //            _listOfDesignation = new List<EDesignation>();
       //        }
       //    }

       //    return _listOfDesignation;

       //}

       static public DateTime? GetDate(DateTimePicker dt)
       {

           try
           {
               if (dt.Checked == false)
               {
                   return null;
               }
               else
               {
                   return dt.Value.Date;
               }
           }
           catch
           {
               return null;
           }
       }
       static public DateTime? ConvertFormmatedDate(string date)
       {
           try
           {
                string[] s = date.Split('-');

                if (s.Length == 3)
                {
                    if (GetValidMonth(s[1]) == false)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }

               return Convert.ToDateTime(date);
           }

           catch
           {
               return null;
           }
       }
       static public DateTime? ConvertDate(string date)
       {
           try
           {
               string[] s = date.Split('-');

               if (s.Length == 3)
               {
                   
                   DateTime? dtstr = Convert.ToDateTime(s[0] + "-" + GetMonthName(Convert.ToInt16(s[1])) + "-" + s[2]);

                   return dtstr;
               }
               else
               {
                   return null;
               }
           }
           catch
           {
               return null;
           }
       }
       static private bool GetValidMonth(string Mn)
       {

           if (Mn.ToUpper() == "JAN" || Mn.ToUpper() == "FEB" || Mn.ToUpper() == "MAR"
               || Mn.ToUpper() == "APR" || Mn.ToUpper() == "MAY" || Mn.ToUpper() == "JUN"
               || Mn.ToUpper() == "JUL" || Mn.ToUpper() == "AUG" || Mn.ToUpper() == "SEP"
               || Mn.ToUpper() == "OCT" || Mn.ToUpper() == "NOV" || Mn.ToUpper() == "DEC"
               )
           {
               return true;
           }
           else
           {
               return false;
           }

       }
       static private string GetMonthName(Int16 dt)
       {
           string res = dt.ToString("MMM");

          
           if (dt == 1)
               res = "Jan";
           else if (dt == 2)
               res = "Feb";
           else if (dt == 3)
               res = "Mar";
           else if (dt == 4)
               res = "Apr";
           else if (dt == 5)
               res = "May";
           else if (dt == 6)
               res = "Jun";
           else if (dt == 7)
               res = "Jul";
           else if (dt == 8)
               res = "Aug";
           else if (dt == 9)
               res = "Sep";
           else if (dt == 10)
               res = "Oct";
           else if (dt == 11)
               res = "Nov";
           else if (dt == 12)
               res = "Dec";




           return res;
       }
       public static void CheckNumeric(object sender, KeyPressEventArgs e)
       {
           TextBox txt = (TextBox)sender;
           if (!char.IsControl(e.KeyChar) && !char.IsNumber(e.KeyChar) && e.KeyChar != '.')
           {
               e.Handled = true;
           }
           if (txt.Text.Contains(".") == true && e.KeyChar == '.')
           {
               e.Handled = true;
           }
          
       }
       public static double ConvertToDouble(string Val)
       {
           try
           {
               return Convert.ToDouble(Val);
           }
           catch
           {
               return 0;
           }

       }
       public static decimal ConvertToDecimal(string Val)
       {
           try
           {
               return Convert.ToDecimal(Val);
           }
           catch
           {
               return 0;
           }

       }
       public static int ConvertToNumber(string Val)
       {
           try
           {
               return Convert.ToInt32(Val);
           }
           catch
           {
               return 0;
           }

       }

     



    }
}
