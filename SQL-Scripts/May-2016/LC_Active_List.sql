USE [HWTMSDB]
GO
/****** Object:  StoredProcedure [dbo].[LC_Active_List]    Script Date: 05/24/2016 11:37:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LC_Active_List]
	
AS
BEGIN

 
   
   select LCNo,FileNo,l.NoOfDaysLC,l.ID,LCValueUDS,isnull(TotalCI,0) TotalCI 
   ,CName,ClosingNotes
   ,LCDate
   
   from LC_Information l
   left join dbo.Customer c on c.CustID=l.CustomerID
    Left join  
   
  ( select LC_ID,sum(NetTotal) TotalCI from LC_COM_INV ci inner join dbo.LC_COM_INV_Product_Master m on ci.ID=m.InvoiceInfoID
   inner join dbo.LC_COM_INV_Product p on p.ProductMasterID=m.ID
   group by LC_ID ) tot on l.ID=LC_ID
   
   where isnull(IsFinishProcess,0)=0

END



