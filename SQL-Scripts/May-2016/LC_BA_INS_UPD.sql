USE [HWTMSDB]
GO
/****** Object:  StoredProcedure [dbo].[LC_BA_INS_UPD]    Script Date: 05/24/2016 11:28:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LC_BA_INS_UPD]
		@ID   int,
		@BA_LDBC_NO varchar(20),
		@BA_LDBC_DATE   datetime,
		@BA_Date   datetime=null,
		@BA_DocAcceptAmountUSD    decimal(12,2),
		@BA_ConvertionRateBDT    decimal(6,2),
		@BA_DOC_MaturityDate  datetime=null,
	    @BA_DocPurchasePer    decimal(10,2),
		@BA_PurchaseRate    decimal(10,2),
	
		@EntryID varchar(20),
	
		@Result 		 varchar(500) out
		 
 as 
 BEGIN
 	 BEGIN TRY
 		 BEGIN TRANSACTION
 		   --declare @NoOfDaysLC int
 		   --set @NoOfDaysLC=(select top 1 NoOfDaysLC from LC_INformation l inner join LC_COM_INV
 		   --      inv on l.id=inv.LC_ID where inv.id=@ID)
 		   --set @NoOfDaysLC=@NoOfDaysLC+20
 		   
			     UPDATE LC_COM_INV_Product_Master 
			     set BA_LDBC_NO=@BA_LDBC_NO,BA_LDBC_DATE=@BA_LDBC_DATE,BA_Date=@BA_Date
			     ,BA_DocAcceptAmountUSD=@BA_DocAcceptAmountUSD,BA_ConvertionRateBDT=@BA_ConvertionRateBDT
			     ,BA_DOC_MaturityDate=@BA_DOC_MaturityDate 
			     ,BA_EntryID=@EntryID,BA_EntryDate=GETDATE()
			     ,DocPurchasePercent=@BA_DocPurchasePer
			     ,DocPurchaseRate=@BA_PurchaseRate
				 where ID = @ID 
			
			    set @Result='1-Data Saved Successfully'
		 COMMIT TRANSACTION
	 END TRY
			 BEGIN CATCH
			 ROLLBACK TRANSACTION
			
			 DECLARE @LogInfo as varchar(max)
			 SET @LogInfo = 'Procedure:'+ ERROR_PROCEDURE() + ',ErrorLine:'+ CAST(ERROR_LINE() as varchar(10)) + ',ErrorNo:' +  CAST(ERROR_NUMBER() as varchar(10)) + ',ErrorMessage:' + ERROR_MESSAGE()
			  set @Result='0'+ @LogInfo
			 EXEC WriteLog  @LogInfo
	 END CATCH
 END
