USE [HWTMSDB]
GO
/****** Object:  StoredProcedure [dbo].[LC_CI_Others_Info_GET]    Script Date: 05/24/2016 11:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER  PROCEDURE [dbo].[LC_CI_Others_Info_GET]
 @invoiceID int
AS
BEGIN

     SELECT [ID]
      ,[InvoiceInfoID]
      ,[CI_INV_NO_SUB]
      ,[InvoiceSL]
      ,[INV_Date]
      ,[Notes]
      ,[IsMain]
      ,[IsSubmited]
      ,[BA_LDBC_NO]
      ,[BA_LDBC_DATE]
      ,[BA_Date]
      ,[BA_DocAcceptAmountUSD]
      ,[BA_ConvertionRateBDT]
      ,[BA_DOC_MaturityDate]
      ,[BA_EntryID]
      ,[BA_EntryDate]
      ,[AR_RealizedDate]
      ,[AR_RealizedAmountUSD]
      ,[AR_ConvertionRateBDT]
      ,[AR_EntryID]
      ,[AR_EntryDate]
      ,[EntryID]
      ,[EntryDate]
      ,DocPurchasePercent
      ,DocPurchaseRate
  FROM [LC_COM_INV_Product_Master]
where ID=@invoiceID
  




		
		  
		 








END

