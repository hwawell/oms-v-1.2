USE [HWTMSDB]
GO
/****** Object:  StoredProcedure [dbo].[LC_CI_Sub_INS]    Script Date: 05/24/2016 13:57:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[LC_CI_Sub_INS]
@MainInvoiceID int,
@SubInvoiceID int,

@CI_NO varchar(20),
@CIDate datetime,
@DATA_LIST varchar(max),
@EntryID varchar(20),
@ResID varchar(50) out
as
BEGIN


  if (@SubInvoiceID=0)
  BEGIN
	 declare @InvoiceID int=0
       		 
     set @InvoiceID=isnull((select count(ID) from [LC_COM_INV_Product_Master] 
       		            where [InvoiceInfoID]=@MainInvoiceID ),0)+1
    
     set @ResID =0
     if (@InvoiceID>1)
     BEGIN
              
         set @CI_NO=@CI_NO + '-'+upper(CHAR(95 + @InvoiceID))
     END  
         INSERT INTO [LC_COM_INV_Product_Master]
					   ([InvoiceInfoID]
					   ,[CI_INV_NO_SUB]
					   ,[InvoiceSL]
					   ,[INV_Date]
					  
					  
					 
					  
					   ,[EntryID]
					   ,[EntryDate])
					VALUES
					   (@MainInvoiceID
					   ,@CI_No
					   ,@InvoiceID
					   ,@CIDate
					   
					   
					   ,@EntryID
					   ,getdate())
					   
					   
     
         set @SubInvoiceID =scope_identity()
     	  set @ResID='Sub Invoice : '+@CI_NO + ' Created Successfully'	            
   END    		
   ELSE
   BEGIN
   
	 delete from [LC_COM_INV_Product] where ProductMasterID=@SubInvoiceID
     set @ResID=@CI_NO + ' Updated Successfully'
   END  
      
      
      
     update  [LC_COM_INV_Product_Master] set [INV_Date]=@CIDate where ID=@SubInvoiceID;
     
     declare @objxml xml
	 declare @len bigint
	 set @len = LEN(@DATA_LIST)
	 set @objxml=SUBSTRING(@DATA_LIST,2, @len)
	 declare @XmlHandle int   
	 
       
       
      	     
     INSERT INTO [LC_COM_INV_Product]
			   (ProductMasterID
			   ,[PINO]
			   ,[OPID]
			   ,[Qty]
			   ,[UNIT]
			   ,[UnitPrice]
			   ,Discount
			   ,NetTotal
			   ,[IsNotShow])
	 SELECT	 
		 @SubInvoiceID,
		 T.Item.query('./PINO').value('.', 'varchar(50)') PINO,
		 T.Item.query('./OPID').value('.', 'int') OPID,
		 T.Item.query('./Qty').value('.', 'decimal(10,2)') Qty,
		 T.Item.query('./Unit').value('.', 'varchar(50)') UNIT,
		  T.Item.query('./UnitPrice').value('.', 'decimal(10,4)') UnitPrice,
		 T.Item.query('./Discount').value('.', 'decimal(10,4)') Discount,
		 
		
		  T.Item.query('./NetTotal').value('.', 'decimal(10,3)') Net,
		 T.Item.query('./IsNotShow').value('.', 'bit') IsNotShow
	   	
      FROM @objxml.nodes('/ArrayOfEOrderProductLC/EOrderProductLC') AS T(Item) 

     
END
