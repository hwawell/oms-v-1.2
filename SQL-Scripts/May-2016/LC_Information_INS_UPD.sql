USE [HWTMSDB]
GO
/****** Object:  StoredProcedure [dbo].[LC_Information_INS_UPD]    Script Date: 05/24/2016 11:16:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[LC_Information_INS_UPD]
		@ID   int,
		@LCNo   varchar  (50),
		@LCDate   datetime,
		@LCExpiryDate   datetime,
		@LCTypeID   int,
		@FileNo   varchar  (20),
		@ShipmentDate   datetime,
		@LCValue decimal(16,2),
		@BankBranchID  int   ,
		@IsTIN bit,
		@IsVAT bit,
		@CustomerID   int,
		@MasterLC  varchar(500),
		@LCAdviceNo varchar(100),
	
		@HSCode varchar(100),
		@NoOfDaysLC   int,
		@Notes varchar(500),
		@ShippingFrom varchar(100),
		@ShippingTo varchar(100),
		@ForwardingNo varchar(100),
		@ForwardingDate   datetime,
		@Addl_Info varchar(1000),
		@EntryID   varchar  (50),
		@Result 		 varchar(500) out
		 
 as 
 BEGIN
 	 BEGIN TRY
 		 BEGIN TRANSACTION
 		 
				 IF @ID>0
				 BEGIN
			
			
						Update LC_Information
						 SET [LCDate]=@LCDate,[LCExpiryDate]=@LCExpiryDate,
						 [LCTypeID]=@LCTypeID,[FileNo]=@FileNo,[ShipmentDate]=@ShipmentDate,
						 BankBranchID=@BankBranchID,LCValueUDS=@LCValue,
						 CustomerID=@CustomerID,NoOfDaysLC=@NoOfDaysLC,MasterLC=@MasterLC,
						 ClosingNotes=@notes
						 ,IsBankTINShow=@IsTIN,IsBankVATShow=@IsVAT,
						 HSCode=@HSCode,EntryDate= getdate(),IsDeleted=0,[EntryID]=@EntryID
						 ,ShippingFrom=@ShippingFrom,ShippingTo=@ShippingTo
						 ,ForwardingNO=@ForwardingNO,ForwardingDate=@ForwardingDate
						 ,Addl_Info=@Addl_Info
						 ,LCAdviceNo=@LCAdviceNo
						 where [LCNo]=@LCNo 
					 
						set @Result='1-Data Updated Successfully'

				 
				
				 END
				 ELSE 
				 BEGIN
					
					 INSERT into LC_Information
					([LCNo],[LCDate],[LCExpiryDate],[LCTypeID],[FileNo],[ShipmentDate]
					,[BankBranchID],[CustomerID],[NoOfDaysLC],
					[MasterLC],ClosingNotes,[HSCode],LCValueUDS
					,IsBankTINShow,IsBankVATShow
					,ShippingFrom,ShippingTo
					,LCAdviceNo
					,[EntryDate],[IsDeleted],[EntryID],ForwardingNO,ForwardingDate,Addl_Info)
					 values (@LCNo,@LCDate,@LCExpiryDate,@LCTypeID,@FileNo,@ShipmentDate,
					 @BankBranchID,@CustomerID,@NoOfDaysLC,
					 @MasterLC,@notes,@HSCode,@LCValue,
					 
					 @IsTIN,@IsVAT,
					 @ShippingFrom,@ShippingTo
					 ,@LCAdviceNo
					 ,GETDATE(),0,@EntryID,@ForwardingNO,@ForwardingDate,@Addl_Info)
					
 


				 END
			 
			
					
				set @Result='1'	  
		 COMMIT TRANSACTION
	 END TRY
			 BEGIN CATCH
			 ROLLBACK TRANSACTION
			 set @Result='0'
			 DECLARE @LogInfo as varchar(max)
			 SET @LogInfo = 'Procedure:'+ERROR_PROCEDURE() + ',ErrorLine:'+ CAST(ERROR_LINE() as varchar(10)) + ',ErrorNo:' +  CAST(ERROR_NUMBER() as varchar(10)) + ',ErrorMessage:' + ERROR_MESSAGE()
			 set @Result=@LogInfo
			 
			 EXEC WriteLog  @LogInfo
	 END CATCH
	 return @Result
 END




