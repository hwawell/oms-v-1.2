USE [HWTMSDB]
GO
/****** Object:  StoredProcedure [dbo].[LC_INFO_GET]    Script Date: 05/24/2016 11:16:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[LC_INFO_GET] 
	
	@LCNo varchar(50)
AS
BEGIN
	
	SELECT l.[ID]
      ,l.[CustomerID]
      ,[LCNo]
      ,[LCDate]
      ,[LCExpiryDate]
      ,[LCTypeID]
      ,[FileNo]
      ,[NoOfDaysLC]
      ,[ShipmentDate]
      ,cus.CName
      ,[MasterLC]
      ,LCValueUDS
      ,l.[TINNo]
      ,[HSCode]
      ,LCAdviceNo
   ,IsBankTINShow
   ,IsBankVATShow
      ,BankBranchID
      ,BankName + ' '+BranchName  BankBranch
      ,BranchAddress
      ,ClosingNotes
      ,ShippingFrom
      ,ShippingTo
      ,ForwardingNO
      ,ForwardingDate
      ,Addl_info
  FROM [LC_Information] l
  left join dbo.CustomerBank c on c.CustomerID=l.[CustomerID]
  left join dbo.Customer cus on cus.CustID=c.CustomerID

  where IsDeleted=0 and 
  
 ( LCNo=@LCNo)
  


END



