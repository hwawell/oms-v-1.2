IF NOT EXISTS ( SELECT  *
            FROM    HWTMSDB.INFORMATION_SCHEMA.COLUMNS
               WHERE TABLE_NAME = N'customer' AND COLUMN_NAME=N'BIN'
 )
    BEGIN
        ALTER table customer add BIN varchar(30)
    END;
------------------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'rpt_LC_CI_Forwarding' )
    BEGIN
        DROP PROCEDURE rpt_LC_CI_Forwarding;
    END;
GO
CREATE PROCEDURE [dbo].[rpt_LC_CI_Forwarding] @InvID INT
AS
    BEGIN
    
        SELECT  [LCNo] ,
                [LCDate] ,
                [LCExpiryDate] ,
                [LCTypeID] ,
                [LCValueUDS] ,
                [FileNo] ,
                [NoOfDaysLC] ,
                [ShipmentDate] ,
                [BankBranchID] ,
                [IsBankTINShow] ,
                [IsBankVATShow] ,
                [MasterLC] ,
                [HSCode] ,
                [LCOrderEntryID] ,
                [LCOrderEntryDate] ,
                [IsFinishProcess] ,
                [ClosingNotes] ,
                [FinalizedBy] ,
                [ShippingFrom] ,
                [ShippingTo] ,
                'SEBL/ESK/LDBC/' + [BA_LDBC_NO] [ForwardingNO] ,
                BA_LDBC_DATE ,
                [ForwardingDate] ,
                Tot.Amount InvoiceValue ,
                cb.[BankName] cus_Bank ,
                cb.[BranchName] cus_branch ,
                cb.[BranchAddress] cus_branch_add ,
                cb.[TINNO] cus_BANK_TIN ,
                cb.[RegNo] cus_BANK_VATREG ,
                [ComName] st_comName ,
                [ComAddress] st_comAddress ,
                [FacAddress] st_facAddress ,
                notes.[BankName] st_comBank ,
                notes.[BankAcNAme] st_comBankAc ,
                notes.[BankAddress] st_comBankAdd ,
                [Inspection] st_inspection ,
                [Origin] st_origin ,
                [Allowance] st_allowance ,
                [Packing] st_packing ,
                [Shipment] st_shipment ,
                [Payment] st_payment ,
                cc.ComLOGO st_comLogo ,
                [CName] cus_Name ,
                [Address] cus_address
        FROM    [LC_COM_INV] c
                INNER JOIN LC_COM_INV_Product_Master i ON c.ID = i.InvoiceInfoID
                INNER JOIN LC_Information l ON l.ID = c.LC_ID
                INNER JOIN Customer cus ON cus.CustID = l.CustomerID
                INNER JOIN dbo.CustomerBank cb ON cb.ID = l.BankBranchID
                CROSS APPLY ( SELECT    SUM(NetTotal) Amount
                              FROM      dbo.LC_COM_INV_Product
                              WHERE     ProductMasterID = @InvID
                            ) AS Tot ,
                dbo.OrderNotes notes ,
                CompanyInfo cc
        WHERE   i.ID = @InvID; 





	
    END;



GO

------------------------------------------------------------------------------------


IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'LC_DELETE' )
    BEGIN
        DROP PROCEDURE LC_DELETE;
    END;
GO

CREATE PROCEDURE [dbo].[LC_DELETE]
    @ID INT ,
    @Operation VARCHAR(20)
AS
    BEGIN
        BEGIN TRY
	
            BEGIN TRANSACTION;
            IF ( @Operation = 'COM_INV' )
                BEGIN
                    DECLARE @INVID INT;
                    SET @INVID = ( SELECT   InvoiceInfoID
                                   FROM     LC_COM_INV_Product_Master
                                   WHERE    ID = @ID
                                 );
                    DELETE  FROM dbo.LC_COM_INV_Product_Master
                    WHERE   ID = @ID;
                    DELETE  FROM dbo.LC_COM_INV_Product
                    WHERE   ProductMasterID = @ID;
	   
                    IF NOT EXISTS ( SELECT  *
                                    FROM    LC_COM_INV_Product_Master
                                    WHERE   InvoiceInfoID = @INVID )
                        DELETE  FROM dbo.LC_COM_INV
                        WHERE   ID = @INVID;
	      --update LC_COM_INV_Product_Master set IsSubmited=1 where InvoiceInfoID=@INVID
                END;
            ELSE
                IF ( @Operation = 'LC_INFO' )
                    BEGIN
	
                        UPDATE  dbo.LC_Information
                        SET     IsDeleted = 1 ,
                                EntryDate = GETDATE()
                        WHERE   ID = @ID;
                    END;
            COMMIT TRANSACTION;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION;
        END CATCH;
    END;

GO

------------------------------------------------------------------------------------

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'LC_Invoice_Summary_GET' )
    BEGIN
        DROP PROCEDURE LC_Invoice_Summary_GET;
    END;
GO

CREATE PROCEDURE LC_Invoice_Summary_GET @LC_ID INT
AS
    SELECT  M.CI_INV_NO_SUB [Invoice No] ,
            TotInvoiceValue [Invoice Value] ,
            Inv.CI_Mushok_No [Mushok No] ,
            CONVERT(VARCHAR(12), Inv.CI_BTMA_ISS_Date, 105) [BTMA Issue Date] ,
            M.BA_LDBC_NO [LDBC NO] ,
            CONVERT(VARCHAR(12), M.BA_LDBC_DATE, 105) [LDBC Date] ,
            CONVERT(VARCHAR(12), DATEADD(DAY, l.NoOfDaysLC, M.INV_Date), 105) [Maturity Date] ,
            CONVERT(VARCHAR(12), M.AR_RealizedDate, 105) [Realization Date]
    FROM    dbo.LC_COM_INV Inv
            INNER JOIN dbo.LC_COM_INV_Product_Master M ON Inv.ID = M.InvoiceInfoID
                                                          AND M.InvoiceSL = 1
            INNER JOIN dbo.LC_Information l ON l.ID = Inv.LC_ID
            INNER JOIN ( SELECT SUM(p.NetTotal) TotInvoiceValue ,
                                p.ProductMasterID
                         FROM   dbo.LC_COM_INV_Product p
                         GROUP BY p.ProductMasterID
                       ) AS k ON k.ProductMasterID = M.ID
    WHERE   Inv.LC_ID = @LC_ID;


GO

------------------------------------------------------------------------------------