﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;


namespace Services
{
    public class DataSerialization
    {
        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>Returns character string.</returns>
        private static String UTF8ByteArrayToString(Byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        /// <summary>
        /// File to Byte[] Converter
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static byte[] GetMediaFile(string p)
        {
            var fs = new FileStream(p, FileMode.Open, FileAccess.Read);
            var br = new BinaryReader(fs);

            byte[] mediaFile = br.ReadBytes((int)fs.Length);

            br.Close();
            fs.Close();

            return mediaFile;
        }


        /// <summary>
        /// Converts the String to UTF8 Byte array and is used in De serialization.
        /// </summary>
        /// <param name="pXmlString"></param>
        /// <returns></returns>
        private static Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        /// <summary>
        /// Method to convert a custom Object to XML string.
        /// </summary>
        /// <param name="pObject">Object that is to be serialized to XML</param>
        /// <returns>XML string</returns>
        public static String SerializeObject<T>(Object pObject)
        {
            try
            {
                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer objSerializer = GetSerializer<T>();
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                objSerializer.Serialize(xmlTextWriter, pObject);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                return XmlizedString;
            }

            catch 
            {
               // LIASLOG.Debug("DataSerialization : " + "SerializeObject() :: " + ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Method to reconstruct an Object from XML string.
        /// </summary>
        /// <param name="pXmlizedString"></param>
        /// <returns></returns>
        public static Object DeserializeObject<T>(String pXmlizedString)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(pXmlizedString));
                XmlSerializer objSerializer = GetSerializer<T>();
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                return objSerializer.Deserialize(memoryStream);
            }
            catch 
            {
                //LIASLOG.Debug("DataSerialization : " + "DeserializeObject() :: " + ex.Message);
                return null;
            }
        }


        private static XmlSerializer GetSerializer<T>()
        {
            XmlSerializer xs;
            xs = new XmlSerializer(typeof(T));
            return xs;
        }

    }

}
